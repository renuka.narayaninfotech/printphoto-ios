//
//  AMConstant.swift
//  Amzan
//
//  Created by AlphaVed Mac on 12/09/18.
//  Copyright © 2018 AlphaVed. All rights reserved.
//

import Foundation
import SystemConfiguration
import UIKit
import AVFoundation
import CoreTelephony

// Check Sim Card Network country code
let networkInfo = CTTelephonyNetworkInfo()
let carrier = networkInfo.subscriberCellularProvider
let simNetworkCode = (carrier!.isoCountryCode)!.uppercased()

//MARK:-********************** LIVE ID **********************
//App Id => ca-app-pub-7238432815417079~1566182906
//Banner => ca-app-pub-7238432815417079/2687692888
//Interstitial => ca-app-pub-7238432815417079/7748447874
//Video => ca-app-pub-7238432815417079/5946257529
//Native Advance => ca-app-pub-7238432815417079/5632958372

//let banner_ID = "ca-app-pub-3940256099942544/2934735716" // Test ID
//let banner_ID = "ca-app-pub-7238432815417079/2687692888" // Live ID

//let Native_Ad_ID = "ca-app-pub-7238432815417079/5632958372"
#if DEBUG
 // This code will be run while installing from Xcode
let AppID = "ca-app-pub-3940256099942544~1458002511"
let banner_ID = "ca-app-pub-3940256099942544/2934735716"//"/6499/example/banner"
let interstitial_ID = "ca-app-pub-3940256099942544/4411468910"
let rewarded_ID = "ca-app-pub-3940256099942544/6978759866"
let open_ID = "ca-app-pub-3940256099942544/5662855259"
let Native_Ad_ID = "ca-app-pub-3940256099942544/2521693316"//"ca-app-pub-3940256099942544/3986624511"

#else
let AppID = "ca-app-pub-3940256099942544~1458002511"
let banner_ID = "/21849154601,22939115363/Ad.Plus-APP-Banner"
let Native_Ad_ID = "/21849154601,22939115363/Ad.Plus-APP-Native"
let interstitial_ID = "/21849154601,22939115363/Ad.Plus-APP-Interstitial"
let open_ID =  "/21849154601,22939115363/Ad.Plus-APP-APPOpen"
let rewarded_ID = "/21849154601,22939115363/Ad.Plus-APP-Rewarded"
#endif

let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String

let appName = "Print Photo"

// Help Video URL
var videoURL : URL?
var indiaVideoURl : URL?
var otherCountryVideoURl : URL?

//********************** USER DEFAULT CONSTANT **********************

let userDefault                        = UserDefaults.standard

//********************** APP CONSTANT STRING **********************
let appDelegate                        = UIApplication.shared.delegate as! AppDelegate

//MARK:-********************** UI CONSTANT **********************

let mainStoryBoard                     = UIStoryboard(name: "Main", bundle: nil)

//MARK:- ********************** DEVICE INFORMATION **********************
struct ScreenSize {
    
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    
    static let IS_IPHONE            = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_7          = IS_IPHONE_6
    static let IS_IPHONE_7P         = IS_IPHONE_6P
    static let IS_IPHONE_8          = IS_IPHONE_7
    static let IS_IPHONE_8P         = IS_IPHONE_7P
    static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPAD_PRO_10_5     = IS_IPAD && ScreenSize.SCREEN_MAX_LENGTH == 1112.0
    static let IS_IPAD_PRO_11       = IS_IPAD && ScreenSize.SCREEN_MAX_LENGTH == 1194.0
    static let IS_IPAD_PRO_12_9     = IS_IPAD && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    static let IS_TV                = UIDevice.current.userInterfaceIdiom == .tv
    static let IS_CAR_PLAY          = UIDevice.current.userInterfaceIdiom == .carPlay
}

struct Version {
    
    static let SYS_VERSION_FLOAT    = (UIDevice.current.systemVersion as NSString).floatValue
    static let iOS7                 = (Version.SYS_VERSION_FLOAT < 8.0 && Version.SYS_VERSION_FLOAT >= 7.0)
    static let iOS8                 = (Version.SYS_VERSION_FLOAT >= 8.0 && Version.SYS_VERSION_FLOAT < 9.0)
    static let iOS9                 = (Version.SYS_VERSION_FLOAT >= 9.0 && Version.SYS_VERSION_FLOAT < 10.0)
    static let iOS10                = (Version.SYS_VERSION_FLOAT >= 10.0 && Version.SYS_VERSION_FLOAT < 11.0)
    static let iOS11                = (Version.SYS_VERSION_FLOAT >= 11.0 && Version.SYS_VERSION_FLOAT < 12.0)
}


public struct CustomFontWeight {
    
    static let reguler    = "HelveticaNeue"
    static let medium     = "HelveticaNeue-Medium"
    static let semibold   = "HelveticaNeue-SemiBold"
    static let bold       = "HelveticaNeue-Bold"
    static let light      = "HelveticaNeue-Light"
}

open class Reachability {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
}

//func hudProggess(_ view:UIView,Show:Bool){
//
//    DispatchQueue.main.async {
//        if Show
//        {
//            MBProgressHUD.showAdded(to: view, animated: true)
//        }
//        else{
//            MBProgressHUD.hide(for: view, animated: false)
//        }
//    }
//}

func ConfigureNodataLbl(viewController : UIViewController) -> UILabel {
    
    let nodataLbl = UILabel(frame: CGRect(x: 0, y: 100, width: viewController.view.Getwidth, height: 50))
    nodataLbl.text = "Item not available"
    nodataLbl.font = UIFont.systemFont(ofSize: 20.0)
    nodataLbl.textColor = UIColor.black
    nodataLbl.textAlignment = .center
    nodataLbl.center = viewController.view.center
    nodataLbl.isHidden = true
    
    return nodataLbl
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

let maskImgpath = "https://printphoto.in/Photo_case/public/mask_image/"
let modelImgpath = "https://printphoto.in/Photo_case/public/modal_image/"
let SpecialCaseImgPath = "https://printphoto.in/Photo_case/public/special_case/"
let BackGrounImgPath = "https://printphoto.in/Photo_case/public/background_images/"
let mugImagePath = "https://printphoto.in/Photo_case/public/mug_image/"

let offersImagePath = "https://printphoto.in/Photo_case/public/offer_image/"

//MARK:-********************** ENTER MOBILE NUMBER  AND EMAIL WHEN SEND OTP **********************

var mobileNumber = String()
var emailId : String?

//********************** BULK SMS **********************

var isBulk_SMS = String()
var mall_enable_status = Int()

//MARK:-********************** EDITING VIEW CONTROLLER **********************

var StickerImage : UIImage!
var StickerTextString : String!
var StickerTextColor : UIColor!
var stickerFontString : String!
var StickerBackgroung : UIImage!
var instagramImage : UIImage!

var deviceToken = userDefault.value(forKey: "FIREBASE_TOKEN")

//MARK:-********************** VERIFY OTP FLAG CHANGE PASSWORD OR REGISTRATION **********************
var isFromRegistration = true

var isFromLocalKilled: Bool = false
var isFromPushKilled: Bool  = false

//MARK:- SET FLAG USER LOGIN OR NOT IN ORDER SCREEN
var isLoginOrNot : String?

////MARK:- SET FLAG USER LOGIN OR NOT IN CART SCREEN
//var isLoginFromCart = true

//MARK:- GET CART ARRAY
var cartInfoMainArray = [[String:Any]]()

//MARK:- SET FLAG USER LOGIN OR NOT IN ORDER SCREEN
var categoryArray = NSMutableArray()
var categoryIdArray = NSMutableArray()
var cancel_reson_Array = NSMutableArray()
//MARK:- Order array used in order screen and order details screen

//MARK:- Get Order Id from order screen
var orderIdFromOrder : String!
var trackingURL : String?

//MARK:- Get tickets URL from HomeVC
var ticketsURL : String?


//MARK:- Get Country, State, City Array

var locationArray = NSMutableArray()
var phoneCodeArray = NSMutableArray()
var countryArray = NSMutableArray()
var stateArray = NSMutableArray()
var cityArray = NSMutableArray()

//MARK:- Add address and Edit address Flag
var isEditAddress = true

//MARK:- Set Flag for Have a promocode
var isPromocode = false

//MARK:- Save Value and used in otherscreen
var EditDict = [String:Any]()

//MARK:- Create Item Array and Item Quantity when used in Place Order Screen
var item_ID_Quantity_Array = NSMutableArray()

//MARK:- Create Item Array and Item Quantity when used in Place Order Screen
var cart_id_array = NSMutableArray()

////MARK:- Default Payment RS. 1
//var rs = 1

//MARK:- Jump to Delivery address from MyAccount screen
var isDeliveryAddress = true
var isReloadTable = false
var addressID = 0
var AddressFilteredArr = [[String:Any]]()

//MARK:- Delivery Address Array
var deliveryAddressArray = [[String:Any]]()

// Selected Delivery Address index
//var addID : Int?
//var selectIndex = 0


//MARK:- Save Dictionary in USer default
var mainDict = [String:Any]()

//MARK:- Enable and disable back button from Place order screen
var isOfferBtnBack = false

//MARK:- Enable and disable back button from My Order Screen
var isOrderBtnBack = false

//MARK:- Enable and disable back button from Offer screen
var isCartBtnBack = false

//MARK:- Pop-up to Cart Screen and and Save Address Screen
var isFromSaveAddress = false

//MARK:- Pop-up to Cart Screen and and Save Address Screen
var isIconCart = true

//MARK:- Managed Selected Tab Index From Offer and Cart
//var isFromOffer = false

//MARK:- Managed Selected Tab Index From My Account and Order
var isFromMyAccount = false

//MARK:- Main Product ID From Home Screen for selected product
var mainProductID : Int?
var mainProductName : String?
var selectedMainProductId : Int?

//MARK:- Main Product ID From Home Screen for selected product
var phoneCode : String?

//MARK:- Main Product ID From Home Screen for selected product
var strOTPForEmailVerification : String?

//MARK:- Managed mobile and Email Verification
var isVerificationUsingMobile = true


//MARK:- Get from Editing View and Mug Editing View
var isSelectSticker : String?


//MARK:- Seller registration is Approve
var sellerID : String?
var strSeller : String?

//MARK:- Managed screen orientation Mug - Login
var isFromMugModule = false

//MARK:- Lock screen Orientation
struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }
    
}

//MARK:- Offer Array
var OffersArray = NSMutableArray()
var imgTemp: UIImage?

//MARK:- Select Background Array
var BackgroundImgArr = [[String:Any]]()

//MARK:- Select Stickers Array
var mainstickerArr = [[String:Any]]()

//MARK:- Select default Images Category Array
var CategoryMainArr = [[String:Any]]()

//MARK:- Request Model/Brand Array
var RequestBrandMainArr = [[String:Any]]()


//MARK:- Tableview scroll to top in home view controller get from other tab and sub category
var isScroll = true


//MARK:- Accept Character in Edit Account Screen Textfield
let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"

let ACCEPTABLE_CHARACTERS_OnlyAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

//MARK:- In Seller Wallete Screen
let privacyPolicyURL = "https://printphoto.in/Photo_case/public/Privacy_Policy.html"
let termsConditionURL = "https://printphoto.in/Photo_case/public/Terms_and_Condition.html"
let refundURL = "https://printphoto.in/Photo_case/public/refund.html"

let sellerPrivacyPilicyURL = "https://printphoto.in/Photo_case/public/Seller-Privacy-Poilcy.html"

var productIdDict = [String:Any]()


// Get instagram images account ID

let clientID = "458ee6ebe1bf4e409143d00a519f6be0"//"3c27a66e603d43608ea92f8e47e355bd"
let REDIRECTURI = "https://www.instagram.com"
let clientSecret = "08e86de19e8545b689e5b9007f652ee4" //"f5d73528bab94b49a4b8ef5c680cfad9"


//MARK:- MALL SEARCH CATEGORY IN MALL SEARCH CATEGORY SCREEN
var mallSearchCategoryArray = [[String:Any]]()
var mallSearchCategoryFilteredArr = [[String:Any]]()

//MARK:- Filter Array in Filter screen ang pass to category view controller
var filterArray = [[String:Any]]()

//MARK:- Is from SearchProductViewController and CategoryListViewController
var isFromSelect  = ""

//MARK:- Filter URl And Sort in Mall Category Filter Screen
var filterURL: String = ""
var sortURL: String = ""

// Disable Copy-Paste Feture on TextField
class TextFeild: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // write code here what ever you want to change property for textfeild.
    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return CGRect.zero
    }
    
    func selectionRects(for range: UITextRange) -> [Any] {
        return []
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.copy(_:)) || action == #selector(UIResponderStandardEditActions.selectAll(_:)) || action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
            }
        // Default
        return super.canPerformAction(action, withSender: sender)
    }
}


// Add Image button show or not when come from FancyframeVC (Index 0 and other) in MugEditingViewController

var isSelectZeroIndex = false


// Currencies code Array used in Edit Account screen from Get_configuration API
var currenciesArray = NSMutableArray()
//var arrCurrencyCode = NSMutableArray()

// First time blanck mask in Multiple photo frame
var isBlankMask = false

// First time set default region in login screen
//var isSetIndex = false

// Save string when user is select region from dropdown in login screen
//var strSelectedRegion : String?


// Gift is true or false
var isGift = false

// When User got to login screen and Change region
var isLogin = false

// Open Offer screen from Placeorder screen and click on back button
var isBackFromOffer = false

// Store Big GIF image
var dictGif = [String: Any]()

// Set Bool When User is Change Region from Dropdown
var isFromMallCategory = false
