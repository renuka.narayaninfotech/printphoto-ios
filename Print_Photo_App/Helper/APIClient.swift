//
//  AMAPIClient.swift
//  
//  Created by AlphaVed Mac on 12/09/18.
//  Copyright © 2018 AlphaVed. All rights reserved.
//

import UIKit
import AFNetworking

//let BaseURL = "https://printphoto.in/Photo_case_release/api/" //"https://printphoto.in/Photo_case/api/" //"https://admin-live.printphoto.in/api/" //"https://admin.printphoto.in/api/" //"https://printphoto.in/Photo_case/api/" // Live URL

let BaseURL = "https://printphoto.in/Photo_case/api/" // Live
//let BaseURL = "https://admin-release.printphoto.in/api/" //"https://printphoto.in/Photo_case_release/api/" // Testing URL

//class APIClient: NSObject { //Using URLSession
//
//    class var sharedInstance : APIClient {
//
//        struct Static {
//
//            static var instance : APIClient = APIClient()
//        }
//        return Static.instance
//    }
//
//    //MARK:- Get API Call
//    
//    func getAPICall(urlString: String!, completionHandler:@escaping (Any?, URLResponse?, Error?)->()) ->() {
//
//        print("Calling API: \(urlString ?? "NOURL")")
//        
//        
//        var request = URLRequest(url: URL(string: urlString)!)
//        request.httpMethod = "GET"
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        
//        let urlSession = URLSession.shared
//        
//        let dataTask = urlSession.dataTask(with: request) { (data,response,error) in
//            
//            if let httpResponse = response as? HTTPURLResponse {
//                print(httpResponse.statusCode)
//            }
//            
//            if error != nil {
//                
//                print(error!.localizedDescription)
//            }
//            else{
//
//                do {
//                                        print(String(data: data!, encoding: String.Encoding.utf8)!)
//                    let resultJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
//                    completionHandler(resultJson, response, nil)
//                    
//                }
//                catch {
//
//                    print("Error -> \(error)")
//                    completionHandler(nil, response, error)
//                }
//            }
//
//        }
//        dataTask.resume()
//    }
//
//    
//    func getAPICall(urlString: String!, parameters: Any!, completionHandler:@escaping (Any?, URLResponse?, Error?)->()) ->() {
//
//        print("Calling API: \(urlString ?? "NOURL")")
//        print("Params : \(parameters)")
//
//        var request = URLRequest(url: URL(string: urlString)!)
//        
//        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//        request.setValue("application/json", forHTTPHeaderField: "Accept")
//        
//        request.httpMethod = "GET"
//        
//        request.httpBody = (parameters as! String).data(using: .utf8)
//        
//        let urlSession = URLSession.shared
//        let dataTask = urlSession.dataTask(with: request) { (data,response,error) in
//
//            if let httpResponse = response as? HTTPURLResponse {
//                print(httpResponse.statusCode)
//            }
//
//            if error != nil {
//
//                print(error!.localizedDescription)
//            }
//            else{
//
//                do {
//                    print(String(data: data!, encoding: String.Encoding.utf8)!)
//                    let resultJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
//                    completionHandler(resultJson, response, nil)
//
//                }
//                catch {
//
//                    print("Error -> \(error)")
//                    completionHandler(nil, response, error)
//                }
//            }
//
//        }
//        dataTask.resume()
//    }
//
//
//    //MARK:- POST API Call
//    
//    func postAPICall(urlString: String!, parameters: Any!, completionHandler:@escaping (Any?, URLResponse?, Error?)->()) ->() {
//
//        print("Calling API: \(urlString ?? "NOURL")")
//        print("Params : \(parameters)")
//        
//        var request = URLRequest(url: URL(string: urlString)!)
//        
//        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//        request.setValue("application/json", forHTTPHeaderField: "Accept")
//        
//        request.httpMethod = "POST"
//
//        request.httpBody = (parameters as! String).data(using: .utf8)
//        
//        let urlSession = URLSession.shared
//        let dataTask = urlSession.dataTask(with: request) { (data,response,error) in
//            
//            if error != nil {
//                
//                completionHandler(nil, response, error)
//            }
//            else{
//                do {
//                    
//                    print(String(data: data!, encoding: String.Encoding.utf8)!)
//                    let resultJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
//                    completionHandler(resultJson, response, nil)
//                    
//                }
//                catch {
//                    completionHandler(nil, response, error)
//                }
//            }
//            
//        }
//
//        dataTask.resume()
//    }
//
//}

class AFNetworkingAPIClient: NSObject,NSURLConnectionDataDelegate { //Using AFNetworking
    
    let manager : AFHTTPSessionManager = AFHTTPSessionManager()
    
    //MARK:- Create Instance of Class
    class var sharedInstance : AFNetworkingAPIClient {
        
        struct Static {
            
            static let instance : AFNetworkingAPIClient = AFNetworkingAPIClient()
        }
        return Static.instance
    }
    
    //MARK:- GET API Call
    func getAPICall(url: String, parameters: NSDictionary!, completionHandler:@escaping (URLSessionDataTask, NSDictionary?, Error?)->()) ->() {
        
        print("Calling API: \(url)")
        
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: JSONSerialization.ReadingOptions.allowFragments) as AFJSONResponseSerializer
        manager.responseSerializer.acceptableContentTypes = NSSet(objects:"application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav") as? Set<String>
        
        manager.requestSerializer = AFJSONRequestSerializer() as AFJSONRequestSerializer
        manager.requestSerializer.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        manager.get(url as String, parameters: parameters, headers: nil, progress: nil, success: { (operation, responseObject) in
            
            //            print("Response: \(responseObject!)")
            completionHandler(operation, responseObject! as? NSDictionary , Error?.self as? Error)
            
        }) {(operation, error) in
            
            print("Error: " + error.localizedDescription)
            completionHandler(operation!, nil ,error)
        }
    }
    
    //MARK:- POST API Call
    
    //POST API
    func postAPICall(url: String, parameters: NSDictionary!, completionHandler:@escaping (URLSessionDataTask, Any?, Error?)->()) ->() {
        
        print("Calling API: \(url)")
        
        manager.securityPolicy.allowInvalidCertificates = true
        manager.securityPolicy.validatesDomainName = false
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: JSONSerialization.ReadingOptions.allowFragments) as AFJSONResponseSerializer
        manager.responseSerializer.acceptableContentTypes = NSSet(objects:"application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav") as? Set<String>
        
        manager.post(url as String, parameters: parameters, headers: nil, progress: nil, success: { (operation, responseObject) in
            
            //            print("Response: \(responseObject!)")
            
            completionHandler(operation,responseObject, Error?.self as? Error)
            
        }) { (operation, error) in
            
            print("Error: " + error.localizedDescription)
            completionHandler(operation!, nil ,error)
        }
    }
    
    //POST API With Single Image
    func postAPICallWithImage(url: String, parameters: NSDictionary!, image : UIImage!, imgKey: String,mimeType:String, completionHandler:@escaping (URLSessionDataTask, NSDictionary?, Error?)->()) ->() {
        
        print("Calling API: \(url)")
        
        manager.post(url as String, parameters: parameters, headers: nil, constructingBodyWith: { (formData: AFMultipartFormData!) in
            
            /*      FOR IMAGE UPLOAD
             if data?.allValues.count != 0 && data != nil
             {
             
             let fileUrl = NSURL(fileURLWithPath: (data?.valueForKey("filePath"))! as! String)
             try! formData.appendPartWithFileURL(fileUrl, name: (data?.valueForKey("key"))! as! String)
             }
             */
            var str = ProcessInfo.processInfo.globallyUniqueString
            str = str.appending(".jpg")
            formData.appendPart(withFileData: image.jpegData(compressionQuality: 1.0)!, name: imgKey, fileName: str, mimeType: "image/jpeg")
            
        }, progress: nil, success: { (operation, responseObject) in
            
            //            print("Response: \(responseObject!)")
            completionHandler(operation,responseObject as? NSDictionary, Error?.self as? Error)
            
        }) { (operation, error) in
            
            print("Error: " + error.localizedDescription)
            completionHandler(operation!, nil ,error)
        }
    }
    
    //...END..//
    
    //POST API With Multiple Image
    func postAPICallWithMultipleImages(url: String, parameters: NSDictionary!, imageArray : [Any], imgKey: [Any], completionHandler:@escaping (URLSessionDataTask, NSDictionary?, Error?)->()) ->() {
        
        print("Calling API: \(url)")
        
        manager.post(url as String, parameters: parameters, headers: nil, constructingBodyWith: { (formData: AFMultipartFormData!) in
            
            for (index, image) in imageArray.enumerated() {
                
                var imageData = Data()
                
                let imgType = (mainProductID == ID.WallWoodenCanvasFrame) ? "image/jpeg" : "image/png"
                var imgExtension : String?
                
                if imgType == "image/jpeg"
                {
                    imageData = (image as! UIImage).jpegData(compressionQuality: 0.6) ?? Data()
                    imgExtension = ".jpg"
                }else
                {
                    imageData = (image as! UIImage).pngData() ?? Data()
                    imgExtension = ".png"
                }
                
                var str = ProcessInfo.processInfo.globallyUniqueString
                str = str.appending("\(index)\(imgExtension ?? "")")
                
                formData.appendPart(withFileData: imageData , name: imgKey[index] as! String, fileName: "\(str)", mimeType: imgType)
                
                //                 formData.appendPart(withFileData: image.jpegData(compressionQuality: 1.0)!, name: imgKey, fileName: str, mimeType: "image/jpeg")
            }
            
        }, progress: nil, success: { (operation, responseObject) in
            
            //            print("Response: \(responseObject!)")
            completionHandler(operation,responseObject as? NSDictionary, Error?.self as? Error)
            
        }) { (operation, error) in
            
            print("Error: " + error.localizedDescription)
            completionHandler(operation!, nil ,error)
        }
    }
    
    //POST API With Image and PDF file
    func postAPICallImageWithPDf(url: String, parameters: NSDictionary!, imageArray : [Any], imgKey: [Any], completionHandler:@escaping (URLSessionDataTask, NSDictionary?, Error?)->()) ->() {
        
        print("Calling API: \(url)")
        
        manager.post(url as String, parameters: parameters, headers: nil, constructingBodyWith: { (formData: AFMultipartFormData!) in
            
            for (index, image) in imageArray.enumerated() {
                
                var imageData = Data()
                let imageStr = "\(image)"
                let componentsArr = imageStr.components(separatedBy: ".")
                
                if let fileName = componentsArr.last
                {
                    print(fileName)
                    
                    if fileName == "pdf"
                    {
                        let pdfType = "application/pdf"
                        let pdfData = NSData.init(contentsOfFile: (image as! URL).absoluteString)
                        var pdfExtension : String?
                        pdfExtension = ".pdf"
                        
                        var strPdf = ProcessInfo.processInfo.globallyUniqueString
                        strPdf = strPdf.appending("\(index)\(pdfExtension ?? "")")
                        formData.appendPart(withFileData: pdfData! as Data, name: imgKey[index] as! String, fileName: "\(strPdf)", mimeType: pdfType)
                        
                    }
                    else
                    {
                        let imgType = "image/png"
                        imageData = (image as! UIImage).pngData() ?? Data()
                        var imgExtension : String?
                        imgExtension = ".png"
                        
                        var strImage = ProcessInfo.processInfo.globallyUniqueString
                        strImage = strImage.appending("\(index)\(imgExtension ?? "")")
                        
                        formData.appendPart(withFileData: imageData , name: imgKey[index] as! String, fileName: "\(strImage)", mimeType: imgType)
                        
                    }
                }
                
            }
            
        }, progress: nil, success: { (operation, responseObject) in
            
            //            print("Response: \(responseObject!)")
            completionHandler(operation,responseObject as? NSDictionary, Error?.self as? Error)
            
        }) { (operation, error) in
            
            print("Error: " + error.localizedDescription)
            completionHandler(operation!, nil ,error)
        }
    }
}
