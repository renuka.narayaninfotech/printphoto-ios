//
//  Extension.swift
//
//
//  Created by AmitMoradiya on 12/09/18.
//  Copyright © 2018 AmitMoradiya. All rights reserved.
//

import Foundation
import SystemConfiguration
import UIKit
import SDWebImage
//import SDWebImageFLPlugin
//import FLAnimatedImage

//import UIActivityIndicator_for_SDWebImage
//import SDWebImage_ProgressView

extension UIView {
    
    func scale(by scale: CGFloat) {
        self.contentScaleFactor = scale
        for subview in self.subviews {
            subview.scale(by: scale)
        }
    }
    
    func getImage(scale: CGFloat? = nil, isMasked: Bool) -> UIImage {
        
        
        let layer1 = self.layer.mask
        
        if !isMasked
        {
            self.layer.mask = nil
        }
        
        let newScale = scale ?? UIScreen.main.scale
        self.scale(by: newScale)
        
        let format = UIGraphicsImageRendererFormat()
        format.scale = newScale
        
        let renderer = UIGraphicsImageRenderer(size: self.bounds.size, format: format)
        
        let image = renderer.image { rendererContext in
            self.layer.render(in: rendererContext.cgContext)
        }
        
        self.layer.mask = layer1
        
        //        let compressData = image.jpegData(compressionQuality: 0.1)
        //        let compressedImage = UIImage(data: compressData!)!
        
        return image
    }
    
    
    //********************** UIVIEW BORDER **********************
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0,y: 0, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width,y: 0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:self.frame.size.height - width, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func setBorder(borderWidth: CGFloat = 2,
                   borderColor: CGColor = UIColor(red:10/255, green:186/255, blue:212/255, alpha: 1).cgColor)
    {
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor
    }
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    
    func addShadow(shadowColor: CGColor = UIColor.darkGray.cgColor,
                   shadowOffset: CGSize = CGSize(width: -2.0, height: -2.0),
                   shadowOpacity: Float = 0.5,
                   shadowRadius: CGFloat = 2.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    func addShadow(color: UIColor, opacity: Float = 0.6, offSet: CGSize, radius: CGFloat = 5.0, scale: Bool = true) {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        
        self.layer.cornerRadius = 2.0
    }
    
    func addshadowAllSide(top: Bool,
                          left: Bool,
                          bottom: Bool,
                          right: Bool,
                          shadowRadius: CGFloat = 0.2) {
        
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = 0.1
        
        let path = UIBezierPath()
        var x: CGFloat = 0
        var y: CGFloat = 0
        var viewWidth = self.frame.width
        var viewHeight = self.frame.height
        
        // here x, y, viewWidth, and viewHeight can be changed in
        // order to play around with the shadow paths.
        if (!top) {
            y+=(shadowRadius+1)
        }
        if (!bottom) {
            viewHeight-=(shadowRadius+1)
        }
        if (!left) {
            x+=(shadowRadius+1)
        }
        if (!right) {
            viewWidth-=(shadowRadius+1)
        }
        // selecting top most point
        path.move(to: CGPoint(x: x, y: y))
        // Move to the Bottom Left Corner, this will cover left edges
        
        path.addLine(to: CGPoint(x: x, y: viewHeight))
        // Move to the Bottom Right Corner, this will cover bottom edge
        
        path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
        // Move to the Top Right Corner, this will cover right edge
        
        path.addLine(to: CGPoint(x: viewWidth, y: y))
        // Move back to the initial point, this will cover the top edge
        
        path.close()
        self.layer.shadowPath = path.cgPath
    }
    
    func setRoundCornerRadius() -> CGFloat {
        
        return self.frame.size.width / 2
    }
    
    func Set_Corner_image(width:CGFloat,height:CGFloat,Corners:UIRectCorner) {
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: Corners, cornerRadii: CGSize(width: width, height: height)).cgPath
        self.layer.mask = rectShape
        
    }
    
    func Set_Corner_image_withborder(width:CGFloat,height:CGFloat,Corners:UIRectCorner) {
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: Corners, cornerRadii: CGSize(width: width, height: height)).cgPath
        self.layer.mask = rectShape
        self.layer.borderWidth = 1.0
        
    }
    var Getwidth : CGFloat {
        get { return self.frame.size.width  }
        set { self.frame.size.width = newValue }
    }
    
    var Getheight : CGFloat {
        get { return self.frame.size.height  }
        set { self.frame.size.height = newValue }
    }
    
    var Getsize:       CGSize  { return self.frame.size}
    
    var Getorigin:     CGPoint { return self.frame.origin }
    var Getx : CGFloat {
        get { return self.frame.origin.x  }
        set { self.frame.origin.x = newValue }
    }
    
    var Gety : CGFloat {
        get { return self.frame.origin.y  }
        set { self.frame.origin.y = newValue }
    }
    
    
    var Getleft:       CGFloat { return self.frame.origin.x }
    var Getright:      CGFloat { return self.frame.origin.x + self.frame.size.width }
    var Gettop:        CGFloat { return self.frame.origin.y }
    var Getbottom:     CGFloat { return self.frame.origin.y + self.frame.size.height }
    
    
    func addDashedBorder() {
        let color = UIColor.black.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1.5
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
    
    func fadeInoutAnimation(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        
        //        DispatchQueue.main.async {
        
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
            self.alpha = 1.0
        }) { (yes) in
            UIView.animate(withDuration: duration,
                           delay: duration,
                           options:[.beginFromCurrentState , .allowUserInteraction, .curveEaseInOut, .autoreverse],
                           animations: { self.alpha = 0.0 },
                           completion: nil)
        }
        //        }
    }
    
    func fadeOutPoorConnection(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
                //                self.layer.removeAllAnimations()
                self.alpha = 1.0
            }) { (yes) in
                self.layer.removeAllAnimations()
                self.alpha = 1.0
            }
        }
    }
    
    
    // Export pdf from Save pdf in drectory and return pdf file path
    func exportAsPdfFromView(name : String) -> String {
        
        let pdfPageFrame = self.bounds
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
        UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
        self.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        return self.saveViewPdf(data: pdfData, name: name)
    }
    
    // Save pdf file in document directory
    func saveViewPdf(data: NSMutableData , name : String) -> String {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDirectoryPath = paths[0]
        let pdfPath = docDirectoryPath.appendingPathComponent("\(name).pdf")
        if data.write(to: pdfPath, atomically: true) {
            return pdfPath.path
        } else {
            return ""
        }
        
    }
}

public extension UIImage {
    
    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
    func resize(targetSize: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size:targetSize).image { _ in
            self.draw(in: CGRect(origin: .zero, size: targetSize))
        }
    }
    
    
    func mask(with color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        defer { UIGraphicsEndImageContext() }
        
        guard let context = UIGraphicsGetCurrentContext() else { return self }
        
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        
        guard let mask = self.cgImage else { return self }
        context.clip(to: rect, mask: mask)
        color.setFill()
        context.fill(rect)
        
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else { return self }
        
        return newImage
    }
    
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func fixedOrientation() -> UIImage {
        
        if imageOrientation == .up {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case .up, .upMirrored:
            break
        }
        
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: (self.cgImage?.colorSpace)!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
            break
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        let cgImage: CGImage = ctx.makeImage()!
        
        return UIImage(cgImage: cgImage)
    }
    
    func rotate(radians: Float) -> UIImage? {
        
        
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()
        
        // Move origin to middle
        context?.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context?.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //        let compressData = newImage?.jpegData(compressionQuality: 0.1)
        //        let compressedImage = UIImage(data: compressData!)!
        
        return newImage
    }
    
    func rotateNew(radians: Float, completion: @escaping((UIImage?) -> ())) -> () {
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            
            //        DispatchQueue.global(qos: .userInitiated).async {
            
            var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
            // Trim off the extremely small float value to prevent core graphics from rounding it up
            newSize.width = floor(newSize.width)
            newSize.height = floor(newSize.height)
            
            UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
            let context = UIGraphicsGetCurrentContext()
            
            // Move origin to middle
            context?.translateBy(x: newSize.width/2, y: newSize.height/2)
            // Rotate around middle
            context?.rotate(by: CGFloat(radians))
            // Draw the image at its center
            self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
            
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            //            DispatchQueue.main.async {
            //                let compressData = newImage?.jpegData(compressionQuality: 0.1)
            //                let compressedImage = UIImage(data: compressData!)!
            
            completion(newImage)
            //            }
        }
        
        
    }
    func cropAlpha() -> UIImage {
        
        let cgImage = self.cgImage!;
        
        let width = cgImage.width
        let height = cgImage.height
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bytesPerPixel:Int = 4
        let bytesPerRow = bytesPerPixel * width
        let bitsPerComponent = 8
        let bitmapInfo: UInt32 = CGImageAlphaInfo.premultipliedLast.rawValue | CGBitmapInfo.byteOrder32Big.rawValue
        
        guard let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo),
            let ptr = context.data?.assumingMemoryBound(to: UInt8.self) else {
                return self
        }
        
        context.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: width, height: height))
        
        var minX = width
        var minY = height
        var maxX: Int = 0
        var maxY: Int = 0
        
        for x in 1 ..< width {
            for y in 1 ..< height {
                
                let i = bytesPerRow * Int(y) + bytesPerPixel * Int(x)
                let a = CGFloat(ptr[i + 3]) / 255.0
                
                if(a>0) {
                    if (x < minX) { minX = x };
                    if (x > maxX) { maxX = x };
                    if (y < minY) { minY = y};
                    if (y > maxY) { maxY = y};
                }
            }
        }
        
        let rect = CGRect(x: CGFloat(minX),y: CGFloat(minY), width: CGFloat(maxX-minX), height: CGFloat(maxY-minY))
        let imageScale:CGFloat = self.scale
        let croppedImage =  self.cgImage!.cropping(to: rect)!
        let ret = UIImage(cgImage: croppedImage, scale: imageScale, orientation: self.imageOrientation)
        
        return ret;
    }
    
    func imageByApplyingClippingBezierPath(_ path: UIBezierPath) -> UIImage {
        // Mask image using path
        let maskedImage = imageByApplyingMaskingBezierPath(path)
        
        // Crop image to frame of path
        let croppedImage = UIImage(cgImage: maskedImage.cgImage!.cropping(to: path.bounds)!)
        return croppedImage
    }
    
    func imageByApplyingMaskingBezierPath(_ path: UIBezierPath) -> UIImage {
        // Define graphic context (canvas) to paint on
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()!
        context.saveGState()
        
        // Set the clipping mask
        path.addClip()
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        let maskedImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        // Restore previous drawing context
        context.restoreGState()
        UIGraphicsEndImageContext()
        
        return maskedImage
    }
}

extension Data {
    
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    //********************** STRIKE LINE OF LABEL **********************
    
    func heightOfLabel(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func widthOfWidth(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    
    func strikeThrough()->NSAttributedString{
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
    
    var expression: NSExpression {
        return NSExpression(format:self)
    }
    
    func localized(withComment comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
    
    func isContainsNumbers() -> Bool {
        
        let numberRegEx  = ".*[0-9]+.*"
        let testCase     = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        return testCase.evaluate(with: self)
    }
    
    func index(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    
    func endIndex(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    
    func indexes(of string: String, options: CompareOptions = .literal) -> [Index] {
        
        var result: [Index] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.upperBound
        }
        return result
    }
    
    func ranges(of string: String, options: CompareOptions = .literal) -> [Range<Index>] {
        
        var result: [Range<Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.upperBound
        }
        return result
    }
    
    var length:Int {
        
        return self.count
    }
    
    func indexOf(target: String) -> Int? {
        
        let range = (self as NSString).range(of: target)
        
        guard Range.init(range) != nil else{
            return nil
        }
        
        return range.location
        
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    var isNumeric: Bool {
        
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    
    func isNullOrWhiteSpace() -> Bool {
        
        if self.trimmingCharacters(in: CharacterSet.whitespaces).count > 0 {
            return false
        }
        
        return true
    }
    
    func digitsOnly() -> String{
        let stringArray = self.components(
            separatedBy: NSCharacterSet.decimalDigits.inverted)
        let newString = stringArray.joined(separator: "")
        
        return newString
    }
    
    private static var digits = UnicodeScalar("0")..."9"
    var digits: String {
        return String(unicodeScalars.filter(String.digits.contains))
    }
    
    func toDate( inputDateFormat format  : String ) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale.current
        return dateFormatter.date(from: self)!
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    public func range(ofText text: String) -> NSRange {
        let fullText = self
        let range = (fullText as NSString).range(of: text)
        return range
    }
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                substring(with: substringFrom..<substringTo)
            }
        }
    }
    
}

extension Date {
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale.current
        return dateFormatter.string(from: self)
    }
    
}

extension NSAttributedString {
    
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension Int {
    func parse(from string: String) -> Int? {
        return Int(string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
    }
}

extension UIViewController {
    
    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if DeviceType.IS_IPAD
        {
            alertController.setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 22),NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedTitle")
            alertController.setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18),NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedMessage")
        }
        
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true) {
                print("KO")
            }
        }
        
    }
    
    func topMostViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
    
    var topbarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //    //MARK:- ********************** API CALLING SET BADGE ICON ON CART TAB **********************
    //    func Set_Badge_Icon() {
    //
    //        if Reachability.isConnectedToNetwork() {
    //
    //            if userDefault.value(forKey: "USER_ID") != nil
    //            {
    //                let ParamDict = ["user_id": userDefault.value(forKey: "USER_ID")!] as NSDictionary
    //
    //                AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)get_cart_data", parameters: ParamDict) { (APIResponce, Responce, error1) in
    //
    //
    //                    if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
    //                    {
    //
    //                        let responceDict = Responce as! [String:Any]
    //
    //                        if responceDict["ResponseCode"] as! String == "1"
    //                        {
    //                            cartInfoMainArray = responceDict["data"] as! [[String:Any]]
    //
    //                            //                            DispatchQueue.main.async {
    //                            if cartInfoMainArray.count <= 0
    //                            {
    //                                if let tabItems = self.tabBarController?.tabBar.items {
    //                                    // In this case we want to modify the badge number of the third tab:
    //                                    let tabItem = tabItems[3]
    //
    //                                    tabItem.badgeValue = nil
    //                                }
    //
    //                            }
    //                            else
    //                            {
    //                                var BadgeNumber = 0
    //                                if let tabItems = self.tabBarController?.tabBar.items {
    //                                    // In this case we want to modify the badge number of the third tab:
    //                                    BadgeNumber = cartInfoMainArray.count
    //                                    let tabItem = tabItems[3]
    //                                    tabItem.badgeValue = "\(BadgeNumber)"
    //                                    tabItem.badgeColor = hexStringToUIColor(hex: "0ABAD4")
    //                                }
    //                            }
    //                            //                            }
    //                            //                            if let tabItems = self.tabBarController?.tabBar.items {
    //                            //                                // In this case we want to modify the badge number of the third tab:
    //                            //                                let tabItem = tabItems[3]
    //                            //
    //                            //                            }
    //
    //                        }
    //
    //                    }
    //                    else
    //                    {
    //                        self.presentAlertWithTitle(title: (error1?.localizedDescription)!, message: "", options: "OK", completion: { (ButtonIndex) in
    //
    //                        })
    //                    }
    //                }
    //            }else
    //            {
    //                if let tabItems = tabBarController?.tabBar.items {
    //                    // In this case we want to modify the badge number of the third tab:
    //                    let tabItem = tabItems[3]
    //                    tabItem.badgeValue = nil
    //                }
    //            }
    //
    //        }
    //        else
    //        {
    //            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
    //
    //            })
    //        }
    //    }
}

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
    
    class var statusBarBackgroundColor: UIColor? {
        get {
            return (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor
        } set {
            (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = newValue
        }
    }
}

func getCurrentViewController()-> UIViewController{
    
    return UIApplication.shared.topMostViewController()!
}


@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable override var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
    
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        
        self.setAttributedTitle(attributedString, for: .normal)
    }
    
    //    func addBackButton(SetImage:UIImage, Frame: CGRect, ImageEdgeInsets:UIEdgeInsets) -> UIButton {
    //
    //        let button: UIButton     = UIButton(type: .custom)
    //        button.frame             = Frame
    //        button.imageEdgeInsets   = ImageEdgeInsets
    //        button.setImage(SetImage, for: .normal)
    //
    //        return button
    //    }
    
}

extension UIImageView {
    
    func setImageWithActivityIndicator(indicatorStyle : UIActivityIndicatorView.Style , imageURL : String) {
        
        self.sd_setShowActivityIndicatorView(true)
        self.sd_setIndicatorStyle(indicatorStyle)
        
        if imageURL.contains(".gif")
        {
            SDImageCache().diskImageExists(withKey: imageURL) { (bool) in
                if bool
                {
                    let data = SDImageCache().diskImageData(forKey: imageURL)
                    
                    //                    let bcf = ByteCountFormatter()
                    //                    bcf.allowedUnits = [.useMB]
                    //                    bcf.countStyle = .file
                    //                    var string = bcf.string(fromByteCount: Int64(data!.count))
                    //                    string = string.replacingOccurrences(of: " MB", with: "")
                    //
                    //                    if Double(string)! > 5.0
                    //                    {
                    //                        self.image = dictGif[imageURL] as? UIImage ?? UIImage()
                    //                    }
                    //                    else
                    //                    {
                    self.image = UIImage.sd_animatedGIF(with: data)
                    //                    }
                    
                }
                else
                {
                    let url = URL.init(string: imageURL)
                    let data = try? Data(contentsOf: url!)
                    if data != nil {
                        
                        let image : UIImage = UIImage.sd_animatedGIF(with: data)
                        self.image = image
                        
                        //                        let bcf = ByteCountFormatter()
                        //                        bcf.allowedUnits = [.useMB]
                        //                        bcf.countStyle = .file
                        //                        var string = bcf.string(fromByteCount: Int64(data!.count))
                        //                        string = string.replacingOccurrences(of: " MB", with: "")
                        //
                        //                        if Double(string)! > 5.0
                        //                        {
                        //                            dictGif[imageURL] = UIImage.sd_animatedGIF(with: data)
                        //                        }
                        
                        SDImageCache().storeImageData(toDisk: data, forKey: imageURL)
                    }else
                    {
                        self.sd_addActivityIndicator()
                        self.sd_setIndicatorStyle(indicatorStyle)
                        self.sd_setShowActivityIndicatorView(true)
                    }
                    
                }
            }
        }
        else
        {
            self.sd_setImage(with: URL(string: imageURL)) { (image, error, cacheType, urls) in
                
                if image == nil
                {
                    //                    if urls!.absoluteString.trim() != "tempString"
                    //                    {
                    //                        self.setImageWithActivityIndicator(indicatorStyle : .gray , imageURL : urls!.absoluteString)
                    //                    }
                    //                    else
                    //                    {
                    //                        self.sd_addActivityIndicator()
                    //                        self.sd_setIndicatorStyle(indicatorStyle)
                    //                        self.sd_setShowActivityIndicatorView(true)
                    //                    }
                    
                    self.sd_addActivityIndicator()
                    self.sd_setIndicatorStyle(indicatorStyle)
                    self.sd_setShowActivityIndicatorView(true)
                }
                else
                {
                    self.image = UIImage(cgImage: (image?.cgImage)!, scale: 1.0, orientation: .up)
                }
            }
        }
    }
    
    func setImageWithRotation(indicatorStyle : UIActivityIndicatorView.Style , imageURL : String) {
        
        self.sd_setShowActivityIndicatorView(true)
        self.sd_setIndicatorStyle(indicatorStyle)
        
        if imageURL.contains(".gif")
        {
            SDImageCache().diskImageExists(withKey: imageURL) { (bool) in
                if bool
                {
                    let data = SDImageCache().diskImageData(forKey: imageURL)
                    
                    //                    let bcf = ByteCountFormatter()
                    //                    bcf.allowedUnits = [.useMB]
                    //                    bcf.countStyle = .file
                    //                    var string = bcf.string(fromByteCount: Int64(data!.count))
                    //                    string = string.replacingOccurrences(of: " MB", with: "")
                    //
                    //                    if Double(string)! > 5.0
                    //                    {
                    //                        self.image = dictGif[imageURL] as? UIImage ?? UIImage()
                    //                    }
                    //                    else
                    //                    {
                    self.image = UIImage.sd_animatedGIF(with: data)
                    //                    }
                }
                else
                {
                    let url = URL.init(string: imageURL)
                    let data = try? Data(contentsOf: url!)
                    if data != nil {
                        
                        let image : UIImage = UIImage.sd_animatedGIF(with: data)
                        self.image = image
                        
                        //                        let bcf = ByteCountFormatter()
                        //                        bcf.allowedUnits = [.useMB]
                        //                        bcf.countStyle = .file
                        //                        var string = bcf.string(fromByteCount: Int64(data!.count))
                        //                        string = string.replacingOccurrences(of: " MB", with: "")
                        //
                        //                        if Double(string)! > 5.0
                        //                        {
                        //                            dictGif[imageURL] = UIImage.sd_animatedGIF(with: data)
                        //                        }
                        
                        SDImageCache().storeImageData(toDisk: data, forKey: imageURL)
                    }else
                    {
                        self.sd_addActivityIndicator()
                        self.sd_setIndicatorStyle(indicatorStyle)
                        self.sd_setShowActivityIndicatorView(true)
                    }
                }
            }
        }
        else
        {
            self.sd_setImage(with: URL(string: imageURL)) { (image, error, cacheType, urls) in
                
                if image == nil
                {
                    //                    if urls!.absoluteString.trim() != "tempString"
                    //                    {
                    //                        self.setImageWithRotation(indicatorStyle : .gray , imageURL : urls!.absoluteString)
                    //                    }else
                    //                    {
                    //                        self.sd_addActivityIndicator()
                    //                        self.sd_setIndicatorStyle(indicatorStyle)
                    //                        self.sd_setShowActivityIndicatorView(true)
                    //                    }
                    
                    self.sd_addActivityIndicator()
                    self.sd_setIndicatorStyle(indicatorStyle)
                    self.sd_setShowActivityIndicatorView(true)
                }
                else
                {
                    self.image = UIImage(cgImage: (image?.cgImage)!, scale: 1.0, orientation: .right)
                }
            }
        }
        
    }
}

//MARK: table view extension
extension UITableView {
    
    func scroll(to: scrollsTo, animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.numberOfSections
            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
            switch to{
            case .top:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.scrollToRow(at: indexPath, at: .top, animated: animated)
                }
                break
            case .bottom:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
                break
            }
        }
    }
    
    enum scrollsTo {
        case top,bottom
    }
}

extension NSMutableAttributedString {
    
    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        
        // Swift 4.2 and above
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        
    }
    
}

extension UILabel {
    
    func boldRange(_ range: Range<String.Index>) {
        if let text = self.attributedText {
            let attr = NSMutableAttributedString(attributedString: text)
            let start = text.string.distance(from: text.string.startIndex, to: range.lowerBound)
            let length = text.string.distance(from: range.lowerBound, to: range.upperBound)
            attr.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: self.font.pointSize)], range: NSMakeRange(start, length))
            self.attributedText = attr
        }
    }
    
    func boldSubstring(_ substr: String) {
        if let text = self.attributedText {
            var range = text.string.range(of: substr)
            let attr = NSMutableAttributedString(attributedString: text)
            while range != nil {
                let start = text.string.distance(from: text.string.startIndex, to: range!.lowerBound)
                let length = text.string.distance(from: range!.lowerBound, to: range!.upperBound)
                var nsRange = NSMakeRange(start, length)
                let font = attr.attribute(NSAttributedString.Key.font, at: start, effectiveRange: &nsRange) as! UIFont
                if !font.fontDescriptor.symbolicTraits.contains(.traitBold) {
                    break
                }
                range = text.string.range(of: substr, options: NSString.CompareOptions.literal, range: range!.upperBound..<text.string.endIndex, locale: nil)
            }
            if let r = range {
                boldRange(r)
            }
        }
    }
}

extension CGPoint {
    static var Center: CGPoint {
        return CGPoint(x: UIScreen.main.bounds.maxX/2, y: UIScreen.main.bounds.maxY/2)
    }
}

extension UIScrollView {
    /// Sets content offset to the top.
    func resetScrollPositionToTop() {
        self.contentOffset = CGPoint(x: -contentInset.left, y: -contentInset.top)
    }
}

