//
//  CCWebViewController.swift
//  CCIntegrationKit_Swift
//
//  Created by Ram Mhapasekar on 7/4/17.
//  Copyright © 2017 Ram Mhapasekar. All rights reserved.
//

import UIKit
import Foundation

/**
 * class: CCWebViewController
 * CCWebViewController is responsible for to take all the values from the merchant and process futher for the payment
 * We will generate the RSA Key for this transaction first by using access code of the merchant and the unique order id for this transaction
 * After generating Successful RSA Key we will use that key for encrypting amount and currency and the encrypted details will use for intiate the transaction
 * Once the transaction is done  we will pass the transaction result to the next page (ie CCResultViewController here)
 */

class CCWebViewController: BaseViewController, UIWebViewDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var viewWeb: UIView!
    
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var orderID : String?
    var accessCode = String()
    var merchantId = String()
    var transactionID = String()
    var amount = String()
    var currency = String()
    var redirectUrl = String()
    var cancelUrl = String()
    var rsaKeyUrl = String()
    var rsaKeyDataStr = String()
    var rsaKey = String()
    var billingCountry = String()
    var billingTel = String()
    var billingEmail = String()
    static var statusCode = 0//zero means success or else error in encrption with rsa
    var encVal = String()
    var request: NSMutableURLRequest?
    
    lazy var webView: UIWebView = {
        let webView = UIWebView()
        webView.backgroundColor = .white
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.delegate = self
        webView.scalesPageToFit = true
        webView.contentMode = UIView.ContentMode.scaleAspectFill
        return webView
    }()
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        SetNavigationBarTitle(Startstring: "Payment", EndString: "")
        
//        let placeOrderVC = mainStoryBoard.instantiateViewController(withIdentifier: "PlaceOrderViewController") as? PlaceOrderViewController
//        placeOrderVC?.Set_Final_Status(payType: "CCAvenue")
        
        addBackButton()
        removeBackButton()
        setupWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /**
         * In viewWillAppear we will call gettingRsaKey method to generate RSA Key for the transaction and use the same to encrypt data
         */
        
        self.gettingRsaKey(){
            (success, object) -> () in
            DispatchQueue.main.sync {
                if success {
                    self.encyptCardDetails(data: object as! Data)
                }
                else{
                    self.displayAlert(msg: object as! String)
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        LoadingOverlay.shared.showOverlay(view: self.view)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- ********************** SETUP WEBVIEW **********************
    
    private func setupWebView(){
        
        //setup webview
        
        webView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 64)
        
        self.viewWeb.addSubview(webView)
    }
    
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** Get RsaKey & encrypt card details **********************
    
    /**
     * In this method we will generate RSA Key from the URL for this we will pass order id and the access code as the request parameter
     * after the successful key generation we'll pass the data to the request handler using complition block
     */
    
    private func gettingRsaKey(completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()){
        
        let serialQueue = DispatchQueue(label: "serialQueue", qos: .userInitiated)
        
        serialQueue.sync {
            self.rsaKeyDataStr = "access_code=\(self.accessCode)&order_id=\(self.transactionID)"
            //            let requestData = self.rsaKeyDataStr.data(using: String.Encoding.utf8, allowLossyConversion: true)
            
            let requestData = self.rsaKeyDataStr.data(using: String.Encoding.utf8)
            
            guard let urlFromString = URL(string: self.rsaKeyUrl) else{
                return
            }
            var urlRequest = URLRequest(url: urlFromString)
            urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
            urlRequest.httpMethod = "POST"
            urlRequest.httpBody = requestData
            
            let session = URLSession(configuration: URLSessionConfiguration.default)
            print("session",session)
            
            
            session.dataTask(with: urlRequest as URLRequest) {
                (data, response, error) -> Void in
                
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode{
                    
                    guard let data = data else{
                        print("No value for data")
                        completion(false, "Not proper data for RSA Key" as AnyObject?)
                        return
                    }
                    print("data :: ",data)
                    completion(true, data as AnyObject?)
                }
                else{
                    completion(false, "Unable to generate RSA Key please check" as AnyObject?)
                }
                }.resume()
        }
    }
    
    
    /**
     * encyptCardDetails method we will use the rsa key to encrypt amount and currency and onece the encryption is done we will pass this encrypted data to the initTrans to initiate payment
     */
    
    func encyptCardDetails(data: Data){
        guard let rsaKeytemp = String(bytes: data, encoding: String.Encoding.ascii) else{
            print("No value for rsaKeyTemp")
            return
        }
        rsaKey = rsaKeytemp
        rsaKey = self.rsaKey.trimmingCharacters(in: CharacterSet.newlines)
        rsaKey =  "-----BEGIN PUBLIC KEY-----\n\(self.rsaKey)\n-----END PUBLIC KEY-----\n"
        print("rsaKey :: ",rsaKey)
        
        let myRequestString = "amount=\(amount)&currency=\(currency)"
        
        let ccTool = CCTool()
        var encVal = ccTool.encryptRSA(myRequestString, key: rsaKey)
        
        encVal = CFURLCreateStringByAddingPercentEscapes(
            nil,
            encVal! as CFString,
            nil,
            "!*'();:@&=+$,/?%#[]" as CFString,
            CFStringBuiltInEncodings.UTF8.rawValue) as String?
        CCWebViewController.statusCode = 0
        
        //Preparing for webview call
        if CCWebViewController.statusCode == 0{
            
            let urlAsString = "https://secure.ccavenue.com/transaction/initTrans"
            let encryptedStr = "merchant_id=\(merchantId)&order_id=\(transactionID)&redirect_url=\(redirectUrl)&cancel_url=\(cancelUrl)&enc_val=\(encVal!)&access_code=\(accessCode)&billingCountry=\(billingCountry)&billingTel=\(billingTel)&billingEmail=\(billingEmail)"
            
            print("encValue :: \(encVal ?? "No val for encVal")")
            print(encryptedStr)
            
            print("encryptedStr :: ",encryptedStr)
            let myRequestData = encryptedStr.data(using: String.Encoding.utf8)
            request = NSMutableURLRequest(url: URL(string: urlAsString)!)
            
            request  = NSMutableURLRequest(url: URL(string: urlAsString)! as URL, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: 30)
            request?.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
            request?.setValue(urlAsString, forHTTPHeaderField: "Referer")
            request?.httpMethod = "POST"
            request?.httpBody = myRequestData

            let session = URLSession(configuration: URLSessionConfiguration.default)
            print("session",session)
            
            session.dataTask(with: request! as URLRequest) {
                (data, response, error) -> Void in
                
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode{
                    
                    guard let data = data else{
                        print("No value for data")
                        return
                    }
                    DispatchQueue.main.async{
                        self.webView.loadRequest(self.request! as URLRequest)
                    }
                    
                    print("data :: ",data)
                }
                else{
                    print("into else")
                    self.displayAlert(msg: "Unable to load webpage currently, Please try again later.")
                }
                }.resume()
            
            //            print(viewWeb.isLoading)
        }
        else{
            displayAlert(msg: "Unable to create requestURL")
        }
    }
    
    func displayAlert(msg: String){
        let alert: UIAlertController = UIAlertController(title: "ERROR", message: msg, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            LoadingOverlay.shared.hideOverlayView()
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func encryptRSA(raw: String, key pubKey: String) {
        
    }
    
    //MARK:- ********************** WebviewDelegate Methods **********************
    
    func webViewDidStartLoad(_ webView: UIWebView) {

    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.RemoveLoader()
        self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
            if ButtonIndex == 0
            {
                self.popViewController()
            }
        })
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingOverlay.shared.hideOverlayView()
        
        //covert the response url to the string and check for that the response url contains the redirect/cancel url if true then chceck for the transaction status and pass the response to the result controller(ie. CCResultViewController)
        let string = (webView.request?.url?.absoluteString)!
        
        if(string.contains(redirectUrl)) //("http://122.182.6.216/merchant/ccavResponseHandler.jsp"))//
        {
            //            print(viewWeb.isLoading)
            guard let htmlTemp:NSString = webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML") as NSString? else{
                print("failed to evaluate javaScript")
                return
            }
            
            let html = htmlTemp
            
            if ((html ).range(of: "tracking_id").location != NSNotFound) && ((html ).range(of: "bin_country").location != NSNotFound) {
                if ((html ).range(of: "Aborted").location != NSNotFound) || ((html ).range(of: "Cancel").location != NSNotFound) {
                    self.presentAlertWithTitle(title: "Transaction Cancelled!", message: "", options: "OK", completion: { (ButtonIndex) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                else if ((html ).range(of: "Success").location != NSNotFound) {
                    self.presentAlertWithTitle(title: "Transaction Successful!", message: "", options: "OK", completion: { (ButtonIndex) in
                       
//                        self.Set_Final_Status(payType: "CCAvenue")
                        
                        let paymentSuccessVC = mainStoryBoard.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
                        paymentSuccessVC.isFromOrderID = self.orderID
                        paymentSuccessVC.paymentType = "CCAvenueGateway"
                        self.pushViewController(paymentSuccessVC)
                    })
                }
                else if ((html ).range(of: "Fail").location != NSNotFound) {
                    self.presentAlertWithTitle(title: "Transaction Failed!", message: "", options: "OK", completion: { (ButtonIndex) in
                        self.navigationController?.popViewController(animated: true)
                    })
                } 
            }
            else{
                displayAlert(msg: "html does not contain any related data for this transaction.")
            }
        }
    }
    
    
//    func Set_Final_Status(payType: String) {
//
//        if Reachability.isConnectedToNetwork() {
//
//            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
//
//            let ParamDict = ["order_id" : orderID ?? "",
//                             "cancel_order" : "0",
//                             "razorpay_order_id" : "",
//                             "razorpay_payment_id" : "",
//                             "razorpay_signature" : "",
//                             "device_type": "ios",
//                             "app_version": appVersion] as NSDictionary
//
//            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)order/set_final_status", parameters: ParamDict) { (APIResponce, Responce, error1) in
//
//                self.RemoveLoader()
//
//                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
//                {
//                    let responceDict = Responce as! [String:Any]
//
//                    if responceDict["ResponseCode"] as! String == "1"
//                    {
//                        let dict  = responceDict["data"] as! NSDictionary
//
//                        let status = dict["status"] as? Int
//                        let message = "\(dict["message"] as? String ?? "")"
//
//                        if status == 1
//                        {
//                            if payType != "Cancel"
//                            {
//                                self.presentAlertWithTitle(title: "Congratulations", message: message, options: "OK", completion: { (ButtonIndex) in
//
//                                    let paymentSuccessVC = mainStoryBoard.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
//                                    paymentSuccessVC.placeOrderDict = dict
//                                    self.pushViewController(paymentSuccessVC)
//                                })
//                            }else
//                            {
//                                self.popViewController()
//                            }
//
//                        }else
//                        {
//                            self.presentAlertWithTitle(title: message, message: "", options: "OK", completion: { (ButtonIndex) in
//                            })
//                        }
//                    }
//                    else
//                    {
//                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
//                        })
//                    }
//                }
//                else
//                {
//                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
//                    })
//                }
//            }
//        }
//        else
//        {
//            self.RemoveLoader()
//
//            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
//            })
//        }
//    }
    
}
