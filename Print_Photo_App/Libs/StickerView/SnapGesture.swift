//
//  SnapGesture.swift
//  StickerViewSample
//
//  Created by des on 10/01/19.
//  Copyright © 2019 Edgar Sia. All rights reserved.
//

import UIKit

/*
 usage:
 
 add gesture:
 yourObjToStoreMe.snapGesture = SnapGesture(view: your_view)
 remove gesture:
 yourObjToStoreMe.snapGesture = nil
 disable gesture:
 yourObjToStoreMe.snapGesture.isGestureEnabled = false
 advanced usage:
 view to receive gesture(usually superview) is different from view to be transformed,
 thus you can zoom the view even if it is too small to be touched.
 yourObjToStoreMe.snapGesture = SnapGesture(transformView: your_view_to_transform, gestureView: your_view_to_recieve_gesture)
 
 */

class SnapGesture: NSObject, UIGestureRecognizerDelegate {
    
    // MARK: - init and deinit
    convenience init(view: UIView) {
        self.init(transformView: view, gestureView: view)
    }
    init(transformView: UIView, gestureView: UIView) {
        super.init()
        
        self.addGestures(v: gestureView)
        self.weakTransformView = transformView
    }
    deinit {
        self.cleanGesture()
    }
    
    // MARK: - private method
    private weak var weakGestureView: UIView?
    private weak var weakTransformView: UIView?
    
    private var panGesture: UIPanGestureRecognizer?
    private var pinchGesture: UIPinchGestureRecognizer?
    private var rotationGesture: UIRotationGestureRecognizer?
    private var doubleTapGesture: UITapGestureRecognizer?

    private func addGestures(v: UIView) {
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(panProcess(_:)))
        v.isUserInteractionEnabled = true
        panGesture?.delegate = self     // for simultaneous recog
        v.addGestureRecognizer(panGesture!)
        
        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchProcess(_:)))
        //view.isUserInteractionEnabled = true
        pinchGesture?.delegate = self   // for simultaneous recog
        v.addGestureRecognizer(pinchGesture!)
        
        rotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(rotationProcess(_:)))
        rotationGesture?.delegate = self
        v.addGestureRecognizer(rotationGesture!)
        
        doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(_:)))
        doubleTapGesture?.delegate = self
        doubleTapGesture?.numberOfTapsRequired = 2
        v.addGestureRecognizer(doubleTapGesture!)
        
        self.weakGestureView = v
    }
    
    @objc func handleDoubleTap(_ gesture: UITapGestureRecognizer){
        
        if let view = gesture.view! as? StickerView
        {
            if view.transform.a <= 1.0
            {
                let img = view.subviews[1] as? UIImageView
                
                let center = view.center
                let trans = view.transform.scaledBy(x: 2.0, y: 2.0)
                
//                UIView.animate(withDuration: 0.4) {
                
                    view.center = center
                    view.transform = trans
                    
                    self.lastScale = self.lastScale + 1.0
//                }
                let t = trans
                let it: CGAffineTransform = t.inverted()
                img?.transform = it
                img?.transform.tx = 0.0
                img?.transform.ty = 0.0

            }
        }
    }
    
    private func cleanGesture() {
        if let view = self.weakGestureView {
            //for recognizer in view.gestureRecognizers ?? [] {
            //    view.removeGestureRecognizer(recognizer)
            //}
            if panGesture != nil {
                view.removeGestureRecognizer(panGesture!)
                panGesture = nil
            }
            if pinchGesture != nil {
                view.removeGestureRecognizer(pinchGesture!)
                pinchGesture = nil
            }
            if rotationGesture != nil {
                view.removeGestureRecognizer(rotationGesture!)
                rotationGesture = nil
            }
        }
        self.weakGestureView = nil
        self.weakTransformView = nil
    }
    
    
    // MARK: - API
    
    private func setView(view:UIView?) {
        self.setTransformView(view, gestgureView: view)
    }
    
    private func setTransformView(_ transformView: UIView?, gestgureView:UIView?) {
        self.cleanGesture()
        
        if let v = gestgureView  {
            self.addGestures(v: v)
        }
        self.weakTransformView = transformView
    }
    
    open func resetViewPosition() {
        UIView.animate(withDuration: 0.4) {
            self.weakTransformView?.transform = CGAffineTransform.identity
        }
    }
    
    open var isGestureEnabled = true
    
    // MARK: - gesture handle
    
    // location will jump when finger number change
    private var initPanFingerNumber:Int = 1
    private var isPanFingerNumberChangedInThisSession = false
    private var lastPanPoint:CGPoint = CGPoint(x: 0, y: 0)
    @objc func panProcess(_ recognizer:UIPanGestureRecognizer) {
        if isGestureEnabled {
            //guard let view = recognizer.view else { return }
//            guard let view = self.weakTransformView else { return }
            
            let view = recognizer.view! as! StickerView
            
            // init
            if recognizer.state == .began {
                lastPanPoint = recognizer.location(in: view)
                initPanFingerNumber = recognizer.numberOfTouches
                isPanFingerNumberChangedInThisSession = false
            }
            
            // judge valid
            if recognizer.numberOfTouches != initPanFingerNumber {
                isPanFingerNumberChangedInThisSession = true
            }
            if isPanFingerNumberChangedInThisSession {
                return
            }
            
            
            // perform change
            let point = recognizer.location(in: view)
            view.transform = view.transform.translatedBy(x: point.x - lastPanPoint.x, y: point.y - lastPanPoint.y)
            lastPanPoint = recognizer.location(in: view)
            
//            view.setHandlerSize(30)
//            Abc.setNeedsDisplay()
        }
    }
    
    private var lastScale:CGFloat = 0.0
    private var lastPinchPoint:CGPoint = CGPoint(x: 0, y: 0)
    @objc func pinchProcess(_ recognizer:UIPinchGestureRecognizer) {
        if isGestureEnabled {
//            guard let view = self.weakTransformView else { return }
            
            let view = recognizer.view! as! StickerView
            let scaleTemp = 1.0 - (lastScale - recognizer.scale);
            print(scaleTemp)

            // Set maximum Zoom limit of Sticker
            if ((view.frame.width > 4000.0 || view.frame.height > 4000.0) && scaleTemp >= 1.0)
            {
                return
            }
            
            // init
            if recognizer.state == .began {
                lastScale = 1.0;
                lastPinchPoint = recognizer.location(in: view)
            }
            
            // judge valid
            if recognizer.numberOfTouches < 2 {
                lastPinchPoint = recognizer.location(in: view)
                return
            }
            
            // Scale
            let scale = 1.0 - (lastScale - recognizer.scale);
            view.transform = view.transform.scaledBy(x: scale, y: scale)
            lastScale = recognizer.scale;
            
            // Translate

            let point = recognizer.location(in: view)
            view.transform = view.transform.translatedBy(x: point.x - lastPinchPoint.x, y: point.y - lastPinchPoint.y)
            lastPinchPoint = recognizer.location(in: view)
            
//            print(view.frame)
//
            
            // Same button size when Zoom-In and Zoom-out sticker
            if let img = view.subviews[1] as? UIImageView
            {
                let t = view.transform
                let it: CGAffineTransform = t.inverted()
                img.transform = it
                img.transform.tx = 0.0
                img.transform.ty = 0.0
            }
    
//            self.lineBreakMode = .ByCharWrapping
//            label.baselineAdjustment = .none

//            var scale = CGPointGetDistance(point1: view.center, point2: recognizer.location(in: view.superview)) / CGPointGetDistance(point1: view.center, point2: recognizer.location(in: view.superview))
////            let minimumScale = CGFloat(view.minimumSize) / min(view.bounds.size.width, view.bounds.size.height)
//            scale = 1.0//max(scale, minimumScale)
//
//            print("scale : \(recognizer.scale)")
//
//
//            let scaledBounds = CGRectScale(view.bounds, wScale: scale, hScale: scale)
//            view.bounds = scaledBounds
            
//            view.backgroundColor = UIColor.green
           
//            view.setHandlerSize(30)
            
//            print("Snap Bounds \(view.bounds)")
//            print("Snap \(view.frame)")
        
        }
    }
    
    
    @objc func rotationProcess(_ recognizer: UIRotationGestureRecognizer) {
        if isGestureEnabled {
//            guard let view = self.weakTransformView else { return }
            
            let view = recognizer.view! as! StickerView
                      
            view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
            
//            view.setHandlerSize(30)
        }
    }
    
    
    //MARK:- UIGestureRecognizerDelegate Methods
    func gestureRecognizer(_: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith shouldRecognizeSimultaneouslyWithGestureRecognizer:UIGestureRecognizer) -> Bool {
        return true
    }
    
}


class SnapBlockGestureRecognizer: UIGestureRecognizer {
    
    init() {
        //self.init(target: self, action: #selector(__dummyAction))
        super.init(target: nil, action: nil)
        
        self.addTarget(self, action: #selector(__dummyAction))
        self.cancelsTouchesInView = false
    }
    
    override init(target: Any?, action: Selector?) {
        super.init(target: target, action: action)
        
        self.cancelsTouchesInView = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        if self.state == .possible {
            self.state = .began
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        self.state = .recognized
    }
    
    override func canBePrevented(by preventingGestureRecognizer: UIGestureRecognizer) -> Bool {
        return self.isGestureRecognizerAllowed(gr:preventingGestureRecognizer)
    }
    
    
    override func canPrevent(_ preventedGestureRecognizer: UIGestureRecognizer) -> Bool {
        return !(self.isGestureRecognizerAllowed(gr: preventedGestureRecognizer))
    }
    
    override func shouldBeRequiredToFail(by otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return !(self.isGestureRecognizerAllowed(gr: otherGestureRecognizer))
    }
    
    override func shouldRequireFailure(of otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    func isGestureRecognizerAllowed(gr: UIGestureRecognizer) -> Bool {
        return gr.view!.isDescendant(of: self.view!)
    }
    
    @objc func __dummyAction() {
        // do nothing
        // print("dummyAction")
    }
}
