//
//  SpeedTest.swift
//  SCAT Demo
//
//  Created by Mac Os on 06/05/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit

class SpeedTest: NSObject, URLSessionDelegate, URLSessionDataDelegate {

    typealias speedTestCompletionHandler = (_ megabytesPerSecond: Double? , _ error: Error?) -> Void
    
    var speedTestCompletionBlock : speedTestCompletionHandler?
    
    var startTime: CFAbsoluteTime!
    var stopTime: CFAbsoluteTime!
    var bytesReceived: Int!
    
    class var shared: SpeedTest {
        struct Singleton {
            static let instance = SpeedTest()
        }
        return Singleton.instance
    }
    
    func test(inSecond: Double, completion: @escaping((Double) -> ())) -> () {
        
        testDownloadSpeedWithTimout(timeout: inSecond) { (speed, error) in
            print("Download Speed:", speed ?? "KO")
            print("Speed Test Error:", error ?? "KO")
            completion(speed ?? 0.0)
        }
    }
    
    func testDownloadSpeedWithTimout(timeout: TimeInterval, withCompletionBlock: @escaping speedTestCompletionHandler) {
        
        guard let url = URL(string: "https://images.apple.com/v/imac-with-retina/a/images/overview/5k_image.jpg") else { return }
        
        startTime = CFAbsoluteTimeGetCurrent()
        stopTime = startTime
        bytesReceived = 0
        
        speedTestCompletionBlock = withCompletionBlock
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForResource = timeout
        let session = URLSession.init(configuration: configuration, delegate: self, delegateQueue: nil)
        session.dataTask(with: url).resume()
        
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        bytesReceived! += data.count
        stopTime = CFAbsoluteTimeGetCurrent()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        let elapsed = stopTime - startTime
        
        if let aTempError = error as NSError?, aTempError.domain != NSURLErrorDomain && aTempError.code != NSURLErrorTimedOut && elapsed == 0  {
            speedTestCompletionBlock?(nil, error)
            return
        }
        
        let speed = elapsed != 0 ? Double(bytesReceived) / elapsed / 1024.0 / 1024.0 : -1
        speedTestCompletionBlock?(speed, nil)
        
    }
}


//SpeedTest.shared.test(inSecond: 3.0) { (speed) in
//    print(speed)
//}
