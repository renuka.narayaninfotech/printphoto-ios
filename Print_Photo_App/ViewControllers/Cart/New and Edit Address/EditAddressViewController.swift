//
//  EditAddressViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 04/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import DropDown

fileprivate enum AMDropDownType {
    case countryCode
    case country
    case state
    case city
}

class EditAddressViewController: BaseViewController,UITextFieldDelegate,UIGestureRecognizerDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var nameBorderView: UIView!
    @IBOutlet weak var nameIconView: UIView!
    @IBOutlet weak var txtName: KTextField!
    @IBOutlet weak var mobileNoBorderView: UIView!
    @IBOutlet weak var lblMobileCode: UILabel!
    @IBOutlet weak var txtMobileNo: KTextField!
    @IBOutlet weak var mobileNoIconView: UIView!
    @IBOutlet weak var pincodeBorderView: UIView!
    @IBOutlet weak var txtPincode: KTextField!
    @IBOutlet weak var pincodeIconView: UIView!
    @IBOutlet weak var address1BorderView: UIView!
    @IBOutlet weak var address1IconView: UIView!
    @IBOutlet weak var txtAddress1: KTextField!
    @IBOutlet weak var address2BorderView: UIView!
    @IBOutlet weak var address2IconView: UIView!
    @IBOutlet weak var txtAddress2: KTextField!
    @IBOutlet weak var txtUserMobileNoView: UIView!
    @IBOutlet weak var txtUserMobileIconView: UIView!
    @IBOutlet weak var txtUserMobileNo: KTextField!
    @IBOutlet weak var countryBorderView: UIView!
    @IBOutlet weak var countryIconView: UIView!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var stateBorderView: UIView!
    @IBOutlet weak var stateIconView: UIView!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var cityBorderView: UIView!
    @IBOutlet weak var cityIconView: UIView!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var btnSaveView: UIView!
    @IBOutlet weak var heightStateAndCityView: NSLayoutConstraint!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    fileprivate var selectedDropDown : AMDropDownType?
    var dropDown = DropDown()
    var cityID : String?
    var editAddressDict = [String:Any]()
    var editDict = NSMutableDictionary()
    var contact : String?

    //MARK:- ********************** VIEW LIFECYLE **********************

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
   
        SetNavigationBarTitle(Startstring: "Address", EndString: "")

        Initialize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true

        ShowDropDown()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dropDown.hide()
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
        
        addBackButton()
        addResetButton()
        setViewBorder()
        
        let userDict = userDefault.value(forKey: "EditAccountDict") as! NSDictionary
        contact = userDict["mobileno"] as? String ?? ""
        
        btnSaveView.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        btnSaveView.cornerRadius = 3.0

        txtName.returnKeyType = .next
        txtMobileNo.returnKeyType = .next
        txtPincode.returnKeyType = .next
        txtAddress1.returnKeyType = .next
        txtAddress2.returnKeyType = .done
        
        editDict.setValuesForKeys(editAddressDict)

        // Check Flag Add Address otherwise Edit Address
        if isEditAddress
        {
//            if userDefault.string(forKey: "RegionCode") == "IN"
//            {
            
                let countyName = editDict.value(forKeyPath: "city.state.country.name") as? String
                let stateName = editDict.value(forKeyPath: "city.state.name") as? String
                
                let dictPerticularCountry = countryArray.first { (($0 as! NSDictionary)["name"] as! String) == countyName } as! NSDictionary
                stateArray = NSMutableArray(array: dictPerticularCountry.value(forKey: "states") as! NSArray)
                
                let dictPerticularState = stateArray.first { (($0 as! NSDictionary)["name"] as! String) == stateName } as! NSDictionary
                cityArray = NSMutableArray(array: dictPerticularState.value(forKey: "cities") as! NSArray)
//            }
            
            EditAddress()
        }else{
            Reset()
        }
    }
    
    //MARK:- **********************  BUTTON EVENT **********************

    @IBAction func press_btn_mobile_code(_ sender: UIButton) {
//        view.endEditing(true)
//        selectedDropDown = AMDropDownType.countryCode
//        dropDown.anchorView = self.lblMobileCode
//        dropDown.dataSource = phoneCodeArray as! [String]
////
////        dropDown.dataSource = ["+\(countryArray.value(forKey: "phonecode") as! [Int])"+" "+"\(countryArray.value(forKey: "sortname") as! [String])"+" "+"\(countryArray.value(forKey: "name") as! [String])"]
//        dropDown.show()
    }
    
    @IBAction func press_btn_country(_ sender: UIButton) {
//        view.endEditing(true)
//        selectedDropDown = AMDropDownType.country
//        dropDown.anchorView = self.lblCountry
//        dropDown.dataSource = countryArray.value(forKey: "name") as! [String]
//        dropDown.show()
        
    }
    
    @IBAction func press_btn_state(_ sender: UIButton) {
        if lblCountry.text == "Select Country"
        {
            self.presentAlertWithTitle(title: "Please select country", message: "", options: "OK", completion: { (ButtonIndex) in
            })
        }else
        {
            view.endEditing(true)
            selectedDropDown = AMDropDownType.state
            dropDown.anchorView = self.lblState
            
            if stateArray.count > 0
            {
                dropDown.dataSource = stateArray.value(forKey: "name") as! [String]
                dropDown.show()
            }
           
        }
    }
    
    @IBAction func press_btn_city(_ sender: UIButton) {
        if lblCountry.text == "Select Country"
        {
            self.presentAlertWithTitle(title: "Please select country", message: "", options: "OK", completion: { (ButtonIndex) in
            })
        }else if lblState.text == "Select State"
        {
            self.presentAlertWithTitle(title: "Please select state", message: "", options: "OK", completion: { (ButtonIndex) in
            })
        }else
        {
            view.endEditing(true)
            selectedDropDown = AMDropDownType.city
            dropDown.anchorView = self.lblCity
            if cityArray.count > 0
            {
                dropDown.dataSource = cityArray.value(forKey: "name") as! [String]
                dropDown.show()
            }
           
        }
    }
    
    @IBAction func press_btn_save_address(_ sender: UIButton) {
        
        if isSaveAddress()
        {
            Add_Address_API()
        }
    }
    

    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************

    func addResetButton() {
        let resetButton = UIButton(type: .custom)
        resetButton.setImage(UIImage(named: "ic_reset"), for: .normal)
        resetButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        resetButton.addTarget(self, action: #selector(self.resetAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: resetButton)
    }
    
    @objc func resetAction(_ sender: UIButton) {
       Reset()
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        isFromSaveAddress = false
        self.popViewController()
        
        // It's not Reload tableview
        isReloadTable = false
    }
    
    //MARK:- ********************** DROP DOWN **********************
    
    func ShowDropDown() {
        
        var stateDict = NSDictionary()
        
        // Managed region from SignInVC and RegionVC get selected region and managed stateArray for perticular Country
        for i in 0..<phoneCodeArray.count
        {
            let regionCode = ((self.removeSpecialCharsFromString(text: phoneCodeArray[i] as! String)).slice(from: "(", to: ")") ?? "")
            
            if userDefault.string(forKey: "RegionCode") == regionCode
            {
                
                let selectRegion = phoneCodeArray[i] as? String
                var region = selectRegion?.components(separatedBy: " ")
                
                self.lblMobileCode.text = region?.first
                
                region!.removeFirst()
                region!.removeFirst()
                
                self.lblCountry.text = region!.joined(separator: " ") //strSelectedRegion
                
                stateDict = countryArray.object(at: i) as! NSDictionary
                stateArray = (stateDict.value(forKey: "states") as! NSArray).mutableCopy() as? NSMutableArray ?? NSMutableArray()
                
                if userDefault.string(forKey: "RegionCode") == "IN"
                {
                    self.txtPincode.keyboardType = .numberPad
                    self.txtPincode.placeholder = "Pincode"
                    
                }else
                {
                    self.txtPincode.keyboardType = .asciiCapable
                    self.txtPincode.placeholder = "Zipcode"
                }
                
                
                // Hide state And City Field when User is Out Of India
                
//                if userDefault.string(forKey: "RegionCode") == "IN"
//                {
//                    self.heightStateAndCityView.constant =  DeviceType.IS_IPAD ? 174.0 : 116.0
//                    self.stateBorderView.isHidden = false
//                    self.cityBorderView.isHidden = false
//                }else
//                {
//                    self.heightStateAndCityView.constant = 0.0
//                    self.stateBorderView.isHidden = true
//                    self.cityBorderView.isHidden = true
//                }
                
                
//                userDefault.removeObject(forKey: "RegionCode")
//                userDefault.set(regionCode, forKey: "RegionCode")
            }
        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            print("Selected item: \(item) at index: \(index)")
           
            if self.selectedDropDown == AMDropDownType.countryCode {
                
                // Get Phonecode
                self.lblMobileCode.text = "+\(item.digitsOnly())"
                
                // Managed region , country code and country
                let regionCode = ((self.removeSpecialCharsFromString(text: phoneCodeArray[index] as! String)).slice(from: "(", to: ")") ?? "")
                
                let selectRegion = phoneCodeArray[index] as? String
                var region = selectRegion?.components(separatedBy: " ")
                
                region!.removeFirst()
                region!.removeFirst()
                
                stateDict = countryArray.object(at: index) as! NSDictionary
                stateArray = (stateDict.value(forKey: "states") as! NSArray).mutableCopy() as? NSMutableArray ?? NSMutableArray()
                
//                userDefault.removeObject(forKey: "RegionCode")
//                userDefault.set(regionCode, forKey: "RegionCode")
                
                if self.lblCountry.text != region!.joined(separator: " ")
                {
                    self.lblState.text = "Select State"
                    self.lblCity.text = "Select City"
                    self.txtMobileNo.text = ""
                    
                    self.lblCountry.text = region!.joined(separator: " ") //strSelectedRegion
                    
                }
                
                // Hide state And City Field when User is Out Of India
                
//                if userDefault.string(forKey: "RegionCode") == "IN"
//                {
//                    self.heightStateAndCityView.constant =  DeviceType.IS_IPAD ? 174.0 : 116.0
//                    self.stateBorderView.isHidden = false
//                    self.cityBorderView.isHidden = false
//                }else
//                {
//                    self.heightStateAndCityView.constant = 0.0
//                    self.stateBorderView.isHidden = true
//                    self.cityBorderView.isHidden = true
//                }
                
            }else if self.selectedDropDown == AMDropDownType.country {
                
                if self.lblCountry.text != item
                {
                    self.lblState.text = "Select State"
                    self.lblCity.text = "Select City"
                    self.txtMobileNo.text = ""

                }
                
                // Managed region , country code and country
                let regionCode = ((self.removeSpecialCharsFromString(text: phoneCodeArray[index] as! String)).slice(from: "(", to: ")") ?? "")
                
                let selectRegion = phoneCodeArray[index] as? String
                let region = selectRegion?.components(separatedBy: " ")
                
                self.lblMobileCode.text = region?.first
                
                // Get country name
                self.lblCountry.text = item
                
                // Get state name in dropdown
                
                if countryArray.object(at: index)as? NSDictionary != nil
                {
                    // Get state name in dropdown
                    let stateDict = countryArray.object(at: index) as? NSDictionary
                    
                    stateArray = (stateDict?.value(forKey: "states") as? NSArray)?.mutableCopy() as? NSMutableArray ?? NSMutableArray()
                }
                else
                {
                    stateArray = NSMutableArray()
                }
                
                if userDefault.string(forKey: "RegionCode") == "IN"
                {
                    self.txtPincode.keyboardType = .numberPad
                    self.txtPincode.placeholder = "Pincode"
                    
                }else
                {
                    self.txtPincode.keyboardType = .asciiCapable
                    self.txtPincode.placeholder = "Zipcode"
                }
                
                // Hide state And City Field when User is Out Of India
                
//                if userDefault.string(forKey: "RegionCode") == "IN"
//                {
//                    self.heightStateAndCityView.constant =  DeviceType.IS_IPAD ? 174.0 : 116.0
//                    self.stateBorderView.isHidden = false
//                    self.cityBorderView.isHidden = false
//                }else
//                {
//                    self.heightStateAndCityView.constant = 0.0
//                    self.stateBorderView.isHidden = true
//                    self.cityBorderView.isHidden = true
//                }
                
//                userDefault.removeObject(forKey: "RegionCode")
//                userDefault.set(regionCode, forKey: "RegionCode")
    
                
            }else if self.selectedDropDown == AMDropDownType.state {
                
                if self.lblState.text != item
                {
                    self.lblCity.text = "Select City"
                }
                
                // Get state name
                self.lblState.text = item
                
                
                if stateArray.object(at: index)as? NSDictionary != nil
                {
                    // Get city name in dropdown
                    let cityDict = stateArray.object(at: index) as? NSDictionary
                    
                    cityArray = (cityDict?.value(forKey: "cities") as? NSArray)?.mutableCopy() as? NSMutableArray ?? NSMutableArray()
                }
                else
                {
                    cityArray = NSMutableArray()
                }
               
            }else if self.selectedDropDown == AMDropDownType.city {

                // Get city name
                self.lblCity.text = item

                if ((cityArray.object(at: index) as? NSDictionary) != nil)
                {
                    let cityDict = cityArray.object(at: index) as? NSDictionary
                    
                    self.cityID = "\(cityDict?.value(forKey: "id") ?? "")"
                    
                    // Update City ID in dictionary
                    self.editDict.setValue(self.cityID, forKey: "city_id")
                }
            }
        }
    }
    
    //MARK:- ********************** UIVIEW BORDER **********************
    
    func setViewBorder() {
        nameIconView.setBorder()
        mobileNoIconView.setBorder()
        pincodeIconView.setBorder()
        address1IconView.setBorder()
        address2IconView.setBorder()
        txtUserMobileIconView.setBorder()
        countryIconView.setBorder()
        countryBorderView.setBorder()
        stateIconView.setBorder()
        stateBorderView.setBorder()
        cityIconView.setBorder()
        cityBorderView.setBorder()
    }
    
    //MARK:- ********************** RESET TEXTFIELD **********************
    
    func Reset()
    {
        txtName.text = ""
        txtMobileNo.text = ""
        txtUserMobileNo.text = contact
        txtPincode.text = ""
        txtAddress1.text = ""
        txtAddress2.text = ""
//        lblCountry.text = "Select Country"
        lblState.text = "Select State"
        lblCity.text = "Select City"
    }
    
    //MARK:- ********************** OTHER FUNCTION **********************

    func EditAddress() {
        
        txtName.text = editDict.value(forKey: "receiver_name") as? String
        txtUserMobileNo.text = contact
        txtMobileNo.text = editDict.value(forKey: "alternate_mobile") as? String
        txtPincode.text = editDict.value(forKey: "pincode") as? String
        txtAddress1.text = editDict.value(forKey: "address") as? String
        txtAddress2.text = editDict.value(forKey: "address_1") as? String
        
        // Only visible state and city in india country
        lblCountry.text = editDict.value(forKeyPath: "city.state.country.name") as? String
        lblState.text = editDict.value(forKeyPath: "city.state.name") as? String
        lblCity.text = editDict.value(forKeyPath: "city.name") as? String
        
//        lblCountry.text = userDefault.string(forKey: "RegionCode") == "IN" ? editDict.value(forKeyPath: "city.state.country.name") as? String : editDict.value(forKeyPath: "country.name") as? String
//        lblState.text = userDefault.string(forKey: "RegionCode") == "IN" ? editDict.value(forKeyPath: "city.state.name") as? String : ""
//        lblCity.text = userDefault.string(forKey: "RegionCode") == "IN" ? editDict.value(forKeyPath: "city.name") as? String : ""
    }
    

    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtName
        {
            textField.resignFirstResponder()
            txtMobileNo.becomeFirstResponder()
        }else if textField == txtMobileNo
        {
            textField.resignFirstResponder()
            txtPincode.becomeFirstResponder()
        }else if textField == txtPincode
        {
            textField.resignFirstResponder()
            txtAddress1.becomeFirstResponder()
        }else if textField == txtAddress1
        {
            textField.resignFirstResponder()
            txtAddress2.becomeFirstResponder()
        }else
        {
            txtAddress2.resignFirstResponder()
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        textField.autocorrectionType = .no
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if textField.tag == 1
        {
            switch string
            {
            case  "0","1","2","3","4","5","6","7","8","9":
                
                if userDefault.string(forKey: "RegionCode") == "IN"
                {
                    if newLength <= 10
                    {
                        return true
                    } else {
                        return false
                    }
                } else if userDefault.string(forKey: "RegionCode") == "SA"
                {
                    if newLength <= 9
                    {
                        return true
                    } else {
                        return false
                    }
                }
                
            case "":
                return true && newLength <= 50
                
            default:
                let array = [string]
                if array.count == 0
                {
                    return true
                }
                return false && newLength <= 4
            }
        }else if textField.tag == 2
        {
            // Remove space before textfield
            if range.location == 0 && (string == " ") {
                return false
            }
            
            if userDefault.string(forKey: "RegionCode") == "IN"
            {
                switch string
                {
                case  "0","1","2","3","4","5","6","7","8","9":
                    
                    if newLength <= 6
                    {
                        return true
                    } else {
                        return false
                    }
                    
                case "":
                    return true && newLength <= 50
                    
                default:
                    let array = [string]
                    if array.count == 0
                    {
                        return true
                    }
                    return false && newLength <= 4
                }
            }
        }else if textField.tag == 3
        {
            if newLength <= 80
            {
                return true
            } else {
                return false
            }
        }
        return true
    }
    
    //MARK:- ********************** VALIDATION **********************
    
    func isSaveAddress()-> Bool {
        
        if Validation.isStringEmpty(txtName.text!) || txtName.text?.trim() == "" {
            presentAlertWithTitle(title: "Please enter name", message: "", options: "OK") { (Int) in
                self.txtName.becomeFirstResponder()
            }
        }
        else if Validation.isStringEmpty(txtMobileNo.text!) {
            presentAlertWithTitle(title: "Please enter alternate mobile number", message: "", options: "OK") { (Int) in
                self.txtMobileNo.becomeFirstResponder()
            }
        }
        else if userDefault.string(forKey: "RegionCode") == "IN" && !Validation.isStringEmpty(txtMobileNo.text!) && Int(txtMobileNo.text!.count) < 10
        {
            self.presentAlertWithTitle(title: "Please enter valid alternate mobile number", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txtMobileNo.becomeFirstResponder()
            })
        }else if !Validation.isPassWordMatch(pass: txtUserMobileNo.text!, confPass: txtMobileNo.text!)
        {
            presentAlertWithTitle(title: "Please provide another mobile number!!", message: "", options: "OK") { (Int) in
                self.txtMobileNo.becomeFirstResponder()
            }
        }
        else if userDefault.string(forKey: "RegionCode") == "IN" && Validation.isStringEmpty(txtPincode.text!) {
            presentAlertWithTitle(title: "Please enter pincode", message: "", options: "OK") { (Int) in
                self.txtPincode.becomeFirstResponder()
            }
        }else if userDefault.string(forKey: "RegionCode") != "IN" && Validation.isStringEmpty(txtPincode.text!) {
            presentAlertWithTitle(title: "Please enter zipcode", message: "", options: "OK") { (Int) in
                self.txtPincode.becomeFirstResponder()
            }
        }
        else if userDefault.string(forKey: "RegionCode") == "IN" && Int(txtPincode.text!.count) < 6
        {
            self.presentAlertWithTitle(title: "Pincode must be 6 digit", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txtPincode.becomeFirstResponder()
            })
        }else if Validation.isStringEmpty(txtAddress1.text!) || txtAddress1.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter address", message: "", options: "OK") { (Int) in
                self.txtAddress1.becomeFirstResponder()
            }
        }else if Validation.isStringEmpty(txtAddress2.text!) || txtAddress2.text?.trim() == "" {
            presentAlertWithTitle(title: "Please enter address", message: "", options: "OK") { (Int) in
                self.txtAddress2.becomeFirstResponder()
            }
        }else if lblCountry.text == "Select Country" {
            presentAlertWithTitle(title: "Please select country", message: "", options: "OK") { (Int) in
            }
        }else if lblState.text == "Select State" //&& userDefault.string(forKey: "RegionCode") == "IN"
        {
            presentAlertWithTitle(title: "Please select state", message: "", options: "OK") { (Int) in
            }
        }else if lblCity.text == "Select City" //&& userDefault.string(forKey: "RegionCode") == "IN"
        {
            presentAlertWithTitle(title: "Please select city", message: "", options: "OK") { (Int) in
            }
        }
        else{
            return true
        }
        
        return false
    }
    
    //MARK:- ********************** API CALLING **********************

    func Add_Address_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            var id : String?
            
            if isEditAddress
            {
                // For Edit Address
                id = "\(editAddressDict["id"] ?? "")"
            }else
            {
                // For Add New Address
                id = ""
            }
            
            var strParam = ""
            var strValue = ""
            
//            if userDefault.string(forKey: "RegionCode") == "IN"
//            {
                // Only in India country
                
                strParam = "city_id"
                strValue = "\(editDict.value(forKey: "city_id") ?? "")"
//            }else
//            {
//                // Only Outside of india country
//
//                strParam = "country_id"
//                strValue = userDefault.string(forKey: "countryId") ?? ""
//            }
            
            
            let ParamDict = ["id": id ?? "",
                             "user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                             "address": "\(txtAddress1.text ?? "")",
                             "address_1": "\(txtAddress2.text ?? "")",
                             "alternate_mobile": "\(txtMobileNo.text ?? "")",
                              strParam: strValue,
                             "pincode": "\(txtPincode.text ?? "")",
                             "receiver_name" : "\(txtName.text ?? "")"] as NSDictionary

            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)save_address", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    var responceDict = Responce as! [String:Any]
                    
                    if responceDict["success"] as! Int == 1
                    {
                        // It's not Reload tableview
                        isReloadTable = true
                        
                        let vc = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - 2]
                        vc.view.makeToast("\(responceDict["message"]!)", duration: 2.0, position: .bottom)
                        
                        deliveryAddressArray = responceDict["data"] as! [[String:Any]]

                        self.popViewController()
                        
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["message"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
        
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ()")
        return String(text.filter {okayChars.contains($0) })
    }
}
