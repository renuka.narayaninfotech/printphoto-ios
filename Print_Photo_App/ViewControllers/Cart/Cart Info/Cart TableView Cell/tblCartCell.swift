//
//  tblCartCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 01/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class tblCartCell: UITableViewCell {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cornerRadiosView: UIView!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var btn_delete: UIButton!
    @IBOutlet weak var btn_minus: UIButton!
    @IBOutlet weak var btn_plus: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblItemName.sizeToFit()
        lblPrice.sizeToFit()
        lblQuantity.sizeToFit()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
