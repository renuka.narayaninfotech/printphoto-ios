//
//  CartViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 01/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import GoogleMobileAds
import UserNotifications
import SKPhotoBrowser
import FacebookCore

class CartViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate, UNUserNotificationCenterDelegate,SKPhotoBrowserDelegate
{
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    @IBOutlet weak var tblCartInfo: UITableView!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var btnCheckOut: UIButton!
    @IBOutlet weak var bottomShadowView: UIView!
    @IBOutlet weak var signIn_signUp_view: UIView!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var nativeAdPlaceholder: UIView!
    @IBOutlet weak var btnSignupVertical: NSLayoutConstraint!
    
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var cartInfoMainArray = [[String:Any]]()
    var itemID : Int!
    var quantity : Int!
    var refreshControl = UIRefreshControl()
    
    // For Native Advanced ads
    var heightConstraint : NSLayoutConstraint?
    var adLoader: GADAdLoader!
    var nativeAdView: GADNativeAdView!
    var currencySymbol : String?
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Cart", EndString: "")
    
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        set_banner()
        
        self.tabBarController?.tabBar.isHidden = false

        self.bottomShadowView.isHidden = true
        self.tblCartInfo.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        
        nativeAdPlaceholder.isHidden = false
        btnSignupVertical.constant = 0.0
        
        if(isLoginOrNot == "cart_signup")
        {
            isLoginOrNot = ""
            ((self.tabBarController!.viewControllers! as NSArray)[(userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3] as! UINavigationController).popToRootViewController(animated: false)
            self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3
        }

        
        if userDefault.value(forKey: "USER_ID") == nil || userDefault.integer(forKey: "USER_ID") == 0
        {
//            bannerView.isHidden = false
            signIn_signUp_view.isHidden = false
            self.nodataLbl.isHidden = true
            self.nodataLbl.text = "No items available"

            // Load Native Ad
          /*  adLoader = GADAdLoader(adUnitID: Native_Ad_ID, rootViewController: self,
                                   adTypes: [ .unifiedNative ], options: nil)
            adLoader.delegate = self
            adLoader.load(GADRequest())
           */
            
        }else
        {
            signIn_signUp_view.isHidden = true
//            bannerView.isHidden = true
            
            registerTableViewCell(cellName: "tblCartCell", to: tblCartInfo)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.Get_Cart_Info_API(isLoader: true)
            })
        }

//        if isCartBtnBack
//        {
//            self.navigationItem.setHidesBackButton(false, animated:false)
            addBackButton()
////            isCartBtnBack = false
//        }
//        else
//        {
//            removeBackButton()
//        }
    }
 
    override func viewWillDisappear(_ animated: Bool) {
//        isCartBtnBack = false
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize()
    {
        addContactUsButton()
        btnSignUp.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        btnCheckOut.isExclusiveTouch = true
        
        DispatchQueue.main.async {
            self.btnCheckOut.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
            self.bottomShadowView.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        }
        
        // Native ads View
     //   let nibObjects = Bundle.main.loadNibNamed("UnifiedNativeAdView", owner: nil, options: nil)
        //let adView = nibObjects!.first as! GADUnifiedNativeAdView
        //setAdView(adView)
        initNativeAds()
    }
    
    func Local_Notification() {
        
        //creating the notification content
        let content = UNMutableNotificationContent()
        
        //adding title, subtitle, body and badge
//        content.title = "Print Photo"
//        content.subtitle = ""
        content.body = "The item which are there in your cart will look good in your hand. Go and Buy your product!!!"
        content.badge = 1
        content.sound = .default
    
        //getting the notification trigger after every day at 10:00 AM

        var dateComponents = DateComponents()
        dateComponents.hour = 10

//        let alarmTime = Calendar.current.date(byAdding: .day, value: 3, to: Date())!
//        let components = Calendar.current.dateComponents([.hour, .minute], from: alarmTime)

        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
//        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        //getting the notification request
        let request = UNNotificationRequest(identifier: "LocalNotification", content: content, trigger: trigger)
        
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
    }
    
    //MARK:- **********************  BUTTON EVENT **********************
    
    @IBAction func press_btn_SignUp(_ sender: UIButton) {
        let RegisterVC = mainStoryBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        isLoginOrNot = "cart_signup"
        pushViewController(RegisterVC)
    }
    
    @IBAction func press_btn_login(_ sender: UIButton) {

        isLoginOrNot = "cart_login"
        
        let lastIndex = (self.tabBarController?.viewControllers?.count ?? 1) - 1
        
        ((tabBarController!.viewControllers! as NSArray)[lastIndex] as! UINavigationController).popToRootViewController(animated: false)
        self.tabBarController?.selectedIndex = lastIndex
        
//        let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
//        pushViewController(signinVC)
    }
    
    @IBAction func press_btn_check_out(_ sender: UIButton) {
       
        Check_Stock_API()
       
    }
    
    @objc func pressButtonDeleteOrder(_ sender: UIButton?) {
        
        itemID = sender?.tag
        
        self.presentAlertWithTitle(title: "Delete", message: "Are you sure to delete this item from cart?", options: "NO","YES") { (ButtonIndex) in
            if ButtonIndex == 0
            {
                self.dismissViewController()
            }else
            {
                self.Delete_Record_API()
            }
        }
        
    }
    
    @objc func pressButtonDecreaseQuantity(_ sender: UIButton?) {
        
        let cell = sender?.superview?.superview?.superview?.superview?.superview as! tblCartCell
        itemID = sender?.tag
        quantity = Int(cell.lblQuantity.text!)!
        if quantity <= 1
        {
            self.topMostViewController().view.makeToast("Quantity can not be less then 1 !!", duration: 3.0, position: .bottom)
            
        }else
        {
            quantity = Int(cell.lblQuantity.text!)! - 1
            Update_Record_API()
        }
    }
    
    @objc func pressButtonIncreaseQuantity(_ sender: UIButton?) {
        
        let cell = sender?.superview?.superview?.superview?.superview?.superview as! tblCartCell
        itemID = sender?.tag
        quantity = Int(cell.lblQuantity.text!)! + 1
        Update_Record_API()
    }
    
    @objc func TaponImage(_ sender : UITapGestureRecognizer)
    {
        let imageView = sender.view as? UIImageView
        
        if imageView?.image != nil
        {
            SKPhotoBrowserOptions.displayAction = false   // action button will be hidden
            SKPhotoBrowserOptions.displayCloseButton = true   // Close Button will be hidden
            SKPhotoBrowserOptions.disableVerticalSwipe = true  // Disable Verticle swipe
            SKPhotoBrowserOptions.displayBackAndForwardButton = false // Disable Back and Forward button
            SKPhotoBrowserOptions.displayPagingHorizontalScrollIndicator = false
            SKPhotoBrowserOptions.displayCounterLabel = false
            SKPhotoBrowserOptions.enableSingleTapDismiss = true
            
            var images = [SKPhoto]()
            let photo = SKPhoto.photoWithImage(imageView?.image ?? UIImage())// add some UIImage
            images.append(photo)
            
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(0)
            present(browser, animated: true, completion: {})
        }
        
//        print("Done")
//        let imageView = sender.view as? UIImageView
//        let img = imageView?.image
//
//        var widthImg: CGFloat = 0.0
//        var heightImg: CGFloat = 0.0
//
//        if img != nil
//        {
//            self.navigationController?.isNavigationBarHidden = true
//            self.tabBarController?.tabBar.isHidden = true
//
//            widthImg = img!.size.width
//            heightImg = img!.size.height
//
//            if widthImg > heightImg
//            {
//                // landscape image
//                if widthImg > ScreenSize.SCREEN_WIDTH - 40.0
//                {
//                    widthImg = ScreenSize.SCREEN_WIDTH - 40.0
//                    heightImg = widthImg / (heightImg/widthImg)
//                }
//            }
//            else
//            {
//                // portrait image
//                if heightImg > ScreenSize.SCREEN_HEIGHT - 60.0
//                {
//                    if (heightImg/widthImg) < (ScreenSize.SCREEN_HEIGHT/ScreenSize.SCREEN_WIDTH)
//                    {
//                        widthImg = ScreenSize.SCREEN_WIDTH - 40.0
//                        heightImg = widthImg / (widthImg/heightImg)
//                    }
//                    else
//                    {
//                        heightImg = ScreenSize.SCREEN_HEIGHT - 60.0
//                        widthImg = heightImg * (widthImg/heightImg)
//                    }
//                }
//                else if widthImg > ScreenSize.SCREEN_WIDTH - 40.0
//                {
//                    widthImg = ScreenSize.SCREEN_WIDTH - 40.0
//                    heightImg = widthImg / (widthImg/heightImg)
//                }
//
//            }
//
//
//        let newView = UIView()
//        newView.frame = self.view.frame
//        newView.backgroundColor = .black
//        newView.isUserInteractionEnabled = true
//
//        let newImageView = UIImageView()
//        newImageView.frame = CGRect.init(x: 0.0, y: 0.0, width: widthImg, height: heightImg)
//        newImageView.center = self.view.center
//        newImageView.backgroundColor = .black
//        newImageView.contentMode = .scaleAspectFit
//        newImageView.image = img
//        newImageView.isUserInteractionEnabled = false
//
//        newView.addSubview(newImageView)
//
//        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage(_:)))
//        newView.addGestureRecognizer(tap)
//        self.view.addSubview(newView)
//
//        }

    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
//        if isCartBtnBack
//        {
//            ((tabBarController!.viewControllers! as NSArray)[1] as! UINavigationController).popToRootViewController(animated: false)
//            self.tabBarController?.selectedIndex = 1
//            isCartBtnBack = false
//        }
//        else
//        {
            ((self.tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)
            self.tabBarController?.selectedIndex = 0
//        }
    }
    
    func addContactUsButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_contact"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.contactUsAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func contactUsAction(_ sender: UIButton) {
        let contactUsVC = mainStoryBoard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        pushViewController(contactUsVC)
    }
    
    //MARK:- ********************** NATIVE ADVANCED ADS **********************
    /*
    func setAdView(_ view: GADUnifiedNativeAdView) {
        // Remove the previous ad view.
        nativeAdView = view
        nativeAdPlaceholder.addSubview(nativeAdView)
        nativeAdView.translatesAutoresizingMaskIntoConstraints = false
        
        // Layout constraints for positioning the native ad view to stretch the entire width and height
        // of the nativeAdPlaceholder.
        let viewDictionary = ["_nativeAdView": nativeAdView!]
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[_nativeAdView]|",
                                                                options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[_nativeAdView]|",
                                                                options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary))
    }
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
        nativeAdView.nativeAd = nativeAd
        
        btnSignupVertical.constant = -150.0

        nativeAdPlaceholder.isHidden = false

        // Set ourselves as the native ad delegate to be notified of native ad events.
        nativeAd.delegate = self
        
        // Deactivate the height constraint that was set when the previous video ad loaded.
        heightConstraint?.isActive = false
        
        // Populate the native ad view with the native ad assets.
        // The headline and mediaContent are guaranteed to be present in every native ad.
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
        
        // Some native ads will include a video asset, while others do not. Apps can use the
        // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
        // UI accordingly.
        if let controller = nativeAd.videoController, controller.hasVideoContent() {
            // By acting as the delegate to the GADVideoController, this ViewController receives messages
            // about events in the video lifecycle.
            //            controller.delegate = self
        }
        else {
        }
        
        // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
        // ratio of the media it displays.
        //        if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
        //            heightConstraint = NSLayoutConstraint(item: mediaView,
        //                                                  attribute: .height,
        //                                                  relatedBy: .equal,
        //                                                  toItem: mediaView,
        //                                                  attribute: .width,
        //                                                  multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
        //                                                  constant: 0)
        //            heightConstraint?.isActive = true
        //        }
        
        // These assets are not guaranteed to be present. Check that they are before
        // showing or hiding them.
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        
        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
        
        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
        
        //        (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(from:nativeAd.starRating)
        //        nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil
        
        //        (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
        //        nativeAdView.storeView?.isHidden = nativeAd.store == nil
        
        //        (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
        //        nativeAdView.priceView?.isHidden = nativeAd.price == nil
        
        (nativeAdView.starRatingView as? UIImageView)?.isHidden = true
        nativeAdView.storeView?.isHidden = true
        nativeAdView.priceView?.isHidden = true
        
        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
        
        // In order for the SDK to process touch events properly, user interaction should be disabled.
        nativeAdView.callToActionView?.isUserInteractionEnabled = false
    }
//
//    func imageOfStars(from starRating: NSDecimalNumber?) -> UIImage? {
//        guard let rating = starRating?.doubleValue else {
//            return nil
//        }
//        if rating >= 5 {
//            return UIImage(named: "stars_5")
//        } else if rating >= 4.5 {
//            return UIImage(named: "stars_4_5")
//        } else if rating >= 4 {
//            return UIImage(named: "stars_4")
//        } else if rating >= 3.5 {
//            return UIImage(named: "stars_3_5")
//        } else {
//            return nil
//        }
//    }
//
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
        
        btnSignupVertical.constant = 0.0

        print("\(adLoader) failed with error: \(error.localizedDescription)")
    }
    
    func nativeAdDidRecordClick(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }
    
    func nativeAdDidRecordImpression(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }
    
    func nativeAdWillPresentScreen(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }
    
    func nativeAdWillDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }
    
    func nativeAdDidDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }
    
    func nativeAdWillLeaveApplication(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }

    */
    //MARK:- ********************** TABLEVIEW  METHOD **********************
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartInfoMainArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tblCartCell", for: indexPath) as! tblCartCell
        
        //        // corner radius
        //        cell.cornerRadiosView.layer.cornerRadius = 10
        //
        //        // border
        ////        cell.cornerRadiosView.layer.borderWidth = 1.0
        ////        cell.cornerRadiosView.layer.borderColor = UIColor.black.cgColor
        //
        //        // shadow
        //        cell.cornerRadiosView.layer.shadowColor = UIColor.black.cgColor
        //        cell.cornerRadiosView.layer.shadowOffset = CGSize(width: 3, height: 3)
        //        cell.cornerRadiosView.layer.shadowOpacity = 0.7
        //        cell.cornerRadiosView.layer.shadowRadius = 4.0
        //
        
        cell.imgItem.isExclusiveTouch = true
        cell.btn_delete.isExclusiveTouch = true
        cell.btn_plus.isExclusiveTouch = true
        cell.btn_minus.isExclusiveTouch = true
        
        DispatchQueue.main.async {
            cell.cornerRadiosView.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
            cell.cornerRadiosView.cornerRadius = 5.0
        }
        
        let dict = cartInfoMainArray[indexPath.row] as NSDictionary
        
        //        cell.imgItem.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: dict.value(forKey: "n_case_image") as! String)
        
        let imageString = ((dict.value(forKey: "display_images") as! NSArray)[0] as! NSDictionary)["image"] as? String ?? ""
        
//        let url = URL(string:imageString)
//        if let data = try? Data(contentsOf: url!)
//        {
//            let imageData: UIImage = UIImage(data: data)!
//            // Rotate Image
//            cell.imgItem.image =
//        }
   
        let imgRotateDict = dict.value(forKeyPath: "display") as? [String:Any]
        
        let isRotate = "\(imgRotateDict?["rotate"] ?? "")"
        
        if isRotate == "1"
        {
            cell.imgItem.setImageWithRotation(indicatorStyle: .gray, imageURL : imageString)
        }else
        {
            cell.imgItem.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL : imageString)
        }
        
        cell.lblItemName.text = dict.value(forKeyPath: "model_name") as? String
        let price = "\(String(format: "%.2f", (dict.value(forKeyPath: "subtotal") as? Double ?? 0.0)))"
        cell.lblPrice.text = "Amount : \(self.currencySymbol ?? "")\(price)"
        cell.lblQuantity.text = "\(dict.value(forKey: "quantity") as! Int)"
        
        itemID = dict.value(forKey: "id") as? Int
        
        
        //        quantity = dict.value(forKey: "quantity") as? Int
        
        // For delete record
        cell.btn_delete.tag = itemID
        cell.btn_delete.addTarget(self, action: #selector(self.pressButtonDeleteOrder(_:)), for: .touchDown)
        
        
        // For decrease quantity
        cell.btn_minus.tag = itemID
        cell.btn_minus.addTarget(self, action: #selector(self.pressButtonDecreaseQuantity(_:)), for: .touchDown)
        
        // For increase quantity
        cell.btn_plus.tag = itemID
        cell.btn_plus.addTarget(self, action: #selector(self.pressButtonIncreaseQuantity(_:)), for: .touchDown)

        DispatchQueue.main.async {
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.TaponImage(_:)))
            tap.delegate = self
            tap.numberOfTouchesRequired = 1
            cell.imgItem.addGestureRecognizer(tap)
        }
        cell.imgItem?.layoutIfNeeded()
        cell.imgItem.setNeedsLayout()
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Get_Cart_Info_API(isLoader: Bool) {
        if Reachability.isConnectedToNetwork() {
            
            if isLoader
            {
                showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            }
            
            let ParamDict = ["user_id": userDefault.value(forKey: "USER_ID")!] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)get_cart", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.tblCartInfo.isHidden = false
                self.bottomShadowView.isHidden = false

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if let theJSONData = try? JSONSerialization.data(
                        withJSONObject: responceDict,
                        options: []) {
                        let theJSONText = String(data: theJSONData,
                                                   encoding: .ascii)
                        print("JSON string = \(theJSONText!)")
                    }
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        cart_id_array.removeAllObjects()
                        
                        let cartDict = responceDict["data"] as? [String:Any]
                        
                        self.currencySymbol = cartDict?["currency_symbol"] as? String ?? ""
                        self.cartInfoMainArray = cartDict?["cart_items"] as! [[String : Any]]
                        
                        // Remove Pending Notification
                        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()

                        if (self.cartInfoMainArray.count == 0)
                        {
                            self.tblCartInfo.isHidden = true
                            self.bottomShadowView.isHidden = true
                            self.nodataLbl.isHidden = false
                            self.nodataLbl.text = "No items available"
                        }
                        else
                        {
                            // Set Local Notification
                            self.Local_Notification()
                            
                            self.tblCartInfo.isHidden = false
                            self.bottomShadowView.isHidden = false
                            self.nodataLbl.isHidden = true
                            
//                            self.cartInfoMainArray.reverse()
                            
                            var fbSubParam = [[:]] as [[String : Any]]
                            for i in 0..<self.cartInfoMainArray.count
                            {
                                let dict = self.cartInfoMainArray[i] as NSDictionary

                                let cartID = "\(dict.value(forKey: "id")!)"

//                                let itemID = "\(dict.value(forKey: "id")!)"
//                                let itemQty = "\(dict.value(forKey: "quantity")!)"
//                                let itemDict = dict.value(forKey: "product_price") as! NSDictionary
                                
//                                let price = "\(itemDict.value(forKey: "price")!)"
//                                totalAmount = totalAmount + Int(price)! * Int(itemQty)!
                                
//                                let item_ID_Quantity_Dict = NSMutableDictionary()
//                                item_ID_Quantity_Dict.setValue(itemID, forKey: "itemid")
//                                item_ID_Quantity_Dict.setValue(itemQty, forKey: "itemqty")
//                                item_ID_Quantity_Array.add(item_ID_Quantity_Dict)
                                
                                let cart_Id_Dict = NSMutableDictionary()
                                cart_Id_Dict.setValue(cartID, forKey: "cartid")
                                
                                cart_id_array.add(cart_Id_Dict)
                                
                                fbSubParam.append([
                                    "item_id": cartID,
                                    "item_name": "\(dict.value(forKey: "model_name")!)",
                                    "price": "\(dict.value(forKey: "subtotal")!)",
                                    "quantity": "\(dict.value(forKey: "quantity")!)"
                                ])
                            }
                            
                            print(cart_id_array)
                            // print(itemQuantityArray)
                            
                            let totalAmount = "\(cartDict?["cart_total"] ?? "")"
                            let amtTotal = Double(totalAmount)
                            
                            self.lblTotalAmount.text = "\(self.currencySymbol ?? "")\(String(format: "%.2f", amtTotal!))"
                                                        
                            print(self.cartInfoMainArray)
                            
                            self.tblCartInfo.reloadData()
                            self.tblCartInfo.scroll(to: .top, animated: false)
                            
                            let params = [
                                "total_items": fbSubParam.count,
                                "price": Double(totalAmount) ?? 0.0,
                                "currency" : "\(self.currencySymbol ?? "")"
                            ] as [String : Any]
                            
                            AppEvents.logEvent(AppEvents.Name(rawValue: "view_cart"), valueToSum: Double(totalAmount) ?? 0.0, parameters: params)
                        }

                    }else
                    {
                        self.tblCartInfo.isHidden = true
                        self.bottomShadowView.isHidden = true
                        self.nodataLbl.isHidden = false
                        self.nodataLbl.text = "No items available"
                        
                        if let tabItems = self.tabBarController?.tabBar.items {
                            // In this case we want to modify the badge number of the third tab:
                            let tabItem = tabItems[2]
                            tabItem.badgeValue = nil
                        }
                    }
                    
                    // Set badge icon for Cart
                    appDelegate.SetBadgeIcon()
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Get_Cart_Info_API(isLoader: true)
                        }
                    })
                }
                self.RemoveLoader()
            }
        }
        else
        {
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Get_Cart_Info_API(isLoader: true)
                }
                
            })
        }
    }
    
    
    func Delete_Record_API() {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["delete": "Delete",
                             "id": itemID,
                             "user_id": userDefault.value(forKey: "USER_ID")!] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)delete", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.presentAlertWithTitle(title: "Delete", message: "Item deleted successfully.", options: "OK", completion: { (ButtonIndex) in
                     
                            self.Get_Cart_Info_API(isLoader: true)
                        })
                        
                    }else{
                        self.presentAlertWithTitle(title: "Failed To Delete", message: "\(responceDict["ResponseMessage"]!)", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Delete_Record_API()
                        }
                    })
                }
                
            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Delete_Record_API()
                }
            })
        }
    }
    
    func Update_Record_API() {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["item_id": itemID,
                             "quantity": quantity] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)update_quantity", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
//                        let updateDict = responceDict["data"] as! NSDictionary
                        
//                        let totalAmount = updateDict.value(forKey: "cart_total") as? Int ?? 0
//                        let amtTotal = Double(totalAmount)
//
//                        self.lblTotalAmount.text = "\(self.currencySymbol ?? "") \(String(format: "%.2f", amtTotal))"
                        
                        self.Get_Cart_Info_API(isLoader: false)
                        
                        self.view.makeToast("Quantity Updated Successfully", duration: 2.0, position: .bottom)
                    }
                    else
                    {
                        self.RemoveLoader()
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.RemoveLoader()

                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Update_Record_API()
                        }
                    })
                }
            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Update_Record_API()
                }
            })
        }
    }
    
    func Check_Stock_API() {
        
        let cartIDs = cart_id_array.value(forKeyPath: "cartid") as! NSArray
        let strCartIDs = cartIDs.componentsJoined(by: ",")
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["cart_ids": strCartIDs] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)check_stock", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]

                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        let stockDict = responceDict["data"] as? [String:Any]
                        
                        let status = "\(stockDict?["status"] ?? "")"
                        
                        if status == "1"
                        {
                            let numberOfQuantity = self.cartInfoMainArray.reduce(0) { ($1["quantity"] as! Int) + $0 }
                            self.cartInfoMainArray.removeAll()
                            self.tblCartInfo.reloadData()
                            
                            isFromSaveAddress = true
                            isDeliveryAddress = false
                      
                            let deliveryAddressVC = mainStoryBoard.instantiateViewController(withIdentifier: "DeliveryAddressViewController") as! DeliveryAddressViewController
                            deliveryAddressVC.numberOfQuantity = numberOfQuantity
                            self.pushViewController(deliveryAddressVC)
                            
                        }else{
                            self.view.makeToast("\(stockDict?["message"] ?? "")", duration: 2.0, position: .bottom)
                        }
                       
                    }else{
                        
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                      
                    })
                }
                
            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Check_Stock_API()
                }
            })
        }
    }
    
//    //MARK:- ********************** BANNER AD **********************
//
//    func set_banner()
//    {
//            self.bannerView.adUnitID = banner_ID
//            self.bannerView.rootViewController = self
//            let request = GADRequest()
//            request.testDevices = ["ce0c8d1e1aa2d1ebcf636d56c2aa6571","eb0e2074bacb6396cd12bd015b847075","06d24ef8a171e533169c992149f07817","d14f6feee5647c7beae207686b72c6d6","4fb88b739b81cca87ff7060574247a87","97f940130f7863cd056bbff53204f9f2","dfa3bf19497c465906a7b6b7ff782f4d","f4fc2c1165675652c987515f68292322","78af841c2188dbd6256ecd991434e7a2",kGADSimulatorID]
//
//            self.bannerView.delegate = self
//            self.bannerView.load(request)
//    }
   
}

extension CartViewController : GADAdLoaderDelegate, GADNativeAdDelegate,GADVideoControllerDelegate,GADNativeAdLoaderDelegate{
    
    func initNativeAds() {
        if let nibObjects = Bundle.main.loadNibNamed("UnifiedNativeAdView", owner: nil, options: nil),
           let adView = nibObjects.first as? GADNativeAdView {
           setAdView(adView)
           refreshAd(nil)
       }
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        print("error:",error)
        btnSignupVertical.constant = 0.0
    }
 
    
    // Mark: - GADNativeAdLoaderDelegate
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
      print("Received native ad: \(nativeAd)")
        btnSignupVertical.constant = -150.0
     // refreshAdButton.isEnabled = true
      // Create and place ad in view hierarchy.
      let nibView = Bundle.main.loadNibNamed("UnifiedNativeAdView", owner: nil, options: nil)?.first
      guard let nativeAdView = nibView as? GADNativeAdView else {
        return
      }
      setAdView(nativeAdView)

      // Set ourselves as the native ad delegate to be notified of native ad events.
      nativeAd.delegate = self

      // Populate the native ad view with the native ad assets.
      // The headline and mediaContent are guaranteed to be present in every native ad.
      (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
       nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
   
        
        let mediaContent = nativeAd.mediaContent
             if mediaContent.hasVideoContent {
               // By acting as the delegate to the GADVideoController, this ViewController receives messages
               // about events in the video lifecycle.
               mediaContent.videoController.delegate = self
                 print("Ad contains a video asset.")}

      // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
      // ratio of the media it displays.

      // These assets are not guaranteed to be present. Check that they are before
      // showing or hiding them.
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        
//        nativeAdView.bodylblView?.text = nativeAd.body
//        nativeAdView.bodylblView?.isHidden = nativeAd.body == nil
        
      (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
      nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil

      (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
      nativeAdView.iconView?.isHidden = nativeAd.icon == nil

      (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(
        from: nativeAd.starRating)
      nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil

      (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
      nativeAdView.storeView?.isHidden = nativeAd.store == nil

      (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
      nativeAdView.priceView?.isHidden = nativeAd.price == nil

      (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
      nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
    

      // In order for the SDK to process touch events properly, user interaction should be disabled.
      nativeAdView.callToActionView?.isUserInteractionEnabled = false

      // Associate the native ad view with the native ad object. This is
      // required to make the ad clickable.
      // Note: this should always be done after populating the ad views.
      nativeAdView.nativeAd = nativeAd
    }
    
    func setAdView(_ view: GADNativeAdView) {
      // Remove the previous ad view.
      nativeAdView = view
      nativeAdPlaceholder.addSubview(nativeAdView)
      nativeAdView.translatesAutoresizingMaskIntoConstraints = false

      // Layout constraints for positioning the native ad view to stretch the entire width and height
      // of the nativeAdPlaceholder.
      let viewDictionary = ["_nativeAdView": nativeAdView!]
      self.view.addConstraints(
        NSLayoutConstraint.constraints(
          withVisualFormat: "H:|[_nativeAdView]|",
          options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
      )
      self.view.addConstraints(
        NSLayoutConstraint.constraints(
          withVisualFormat: "V:|[_nativeAdView]|",
          options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
      )
    }
    
    func imageOfStars(from starRating: NSDecimalNumber?) -> UIImage? {
      guard let rating = starRating?.doubleValue else {
        return nil
      }
      if rating >= 5 {
        return UIImage(named: "stars_5")
      } else if rating >= 4.5 {
        return UIImage(named: "stars_4_5")
      } else if rating >= 4 {
        return UIImage(named: "stars_4")
      } else if rating >= 3.5 {
        return UIImage(named: "stars_3_5")
      } else {
        return UIImage(named: "stars_3_5")
      }
    }
    
    func refreshAd(_ sender: AnyObject!) {
        let videoOptions = GADVideoOptions()
        let multipleAdsOptions = GADMultipleAdsAdLoaderOptions()
                multipleAdsOptions.numberOfAds = 1
      adLoader = GADAdLoader(
        adUnitID: Native_Ad_ID, rootViewController: self,
        adTypes: [GADAdLoaderAdType.native], options:[multipleAdsOptions])
      adLoader.delegate = self
      adLoader.load(GADRequest())
    }
    
    // GADVideoControllerDelegate methods
     func videoControllerDidPlayVideo(_ videoController: GADVideoController) {
       // Implement this method to receive a notification when the video controller
       // begins playing the ad.
     }

     func videoControllerDidPauseVideo(_ videoController: GADVideoController) {
       // Implement this method to receive a notification when the video controller
       // pauses the ad.
     }

     func videoControllerDidEndVideoPlayback(_ videoController: GADVideoController) {
       // Implement this method to receive a notification when the video controller
       // stops playing the ad.
     }

     func videoControllerDidMuteVideo(_ videoController: GADVideoController) {
       // Implement this method to receive a notification when the video controller
       // mutes the ad.
     }

     func videoControllerDidUnmuteVideo(_ videoController: GADVideoController) {
       // Implement this method to receive a notification when the video controller
       // unmutes the ad.
     }
}
