//
//  tblDeliveryAddress.swift
//  Print_Photo_App
//
//  Created by Prakash on 04/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class tblDeliveryAddress: UITableViewCell {

    //MARK:- ********************** OUTLATE DECLARATION **********************
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnRadio: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var constraintBtnDeleteWidth: NSLayoutConstraint!
    @IBOutlet weak var lblAlternateMobileNo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblName.sizeToFit()
        lblAddress.sizeToFit()
        lblAlternateMobileNo.sizeToFit()
        lblState.sizeToFit()
        lblCountry.sizeToFit()
        lblMobileNo.sizeToFit()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
