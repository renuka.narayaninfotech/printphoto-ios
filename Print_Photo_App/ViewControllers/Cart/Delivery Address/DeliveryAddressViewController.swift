//
//  DeliveryAddressViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 04/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class DeliveryAddressViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIGestureRecognizerDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var tblDeliveryAddress: UITableView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var constraintBtnContinueHeight: NSLayoutConstraint!
    @IBOutlet weak var btnContinueView: UIView!
    @IBOutlet weak var constraintsSearchBarHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintsBottomContinueButton: NSLayoutConstraint!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var ID : Int!
    var selectIndex = 0
    var pincode : String!
    var homeNo : String!
    var street : String!
    var landmark : String!
    var mobileNO : String!
    var country : String!
    var state : String!
    var city : String!
    var name : String!
    var city_id : Int!
    
    var addressDict = [String:Any]()
    var getKeyBoardHeight : CGFloat!
    
    var numberOfQuantity: Int = 0

    
    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    
        SetNavigationBarTitle(Startstring: "Delivery Address", EndString: "")
        Initialize()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true

        registerTableViewCell(cellName: "tblDeliveryAddress", to: tblDeliveryAddress)
        
            if isDeliveryAddress
            {
                // Push from MyAccount View
                self.btnContinue.isHidden = true
                self.btnContinueView.isHidden = true
                self.constraintBtnContinueHeight.constant = 0.0
                self.searchBar.isHidden = true
                constraintsSearchBarHeight.constant = 0.0
            }else
            {
                // Push from CartInfo View
                self.btnContinue.isHidden = AddressFilteredArr.count == 0 ? true : false
                self.btnContinueView.isHidden = false
                self.searchBar.isHidden = false
                constraintsSearchBarHeight.constant = 50.0

                if DeviceType.IS_IPAD
                {
                    self.constraintBtnContinueHeight.constant = 72.0
                }else
                {
                    self.constraintBtnContinueHeight.constant = 48.0
                }
            }
        
            self.view.layoutIfNeeded()
            self.view.setNeedsLayout()
        
            if deliveryAddressArray.count == 0
            {
                self.searchBar.text = ""

                DispatchQueue.main.async {
                    self.Get_Delivery_Address_API()
                }
            }else
            {
                if isReloadTable
                {
                    self.tblDeliveryAddress.isHidden = false
                    self.nodataLbl.isHidden = true
                    self.btnContinue.isHidden = false
                    self.searchBar.isHidden = false
                    self.btnContinueView.isHidden = false
                    
                    AddressFilteredArr = deliveryAddressArray
                    
                    let dict = AddressFilteredArr[selectIndex] as NSDictionary
                    let id = dict.value(forKey: "id") as? Int ?? 0
                    addressID = id
                    
                    self.searchBar.text = ""
                    self.tblDeliveryAddress.reloadData()
                    
                    print(AddressFilteredArr as NSArray)
                }
            }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)

    }
    
    override func viewDidAppear(_ animated: Bool) {
      
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {

        addNewButton()
        addBackButton()
        btnContinue.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        btnContinue.cornerRadius = 3.0
//        self.btnContinue.isHidden = true
//        btnContinueView.isHidden = true
        
        btnContinue.isExclusiveTouch = true
       
        // First time load Array and Tableview
        self.searchBar.text = ""
        AddressFilteredArr = deliveryAddressArray
        self.tblDeliveryAddress.reloadData()
        self.tblDeliveryAddress.scroll(to: .top, animated: false)
        
        //set observer to get height of keyboard

        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow),name: UIResponder.keyboardWillShowNotification,object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboardNotification(notif:)), name: UIResponder.keyboardWillHideNotification , object: nil)
        
    }
   
    //MARK:- **********************  BUTTON EVENT **********************

    @IBAction func press_btn_continue(_ sender: UIButton) {

        searchBar.resignFirstResponder()
        Create_Order_API()
        
//        let longPress = UILongPressGestureRecognizer(target: sender, action: #selector(handleLongPress(_ :)))
//        longPress.delegate = self
//        sender.addGestureRecognizer(longPress)
    
    }
    
//    @objc func handleLongPress(_ gestureReconizer: UILongPressGestureRecognizer) {
//        if gestureReconizer.state != UIGestureRecognizer.State.ended {
//            searchBar.resignFirstResponder()
//        }
//        else {
//            searchBar.resignFirstResponder()
//            Create_Order_API()
//        }
//    }
    
    //MARK:- **********************  KEY-BOARD EVENT **********************

//    @objc func keyboardWillShow(_ notification: Notification) {
//        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
//            let keyboardRectangle = keyboardFrame.cgRectValue
//            let keyboardHeight = keyboardRectangle.height
//
//            getKeyBoardHeight = keyboardHeight
//
//            constraintsBottomContinueButton.constant = keyboardHeight
//        }
//    }
//
    @objc func keyboardWillShow(_ notification: NSNotification) {
        
        if let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("keyboard height = \(keyboardRect.height)")
            
            DispatchQueue.main.async {
                //set bottom of segment view
                if #available(iOS 11.0, *) {
                    let window = UIApplication.shared.keyWindow
                    let bottomPadding = window?.safeAreaInsets.bottom

                    self.constraintsBottomContinueButton.constant = (keyboardRect.height - bottomPadding!)
                
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    

    @objc func hideKeyboardNotification(notif: NSNotification) -> Void {
        guard let userInfo = notif.userInfo else {return}
        
        if (userInfo["UIKeyboardFrameEndUserInfoKey"] as? CGRect) != nil {
            
            //set bottom of segment view
            constraintsBottomContinueButton.constant = 0.0
            
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************

    func addNewButton() {
        let newButton = UIButton(type: .custom)
        newButton.setImage(UIImage(named: "ic_plus"), for: .normal)
        newButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        newButton.addTarget(self, action: #selector(self.addNewAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: newButton)
    }
    
    @objc func addNewAction(_ sender: UIButton) {
        isFromSaveAddress = false
        isEditAddress = false
        
        let addressVC = mainStoryBoard.instantiateViewController(withIdentifier: "EditAddressViewController") as! EditAddressViewController
        pushViewController(addressVC)
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
        
        // It's not Reload tableview
        isReloadTable = false
        
    }
    
    @objc func pressButtonEditOrder(_ sender: UIButton?) {
    
        guard AddressFilteredArr.count > 0 else {
            return
        }
        
        isEditAddress = true

        let dict = AddressFilteredArr[(sender?.tag)!] as [String:Any]
        
        print(sender!.tag)
        print(dict)

        let addressVC = mainStoryBoard.instantiateViewController(withIdentifier: "EditAddressViewController") as! EditAddressViewController
        addressVC.editAddressDict = dict
        pushViewController(addressVC)
        
    }
    
    @objc func pressButtonDeleteOrder(_ sender: UIButton?) {
        
        guard AddressFilteredArr.count > 0 else {
            return
        }
        
        let dict = AddressFilteredArr[(sender?.tag)!] as NSDictionary
        ID = dict.value(forKey: "id") as? Int
        
        self.presentAlertWithTitle(title: "Delete", message: "Are you sure to delete this Address ?", options: "NO","YES") { (ButtonIndex) in
            if ButtonIndex == 0
            {
                self.dismissViewController()
            }else
            {
                self.Delete_Delivery_Address_API()
            }
        }
    }
    
    @objc func pressBtnRadio(_ sender: UIButton?) {
        selectIndex = sender?.tag ?? 0
        tblDeliveryAddress.reloadData()

    }
    
    //MARK:- ********************** TABLEVIEW  METHOD **********************

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AddressFilteredArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "tblDeliveryAddress", for: indexPath) as! tblDeliveryAddress
        
        cell.btnEdit.isExclusiveTouch = true
        cell.btnDelete.isExclusiveTouch = true
        
            if isDeliveryAddress
            {
                // Push from MyAccount View
                self.searchBar.isHidden = true
                cell.btnRadio.isHidden = true
            }else
            {
                // Push from CartInfo View
                self.searchBar.isHidden = false
                cell.btnRadio.isHidden = false
            }
        
        DispatchQueue.main.async {
            if AddressFilteredArr.count == 1
            {
                cell.constraintBtnDeleteWidth.constant = 2.0
            }else
            {
                cell.constraintBtnDeleteWidth.constant = cell.borderView.bounds.width/2
            }
        }
        
        cell.borderView.setBorder()
        cell.btnEdit.setBorder()
        cell.btnDelete.setBorder()
        
        let dict = AddressFilteredArr[indexPath.row] as NSDictionary
        cell.lblName.text = "\(dict.value(forKey: "receiver_name") ?? "")"
        cell.lblAddress.text = "\(dict.value(forKey: "address") ?? ""),\(dict.value(forKey: "address_1") ?? ""),\(dict.value(forKeyPath: "city.name") ?? ""),"
        cell.lblState.text = "\(dict.value(forKeyPath: "city.state.name") ?? ""),"
        cell.lblCountry.text = "\(dict.value(forKeyPath: "city.state.country.name") ?? "") - \(dict.value(forKey: "pincode") ?? "")"

        // Only visible state and city in india country
//        cell.lblAddress.text = userDefault.string(forKey: "RegionCode") == "IN" ? "\(dict.value(forKey: "address") ?? ""),\(dict.value(forKey: "address_1") ?? ""),\(dict.value(forKeyPath: "city.name") ?? "")," : "\(dict.value(forKey: "address") ?? ""),\(dict.value(forKey: "address_1") ?? ""),"
//        cell.lblState.text = userDefault.string(forKey: "RegionCode") == "IN" ? "\(dict.value(forKeyPath: "city.state.name") ?? "")," : ""
//        cell.lblCountry.text = userDefault.string(forKey: "RegionCode") == "IN" ? "\(dict.value(forKeyPath: "city.state.country.name") ?? "") - \(dict.value(forKey: "pincode") ?? "")" : "\(dict.value(forKeyPath: "country.name") ?? "") - \(dict.value(forKey: "pincode") ?? "")"
        
        let userDict = userDefault.value(forKey: "EditAccountDict") as! NSDictionary
        let contact = userDict["mobileno"] as? String ?? ""
        
        cell.lblMobileNo.text = "Mobile No. : \(contact)"
        cell.lblAlternateMobileNo.text = "Alternate Mobile No. : \(dict.value(forKey: "alternate_mobile") ?? "")"
        
        // For Edit record
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(self.pressButtonEditOrder(_:)), for: .touchUpInside)
        
        // For delete record
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(self.pressButtonDeleteOrder(_:)), for: .touchUpInside)
        
        // select Radio Button
        cell.btnRadio.tag = indexPath.row
        cell.btnRadio.addTarget(self, action: #selector(self.pressBtnRadio(_:)), for: .touchUpInside)
        
        let id = dict.value(forKey: "id") as? Int ?? 0

        if id == addressID
        {
            let img = UIImage(named: "ic_check")
            cell.btnRadio.setImage(img, for: .normal)
        }else
        {
            let img = UIImage(named: "ic_uncheck")
            cell.btnRadio.setImage(img, for: .normal)
        }
 
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let dict = AddressFilteredArr[indexPath.row] as NSDictionary
        let id = dict.value(forKey: "id") as? Int ?? 0
        addressID = id
        
//        selectIndex = addressID!
        
        self.btnContinue.isHidden = false
        self.btnContinueView.isHidden = false
        
        tblDeliveryAddress.reloadData()

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return  119.0
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Get_Delivery_Address_API() {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["user_id": userDefault.value(forKey: "USER_ID")!] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)addresses", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let AddressResponceDict = Responce as! [String:Any]
                    
                    if AddressResponceDict["success"] as! Int == 1
                    {
                        deliveryAddressArray = AddressResponceDict["data"] as! [[String:Any]]
                        AddressFilteredArr = deliveryAddressArray
                        
                        if (AddressFilteredArr.count == 0)
                        {
                            self.tblDeliveryAddress.isHidden = true
                            self.nodataLbl.isHidden = false
                            self.nodataLbl.text = "No address available"
                            self.btnContinue.isHidden = true
                            isEditAddress = false
                            self.searchBar.isHidden = true
                            self.btnContinueView.isHidden = true

                            if isFromSaveAddress == true
                            {
                                let addressVC = mainStoryBoard.instantiateViewController(withIdentifier: "EditAddressViewController") as! EditAddressViewController
                                self.pushViewController(addressVC)
                            }
                        }
                        else
                        {
                            let dict = AddressFilteredArr[0] as NSDictionary
                            let id = dict.value(forKey: "id") as? Int ?? 0
                            addressID = id
                    
                            self.tblDeliveryAddress.isHidden = false
                            self.nodataLbl.isHidden = true
                            self.btnContinue.isHidden = false
                            self.searchBar.isHidden = false
                            self.btnContinueView.isHidden = false

                        }

                        self.tblDeliveryAddress.reloadData()
                        self.tblDeliveryAddress.scroll(to: .top, animated: false)

                    }else{
                        self.presentAlertWithTitle(title: "\(AddressResponceDict["message"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
//                            self.Get_Delivery_Address_API()
                        }
                    })
                }
            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Get_Delivery_Address_API()
                }
                
            })
        }
    }
    
    
    func Delete_Delivery_Address_API() {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["id": ID] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)delete_address", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["success"] as! Int == 1
                    {
                        self.Get_Delivery_Address_API()
                        self.topMostViewController().view.makeToast("Address deleted successfully.", duration: 3.0, position: .bottom)
                        self.selectIndex = 0
                        
                    }else{
                        self.presentAlertWithTitle(title: "\(responceDict["message"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                    
                    self.tblDeliveryAddress.reloadData()
                    self.tblDeliveryAddress.scroll(to: .top, animated: false)

                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
//                            self.Delete_Delivery_Address_API()
                        }
                    })
                }
            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Delete_Delivery_Address_API()
                }
                
            })
        }
    }
    
    func Create_Order_API() {
        
        print(cart_id_array)
        let cartIDs = cart_id_array.value(forKeyPath: "cartid") as! NSArray
        let strCartIDs = cartIDs.componentsJoined(by: ",")
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            var addDict = [String:Any]()
            
            for i in 0..<AddressFilteredArr.count
            {
                if ((AddressFilteredArr[i])["id"] as? Int ?? 0) == addressID
                {
                    selectIndex = i
                    
                    addDict = AddressFilteredArr[i]
                    break
                }
                
            }
            
            let ParamDict = ["address_id" : addressID ,
                             "cart_ids" : strCartIDs] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.manager.requestSerializer.timeoutInterval = 300
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)order/create", parameters: ParamDict) { (APIResponce, Responce, error1) in //order/create
                
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        
                        let createOrderDict = responceDict["data"] as! [String:Any]
                        
                        let pincodeStatus = "\(createOrderDict["status"] ?? "")"
                        
                        if pincodeStatus == "0"
                        {
                            self.view.makeToast("\(createOrderDict["message"] ?? "")", duration: 2.0, position: .bottom)
                        }else
                        {
                            // It's not Reload tableview
                            isReloadTable = false
                            isPromocode = false
                            
                            let PlaceOrderVC = mainStoryBoard.instantiateViewController(withIdentifier: "PlaceOrderViewController") as! PlaceOrderViewController
                            PlaceOrderVC.orderPlaceDict = addDict
                            PlaceOrderVC.currencySymbol = "\(createOrderDict["currency_symbol"] ?? "")"
                            PlaceOrderVC.isFromDeliveryAddressDict = createOrderDict
                            PlaceOrderVC.numberOfQuantity = self.numberOfQuantity
                            self.pushViewController(PlaceOrderVC)
                        }
                        
//                        // COD payment Button Enable
//                        isCOD = false
//
//                        let PlaceOrderVC = mainStoryBoard.instantiateViewController(withIdentifier: "PlaceOrderViewController") as! PlaceOrderViewController
//                        PlaceOrderVC.orderPlaceDict = dict
//                        self.pushViewController(PlaceOrderVC)
//                    }
//                    else
//                    {
//                        // COD payment Button Desable
//                        isCOD = true
//
//                        let PlaceOrderVC = mainStoryBoard.instantiateViewController(withIdentifier: "PlaceOrderViewController") as! PlaceOrderViewController
//                        PlaceOrderVC.orderPlaceDict = dict
//                        self.pushViewController(PlaceOrderVC)
//
//                        PlaceOrderVC.view.makeToast("This order is not capable for Cash On Delivery payment", duration: 4.0, position: .bottom)
                    }else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
 
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
            
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    
    // MARK: - ********************** TOUCHES LIFECYCLE **********************
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.endEditing(true)
    }
    
    //MARK:- ********************** SEARCH METHODS **********************
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
 
        searchBar.resignFirstResponder()

    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //            FilteredArr = searchText.isEmpty ? mainArr : mainArr.filter { (item: Any) -> Bool in
        //                return ((item as! [String:Any])["company_name"] as! String).range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        //                } as! NSMutableArray
        
        
        var isThere: Bool = false

        AddressFilteredArr = searchText.isEmpty ? deliveryAddressArray : deliveryAddressArray.filter { team in
            
            print(team)
            return (team["receiver_name"] as! String).lowercased().contains(searchText.lowercased())
        }
        
        for dict in AddressFilteredArr
        {
            let id = (dict as NSDictionary)["id"] as? Int
            
            // Show and Hide continue button when selected Perticular selected index
            if addressID == id
            {
                isThere = true
            }
        }
        
        if (AddressFilteredArr.count <= 0)
        {
            self.tblDeliveryAddress.isHidden = true
            self.nodataLbl.isHidden = false
            self.nodataLbl.text = "Address not available"
        }
        else
        {
            self.tblDeliveryAddress.isHidden = false
            self.nodataLbl.isHidden = true
        }

        if isThere
        {
            self.btnContinue.isHidden = false
            self.btnContinueView.isHidden = false
        }else
        {
            self.btnContinue.isHidden = true
            self.btnContinueView.isHidden = true
        }
        
        self.tblDeliveryAddress.reloadData()
        self.tblDeliveryAddress.scroll(to: .top, animated: false)

    }
}
