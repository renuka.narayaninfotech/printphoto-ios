//
//  PlaceOrderViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 06/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
//import PaymentSDK
import Razorpay
//import SafariServices

class PlaceOrderViewController: BaseViewController,UITextFieldDelegate, RazorpayPaymentCompletionProtocolWithData, PGTransactionDelegate //,SFSafariViewControllerDelegate
    
{
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var deliveryBorderView: UIView!
    @IBOutlet weak var lblDeliveryToName: UILabel!
    @IBOutlet weak var lblHomeNo: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblAlternateMobile: UILabel!
    @IBOutlet weak var productDetailBorderView: UIView!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblDiscountAmount: UILabel!
    @IBOutlet weak var lblDileveryCharges: UILabel!
    @IBOutlet weak var lblPaybleAmount: UILabel!
    @IBOutlet weak var promocodeBorderView: UIView!
    @IBOutlet weak var txtPromocode: UITextField!
    @IBOutlet weak var paytmBorderVIew: UIView!
    @IBOutlet weak var codeBorderView: UIView!
    @IBOutlet weak var constraintPromocodeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintApplyCodeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnHaveAPromocode: UIButton!
    @IBOutlet weak var btnRemoveCode: UIButton!
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet weak var lblPromocode: UILabel!
    @IBOutlet weak var constraintBtnCODWidth: NSLayoutConstraint!
    @IBOutlet weak var btn_COD: UIButton!
    @IBOutlet weak var btnMakePayment: UIButton!
    //    @IBOutlet weak var constraintBtnMakePaymentWidth: NSLayoutConstraint!
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var constratintBtnMakePaymentLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var constraintsPaytmViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintsPaymentButtonViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imgPaytm: UIImageView!
    @IBOutlet weak var btnPaytm: UIButton!
    @IBOutlet weak var paypalBorderView: UIView!
    @IBOutlet weak var constraintsBtnPaypalWidth: NSLayoutConstraint!
    @IBOutlet weak var imagePaypal: UIImageView!
    @IBOutlet weak var constraintsBtnPaytmLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var giftWrapBorderView: UIView!
    @IBOutlet weak var btnGiftWrap: UIButton!
    @IBOutlet weak var lblAmountGift: UILabel!
    @IBOutlet weak var heightCODButton: NSLayoutConstraint!
    @IBOutlet weak var heightPaypalButtonView: NSLayoutConstraint!

    @IBOutlet weak var constGiftWrapHeight: NSLayoutConstraint!

    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var orderPlaceDict = [String:Any]()
    var isFromDeliveryAddressDict = [String:Any]()
    
    var isRemove = false
    var isApplyCode = false
    var OrderId : String!
    var userID : String!
    var paymentAmount : String!
    var paymentOrderID : String!
    var totalAmt : String!
    
    // For Used Paytm
    var txnController = PGTransactionViewController()
    var CheckSmStr = String()
    
    var discountAmt : String?
    var discountType : String?
    var discountID : String?
    
    //    var razorpay: Razorpay!
    
    var paymentTypeArray = NSArray()
    
    var isAvailableKeychain = false
    var transactionId : String?
    var shippingCharge : String?
    
    var razorPayDict = [AnyHashable : Any]()
    
    var bestOffer : String?
    var isAvailableOfferCode = false
    
    var currencySymbol : String?
    //    var isGift : String?
        
    //    var svc: SFSafariViewController!
    
    // For Used CCAvenue
    //    var initCont = InitialViewController()
    //    let controller:CCWebViewController = CCWebViewController()
    
    var numberOfQuantity: Int = 0
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Place Order", EndString: "")
        
        Initialize()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
        self.RemoveLoader()
        
        isOfferBtnBack = false
        
        //        CheckOut()
        
        // Get promocode from Offer Screen
        
        if isPromocode
        {
            //            Get_Data()
            
            lblPromocode.text = userDefault.string(forKey: "OfferCode")
            txtPromocode.text = userDefault.string(forKey: "OfferCode")

            lblDiscount.text = "- \(currencySymbol ?? "")\(String(format: "%.2f", (userDefault.double(forKey: "OfferCodeAmount"))))"
            
            let offerCodeDict = userDefault.value(forKey: "OfferCodeDict") as! NSDictionary
            
            self.lblTotalAmount.text = "\(currencySymbol ?? "")\(String(format: "%.2f", (offerCodeDict["order_total"] as? Double ?? 0.0)))"
            
            let disAmt = "\(String(format: "%.2f", (offerCodeDict["discount_amount"] as? Double ?? 0.0)))"
            
            if disAmt == "0.00"
            {
                self.lblDiscountAmount.text = "\(currencySymbol ?? "")\(disAmt)"
            }else
            {
                self.lblDiscountAmount.text = "- \(currencySymbol ?? "")\(disAmt)"
            }
            
            self.lblDileveryCharges.text = "+ \(currencySymbol ?? "")\(String(format: "%.2f", ((offerCodeDict["shipping_charge"] as? Double ?? 0.0) + ((userDefault.string(forKey: "RegionCode") == "SA") ? Double((numberOfQuantity - 1) * 4) : 0))) ))"
            self.lblPaybleAmount.text = "\(currencySymbol ?? "")\(String(format: "%.2f", (((offerCodeDict["net_payable"]) as? Double ?? 0.0) + ((userDefault.string(forKey: "RegionCode") == "SA") ? Double((numberOfQuantity - 1) * 4) : 0))))"
            
            self.paymentAmount = String(format: "%.2f", (((offerCodeDict["net_payable"]) as? Double ?? 0.0) + ((userDefault.string(forKey: "RegionCode") == "SA") ? Double((numberOfQuantity - 1) * 4) : 0)))
            
            //            self.paymentAmount = "1"
            
//            self.txtPromocode.text = ""
            
//            if userDefault.string(forKey: "RegionCode") == "IN"
//            {
                if let discount = offerCodeDict["discount_applied"] as? Int, discount > 0
                {
                    // APPLY CODE
                    
                    // Managed Constraints when Apply promocode
                    self.constraintApplyCodeViewHeight.constant = 0.0
                    if DeviceType.IS_IPAD
                    {
                        self.constraintPromocodeViewHeight.constant = 52.0
                    }else{
                        self.constraintPromocodeViewHeight.constant = 35.0
                    }
                    self.btnRemoveCode.isHidden = false
                    self.btnHaveAPromocode.isHidden = true
                    self.promocodeBorderView.isHidden = true
                }
                else{
                    // REMOVE CODE
                    
                    // Managed Constraints when remove promocode
                    self.constraintPromocodeViewHeight.constant = 0.0
                    if DeviceType.IS_IPAD
                    {
                        self.constraintApplyCodeViewHeight.constant = 117.0
                    }else{
                        self.constraintApplyCodeViewHeight.constant = 78.0
                    }
                    self.btnRemoveCode.isHidden = true
                    self.btnHaveAPromocode.isHidden = false
                    self.promocodeBorderView.isHidden = false
                }
                
                isPromocode = false
//            }
            
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.RemoveLoader()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //        self.RemoveLoader()
    }
    
    //MARK:- ********************** Razorpay **********************
    
    func showRazorpayPopup()
    {
        let razorpay = RazorpayCheckout.initWithKey("rzp_live_UMRhd1BbuPUIjV", andDelegateWithData: self)
        
        let userDict = userDefault.value(forKey: "EditAccountDict") as! NSDictionary
        let email = userDict["email"] as? String ?? ""
        let contact = userDict["mobileno"] as? String ?? ""
        
        let dictFill = ["email" : email, "contact" : contact]
        
        let options : [AnyHashable: Any] = [
            "name" : "Print Photo",
            "description" : self.transactionId ?? "",
            "order_id" : self.transactionId ?? "",
            "currency" : "INR",
            "amount" : paymentAmount + "00",
            "prefill" : dictFill
        ]
        
        razorpay.open(options, displayController: self)
    }
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        
        print("Success response : ", response ?? "error")
        
        if let dict = response
        {
            //            razorPayDict = dict
            
            let paymentSuccessVC = mainStoryBoard.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
            paymentSuccessVC.razorPayDict = dict as? [String : Any] ?? [String : Any]()
            paymentSuccessVC.isFromOrderID = self.OrderId
            paymentSuccessVC.paymentType = "Razorpay"
            self.pushViewController(paymentSuccessVC)
            
            //            Set_Final_Status(payType: "Razorpay")
        }
        
    }
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        
        print("Error response : ", response ?? "error")
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        OrderId = "\(self.isFromDeliveryAddressDict["order_id"] ?? "")"
        
        btnPaytm.isExclusiveTouch = true
        btn_COD.isExclusiveTouch = true
        btnMakePayment.isExclusiveTouch = true
        
        userDefault.removeObject(forKey: "OfferCode")
        userDefault.removeObject(forKey: "OfferCodeAmount")
        userDefault.removeObject(forKey: "OfferCodeDict")
        
        // Keychain dialogue show or not
        let keychainExistDict = self.isFromDeliveryAddressDict["extra"] as? [String:Any]
        let keychainAvailable = "\(keychainExistDict?["keychain_exist"] ?? 0)"
        
        if keychainAvailable == "1"
        {
            isAvailableKeychain = true
        }else
        {
            isAvailableKeychain = false
        }
        
        // First time get data from cart view
        Get_Data()
        
        // Get Payment Type Array
        paymentTypeArray = self.isFromDeliveryAddressDict["enabled_payment_gateways"] as! NSArray
        print(paymentTypeArray)
        
        DispatchQueue.main.async{
         
            // Paypal button Enable or Disable
            
            if self.paymentTypeArray.contains("Paypal") && self.paymentTypeArray.contains("Paytm")
            {
                // Paypal button Enable
                self.imagePaypal.isHidden = false
                self.paypalBorderView.isHidden = false

                // Paytm button Enable
                self.imgPaytm.isHidden = false
                self.paytmBorderVIew.isHidden = false
                
                if DeviceType.IS_IPAD
                {
                    self.constraintsBtnPaypalWidth.constant = self.paymentView.bounds.width/2 - 6
                    self.constraintsBtnPaytmLeftMargin.constant = 12.0
                }else{
                    self.constraintsBtnPaypalWidth.constant = self.paymentView.bounds.width/2 - 4
                    self.constraintsBtnPaytmLeftMargin.constant = 8.0
                }

            }else
            {
                
                if self.paymentTypeArray.contains("Paypal")
                {
                    // Paypal button Enable
                    self.imagePaypal.isHidden = false
                    self.paypalBorderView.isHidden = false
                    
                    self.constraintsBtnPaypalWidth.constant = self.paymentView.bounds.width
                    
                }else
                {
                    // Paypal button Desable
                    
                    self.imagePaypal.isHidden = true
                    self.paypalBorderView.isHidden = true
                    
//                    self.constraintsBtnPaypalWidth.constant = 0.0
                    self.constraintsBtnPaytmLeftMargin.constant = 0.0
                }
                
                if self.paymentTypeArray.contains("Paytm")
                {
                    
                    self.constraintsBtnPaypalWidth.constant = self.paymentView.bounds.width
                    
                    // Paytm button Enable
                    self.imgPaytm.isHidden = false
                    self.paytmBorderVIew.isHidden = false

                }else
                {
                    // Paytm button Desable
                    
                    self.imgPaytm.isHidden = true
                    self.paytmBorderVIew.isHidden = true
                    
                    self.constraintsBtnPaypalWidth.constant = 0.0
                    self.constraintsBtnPaytmLeftMargin.constant = 0.0
                }
              
            }

            self.paypalBorderView.layoutIfNeeded()
            self.paypalBorderView.setNeedsLayout()

           
            let codButtonStatus = "\(self.isFromDeliveryAddressDict["display_cod_button"] ?? "")"
        
            /*
            if self.paymentTypeArray.contains("Razorpay") && codButtonStatus == "1"
            {
                if DeviceType.IS_IPAD
                {
                    self.constraintBtnCODWidth.constant = self.paymentView.bounds.width/2 - 6
                    self.constratintBtnMakePaymentLeftMargin.constant = 12.0
                }else{
                    self.constraintBtnCODWidth.constant = self.paymentView.bounds.width/2 - 4
                    self.constratintBtnMakePaymentLeftMargin.constant = 8.0
                }
            }
            else
            {
                if self.paymentTypeArray.contains("Razorpay")
                {
                    self.constraintBtnCODWidth.constant = 0.0
                    self.constratintBtnMakePaymentLeftMargin.constant = 0.0
                }
                else if codButtonStatus == "1"
                {
                    self.constraintBtnCODWidth.constant = self.paymentView.bounds.width
                    self.constratintBtnMakePaymentLeftMargin.constant = 0.0
                }
            }
            */
            //Hide COD button
            self.constraintBtnCODWidth.constant = 0.0
            self.constratintBtnMakePaymentLeftMargin.constant = 0.0
            
            self.paytmBorderVIew.layoutIfNeeded()
            self.paytmBorderVIew.setNeedsLayout()

//            if userDefault.string(forKey: "RegionCode") == "IN"
//            {
                self.constraintPromocodeViewHeight.constant = 0.0
                if DeviceType.IS_IPAD
                {
                    self.constraintApplyCodeViewHeight.constant = 117.0
                }else{
                    self.constraintApplyCodeViewHeight.constant = 78.0
                }
                
                self.btnRemoveCode.isHidden = true
                self.btnHaveAPromocode.isHidden = false
                self.promocodeBorderView.isHidden = false
                
//            }else
//            {
//                self.constraintApplyCodeViewHeight.constant = 0.0
//                self.constraintPromocodeViewHeight.constant = 0.0
//                self.btnRemoveCode.isHidden = true
//                self.btnHaveAPromocode.isHidden = true
//                self.promocodeBorderView.isHidden = true
//
//            }
            
            // When Razorpay and COD Button are desable
            
            if self.paymentTypeArray.contains("Razorpay") || codButtonStatus == "1"
            {
                self.heightCODButton.constant = DeviceType.IS_IPAD ? 75.0 : 50.0
            }else
            {
                self.heightCODButton.constant = 0
            }
            
            // When Paypal and Paytm are desable
            
            if self.paymentTypeArray.contains("Paytm") || self.paymentTypeArray.contains("Paypal")
            {
                self.constraintsPaytmViewHeight.constant = DeviceType.IS_IPAD ? 75.0 : 50.0
                self.heightPaypalButtonView.constant = DeviceType.IS_IPAD ? 75.0 : 50.0
            }else
            {
                self.constraintsPaytmViewHeight.constant = 0
                self.heightPaypalButtonView.constant = 0
            }
            
            // Managed Main payment View height
            if (self.paymentTypeArray.contains("Razorpay") || codButtonStatus == "1") && (self.paymentTypeArray.contains("Paytm") || self.paymentTypeArray.contains("Paypal"))
            {
                self.constraintsPaymentButtonViewHeight.constant = DeviceType.IS_IPAD ? 162.0 : 108.0
            }
            else
            {
                self.constraintsPaymentButtonViewHeight.constant = DeviceType.IS_IPAD ? 87.0 : 58.0
            }
            
            self.btn_COD.layoutIfNeeded()
            self.btn_COD.setNeedsLayout()
            
            self.paymentView.setNeedsLayout()
            self.paymentView.layoutIfNeeded()
            
        }
        
        //        }
        
        setViewBorder()
        addBackButton()
        removeBackButton()
        
        EditDict = userDefault.value(forKey: "EditAccountDict")! as! [String : Any]
        print(EditDict)
        
        txtPromocode.returnKeyType = .done
        txtPromocode.text = ""
        
        hideKeyboardWhenTappedAround()
        
    }
    
    //MARK:- ********************** OTHER FUNCTION**********************
    
    func Get_Data() {
        
        userID = "\(userDefault.value(forKey: "USER_ID") ?? "")"
        
        lblDeliveryToName.text = "Delivery To : \(orderPlaceDict["receiver_name"] ?? "")"
        lblHomeNo.text = "\(orderPlaceDict["address"] ?? ""),\(orderPlaceDict["address_1"] ?? ""),\((orderPlaceDict["city"] as! NSDictionary)["name"] as? String ?? ""),"
        lblState.text = "\(((orderPlaceDict["city"] as! NSDictionary)["state"] as! NSDictionary)["name"] as? String ?? ""),"
        lblCountry.text = "\((((orderPlaceDict["city"] as! NSDictionary)["state"] as! NSDictionary)["country"] as! NSDictionary)["name"] as? String ?? "") - \(orderPlaceDict["pincode"] ?? "")"
        
//        lblHomeNo.text = userDefault.string(forKey: "RegionCode") == "IN" ? "\(orderPlaceDict["address"] ?? ""),\(orderPlaceDict["address_1"] ?? ""),\((orderPlaceDict["city"] as! NSDictionary)["name"] as? String ?? "")," : "\(orderPlaceDict["address"] ?? ""),\(orderPlaceDict["address_1"] ?? ""),"
//        lblState.text = userDefault.string(forKey: "RegionCode") == "IN" ? "\(((orderPlaceDict["city"] as! NSDictionary)["state"] as! NSDictionary)["name"] as? String ?? "")," : ""
//        lblCountry.text = userDefault.string(forKey: "RegionCode") == "IN" ? "\((((orderPlaceDict["city"] as! NSDictionary)["state"] as! NSDictionary)["country"] as! NSDictionary)["name"] as? String ?? "") - \(orderPlaceDict["pincode"] ?? "")" : "\((orderPlaceDict["country"] as! NSDictionary)["name"] as? String ?? "") - \(orderPlaceDict["pincode"] ?? "")"
        
        let userDict = userDefault.value(forKey: "EditAccountDict") as! NSDictionary
        let contact = userDict["mobileno"] as? String ?? ""
        
        lblMobileNo.text = "Mobile No. : \(contact)"
        lblAlternateMobile.text = "Alternate Mobile No. : \(orderPlaceDict["alternate_mobile"] ?? "")"
        
        let orderDetalisDict = self.isFromDeliveryAddressDict["order_details"] as? [String:Any]
        
        self.lblTotalAmount.text = "\(currencySymbol ?? "")\(String(format: "%.2f", (orderDetalisDict?["order_total"] as? Double ?? 0.0)))"
        
        lblAmountGift.text = "\(currencySymbol ?? "")\(String(format: "%.2f", (self.isFromDeliveryAddressDict["display_gift_charge"] as? Double ?? 0.0)))"
        
        let orderDisAmt = "\(String(format: "%.2f", (orderDetalisDict?["discount_amount"] as? Double ?? 0.0)))"
        
        if orderDisAmt == "0.00"
        {
            self.lblDiscountAmount.text = "\(currencySymbol ?? "")\(orderDisAmt)"
        }else
        {
            self.lblDiscountAmount.text = "- \(currencySymbol ?? "")\(orderDisAmt)"
        }
        
        shippingCharge = "\(String(format: "%.2f", ((orderDetalisDict?["shipping_charge"] as? Double ?? 0.0) + ((userDefault.string(forKey: "RegionCode") == "SA") ? Double((numberOfQuantity - 1) * 4) : 0)) ))"
        self.lblDileveryCharges.text = "+ \(currencySymbol ?? "")\(shippingCharge ?? "")"
        self.lblPaybleAmount.text = "\(currencySymbol ?? "")\(String(format: "%.2f", ((orderDetalisDict?["net_payable"] as? Double ?? 0.0) + ((userDefault.string(forKey: "RegionCode") == "SA") ? Double((numberOfQuantity - 1) * 4) : 0))))"
        paymentAmount = String(format: "%.2f", ((orderDetalisDict?["net_payable"] as? Double ?? 0.0) + ((userDefault.string(forKey: "RegionCode") == "SA") ? Double((numberOfQuantity - 1) * 4) : 0)))
        
        //        paymentAmount = "1"
        
    }
    
    //MARK:- **********************  BUTTON EVENT **********************
    @IBAction func press_btn_Apply(_ sender: UIButton) {
        
//        isRemove = false
//        isApplyCode = true
//
//        if isApplyPromocode()
//        {
//            userDefault.set((txtPromocode.text ?? ""), forKey: "OfferCode")
//            Apply_Remove_Promocode(isCODOfferCode: false, isGiftApplied: btnGiftWrap.isSelected)
//        }

        isBackFromOffer = false
        isRemove = false
        isApplyCode = true
        
        if isApplyPromocode()
        {
            Apply_Remove_Promocode(isCODOfferCode: false)
        }
        
    }
    
    @IBAction func press_btn_giftwrap(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork() {
        
            // Set checkbox image in Main Storyboard
            sender.isSelected = sender.isSelected ? false : true
            isGift = sender.isSelected
            Apply_Remove_Promocode(isCODOfferCode: isPromocode)
        }
        else
        {

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
        
    }
    
    @IBAction func press_btn_Have_a_promocode(_ sender: UIButton) {
        
        isRemove = false
        isPromocode = true
        isApplyCode = false
        isOfferBtnBack = true
        isIconCart = false
        
        let OffersVC = mainStoryBoard.instantiateViewController(withIdentifier: "OffersViewController") as! OffersViewController
        OffersVC.orderID = OrderId
        pushViewController(OffersVC)
        
    }
    
    @IBAction func press_btn_paypal(_ sender: UIButton) {
        if isAvailableKeychain
        {
            self.presentAlertWithTitle(title: "Buy One Get One Keychain", message: "Buy 1 get 1 keychain free by applying KEYCHAIN promocode!!", options: "OK", completion: { (ButtonIndex) in
                
                self.isAvailableKeychain = false
                
            })
            
        }else
        {
            //            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            Initiate_Transaction(transaction_type: "Paypal")
        }
    }
    
    @IBAction func press_btn_paytm(_ sender: UIButton) {
        
        if isAvailableKeychain
        {
            self.presentAlertWithTitle(title: "Buy One Get One Keychain", message: "Buy 1 get 1 keychain free by applying KEYCHAIN promocode!!", options: "OK", completion: { (ButtonIndex) in
                
                self.isAvailableKeychain = false
                
            })
            
        }else
        {
            Initiate_Transaction(transaction_type: "Paytm")
        }
    }
    
    @IBAction func press_btn_cash_on_delivery(_ sender: UIButton) {
        
        let extraDict = isFromDeliveryAddressDict["extra"] as? [String:Any]
        
        bestOffer = "\(extraDict?["best_offer"] as? String ?? "")"
        
        if !isAvailableOfferCode && bestOffer != ""
        {
            isAvailableOfferCode = true
            isRemove = false
            isApplyCode = true
            isBackFromOffer = false
            
            self.presentAlertWithTitle(title: "Prepaid Best Offer", message: "\(extraDict?["best_offer_description"] as? String ?? "")", options: "DISMISS","APPLY") { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.dismissViewController()
                }else
                {
//                    userDefault.removeObject(forKey: "OfferCode")
//                    self.Apply_Remove_Promocode(isCODOfferCode: true, isGiftApplied: self.btnGiftWrap.isSelected)
                    
                    self.txtPromocode.text = self.bestOffer
                    self.Apply_Remove_Promocode(isCODOfferCode: true)

                }
            }
            
        }else
        {
            if isAvailableKeychain
            {
                self.presentAlertWithTitle(title: "Buy One Get One Keychain", message: "Buy 1 get 1 keychain free by applying KEYCHAIN promocode!!", options: "OK", completion: { (ButtonIndex) in
                    
                    self.isAvailableKeychain = false
                    
                })
                
            }else
            {
                let codAvailable = "\(isFromDeliveryAddressDict["cod_available"] ?? "")"
                
                if codAvailable == "0"
                {
                    self.presentAlertWithTitle(title: "Alert", message: "\(isFromDeliveryAddressDict["cod_message"] ?? "")", options: "OK", completion: { (ButtonIndex) in
                    })
                    
                }
                else
                {
                    // Check Promocode textfield is blank or not
                    // if there is a promocode in COD then show alert remove promocode
                    if lblPromocode.text != ""
                    {
                        self.presentAlertWithTitle(title: "Terms & Conditions", message: "Promocode and offers are not applicable on Cash on Delivery.", options: "OK", completion: { (ButtonIndex) in
                            self.isRemove = true
//                            self.Apply_Remove_Promocode(isCODOfferCode: false, isGiftApplied: self.btnGiftWrap.isSelected)
                            
                            self.txtPromocode.text = ""
                            self.Apply_Remove_Promocode(isCODOfferCode: false)

                        })
                    }else
                    {
                        self.presentAlertWithTitle(title: "Place Order", message: "Are you sure you want to purchase this product using cash on delivery?", options: "NO","YES") { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.dismissViewController()
                            }else
                            {
                                // Call place order API for COD
                                self.Initiate_Transaction(transaction_type: "COD")
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    @IBAction func press_btn_make_payment(_ sender: UIButton) {
        
        if isAvailableKeychain
        {
            self.presentAlertWithTitle(title: "Buy One Get One Keychain", message: "Buy 1 get 1 keychain free by applying KEYCHAIN promocode!!", options: "OK", completion: { (ButtonIndex) in
                
                self.isAvailableKeychain = false
                
            })
            
        }else
        {
            var paymentType : String?
            
            if paymentTypeArray.contains("CCAvenueGateway")
            {
                paymentType = "CCAvenueGateway"
                
            }else if paymentTypeArray.contains("Razorpay")
            {
                paymentType = "Razorpay"
                
            }
            
            self.Initiate_Transaction(transaction_type: paymentType ?? "")
            
            //            CCAvenue_Intigration()
        }
    }
    
    @IBAction func press_btn_remove_code(_ sender: UIButton) {
        
//        isRemove = true
//        Apply_Remove_Promocode(isCODOfferCode: false, isGiftApplied: btnGiftWrap.isSelected)
        
        txtPromocode.text = ""
        isRemove = true
        Apply_Remove_Promocode(isCODOfferCode: false)
    }
    
    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
        isGift = false
        
        self.popViewController()
        
        //        self.presentAlertWithTitle(title: "Alert", message: "Are you sure you want to cancel the order?", options: "NO","YES") { (ButtonIndex) in
        //            if ButtonIndex == 0
        //            {
        //                self.dismissViewController()
        //            }else
        //            {
        //                self.Set_Final_Status(payType: "Cancel")
        //            }
        //        }
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** UIVIEW BORDER **********************
    
    func setViewBorder() {
        deliveryBorderView.setBorder()
        productDetailBorderView.setBorder()
        promocodeBorderView.setBorder()
        codeBorderView.setBorder()
        if(userDefault.string(forKey: "RegionCode") == "SA")
        {
            constGiftWrapHeight.constant = 0
            giftWrapBorderView.isHidden = true
        } else {
            giftWrapBorderView.setBorder()
        }
        
        paytmBorderVIew.layer.borderColor = UIColor.black.cgColor
        //        paytmBorderVIew.layer.borderWidth = 2.0
        paypalBorderView.layer.borderColor = UIColor.black.cgColor
        
    }
    
    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        
        textField.text = (textField.text! as NSString).replacingCharacters(in: range, with: string.uppercased())
        
        return false
    }
    
    //MARK:- ********************** VALIDATION **********************
    
    func isApplyPromocode()-> Bool {
        
        if Validation.isStringEmpty(txtPromocode.text!) || txtPromocode.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter promocode", message: "", options: "OK") { (Int) in
            }
        }
        else{
            
            return true
        }
        return false
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Initiate_Transaction(transaction_type:String) {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            var ParamDict: NSDictionary!
            
            if(userDefault.string(forKey: "RegionCode") == "SA")
            {
                ParamDict = ["order_id" : OrderId,
                                 "transaction_type" : transaction_type,
                                 "device_type" : "ios",
                                 "additional_shipping_charge": Double((numberOfQuantity - 1) * 4)
                    ] as NSDictionary
            } else {
                ParamDict = ["order_id" : OrderId,
                             "transaction_type" : transaction_type,
                             "device_type" : "ios"
                    ] as NSDictionary
            }
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)order/initiate_transaction", parameters: ParamDict) { (APIResponce, Responce, error1) in // order/initiate_transaction
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        let transactionDict = responceDict["data"] as? [String:Any]
                        
                        self.transactionId = "\(transactionDict?["transaction_id"] ?? "")"
                        
                        if transaction_type == "Paypal"
                        {
                            let status = transactionDict!["status"] as? Int ?? 0
                            
                            if status == 1
                            {
                                let url = transactionDict!["paypal_redirect_url"] as? String ?? ""

                                //                            DispatchQueue.main.async {
                                let PaypalVC = mainStoryBoard.instantiateViewController(withIdentifier: "PaypalViewController") as! PaypalViewController
                                PaypalVC.url = url
                                self.pushViewController(PaypalVC)
                                //                            }
                            }else{
                                self.RemoveLoader()
                                self.presentAlertWithTitle(title: "\(transactionDict?["message"]! ?? "")", message: "", options: "OK", completion: { (ButtonIndex) in
                                })
                            }
                           
                        }else if transaction_type == "Paytm"
                        {
                            self.Generate_Paytm_CheckSum()
                        }else if transaction_type == "COD"
                        {
                            self.dismissViewController()
                         
                            if isGift
                            {
                                var statusCode : Int = 1
                                
                                if let dict = responceDict["data"] as? NSDictionary
                                {
                                    if let status = dict["status"] as? Int
                                    {
                                        statusCode = status
                                    }
                                }
                                
                                if statusCode == 1
                                {
                                    let paymentSuccessVC = mainStoryBoard.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
                                    paymentSuccessVC.isFromOrderID = self.OrderId
                                    paymentSuccessVC.paymentType = transaction_type
                                    self.pushViewController(paymentSuccessVC)
                                }
                                else
                                {
                                    self.RemoveLoader()
                                    if let dict = responceDict["data"] as? NSDictionary
                                    {
                                        if let message = dict["message"] as? String
                                        {
                                            self.presentAlertWithTitle(title: message, message: "", options: "OK", completion: { (ButtonIndex) in
                                            })
                                        }
                                    }
                                }
                            }else
                            {
                                let paymentSuccessVC = mainStoryBoard.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
                                paymentSuccessVC.isFromOrderID = self.OrderId
                                paymentSuccessVC.paymentType = transaction_type
                                self.pushViewController(paymentSuccessVC)
                            }
                            
                        //   self.Set_Final_Status(payType: "COD")
                        }else if transaction_type == "CCAvenueGateway"
                        {
                            self.CCAvenue_Intigration()
                            
                        }else if transaction_type == "Razorpay"
                        {
                            self.showRazorpayPopup()
                        }
                        
                    }
                    else
                    {
                        self.RemoveLoader()
                        
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.RemoveLoader()
                    
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
                
                //  self.RemoveLoader()
                
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    func Set_Final_Status(payType: String) {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            var cancelOrder : String?
            var razorPay_OrderID : String?
            var razorPay_PaymentId : String?
            var razorPay_Signature : String?
            
            if payType == "Paytm" || payType == "COD"
            {
                cancelOrder = "0"
                razorPay_OrderID = ""
                razorPay_PaymentId = ""
                razorPay_Signature = ""
            }else if payType == "Razorpay"
            {
                cancelOrder = "0"
                razorPay_OrderID = razorPayDict["razorpay_order_id"] as? String ?? ""
                razorPay_PaymentId = razorPayDict["razorpay_payment_id"] as? String ?? ""
                razorPay_Signature = razorPayDict["razorpay_signature"] as? String ?? ""
            }else
            {
                cancelOrder = "1"
                razorPay_OrderID = ""
                razorPay_PaymentId = ""
                razorPay_Signature = ""
            }
            
            let ParamDict = ["order_id" : OrderId,
                             "cancel_order" : cancelOrder ?? "",
                             "razorpay_order_id" : razorPay_OrderID ?? "",
                             "razorpay_payment_id" : razorPay_PaymentId ?? "",
                             "razorpay_signature" : razorPay_Signature ?? "",
                             "device_type": "ios",
                             "app_version": appVersion] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)order/set_final_status", parameters: ParamDict) { (APIResponce, Responce, error1) in //order/set_final_status
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        let dict  = responceDict["data"] as! NSDictionary
                        
                        let status = dict["status"] as? Int
                        //                        let message = "\(dict["message"] as? String ?? "")"
                        //                        let transactionType = "\(dict["transaction_type"] as? String ?? "")"
                        
                        //                        var title = ""
                        //
                        //                        if transactionType == "COD"
                        //                        {
                        //                            title = "Order Placed"
                        //                        }else
                        //                        {
                        //                            title = "Congratulations"
                        //                        }
                        
                        if status == 1
                        {
                            if payType == "Cancel"
                            {
                                self.popViewController()
                            }
                            
                        }
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    //    func Place_Order(transaction_type:String) {
    //
    //        print(item_ID_Quantity_Array)
    //        let arrIDs = item_ID_Quantity_Array.value(forKeyPath: "itemid") as! NSArray
    //        let strIDs = arrIDs.componentsJoined(by: ",")
    //        let arrQty = item_ID_Quantity_Array.value(forKeyPath: "itemqty") as! NSArray
    //        let strQty = arrQty.componentsJoined(by: ",")
    //
    //        //        DispatchQueue.main.async {
    //        self.totalAmt = self.lblTotalAmount.text?.digitsOnly() ?? ""
    //        //        }
    //
    //        if Reachability.isConnectedToNetwork() {
    //
    //            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
    //
    //            var sellerID : String?
    //            var offereID : String?
    //
    //            if transaction_type == "COD"
    //            {
    //                sellerID = ""
    //                offereID = ""
    //            }else
    //            {
    //                if discountType == "0"
    //                {
    //                    sellerID = ""
    //                    offereID = discountID ?? ""
    //                }else
    //                {
    //                    sellerID = discountID ?? ""
    //                    offereID = ""
    //                }
    //            }
    //
    //            let ParamDict = ["user_id" : userID,
    //                             "address_id" : "\(orderPlaceDict["id"] ?? "")",
    //                "transaction_type" : transaction_type,
    //                "transaction_id" : self.OrderId,
    //                "shipping" : "\(userDefault.string(forKey: "ShippingCharges") ?? "")",
    //                "item_id" : strIDs,
    //                "item_quantity" : strQty,
    //                "total_amount" : totalAmt,
    //                "paid_amount" : paymentAmount,
    //                "discount" : 0,
    //                "coupon" : 0,
    //                "seller_id" : sellerID ?? "",
    //                "offer_id" : offereID ?? "",
    //                "gift" : 0] as NSDictionary
    //
    //            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)place_order", parameters: ParamDict) { (APIResponce, Responce, error1) in
    //
    //                self.RemoveLoader()
    //
    //                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
    //                {
    //                    let responceDict = Responce as! [String:Any]
    //
    //                    if responceDict["ResponseCode"] as! String == "1"
    //                    {
    //                        item_ID_Quantity_Array.removeAllObjects()
    //
    //                        let dict  = responceDict["data"] as! NSDictionary
    //
    //                        let status = dict["status"] as? Int
    //                        let message = dict["order_status"] as? String
    //
    //                        var title = ""
    //                        var description = ""
    //
    //                        if transaction_type == "COD"
    //                        {
    //                            title = "Order Placed"
    //                            description = "Order place successfully"
    //                        }else
    //                        {
    //                            title = "Congratulations"
    //                            description = "Your transaction completed successfully"
    //                        }
    //
    //                        if status == 1
    //                        {
    //                            self.presentAlertWithTitle(title: title, message: description, options: "OK", completion: { (ButtonIndex) in
    //                                let paymentSuccessVC = mainStoryBoard.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
    //                                paymentSuccessVC.placeOrderDict = dict
    //                                self.pushViewController(paymentSuccessVC)
    //                            })
    //                        }else
    //                        {
    //                            self.presentAlertWithTitle(title: message ?? "", message: "", options: "OK", completion: { (ButtonIndex) in
    //                            })
    //                        }
    //                    }
    //                    else
    //                    {
    //                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
    //                        })
    //                    }
    //                }
    //                else
    //                {
    //                    self.presentAlertWithTitle(title: (error1?.localizedDescription)!, message: "", options: "OK", completion: { (ButtonIndex) in
    //                    })
    //                }
    //            }
    //        }
    //        else
    //        {
    //            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
    //            })
    //        }
    //    }
    
    func Apply_Remove_Promocode(isCODOfferCode:Bool) {

        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            var ParamDict = NSDictionary()
            
            // Apply Offer code and Enter Promocode
            
            if !isRemove
            {
                if isApplyCode || !isRemove
                {
                    if isCODOfferCode
                    {
                        // Apply COD Offer code
                        
                        ParamDict = ["order_id" : OrderId,
                                     "discount_code" : bestOffer ?? "",
                                    "gift" : isGift]
                        
                    }else
                    {
                        
                        // When Enter Promocode in Textfield
                        ParamDict = ["order_id" : OrderId,
                                     "discount_code" : txtPromocode.text ?? "",
                                    "gift" : isGift]
                    }
                    
                }else
                {
                    // When get promocode from offerscreen
                    
                    ParamDict = ["order_id" : OrderId,
                                 "discount_code" : "\(userDefault.string(forKey: "OfferCode") ?? "")",
                                "gift" : isGift]
                }
            }else
            {
                // Remove Promocode
                
                ParamDict = ["order_id" : OrderId,
                             "discount_code" : "",
                            "gift" : isGift]
                
            }
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)order/apply_discount", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        let dict = responceDict["data"] as! [String:Any]
                        
                        self.lblTotalAmount.text = "\(self.currencySymbol ?? "")\(String(format: "%.2f", (dict["order_total"] as? Double ?? 0.0)))"
                        
                        let dictDisAmt = "\(String(format: "%.2f", (dict["discount_amount"] as? Double ?? 0.0)))"
                        
                        if dictDisAmt == "0.00"
                        {
                            self.lblDiscountAmount.text = "\(self.currencySymbol ?? "")\(dictDisAmt)"
                        }else
                        {
                            self.lblDiscountAmount.text = "- \(self.currencySymbol ?? "")\(dictDisAmt)"
                        }
                        
                        self.lblDileveryCharges.text = "+ \(self.currencySymbol ?? "")\(String(format: "%.2f", ((dict["shipping_charge"] as? Double ?? 0.0) + ((userDefault.string(forKey: "RegionCode") == "SA") ? Double((self.numberOfQuantity - 1) * 4) : 0)) ))"
                        self.lblPaybleAmount.text = "\(self.currencySymbol ?? "")\(String(format: "%.2f", (((dict["net_payable"]) as? Double ?? 0.0) + ((userDefault.string(forKey: "RegionCode") == "SA") ? Double((self.numberOfQuantity - 1) * 4) : 0))))"
                        
                        self.lblDiscount.text = "- \(self.currencySymbol ?? "")\(String(format: "%.2f", (dict["discount_amount"] as? Double ?? 0.0)))"
                        
                        self.paymentAmount = String(format: "%.2f", (((dict["net_payable"]) as? Double ?? 0.0) + ((userDefault.string(forKey: "RegionCode") == "SA") ? Double((self.numberOfQuantity - 1) * 4) : 0)))
                        
                        //                        self.paymentAmount = "1"
                        
//                        if let discount = dict["discount_applied"] as? Int, discount > 0
//                        {
//                            if isCODOfferCode && self.isApplyCode
//                            {
//                                if userDefault.string(forKey: "OfferCode") != nil
//                                {
//                                    self.lblPromocode.text = "\(userDefault.string(forKey: "OfferCode") ?? "")"
//                                }
//                                else if self.bestOffer != nil
//                                {
//                                    // Used Best Offer when click on COD button on First time
//                                    self.lblPromocode.text = self.bestOffer
//                                }
//
//                            }
//                            else
//                            {
//                                self.lblPromocode.text = "\(userDefault.string(forKey: "OfferCode") ?? "")"
//                            }
//
//                            self.txtPromocode.text = ""
//
//                        }else
//                        {
//                            self.lblPromocode.text = ""
//                            userDefault.removeObject(forKey: "OfferCode")
//                            if let msg = dict["discount_message"] as? String, msg.count > 0
//                            {
//                                self.presentAlertWithTitle(title: msg, message: "", options: "OK", completion: { (ButtonIndex) in
//                                    self.txtPromocode.text = ""
//                                })
//                            }
//
//                        }

                        if !isBackFromOffer
                        {
                            if let discount = dict["discount_applied"] as? Int, discount > 0
                            {
                                if isCODOfferCode
                                {
                                    // Used Best Offer when click on COD button on First time
                                    self.lblPromocode.text = self.bestOffer
                                    
                                }else
                                {
                                    self.lblPromocode.text = self.txtPromocode.text ?? ""
                                }
                                
                                //                            self.txtPromocode.text = ""
                                
                            }else
                            {
                                self.lblPromocode.text = ""
                                if let msg = dict["discount_message"] as? String, msg.count > 0
                                {
                                    self.presentAlertWithTitle(title: msg, message: "", options: "OK", completion: { (ButtonIndex) in
                                            self.txtPromocode.text = ""
                                    })
                                }
                            }
                            
//                            if userDefault.string(forKey: "RegionCode") == "IN"
//                            {
                                //                            if let discount = dict["discount_applied"] as? Int, discount > 0
                                //                            {
                                //                                // APPLY CODE
                                //
                                //                                // Managed Constraints when Apply promocode
                                //                                self.constraintApplyCodeViewHeight.constant = 0.0
                                //                                if DeviceType.IS_IPAD
                                //                                {
                                //                                    self.constraintPromocodeViewHeight.constant = 52.0
                                //                                }else{
                                //                                    self.constraintPromocodeViewHeight.constant = 35.0
                                //                                }
                                //                                self.btnRemoveCode.isHidden = false
                                //                                self.btnHaveAPromocode.isHidden = true
                                //                                self.promocodeBorderView.isHidden = true
                                //                                isPromocode = true
                                //                            }
                                //                            else{
                                //                                // REMOVE CODE
                                //
                                //                                // Managed Constraints when remove promocode
                                //                                self.constraintPromocodeViewHeight.constant = 0.0
                                //                                if DeviceType.IS_IPAD
                                //                                {
                                //                                    self.constraintApplyCodeViewHeight.constant = 117.0
                                //                                }else{
                                //                                    self.constraintApplyCodeViewHeight.constant = 78.0
                                //                                }
                                //                                self.btnRemoveCode.isHidden = true
                                //                                self.btnHaveAPromocode.isHidden = false
                                //                                self.promocodeBorderView.isHidden = false
                                //                                isPromocode = false
                                //                            }
                                
                                if let discount = dict["discount_applied"] as? Int, discount > 0
                                {
                                    // APPLY CODE
                                    
                                    // Managed Constraints when Apply promocode
                                    self.constraintApplyCodeViewHeight.constant = 0.0
                                    if DeviceType.IS_IPAD
                                    {
                                        self.constraintPromocodeViewHeight.constant = 52.0
                                    }else{
                                        self.constraintPromocodeViewHeight.constant = 35.0
                                    }
                                    self.btnRemoveCode.isHidden = false
                                    self.btnHaveAPromocode.isHidden = true
                                    self.promocodeBorderView.isHidden = true
                                }
                                else{
                                    // REMOVE CODE
                                    
                                    // Managed Constraints when remove promocode
                                    self.constraintPromocodeViewHeight.constant = 0.0
                                    if DeviceType.IS_IPAD
                                    {
                                        self.constraintApplyCodeViewHeight.constant = 117.0
                                    }else{
                                        self.constraintApplyCodeViewHeight.constant = 78.0
                                    }
                                    self.btnRemoveCode.isHidden = true
                                    self.btnHaveAPromocode.isHidden = false
                                    self.promocodeBorderView.isHidden = false
                                }
//                            }
                        }
                        
                    }
                    else
                    {
                        self.view.makeToast("\(responceDict["ResponseMessage"]!)", duration: 3.0, position: .center)
                        
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: "Alert", message: (error1?.localizedDescription ?? ""), options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    //    func CheckOut() {
    //        if Reachability.isConnectedToNetwork() {
    //
    //            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
    //
    //            let ParamDict = ["user_id": userID] as NSDictionary
    //
    //            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)checkout", parameters: ParamDict) { (APIResponce, Responce, error1) in
    //
    //                self.RemoveLoader()
    //
    //                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
    //                {
    //
    //                    let responceDict = Responce as! [String:Any]
    //
    //                    if responceDict["ResponseCode"] as! String == "1"
    //                    {
    //                        let dict = responceDict["data"] as! NSDictionary
    //                        self.OrderId = "\(dict.value(forKey: "order_id") as? String ?? "")"
    //                    }else
    //                    {
    //                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
    //                        })
    //                    }
    //
    //                }
    //                else
    //                {
    //                    self.presentAlertWithTitle(title: (error1?.localizedDescription)!, message: "", options: "Retry", completion: { (ButtonIndex) in
    //                    })
    //                }
    //
    //            }
    //        }
    //        else
    //        {
    //            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
    //            })
    //        }
    //    }
    
    //MARK:- ********************** PAYTM API CALLING **********************
    
    
    func Generate_Paytm_CheckSum() {
        
        if Reachability.isConnectedToNetwork() {
            
            //            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            //            let ParamDict = ["CUST_ID": userID,
            //                            "ORDER_ID" : OrderId,
            //                            "TXN_AMOUNT" : self.paymentAmount] as NSDictionary
            //
            //
            //            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "https://printphoto.in/Photo_case/paytm/generateChecksum.php", parameters: ParamDict) { (APIResponce, Responce, error1) in
            //
            //                self.RemoveLoader()
            //
            //                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
            //                {
            //
            //                    let responceDict = Responce as! [String:Any]
            //
            //                    if responceDict["ResponseStatus"] as! String == "1"
            //                    {
            //                        self.CheckSmStr = responceDict["CHECKSUMHASH"] as! String
            //
            //                        let type :ServerType = .eServerTypeProduction// For Testing Mode payment
            //                        let merchantConfig = PGMerchantConfiguration.defaultConfiguration()
            //
            //                        // for authenticate with payTM that the request sent by backend is same as yours
            //                        let order = PGOrder()
            //
            //
            //                        order.params = ["MID": "RAMANI43483626857623", //provided by paytm
            //                            "ORDER_ID": "\(self.OrderId!)", // you must provide this in unique string
            //                            "CUST_ID": "\(self.userID!)", // you must provide this
            //                            "CHANNEL_ID": "WAP", // static for staging
            //                            "WEBSITE": "APPPROD", // static for staging
            //                            "TXN_AMOUNT": "\(self.paymentAmount)", // you must provide this
            //                            "INDUSTRY_TYPE_ID": "Retail109", // static for staging
            //                            "CHECKSUMHASH": "\(self.CheckSmStr)", // value given in responce by backend
            //                            "CALLBACK_URL": "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(self.OrderId!)",  //sta
            //                            "PAYMENT_MODE_ONLY":"YES", //for enable only wallet
            //                            "AUTH_MODE":"USRPWD", // for enable only wallet
            //                            "PAYMENT_TYPE_ID":"PPI" // for enable only wallet
            //                        ]
            //
            //                        DispatchQueue.main.async {
            //
            //                            self.txnController =  (self.txnController.initTransaction(for: order) as? PGTransactionViewController)!
            //
            //                            self.txnController.title = "Paytm Payments"
            //
            //                            self.txnController.merchant = merchantConfig
            //                            self.txnController.isLoggingEnabled = false
            //                            if(type != ServerType.eServerTypeNone) {
            //                                self.txnController.serverType = type;
            //                            } else {
            //                                return
            //                            }
            //                            self.txnController.merchant = PGMerchantConfiguration.defaultConfiguration()
            //                            self.txnController.delegate = self
            //                            self.pushViewController(self.txnController)
            //                        }
            //                    }
            //
            //                }
            //                else
            //                {
            //                    self.presentAlertWithTitle(title: (error1?.localizedDescription)!, message: "", options: "OK", completion: { (ButtonIndex) in
            //                    })
            //                }
            //            }
            
            
            // Used to JSonSerialization because get OrderID NULL when using AFNWtworking
            
            var request = URLRequest(url: URL(string: "https://printphoto.in/Photo_case/paytm/generateChecksum.php")!)
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            let param = "CUST_ID=\(userID!)&ORDER_ID=\(self.transactionId!)&TXN_AMOUNT=\(paymentAmount ?? "0")"
            request.httpBody = param.data(using: .utf8)
            
            let urlSession = URLSession.shared
            let dataTask = urlSession.dataTask(with: request) { (data,response,error) in
                
                //                self.RemoveLoader()
                
                if error != nil {
                    self.RemoveLoader()
                    
                    self.presentAlertWithTitle(title: (error?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
                else{
                    do {
                        let httpResponse = response as? HTTPURLResponse
                        
                        print(String(data: data!, encoding: String.Encoding.utf8)!)
                        let resultJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        print(resultJson)
                        
                        
                        self.RemoveLoader()
                        
                        
                        if httpResponse!.statusCode == 200 && error == nil
                        {
                            let responceDict = resultJson as! [String:Any]
                            
                            if responceDict["ResponseStatus"] as! String == "1"
                            {
                                
                                self.CheckSmStr = responceDict["CHECKSUMHASH"] as! String
                                
                                self.paymentOrderID = responceDict["ORDER_ID"] as? String
                                
                                //                                let type :ServerType = .eServerTypeProduction// For Testing Mode payment
                                //                                let merchantConfig = PGMerchantConfiguration.defaultConfiguration()
                                
                                let type :ServerType = ServerType.init(rawValue: 0)// .eServerTypeProduction// For Testing Mode payment
                                let merchantConfig = PGMerchantConfiguration.default()
                                
                                // for authenticate with payTM that the request sent by backend is same as yours
                                let order = PGOrder()
                                
                                order.params = ["MID": "RAMANI43483626857623", //provided by paytm
                                    "ORDER_ID": "\(self.transactionId ?? "")", // you must provide this in unique string
                                    "CUST_ID": "\(self.userID!)", // you must provide this
                                    "CHANNEL_ID": "WAP", // static for staging
                                    "WEBSITE": "APPPROD", // static for staging
                                    "TXN_AMOUNT": "\(self.paymentAmount ?? "0")", // you must provide this
                                    "INDUSTRY_TYPE_ID": "Retail109", // static for staging
                                    "CHECKSUMHASH": "\(self.CheckSmStr)", // value given in responce by backend
                                    "CALLBACK_URL": "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(self.transactionId ?? "")",  //sta
                                    "PAYMENT_MODE_ONLY":"YES", //for enable only wallet
                                    "AUTH_MODE":"USRPWD", // for enable only wallet
                                    "PAYMENT_TYPE_ID":"PPI" // for enable only wallet
                                ]
                                
                                //                                DispatchQueue.main.async {
                                //
                                ////                                    self.txnController =  (self.txnController.initTransaction(for: order) as? PGTransactionViewController)!
                                ////                                    self.txnController.view.frame.size.height = 400
                                //                                    self.txnController = PGTransactionViewController.init(transactionParameters: order.params)
                                //                                    self.txnController.title = "Paytm Payments"
                                //
                                //                                    self.txnController.merchant = merchantConfig
                                //                                    self.txnController.isLoggingEnabled = false
                                //                                    if(type != ServerType.eServerTypeNone) {
                                //                                        self.txnController.serverType = type;
                                //                                    } else {
                                //                                        return
                                //                                    }
                                //                                    self.txnController.merchant = PGMerchantConfiguration.defaultConfiguration()
                                //                                    self.txnController.delegate = self
                                //                                    self.pushViewController(self.txnController)
                                //                                }
                                
                                DispatchQueue.main.async {
                                    
                                    self.txnController = PGTransactionViewController.init(transactionFor: order)
                                    self.txnController.hidesBottomBarWhenPushed = true
                                    //                                    self.txnController =  (self.txnController.initTransaction(for: order) as? PGTransactionViewController)!
                                    
                                    self.txnController.title = "Paytm Payments"
                                    
                                    self.txnController.merchant = merchantConfig
                                    self.txnController.loggingEnabled = false
                                    if(type != ServerType.init(rawValue: 2)) { // eServerTypeNone
                                        self.txnController.serverType = type;
                                    } else {
                                        return
                                    }
                                    self.txnController.merchant = PGMerchantConfiguration.default()
                                    self.txnController.delegate = self
                                    self.pushViewController(self.txnController)
                                }
                            }
                        }
                        else
                        {
                            
                            self.presentAlertWithTitle(title: (error?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                            })
                        }
                    }
                    catch {
                        self.RemoveLoader()
                        
                    }
                    
                }
            }
            
            dataTask.resume()
            
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
        
    }
    
    /*
     func Check_TXNStatus() {
     
     //        if Reachability.isConnectedToNetwork() {
     //
     //            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
     //
     //            let ParamDict = ["ORDER_ID" : "123457"] as NSDictionary
     //
     //            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "https://printphoto.in/Photo_case/paytm/TXNStatus.php", parameters: ParamDict) { (APIResponce, Responce, error1) in
     //
     //                self.RemoveLoader()
     //
     //                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
     //                {
     //                    let responceDict = Responce as! [String:Any]
     //
     //                    if responceDict["ResponseCode"] as! String == "1"
     //                    {
     //                        let status = (responceDict["data"] as! NSDictionary)["STATUS"] as? String
     //
     //                        if status == "TXN_SUCCESS"
     //                        {
     //                            // Call API - For Check Status Success Or Not
     //                           self.Place_Order()
     //                        }
     //
     //                    }
     //                    else
     //                    {
     //                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
     //                        })
     //                    }
     //                }
     //                else
     //                {
     //                    self.presentAlertWithTitle(title: (error1?.localizedDescription)!, message: "", options: "OK", completion: { (ButtonIndex) in
     //                    })
     //                }
     //            }
     //        }
     //        else
     //        {
     //            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
     //            })
     //        }
     
     
     if Reachability.isConnectedToNetwork() {
     
     showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
     
     var request = URLRequest(url: URL(string: "https://printphoto.in/Photo_case/paytm/TXNStatus.php")!)
     request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
     request.setValue("application/json", forHTTPHeaderField: "Accept")
     request.httpMethod = "POST"
     let param = "ORDER_ID=\(paymentOrderID ?? "")"
     request.httpBody = param.data(using: .utf8)
     
     let urlSession = URLSession.shared
     let dataTask = urlSession.dataTask(with: request) { (data,response,error) in
     
     self.RemoveLoader()
     
     if error != nil {
     
     self.presentAlertWithTitle(title: (error?.localizedDescription)!, message: "", options: "OK", completion: { (ButtonIndex) in
     })
     }
     else{
     do {
     let httpResponse = response as? HTTPURLResponse
     
     print(String(data: data!, encoding: String.Encoding.utf8)!)
     let resultJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
     print(resultJson)
     
     if httpResponse!.statusCode == 200 && error == nil
     {
     let responceDict = resultJson as! [String:Any]
     
     if responceDict["ResponseCode"] as! String == "1"
     {
     let status = (responceDict["data"] as! NSDictionary)["STATUS"] as? String
     
     if status == "TXN_SUCCESS"
     {
     // Call API - For Check Status Success Or Not
     //                                    self.Initiate_Transaction(transaction_type: "")
     }else
     {
     self.presentAlertWithTitle(title: "Transaction Fail !", message: "", options: "OK", completion: { (ButtonIndex) in
     })
     }
     }else
     {
     self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
     })
     }
     }
     else
     {
     self.presentAlertWithTitle(title: (error?.localizedDescription)!, message: "", options: "OK", completion: { (ButtonIndex) in
     })
     }
     }
     catch {
     
     }
     
     }
     }
     
     dataTask.resume()
     
     }
     else
     {
     self.presentAlertWithTitle(title: "Internet is slow. Please check internet connection.", message: "", options: "OK", completion: { (ButtonIndex) in
     })
     }
     
     }
     */
    
    //MARK:- ********************** PAYTM METHOD **********************
    
    //this function triggers when transaction gets finished
    func didFinishedResponse(_ controller: PGTransactionViewController, response responseString: String) {
        
        if let data = responseString.data(using: String.Encoding.utf8) {
            do {
                if let jsonresponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] , jsonresponse.count > 0{
                    let titlemsg = jsonresponse["STATUS"] as? String ?? ""
                    if jsonresponse["RESPCODE"] as? String == "01" {
                        
                        // Call API - Check payment success or not
                        //                        Check_TXNStatus()
                        
                        //                        self.Set_Final_Status(payType: "Paytm")
                        
                        let paymentSuccessVC = mainStoryBoard.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
                        paymentSuccessVC.isFromOrderID = self.OrderId
                        paymentSuccessVC.paymentType = "Paytm"
                        self.pushViewController(paymentSuccessVC)
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "Order Failure", message: "Your transaction has been failed", options: "OK", completion: { (ButtonIndex) in
                            
                            //                            let paymentSuccessVC = mainStoryBoard.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
                            //                            paymentSuccessVC.isFromOrderID = self.OrderId
                            //                            paymentSuccessVC.paymentType = "Paytm"
                            //                            self.pushViewController(paymentSuccessVC)
                            
                            controller.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            } catch {
                self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
                })
            }
        }
    }
    
    //this function triggers when transaction gets cancelled
    func didCancelTrasaction(_ controller : PGTransactionViewController) {
        controller.navigationController?.popViewController(animated: true)
    }
    //Called when a required parameter is missing.
    func errorMisssingParameter(_ controller : PGTransactionViewController, error : Error?) {
        controller.navigationController?.popViewController(animated: true)
    }
    
    //    //MARK:- ********************** Paypal Method **********************
    //
    //    func safariViewControllerDidFinish(_ controller: SFSafariViewController)
    //    {
    //        controller.dismiss(animated: true, completion: nil)
    //
    //    }
    
    
    //MARK:- ********************** CCAVENUE INTEGRATION **********************
    //
    func CCAvenue_Intigration() {
        
        //         initCont.delegate = self
        //        initCont = InitialViewController.init(orderId: "\(OrderId!)", merchantId: "189816", accessCode: "AVLX80FI26AN89XLNA", custId: "", amount: "\(1).00", currency: "INR", rsaKeyUrl: "https://printphoto.in/Photo_case/public/api/GetRSA", redirectUrl: "https://printphoto.in/Photo_case/CCAvenue/ccavResponseHandler.php", cancelUrl: "https://printphoto.in/Photo_case/CCAvenue/ccavResponseHandler.php", showAddress: "", billingName: "", billingAddress: "", billingCity: "", billingState: "", billingCountry: "India", billingTel: "\(EditDict["mobileno"] as? String ?? "")", billingEmail: "\(EditDict["email"] as? String ?? "")", deliveryName: "", deliveryAddress: "", deliveryCity: "", deliveryState: "", deliveryCountry: "", deliveryTel: "", promoCode: "", merchant_param1: "", merchant_param2: "", merchant_param3: "", merchant_param4: "", merchant_param5: "", useCCPromo: "")
        //        self.present(initCont, animated: true, completion: nil)
        
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CCWebViewController") as! CCWebViewController
        controller.accessCode = "AVLX80FI26AN89XLNA"
        controller.merchantId = "189816"
        controller.amount = "\(paymentAmount ?? "0")"
        controller.currency = "INR"
        controller.orderID = OrderId
        controller.transactionID = "\(self.transactionId ?? "")"
        controller.redirectUrl = "https://printphoto.in/Photo_case/CCAvenue/ccavResponseHandler.php"
        controller.cancelUrl = "https://printphoto.in/Photo_case/CCAvenue/ccavResponseHandler.php"
        controller.rsaKeyUrl = "https://printphoto.in/Photo_case/public/api/GetRSA"
        controller.billingCountry = "India"
        controller.billingTel = "\(EditDict["mobileno"] as? String ?? "")"
        controller.billingEmail = "\(EditDict["email"] as? String ?? "")"
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //    func getResponse(_ responseDict_: NSMutableDictionary!) {
    //    }
}
