//
//  PaypalViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 28/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import WebKit
import Alamofire

class PaypalViewController: BaseViewController,UIWebViewDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var webView: UIWebView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var url : String?
    var isLoader = false
    
    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()

        SetNavigationBarTitle(Startstring: "Paypal Payment", EndString: "")

        Initialize()
        
        // Do any additional setup after loading the view.
    }
   
    func Initialize()
    {
        addBackButton()
        removeBackButton()
        
        webView.delegate = self
        
        checkRechabilityStatus()
      
        NotificationCenter.default.addObserver(self, selector: #selector(self.cancelPaypalPaymentAction(notification:)), name: Notification.Name("CancelPaypalPayment"), object: nil)

          NotificationCenter.default.addObserver(self, selector: #selector(self.continuePaypalPaymentAction(notification:)), name: Notification.Name("ContinuePaypalPayment"), object: nil)
    }
    
    
    //MARK:- ********************** CHECK RECHABILITY STATUS **********************
    
    func checkRechabilityStatus()
    {
        let reachabilityManager = NetworkReachabilityManager()
        
        reachabilityManager?.startListening(onUpdatePerforming: { _ in
            if let isNetworkReachable = reachabilityManager?.isReachable,
                isNetworkReachable == true {
                
                // Remove all cache
                URLCache.shared.removeAllCachedResponses()
                
                // Delete any associated cookies
                if let cookies = HTTPCookieStorage.shared.cookies {
                    for cookie in cookies {
                        HTTPCookieStorage.shared.deleteCookie(cookie)
                    }
                }

                let urlRequest = URLRequest(url: URL(string: self.url!)!)
//                let urlRequest = URLRequest(url: URL(string: self.url!)!, cachePolicy:NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData,timeoutInterval: 10.0)
                self.webView.loadRequest(urlRequest)
                
            } else {
                //Internet Not Available"
                print("Internet Not Available")
                
                self.RemoveLoader()
                
                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                    
                   self.popViewController()
                })
            }
        })
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    @objc func cancelPaypalPaymentAction(notification: Notification)
    {
        self.popViewController()
    }

    @objc func continuePaypalPaymentAction(notification: Notification)
    {
        let paymentSuccessVC = mainStoryBoard.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
        paymentSuccessVC.isFromOrderID = notification.object as? String ?? ""
        paymentSuccessVC.paymentType = "Paypal"
        self.pushViewController(paymentSuccessVC)
    }

    //MARK:- ********************** WebviewDelegate Methods **********************
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        self.RemoveLoader()
        
        
//        var isCancel: Bool = false
//
//        if let info = error._userInfo as? [String: Any]
//        {
//            if let url = info["NSErrorFailingURLKey"] as? URL
//            {
//                if url.absoluteString == "com.vasu.printphoto:/paypal_cancel"
//                {
//                    isCancel = true
//                }
//            }
//        }

        // Get only Alert box when load Webview is fail
        if error._code == -1001
        {
            self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.popViewController()
                }
            })
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        // Only one time show loader
        if !isLoader
        {
            isLoader = true
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            if ((self.view?.window) != nil)
            {
                self.RemoveLoader()
            }
        }
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {

        return true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    {
        print("Finished navigating to url \(String(describing: webView.url))")
        
    }
    
    func webView(_ webView: WKWebView, commitPreviewingViewController previewingViewController: UIViewController)
    {
        print(previewingViewController)
        print(webView)
    }

}
