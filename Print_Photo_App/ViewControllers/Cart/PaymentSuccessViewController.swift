//
//  PaymentSuccessViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 15/03/19.
//  Copyright © 2019 des. All rights reserved.
//
import UIKit
import FacebookCore
import FBSDKCoreKit
//import FBSDKMarketingKit
import Firebase

class PaymentSuccessViewController: BaseViewController {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDetails1: UILabel!
    @IBOutlet weak var lblDetail2: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    @IBOutlet weak var lblTransactionID: UILabel!
    @IBOutlet weak var constraintsTransactionIdBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var paymentSuccessView: UIView!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var gifImageView: UIImageView!
    @IBOutlet weak var lblOrderTitle: UILabel!
    
    @IBOutlet var viewMainPopup: UIView!
    @IBOutlet var viewPopup: UIView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var placeOrderDict = NSDictionary()
    var flashing = false
    let currentDateTime = Date()
    var paymentType : String?
    var razorPayDict = [String:Any]()
    var isFromOrderID : String?
    var currencySymbol : String?
    
    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()

        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.navigationItem.setHidesBackButton(true, animated:false)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {

        self.paymentSuccessView.isHidden = true
        self.btnView.isHidden = true
        
        // App Review Popup design
        App_Rate()
        
//        paymentType = "\(placeOrderDict.value(forKey: "transaction_type") ?? "")"
   
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.Set_Final_Status(payType: self.paymentType ?? "")
        }
    }
    
    //MARK:- ********************** BUTTON EVENT **********************

    @IBAction func press_btn_Continue_Shopping(_ sender: UIButton) {

        // Reload Delivery address tableview
        isReloadTable = true
        
        // Set badge icon for Cart
        appDelegate.SetBadgeIcon()
        
//        // Purchase Analytics of Facebook
//        let orderID = "\(placeOrderDict.value(forKey: "order_id") ?? "")"
//
//        logPurchaseEvent(amount: placeOrderDict.value(forKey: "paid_amount") as! Int, currency: "INR", order_id: orderID)
//
//        // Purchase Analytics of Firebase
//        let param = ["Payment_Completed":1] as [String:Any]
//        Analytics.logEvent("order_confirm", parameters: param)

        self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 0 : 1

    }
    
    //MARK:- ********************** APP REVIEW POPUP **********************

    func App_Rate() {
        
        viewMainPopup.isHidden = true
        
        viewMainPopup.alpha = 0.0
        
        let col = UIColor.init(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        viewPopup.backgroundColor = col
        viewPopup.layer.cornerRadius = 8.0
        
        let gradient : CAGradientLayer = CAGradientLayer()
        gradient.frame = viewPopup.bounds
        gradient.frame.size.width = 290.0
        gradient.frame.size.height = 240.0
        gradient.colors = [col.cgColor, col.cgColor]
        gradient.startPoint = CGPoint.init(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint.init(x: 1.0, y: 0.5)
        gradient.opacity = 0.5
        gradient.cornerRadius = 8.0
        gradient.shadowColor = UIColor.darkGray.cgColor
        gradient.shadowOpacity = 1.0
        gradient.shadowRadius = 10.0
        gradient.shadowOffset = CGSize.init(width: 0.0, height: 0.0)
        viewPopup.layer.insertSublayer(gradient, at: 0)
    }
    
    func openRatePopup()
    {
        UIView.animate(withDuration: 0.3) {
            self.viewMainPopup.alpha = 1.0
        }
    }
    
    @IBAction func btnRate(_ sender: Any) {
        
//        userDefault.set(0, forKey: "PaymentRate")

        guard let writeReviewURL = URL(string: "https://itunes.apple.com/app/id1458325325?action=write-review")
            else { fatalError("Expected a valid URL") }
        UIApplication.shared.open(writeReviewURL, options: [:], completionHandler: nil)
        closeRatePopup()
    }
    
    @IBAction func btnLater(_ sender: Any) {
        closeRatePopup()
    }
    
    @IBAction func btnNoThanks(_ sender: Any) {
        closeRatePopup()
    }
    
    func closeRatePopup()
    {
        UIView.animate(withDuration: 0.3, animations: {
            self.viewMainPopup.alpha = 0.0
            
        }, completion: { (bool) in
            self.viewMainPopup.removeFromSuperview()
        })
    }
    
    //MARK:- ********************** GET DATA **********************

    func Get_Data() {
        
//       self.imgTick.fadeInoutAnimation()
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
//            self.imgTick.layer.removeAllAnimations()
//            self.imgTick.alpha = 1.0
//        }
//
        
        let EditDict = userDefault.value(forKey: "EditAccountDict")! as! [String : Any]

        lblAmount.text = "\(currencySymbol ?? "")\(String(format: "%.2f", (placeOrderDict.value(forKey: "paid_amount") as? Double ?? 0.0)))" //\(placeOrderDict.value(forKey: "paid_amount") ?? "")"
        lblOrderID.text = "Order ID : \(placeOrderDict.value(forKey: "order_id") ?? "")"
        lblMobileNo.text = "Mo : \(EditDict["mobileno"] ?? "")"
        
        let transactionType = "\(placeOrderDict.value(forKey: "transaction_type") ?? "")"
        
        if transactionType == "COD"
        {
            constraintsTransactionIdBottomMargin.constant = 0.0
            lblTransactionID.text = ""
        }
        else
        {
            if DeviceType.IS_IPAD
            {
                constraintsTransactionIdBottomMargin.constant = 22.0
            }else
            {
                constraintsTransactionIdBottomMargin.constant = 15.0
            }
            lblTransactionID.text = "Transaction ID : \(placeOrderDict.value(forKey: "transaction_id") ?? "")"
        }
        
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.dateStyle = .medium
        formatter.string(from: currentDateTime)
        
        lblDate.text = formatter.string(from: currentDateTime)
        
        
        self.paymentSuccessView.isHidden = false
        self.btnView.isHidden = false
    }
    
    /**
     * For more details, please take a look at:
     * developers.facebook.com/docs/swift/appevents
     */
    func logPurchaseEvent(amount : Int, currency : String, order_id : String) {
        let params = [
            "amount" : NSNumber(value:amount),
            "currency" : currency,
            "order_id" : order_id
            ] as [String : Any]
        
        AppEvents.logEvent(AppEvents.Name(rawValue: "Purchase"), parameters: params)
    }
    
    func Set_Final_Status(payType: String) {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .gray)

            var cancelOrder : String?
            var razorPay_OrderID : String?
            var razorPay_PaymentId : String?
            var razorPay_Signature : String?
            
            if payType == "Paytm" || payType == "COD" || payType == "CCAvenueGateway" || payType == "Paypal"
            {
                cancelOrder = "0"
                razorPay_OrderID = ""
                razorPay_PaymentId = ""
                razorPay_Signature = ""
            }else if payType == "Razorpay"
            {
                cancelOrder = "0"
                razorPay_OrderID = razorPayDict["razorpay_order_id"] as? String ?? ""
                razorPay_PaymentId = razorPayDict["razorpay_payment_id"] as? String ?? ""
                razorPay_Signature = razorPayDict["razorpay_signature"] as? String ?? ""
            }
//            else
//            {
//                cancelOrder = "1"
//                razorPay_OrderID = ""
//                razorPay_PaymentId = ""
//                razorPay_Signature = ""
//            }
            
            let ParamDict = ["order_id" : isFromOrderID ?? "",
                             "cancel_order" : cancelOrder ?? "",
                             "razorpay_order_id" : razorPay_OrderID ?? "",
                             "razorpay_payment_id" : razorPay_PaymentId ?? "",
                             "razorpay_signature" : razorPay_Signature ?? "",
                             "device_type": "ios",
                             "app_version": appVersion] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)order/set_final_status", parameters: ParamDict) { (APIResponce, Responce, error1) in //order/set_final_status
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.placeOrderDict  = responceDict["data"] as! NSDictionary
                        
                        self.currencySymbol = self.placeOrderDict["currency_symbol"] as? String ?? ""
                        let status = self.placeOrderDict["status"] as? Int
                        let message = "\(self.placeOrderDict["message"] as? String ?? "")"
                        let transactionType = "\(self.placeOrderDict["transaction_type"] as? String ?? "")"
                        
                        var title = ""
                        
                        if transactionType == "COD"
                        {
                            title = "Order Placed"
                        }else
                        {
                            title = "Congratulations"
                        }
                        
                        if status == 1
                        {
                            // Success
                            self.gifImageView.image = UIImage.gifImageWithName("done_gif")
                            self.lblOrderTitle.text = "Order Received"
                            self.Get_Data()
                        
                            // Open App Rate Popup
                        
//                          if userDefault.value(forKey: "PaymentRate") == nil
//                          {
                            self.viewMainPopup.isHidden = false
//                          }
                            
                            // Purchase Analytics of Facebook
                            let orderID = "\(self.placeOrderDict.value(forKey: "order_id") ?? "")"
                            self.logPurchaseEvent(amount: self.placeOrderDict.value(forKey: "paid_amount") as! Int, currency: "₹", order_id: orderID)
                            
                            // Purchase Analytics of Firebase
                            let param = ["Payment_Completed":1] as [String:Any]
                            Analytics.logEvent("order_confirm", parameters: param)
                            
                            
                            self.presentAlertWithTitle(title: title, message: message, options: "OK", completion: { (ButtonIndex) in
                                self.openRatePopup()
                            })
                        }
                        else
                        {
//                            // Failure
                        
                            self.presentAlertWithTitle(title: title, message: message, options: "OK", completion: { (ButtonIndex) in
                                self.gifImageView.image = UIImage.gifImageWithName("cancel_gif")
                                self.lblOrderTitle.text = "Order Canceled"
                                self.Get_Data()
                            })
                        }
                       
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "Retry", completion: { (ButtonIndex) in
                            self.Set_Final_Status(payType: payType)

                        })
                    }

                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        self.Set_Final_Status(payType: payType)
                    })
                }

            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                    self.Set_Final_Status(payType: payType)
            })
        }
    }
}
