//
//  ForgetPasswordViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 07/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: BaseViewController,UITextFieldDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var new_password_icon_view: UIView!
    @IBOutlet weak var confirm_password_iconview: UIView!
    @IBOutlet weak var txtNewPassword: KTextField!
    @IBOutlet weak var txtConfirmPassword: KTextField!
    
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    SetNavigationBarTitle(Startstring: "Forget Password", EndString: "")

       Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        addBackButton()
        ViewBorder()
        addResetButton()
        
        txtNewPassword.returnKeyType = .next
        txtConfirmPassword.returnKeyType = .done
        
        self.hideKeyboardWhenTappedAround()
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    @IBAction func press_button_forget_password(_ sender: UIButton) {
        if isReadyToForgetPassword()
        {
            Call_Forgot_Password_API()
        }
    }
    
    func addBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        let VCarray = self.navigationController?.viewControllers
        for Controller in VCarray! {
            if Controller is SignInViewController {
                self.navigationController?.popToViewController(Controller, animated: true)
            }
        }
    }
    
    func addResetButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_reset"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.resetAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func resetAction(_ sender: UIButton) {
        txtNewPassword.text = ""
        txtConfirmPassword.text = ""
    }
    
    //MARK:- ********************** UIVIEW BORDER **********************
    
    func ViewBorder() {
        
        new_password_icon_view.setBorder()
        confirm_password_iconview.setBorder()
        
    }
    
    //MARK:- ********************** VALIDATION **********************
    
    func isReadyToForgetPassword()-> Bool {
        
        if Validation.isStringEmpty(txtNewPassword.text!) || txtNewPassword.text?.trim() == ""{
            
            presentAlertWithTitle(title: "Please enter new password", message: "", options: "OK") { (Int) in
                self.txtNewPassword.becomeFirstResponder()
            }
        }else if Int(txtNewPassword.text!.count) < 6
        {
            self.presentAlertWithTitle(title: "Password must be more than 6 digit", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txtNewPassword.becomeFirstResponder()
            })
        }else if Validation.isStringEmpty(txtConfirmPassword.text!) || txtConfirmPassword.text?.trim() == ""{
            
            presentAlertWithTitle(title: "Please enter confirm password", message: "", options: "OK") { (Int) in
                self.txtConfirmPassword.becomeFirstResponder()
            }
        }else if Validation.isPassWordMatch(pass: txtNewPassword.text!, confPass: txtConfirmPassword.text!)
        {
            presentAlertWithTitle(title: "New Password and Confirm Password are not same", message: "", options: "OK") { (Int) in
                self.txtConfirmPassword.becomeFirstResponder()
            }
        }
        else{
            return true
        }
        
        return false
    }
    
    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtNewPassword
        {
            textField.resignFirstResponder()
            txtConfirmPassword.becomeFirstResponder()
        }
        else{
            txtConfirmPassword.resignFirstResponder()
        }
        
        return false
        
//        self.view.endEditing(true)
//        return false
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Call_Forgot_Password_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            // Change Password using mobile number or email id
            
            /*
            var email_mobile : String?
            
            if isVerificationUsingMobile
            {
                email_mobile = mobileNumber
            }else{
                email_mobile = emailId
            }
            */
            
            let mobileOrEmail = (userDefault.string(forKey: "RegionCode") != "IN") ? emailId : mobileNumber

            let ParamDict = ["password": txtNewPassword.text!,
                             "mobile": mobileOrEmail ?? ""] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)change_password", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            let VCarray = self.navigationController?.viewControllers
                            for Controller in VCarray! {
                                if Controller is SignInViewController {
                                    self.navigationController?.popToViewController(Controller, animated: true)
                                }
                            }
                        })
                       
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                       
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
               
            })
        }
        
    }
}
