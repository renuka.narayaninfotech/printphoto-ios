//
//  OTPViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 04/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import CoreTelephony
import SVPinView
import Firebase
import IQKeyboardManagerSwift
import FirebaseAuth

class OTPViewController: BaseViewController {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet var txtOTP: SVPinView!
    @IBOutlet weak var lblRemainingSecond: UILabel!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var btnResend: UIButton!   
    @IBOutlet weak var lblSecondRemaining: UILabel!
    @IBOutlet weak var lblSecondsRemainingHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDontRecieveOTPHeight: NSLayoutConstraint!
    @IBOutlet weak var btnResendOTPHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblHeader: UILabel!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var otpVerify : Int!
    var OTPTimeSeconds = 60
    var timer = Timer()
    var isTimerRunning = false
    var isDictFromRegistration = [String:Any]() // Get dictionary from register
    
    var stopTime : Int?
    var startTime : Int?
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Code Verification", EndString: "")

        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = true
        
        IQKeyboardManager.shared.enable = false

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
//        navigationController?.setNavigationBarHidden(false, animated: false)
        
        addBackButton()
        addResetButton()

        if userDefault.string(forKey: "RegionCode") == "IN"
        {
            lblHeader.text = "OTP has been send to your mobile number"
            lblMobileNo.text = "Sent to +91\(mobileNumber)"
        } else if(userDefault.string(forKey: "RegionCode") == "SA")
        {
            lblHeader.text = "OTP has been send to your mobile number"
            lblMobileNo.text = "Sent to +966\(mobileNumber)"
        }
        else
        {
            lblHeader.text = "OTP has been send to your EmailID"
            lblMobileNo.text = "sent to \(emailId ?? "")"
        }
        
        btnResend.isHidden = true
        if DeviceType.IS_IPAD
        {
            lblSecondsRemainingHeight.constant = 30
        }else
        {
            lblSecondsRemainingHeight.constant = 20
        }
        lblDontRecieveOTPHeight.constant = 0
        btnResendOTPHeight.constant = 0
        
        runTimer()
        
        configurePinView()
        
    }

    //MARK:- ********************** SVPINVIEW OTP VERIFICATION **********************
    
    override func viewDidLayoutSubviews() {
        txtOTP.activeBorderLineThickness = 3
        txtOTP.fieldBackgroundColor = UIColor.clear
        txtOTP.activeFieldBackgroundColor = UIColor.clear
        txtOTP.fieldCornerRadius = 0
        txtOTP.activeFieldCornerRadius = 0
        txtOTP.style = .box
        txtOTP.tintColor = UIColor.clear

    }
    
    
    //MARK:- ********************** OTP SECOND REMAINING **********************
 
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
//        RunLoop.current.add(timer, forMode:RunLoop.Mode.common)
    }
    
    @objc func updateTimer() {
        
        if OTPTimeSeconds > 0
        {
            OTPTimeSeconds -= 1     //This will decrement(count down)the seconds.
            lblSecondRemaining.text = "seconds remaining: \(OTPTimeSeconds)"
        }else
        {
            btnResend.isHidden = false
            lblSecondsRemainingHeight.constant = 0
            
            if DeviceType.IS_IPAD
            {
                lblDontRecieveOTPHeight.constant = 30
                btnResendOTPHeight.constant = 60
                
            }else
            {
                lblDontRecieveOTPHeight.constant = 20
                btnResendOTPHeight.constant = 40
                
            }
        }
    }
    
    func configurePinView() {
        
        txtOTP.pinLength = 6
        txtOTP.secureCharacter = "\u{25CF}"
        txtOTP.interSpace = 5
        txtOTP.borderLineThickness = 2
        //        txtOTP.shouldSecureText = true
        txtOTP.allowsWhitespaces = false
        txtOTP.style = .none
        txtOTP.fieldBackgroundColor = UIColor.white.withAlphaComponent(0.3)
        txtOTP.activeFieldBackgroundColor = UIColor.white.withAlphaComponent(0.5)
        txtOTP.fieldCornerRadius = 15
        txtOTP.activeFieldCornerRadius = 15
        txtOTP.becomeFirstResponderAtIndex = 0
        if DeviceType.IS_IPAD
        {
            txtOTP.interSpace = 7
            txtOTP.font = UIFont.systemFont(ofSize: 30)
        }else
        {
            txtOTP.interSpace = 5
            txtOTP.font = UIFont.systemFont(ofSize: 20)
        }
        
        txtOTP.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        txtOTP.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
            
        }
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    @IBAction func press_btn_verify(_ sender: UIButton) {
                
        if txtOTP.getPin().isEmpty == true
        {
            self.presentAlertWithTitle(title: "Please enter OTP", message: "", options: "OK", completion: { (ButtonIndex) in
            })
        }else
        {
            self.view.endEditing(true)

//            if isVerificationUsingMobile
//            {
            
                verifyOTPUsingMobile()
            
//            }else
//            {
//                verifyOTPUsingEmail()
//            }
        }
    }
    
    @IBAction func press_btn_resend(_ sender: UIButton) {
        
        txtOTP.clearPin()
        OTPTimeSeconds = 60
        lblSecondRemaining.text = "seconds remaining: \(OTPTimeSeconds)"
        timer.invalidate()

        
//        if isVerificationUsingMobile
//        {
            ResendOTP()

//        }else
//        {
//            ResendOTPUsingEmail()
//        }
        
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        var isDone: Bool = false
        let VCarray = self.navigationController?.viewControllers
        for Controller in VCarray! {
            if Controller is SignInViewController {
                isDone = true
                self.navigationController?.popToViewController(Controller, animated: true)
            }
        }
        
        if !isDone
        {
//            if (self.navigationController?.viewControllers[0] is OrderViewController) || (self.navigationController?.viewControllers[0] is CartViewController)
//            {
                self.navigationController?.popToViewController((self.navigationController?.viewControllers[0])!, animated: true)
//            }
        }
    }
    
    func addResetButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_reset"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.resetAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func resetAction(_ sender: UIButton) {
        txtOTP.clearPin()
    }
    
    //MARK:- ********************** API CALLING **********************
    
    
    func ResendOTP() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            var strUrl : String?
            
            if userDefault.string(forKey: "RegionCode") == "IN" || userDefault.string(forKey: "RegionCode") == "SA"
            {
                // Used for india country
                strUrl = "\(BaseURL)can_send_otp?mobile=\(mobileNumber)&email="
                
            }else
            {
                // Used for out side of india country
                strUrl = "\(BaseURL)can_send_otp?mobile=&email=\(emailId ?? "")"
                
            }
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: strUrl ?? "", parameters: nil) { (APIResponce, Responce, error1) in

                self.RemoveLoader()

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    // if user come from login
                    if !isFromRegistration
                    {
                        if responceDict["status"] as! Int == 1
                        {
                            self.view.endEditing(true)
                            self.ResendOTPUsingMobile()
                        }
                        else
                        {
                            self.presentAlertWithTitle(title: responceDict["message"] as? String ?? "", message: "", options: "OK", completion: { (ButtonIndex) in
                            })
                        }
                    }
                    else // if user comes from registration
                    {
                        if responceDict["status"] as! Int == 1
                        {
                            self.presentAlertWithTitle(title: responceDict["message"] as? String ?? "", message: "", options: "OK", completion: { (ButtonIndex) in
                            })
                        }
                        else
                        {
                            self.ResendOTPUsingMobile()
                        }
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    func ResendOTPUsingMobile()
    {
        print(isBulk_SMS)
        
        // START TIMER WHEN RESEND OTP
        
        self.runTimer()
        self.btnResend.isHidden = true
        if DeviceType.IS_IPAD
        {
            self.lblSecondsRemainingHeight.constant = 30
        }else
        {
            self.lblSecondsRemainingHeight.constant = 20
        }
        self.lblDontRecieveOTPHeight.constant = 0
        self.btnResendOTPHeight.constant = 0
        
        if isBulk_SMS == "0"
        {
            // OTP verify using Firebase
            
            if userDefault.string(forKey: "RegionCode") == "IN"
            {
                self.Firebase_Mobile_Verify_API()
            }else
            {
                self.Bulk_SMS_Mobile_Verify_API(mobileno: "", email: emailId ?? "")
            }
            
        }else if isBulk_SMS == "1"
        {
            // OTP verify using Bulk SMS
            
            if userDefault.string(forKey: "RegionCode") == "IN" || userDefault.string(forKey: "RegionCode") == "SA"
            {
                self.Bulk_SMS_Mobile_Verify_API(mobileno: mobileNumber, email: "")
            }else
            {
                self.Bulk_SMS_Mobile_Verify_API(mobileno: "", email: emailId ?? "")
            }
        }
    }
    
//    func ResendOTPUsingEmail()
//    {
//        if Reachability.isConnectedToNetwork() {
//
//            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
//
//            let ParamDict = ["email": "\(emailId ?? "")",
//                            "verifyEmail": 1] as NSDictionary
//
//            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)send_otp_email", parameters: ParamDict) { (APIResponce, Responce, error1) in
//
//                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
//                {
//                    let responceDict = Responce as! [String:Any]
//
//                    if responceDict["ResponseCode"] as! String == "1"
//                    {
//                        let otpDict = responceDict["data"] as? [String:Any]
//                        strOTPForEmailVerification = "\(otpDict?["otp"] ?? "")"
//
//                        // Restart Counte timer
//                        // START TIMER WHEN RESEND OTP
//
//                        self.runTimer()
//                        self.btnResend.isHidden = true
//                        if DeviceType.IS_IPAD
//                        {
//                            self.lblSecondsRemainingHeight.constant = 30
//                        }else
//                        {
//                            self.lblSecondsRemainingHeight.constant = 20
//                        }
//                        self.lblDontRecieveOTPHeight.constant = 0
//                        self.btnResendOTPHeight.constant = 0
//
//
//                    }
//                    else
//                    {
//                        self.topMostViewController().view.makeToast("\(responceDict["ResponseMessage"] ?? "")", duration: 3.0, position: .bottom)
//                    }
//                }
//                else
//                {
//                    self.topMostViewController().view.makeToast((error1?.localizedDescription ?? ""), duration: 2.0, position: .bottom)
//                }
//                self.RemoveLoader()
//            }
//        }
//        else
//        {
//            self.RemoveLoader()
//
//            self.topMostViewController().view.makeToast("Internet is slow. Please check internet connection.", duration: 2.0, position: .bottom)
//        }
//    }
    
    //MARK:- ********************** FIREBASE MOBILE NUMBER VERIFY **********************
    
    func Firebase_Mobile_Verify_API()
    {
        showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)

        PhoneAuthProvider.provider().verifyPhoneNumber("\(phoneCode ?? "")"+""+"\(mobileNumber)", uiDelegate: nil) { (verificationID, error) in
            
            self.RemoveLoader()

            if ((error) != nil) {
                // Verification code not sent.
                //                print(error)
                
                self.presentAlertWithTitle(title: (error?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                })
                
            } else {
                // Successful. -> it's sucessfull here
                print("VERIFICATION ID\(verificationID ?? "")")
                
                userDefault.set(verificationID!, forKey: "VarificationId")
            }
        }
    }
    
    //MARK:- ********************** BULK SMS MOBILE NUMBER VERIFY **********************
    
    func Bulk_SMS_Mobile_Verify_API(mobileno: String, email: String)
    {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = [
                "mobile_number": mobileno,
                 "email": email,
                 "is_international": userDefault.string(forKey: "RegionCode") == "IN" ? "0" : "1"
            ] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)send_otp", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        
                        let dataArray = responceDict["data"] as! [String:Any]
                        print(dataArray)
                        
                        self.otpVerify = dataArray["otp"] as? Int
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                          
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                       
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
               
            })
        }
    }
    
    func Call_Register_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            var strParam = ""
            var strValue = ""
            
//            if userDefault.string(forKey: "RegionCode") == "IN"
//            {
                // Only in India country
                
                strParam = "city_id"
                strValue = "\(isDictFromRegistration["cityID"] ?? "")"
//            }else
//            {
//                // Only Outside of india country
//
//                strParam = "country_id"
//                strValue = userDefault.string(forKey: "countryId") ?? ""
//            }
            
            let ParamDict = ["name": "\(isDictFromRegistration["firstname"] ?? "")"+" "+"\(isDictFromRegistration["lastname"] ?? "")",
                            "email": isDictFromRegistration["emailId"] ?? "",
                            "pincode": "",
                            "mobile_1": isDictFromRegistration["mobilenumber"] ?? "",
                            "mobile_2": "",
                            "password": isDictFromRegistration["password"] ?? "",
                            "device_token": deviceToken ?? "",
                             strParam: strValue] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)registration", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.presentAlertWithTitle(title: "Registration", message: "Registration Successfully", options: "OK", completion: { (ButtonIndex) in
                            
                            // Vsn - 29/08/2020
                            if isLoginOrNot == "cart_signup"
                            {
                                let VCarray = self.navigationController?.viewControllers
                                for Controller in VCarray! {
                                    if Controller is CartViewController {
                                        self.navigationController?.popToViewController(Controller, animated: true)
                                    }
                                }
                            }else if isLoginOrNot == "order_signup"
                            {
                                let VCarray = self.navigationController?.viewControllers
                                for Controller in VCarray! {
                                    if Controller is OrderViewController {
                                        self.navigationController?.popToViewController(Controller, animated: true)
                                    }
                                }
                                // End
                            }else {
                                let VCarray = self.navigationController?.viewControllers
                                for Controller in VCarray! {
                                    if Controller is SignInViewController {
                                        self.navigationController?.popToViewController(Controller, animated: true)
                                    }
                                }
                            }
                        })
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                       
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
               
            })
        }
        
    }
    
    //MARK:- ********************** VERIFY OTP FUNCTION **********************
    
    func verifyOTPUsingMobile()
    {
        print(isBulk_SMS)
        let enteredOTP = txtOTP.getPin()
        print(enteredOTP)
        
        if isBulk_SMS == "0"
        {
            
            if userDefault.string(forKey: "RegionCode") == "IN"
            {
                let credential = PhoneAuthProvider.provider().credential(
                    withVerificationID: UserDefaults.standard.string(forKey: "VarificationId")!,
                    verificationCode: enteredOTP)
                
                showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                Auth.auth().signIn(with: credential) { (authResult, error) in
                    
                    self.RemoveLoader()
                    
                    if (error != nil) && error?.localizedDescription.contains("invalid") ?? false {
                        // Verification code not sent.
                        
                        self.presentAlertWithTitle(title: "Invalid OTP", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                        
                    } else if (error != nil) && error?.localizedDescription.contains("expired") ?? false
                    {
                        self.presentAlertWithTitle(title: "\(error?.localizedDescription ?? "")", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                    else {
                        if !isFromRegistration
                        {
                            let ForgetPasswordVC = mainStoryBoard.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
                            self.pushViewController(ForgetPasswordVC)
                        }else{
                            self.Call_Register_API()
                        }
                    }
                }
            }else
            {
                if Int(enteredOTP) == otpVerify
                {
                    if !isFromRegistration
                    {
                        let ForgetPasswordVC = mainStoryBoard.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
                        self.pushViewController(ForgetPasswordVC)
                    }else{
                        
                        self.Call_Register_API()
                    }
                }else
                {
                    self.presentAlertWithTitle(title: "Please enter valid OTP", message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
            
        }else if isBulk_SMS == "1"
        {
            if Int(enteredOTP) == otpVerify
            {
                if !isFromRegistration
                {
                    let ForgetPasswordVC = mainStoryBoard.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
                    self.pushViewController(ForgetPasswordVC)
                }else{
                    
                    self.Call_Register_API()
                }
            }else
            {
                self.presentAlertWithTitle(title: "Please enter valid OTP", message: "", options: "OK", completion: { (ButtonIndex) in
                })
            }
        }
        
    }
    
    func verifyOTPUsingEmail() {
        
        let enteredOTP = txtOTP.getPin()
        
        if enteredOTP == strOTPForEmailVerification
        {
            if !isFromRegistration
            {
                // OTP is verify
                let ForgetPasswordVC = mainStoryBoard.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
                self.pushViewController(ForgetPasswordVC)
            }else{
                
                self.Call_Register_API()
            }
        }else
        {
            self.presentAlertWithTitle(title: "Please enter valid OTP", message: "", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
   
}
