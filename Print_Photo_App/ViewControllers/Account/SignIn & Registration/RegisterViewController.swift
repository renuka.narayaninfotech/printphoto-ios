//
//  RegisterViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 02/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import DropDown
import Firebase
import CoreTelephony
import FirebaseAuth

fileprivate enum AMDropDownType {
    case countryCode
    case country
    case state
    case city
}

class RegisterViewController: BaseViewController,UITextFieldDelegate,UIGestureRecognizerDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var icon_first_name_view: UIView!
    @IBOutlet weak var icon_last_name_view: UIView!
    @IBOutlet weak var icon_mobile_no_view: UIView!
    @IBOutlet weak var icon_email_view: UIView!
    @IBOutlet weak var icon_password_view: UIView!
    @IBOutlet weak var icon_state_view: UIView!
    @IBOutlet weak var icon_city_view: UIView!
    @IBOutlet weak var icon_re_enter_password: UIView!
    @IBOutlet weak var state_view_border: UIView!
    @IBOutlet weak var city_view_border: UIView!
    @IBOutlet weak var txt_first_name: KTextField!
    @IBOutlet weak var txt_last_name: KTextField!
    @IBOutlet weak var txt_mobile_no: KTextField!
    @IBOutlet weak var txt_email_id: KTextField!
    @IBOutlet weak var txt_password: KTextField!
    @IBOutlet weak var txtReEnterPassword: KTextField!
    @IBOutlet weak var lblPhoneCode: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var country_Border_View: UIView!
    @IBOutlet weak var icon_country_View: UIView!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var heightStateAndCityView: NSLayoutConstraint!
        
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    fileprivate var selectedDropDown : AMDropDownType?
    var dropDown = DropDown()
    var mainDataArray = [[String:Any]]()
    var city_ID : String?
    var registrationDataDict = [String:Any]()
    
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Register", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        
        ShowDropDown()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dropDown.hide()
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        addBackButton()
        ViewBorder()
        addResetButton()
        btnRegister.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        
        txt_first_name.returnKeyType = .next
        txt_last_name.returnKeyType = .next
        txt_mobile_no.returnKeyType = .next
        txt_email_id.returnKeyType = .next
        txt_password.returnKeyType = .next
        txtReEnterPassword.returnKeyType = .done
        
    }
    
    
    //MARK:- ********************** UIVIEW BORDER **********************
    
    func ViewBorder() {
        
        icon_first_name_view.setBorder()
        icon_last_name_view.setBorder()
        icon_mobile_no_view.setBorder()
        icon_email_view.setBorder()
        icon_password_view.setBorder()
        icon_state_view.setBorder()
        icon_city_view.setBorder()
        icon_re_enter_password.setBorder()
        state_view_border.setBorder()
        city_view_border.setBorder()
        country_Border_View.setBorder()
        icon_country_View.setBorder()
    }
    
    //MARK:- ********************** RESET TEXTFIELD **********************
    
    func Reset()
    {
        txt_first_name.text = ""
        txt_last_name.text = ""
        txt_mobile_no.text = ""
        txt_email_id.text = ""
        txt_password.text = ""
        txtReEnterPassword.text = ""
//        lblCountry.text = "Select Country"
        lblState.text = "Select State"
        lblCity.text = "Select City"
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    @IBAction func press_btn_phoneCode(_ sender: UIButton) {
        view.endEditing(true)
        selectedDropDown = AMDropDownType.countryCode
        dropDown.anchorView = self.lblPhoneCode
        dropDown.dataSource = phoneCodeArray as! [String] //["+91"]
        dropDown.show()
    }
    
    @IBAction func press_btn_select_Country(_ sender: UIButton) {
        view.endEditing(true)
        selectedDropDown = AMDropDownType.country
        dropDown.anchorView = self.lblCountry
        dropDown.dataSource = countryArray.value(forKey: "name") as! [String]
        dropDown.show()
    }
    
    @IBAction func press_btn_select_state(_ sender: UIButton) {
        if lblCountry.text == "Select Country"
        {
            self.presentAlertWithTitle(title: "Please select country", message: "", options: "OK", completion: { (ButtonIndex) in
            })
        }else
        {
            view.endEditing(true)
            selectedDropDown = AMDropDownType.state
            dropDown.anchorView = self.lblState
            dropDown.dataSource = stateArray.value(forKey: "name") as! [String]
            dropDown.show()
        }
        
    }
    
    @IBAction func press_btn_select_city(_ sender: UIButton) {
        
        if lblCountry.text == "Select Country"
        {
            self.presentAlertWithTitle(title: "Please select country", message: "", options: "OK", completion: { (ButtonIndex) in
            })
        }else if lblState.text == "Select State"
        {
            self.presentAlertWithTitle(title: "Please select state", message: "", options: "OK", completion: { (ButtonIndex) in
            })
        }else
        {
            view.endEditing(true)
            selectedDropDown = AMDropDownType.city
            dropDown.anchorView = self.lblState
            dropDown.dataSource = cityArray.value(forKey: "name") as! [String]
            dropDown.show()
        }
        
    }
    
    @IBAction func press_btn_register(_ sender: UIButton) {
        
        if isReadyToRegister()
        {
            self.view.endEditing(true)
            
            registrationDataDict = ["firstname": "\(txt_first_name.text ?? "")",
                "lastname" : "\(txt_last_name.text ?? "")",
                "mobilenumber" : "\(txt_mobile_no.text ?? "")",
                "emailId" : "\(txt_email_id.text ?? "")",
                "password" : "\(txt_password.text ?? "")",
                "reenterpassword" : "\(txtReEnterPassword.text ?? "")",
                "country" : "\(lblCountry.text ?? "")",
                "state" : "\(lblState.text ?? "")",
                "cityID" : "\(city_ID ?? "")"] as [String:Any]
            
            
            //            verifyMobileNumber()
            
            check_SIM_Available_OR_Not()
        }
        
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addResetButton() {
        let resetButton = UIButton(type: .custom)
        resetButton.setImage(UIImage(named: "ic_reset"), for: .normal)
        resetButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        resetButton.addTarget(self, action: #selector(self.resetAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: resetButton)
    }
    
    @objc func resetAction(_ sender: UIButton) {
        Reset()
    }
    
    //MARK:- ********************** DROP DOWN **********************
    
    func ShowDropDown() {
        
        //        self.lblPhoneCode.text = "+\((phoneCodeArray[0] as! String).digitsOnly())"
        
        var stateDict = NSDictionary()
        
        // Managed region from SignInVC and RegionVC get selected region and managed stateArray for perticular Country
        for i in 0..<phoneCodeArray.count
        {
            let regionCode = ((self.removeSpecialCharsFromString(text: phoneCodeArray[i] as! String)).slice(from: "(", to: ")") ?? "")
            
            if userDefault.string(forKey: "RegionCode") == regionCode
            {
                let selectRegion = phoneCodeArray[i] as? String
                var region = selectRegion?.components(separatedBy: " ")
                
                self.lblPhoneCode.text = region?.first
                
                region!.removeFirst()
                region!.removeFirst()
                
                self.lblCountry.text = region!.joined(separator: " ") //strSelectedRegion
                
                stateDict = countryArray.object(at: i) as! NSDictionary
                stateArray = (stateDict.value(forKey: "states") as! NSArray).mutableCopy() as? NSMutableArray ?? NSMutableArray()
                
                userDefault.removeObject(forKey: "RegionCode")
                userDefault.set(regionCode, forKey: "RegionCode")
                
                // Set Help Video URL
                videoURL = (userDefault.string(forKey: "RegionCode") == "IN") ? indiaVideoURl : otherCountryVideoURl

                // Remove and Add Country Id
                let countryID = stateDict.value(forKey: "id") as? Int ?? 0
                userDefault.removeObject(forKey: "countryId")
                userDefault.set(countryID, forKey: "countryId")
                
                // Hide state And City Field when User is Out Of India
                
//                if userDefault.string(forKey: "RegionCode") == "IN"
//                {
//                    self.heightStateAndCityView.constant =  DeviceType.IS_IPAD ? 180.0 : 120.0
//                    self.state_view_border.isHidden = false
//                    self.city_view_border.isHidden = false
//                }else
//                {
//                    self.heightStateAndCityView.constant = 0.0
//                    self.state_view_border.isHidden = true
//                    self.city_view_border.isHidden = true
//                }
            }
        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            print("Selected item: \(item) at index: \(index)")
            
            if self.selectedDropDown == AMDropDownType.countryCode {
                
                isLogin = true
                isFromMallCategory = true
                
                // Get Phonecode
                self.lblPhoneCode.text = "+\(item.digitsOnly())"
                
                // Managed region , country code and country
                let regionCode = ((self.removeSpecialCharsFromString(text: phoneCodeArray[index] as! String)).slice(from: "(", to: ")") ?? "")
                
                let selectRegion = phoneCodeArray[index] as? String
                var region = selectRegion?.components(separatedBy: " ")
                
                region!.removeFirst()
                region!.removeFirst()
                
                stateDict = countryArray.object(at: index) as! NSDictionary
                stateArray = (stateDict.value(forKey: "states") as! NSArray).mutableCopy() as? NSMutableArray ?? NSMutableArray()
                
                userDefault.removeObject(forKey: "RegionCode")
                userDefault.set(regionCode, forKey: "RegionCode")
                
                // Set Help Video URL
                videoURL = (userDefault.string(forKey: "RegionCode") == "IN") ? indiaVideoURl : otherCountryVideoURl

                // Remove and Add Country Id
                let countryID = stateDict.value(forKey: "id") as? Int ?? 0
                userDefault.removeObject(forKey: "countryId")
                userDefault.set(countryID, forKey: "countryId")
                
                if self.lblCountry.text != region!.joined(separator: " ")
                {
                    self.lblState.text = "Select State"
                    self.lblCity.text = "Select City"
                    self.txt_mobile_no.text = ""

                    self.lblCountry.text = region!.joined(separator: " ") //strSelectedRegion
                }
                
                // Hide state And City Field when User is Out Of India
                
//                if userDefault.string(forKey: "RegionCode") == "IN"
//                {
//                    self.heightStateAndCityView.constant =  DeviceType.IS_IPAD ? 180.0 : 120.0
//                    self.state_view_border.isHidden = false
//                    self.city_view_border.isHidden = false
//                }else
//                {
//                    self.heightStateAndCityView.constant = 0.0
//                    self.state_view_border.isHidden = true
//                    self.city_view_border.isHidden = true
//                }
                
            }else if self.selectedDropDown == AMDropDownType.country {
                
                isLogin = true
                isFromMallCategory = true
                
                if self.lblCountry.text != item
                {
                    self.lblState.text = "Select State"
                    self.lblCity.text = "Select City"

                    self.txt_mobile_no.text = ""
                }
                
                // Managed region , country code and country
                let regionCode = ((self.removeSpecialCharsFromString(text: phoneCodeArray[index] as! String)).slice(from: "(", to: ")") ?? "")
                
                let selectRegion = phoneCodeArray[index] as? String
                let region = selectRegion?.components(separatedBy: " ")
                
                self.lblPhoneCode.text = region?.first
                
                // Get country name
                self.lblCountry.text = item
                
                // Get state name in dropdown
                
                stateDict = countryArray.object(at: index) as! NSDictionary
                stateArray = (stateDict.value(forKey: "states") as! NSArray).mutableCopy() as? NSMutableArray ?? NSMutableArray()
                
                userDefault.removeObject(forKey: "RegionCode")
                userDefault.set(regionCode, forKey: "RegionCode")
                
                // Set Help Video URL
                videoURL = (userDefault.string(forKey: "RegionCode") == "IN") ? indiaVideoURl : otherCountryVideoURl

                // Remove and Add Country Id
                let countryID = stateDict.value(forKey: "id") as? Int ?? 0
                userDefault.removeObject(forKey: "countryId")
                userDefault.set(countryID, forKey: "countryId")
                
                // Hide state And City Field when User is Out Of India
                
//                if userDefault.string(forKey: "RegionCode") == "IN"
//                {
//                    self.heightStateAndCityView.constant =  DeviceType.IS_IPAD ? 180.0 : 120.0
//                    self.state_view_border.isHidden = false
//                    self.city_view_border.isHidden = false
//                }else
//                {
//                    self.heightStateAndCityView.constant = 0.0
//                    self.state_view_border.isHidden = true
//                    self.city_view_border.isHidden = true
//                }
                
            }else if self.selectedDropDown == AMDropDownType.state {
                
                if self.lblState.text != item
                {
                    self.lblCity.text = "Select City"
                }
                
                // Get state name
                self.lblState.text = item
                
                // Get city name in dropdown
                cityArray = ((stateArray.object(at: index) as! NSDictionary).value(forKey: "cities") as! NSArray).mutableCopy() as? NSMutableArray ?? NSMutableArray()
                //                self.lblCity.text = "Select City"
                
            }else if self.selectedDropDown == AMDropDownType.city {
                
                // Get city name
                self.lblCity.text = item
                
                let cityDict = cityArray.object(at: index) as! NSDictionary
                
                self.city_ID = "\(cityDict.value(forKey: "id") ?? "")"
                
            }
        }
    }
    
    //MARK:- ********************** VALIDATION **********************
    
    func isReadyToRegister()-> Bool {
        
        if Validation.isStringEmpty(txt_first_name.text!) || txt_first_name.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter first name", message: "", options: "OK") { (Int) in
                self.txt_first_name.becomeFirstResponder()
            }
        }
        else if Validation.isStringEmpty(txt_last_name.text!) || txt_last_name.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter last name", message: "", options: "OK") { (Int) in
                self.txt_last_name.becomeFirstResponder()
            }
        }else if Validation.isStringEmpty(txt_mobile_no.text!) || txt_mobile_no.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter mobile number", message: "", options: "OK") { (Int) in
                self.txt_mobile_no.becomeFirstResponder()
            }
        }else if userDefault.string(forKey: "RegionCode") == "IN" && Int(txt_mobile_no.text!.count) < 10
        {
            self.presentAlertWithTitle(title: "Please enter 10 digit mobile number", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txt_mobile_no.becomeFirstResponder()
            })
        }else if userDefault.string(forKey: "RegionCode") == "SA" && Int(txt_mobile_no.text!.count) != 9
        {
            self.presentAlertWithTitle(title: "Please enter 9 digit mobile number", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txt_mobile_no.becomeFirstResponder()
            })
        }else if Validation.isStringEmpty(txt_email_id.text!) || txt_email_id.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter email id", message: "", options: "OK") { (Int) in
                self.txt_email_id.becomeFirstResponder()
            }
        }else if !Validation.isValidEmail(emailString: txt_email_id.text!)
        {
            presentAlertWithTitle(title: "Please enter valid email id", message: "", options: "OK") { (Int) in
                self.txt_email_id.becomeFirstResponder()
            }
        }else if Validation.isStringEmpty(txt_password.text!) || txt_password.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter password", message: "", options: "OK") { (Int) in
                self.txt_password.becomeFirstResponder()
            }
        }else if Int(txt_password.text!.count) < 6
        {
            self.presentAlertWithTitle(title: "Password must be more than 6 digit", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txt_password.becomeFirstResponder()
            })
        }else if Validation.isStringEmpty(txtReEnterPassword.text!) || txtReEnterPassword.text?.trim() == ""{
            presentAlertWithTitle(title: "Please re enter password", message: "", options: "OK") { (Int) in
                self.txtReEnterPassword.becomeFirstResponder()
            }
        }else if Validation.isPassWordMatch(pass: txt_password.text!, confPass: txtReEnterPassword.text!)
        {
            presentAlertWithTitle(title: "New password and confirm password are not same", message: "", options: "OK") { (Int) in
                self.txtReEnterPassword.becomeFirstResponder()
            }
        }else if lblCountry.text == "Select Country" {
            presentAlertWithTitle(title: "Please select country", message: "", options: "OK") { (Int) in
            }
        }else if lblState.text == "Select State" //&& userDefault.string(forKey: "RegionCode") == "IN"
        {
            presentAlertWithTitle(title: "Please select state", message: "", options: "OK") { (Int) in
            }
        }else if lblCity.text == "Select City" //&& userDefault.string(forKey: "RegionCode") == "IN"
        {
            presentAlertWithTitle(title: "Please select city", message: "", options: "OK") { (Int) in
            }
        }
        else{
            
            return true
        }
        
        return false
    }
    
    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txt_first_name
        {
            textField.resignFirstResponder()
            txt_last_name.becomeFirstResponder()
        }else if textField == txt_last_name
        {
            textField.resignFirstResponder()
            txt_mobile_no.becomeFirstResponder()
        }else if textField == txt_mobile_no
        {
            textField.resignFirstResponder()
            txt_email_id.becomeFirstResponder()
        }else if textField == txt_email_id
        {
            textField.resignFirstResponder()
            txt_password.becomeFirstResponder()
        }else if textField == txt_password
        {
            textField.resignFirstResponder()
            txtReEnterPassword.becomeFirstResponder()
        }else
        {
            txtReEnterPassword.resignFirstResponder()
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        
        if (string == " ") {
            return false
        }
        
        if textField == txt_first_name || textField == txt_last_name
        {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS_OnlyAlphabet).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        
        textField.autocorrectionType = .no
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if textField.tag == 1
        {
            switch string
            {
            case  "0","1","2","3","4","5","6","7","8","9":
                
                if userDefault.string(forKey: "RegionCode") == "IN"
                {
                    if newLength <= 10
                    {
                        return true
                    } else {
                        return false
                    }
                } else if userDefault.string(forKey: "RegionCode") == "SA"
                {
                    if newLength <= 9
                    {
                        return true
                    } else {
                        return false
                    }
                }
                
            case "":
                return true && newLength <= 50
                
            default:
                let array = [string]
                if array.count == 0
                {
                    return true
                }
                return false && newLength <= 4
            }
        }else if textField.tag == 2
        {
            if newLength <= 25
            {
                return true
            } else {
                return false
            }
        }
        return true
    }
    
    //MARK:- ********************** CHECK SIM CARD AVAILABLE OR NOT **********************
    
    func check_SIM_Available_OR_Not()
    {
        //                if UIApplication.shared.canOpenURL(URL(string: "tel://\(txt_mobile_no.text ?? "")")!) {
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier: CTCarrier? = networkInfo.subscriberCellularProvider
        let code: String? = carrier?.mobileNetworkCode
        if (code != nil) {
            verifyMobileNumber(isSimAvailable : true)
        }
        else {
            //                        verifyEmail()
            verifyMobileNumber(isSimAvailable : false)
        }
        
        //                }
        //                else {
        //                    self.presentAlertWithTitle(title: "Please Insert SIM card to get OTP", message: "", options: "OK", completion: { (ButtonIndex) in
        //                    })
        //                }
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func verifyMobileNumber(isSimAvailable : Bool) {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)can_register?mobile=\(txt_mobile_no.text ?? "")&email=\(txt_email_id.text ?? "")", parameters: nil) { (APIResponce, Responce, error1) in
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    
                    if responceDict["status"] as! Int == 0
                    {
                        self.RemoveLoader()
                        
                        self.presentAlertWithTitle(title: responceDict["message"] as? String ?? "", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                    else
                    {
                        isFromRegistration = true
                        print(isBulk_SMS)
                        
                        if isBulk_SMS == "1" || !isSimAvailable
                        {
                            isBulk_SMS = "1"
                            
                            // OTP verify using Bulk SMS
                            
                            if userDefault.string(forKey: "RegionCode") == "IN" || userDefault.string(forKey: "RegionCode") == "SA"
                            {
                                self.Bulk_SMS_Mobile_Verify_API(mobileno: self.txt_mobile_no.text ?? "", email: "")
                            }else
                            {
                                self.Bulk_SMS_Mobile_Verify_API(mobileno: "", email: self.txt_email_id.text ?? "")
                            }
                        }
                        else if isBulk_SMS == "0"
                        {
                            // OTP verify using Firebase
                            
                            if userDefault.string(forKey: "RegionCode") == "IN" || userDefault.string(forKey: "RegionCode") == "SA"
                            {
                                self.Firebase_Mobile_Verify_API()
                            }else
                            {
                                self.Bulk_SMS_Mobile_Verify_API(mobileno: "", email: self.txt_email_id.text ?? "")
                            }
                        }
                        
                    }
                }
                else
                {
                    self.RemoveLoader()
                    
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    
    //MARK:- ********************** FIREBASE MOBILE NUMBER VERIFY **********************
    
    func Firebase_Mobile_Verify_API()
    {
        
        PhoneAuthProvider.provider().verifyPhoneNumber("\(self.lblPhoneCode.text ?? "")"+""+"\(self.txt_mobile_no.text!)", uiDelegate: nil) { (verificationID, error) in
            
            self.RemoveLoader()
            
            if ((error) != nil) {
                // Verification code not sent.
                //                print(error)
                
                self.presentAlertWithTitle(title: (error?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                })
                
            } else {
                // Successful. -> it's sucessfull here
                print("VERIFICATION ID\(verificationID ?? "")")
                
                userDefault.set(verificationID!, forKey: "VarificationId")
                let OTPVC = mainStoryBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                mobileNumber = "\(self.txt_mobile_no.text ?? "")"
                emailId = "\(self.txt_email_id.text ?? "")"
                
                //                isVerificationUsingMobile = true
                OTPVC.isDictFromRegistration = self.registrationDataDict
                self.pushViewController(OTPVC)
            }
        }
    }
    
    //MARK:- ********************** BULK SMS MOBILE NUMBER VERIFY **********************
    
    func Bulk_SMS_Mobile_Verify_API(mobileno: String, email: String)
    {
        if Reachability.isConnectedToNetwork() {
            
            //            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = [
                "mobile_number": mobileno,
                 "email": email,
                 "is_international": userDefault.string(forKey: "RegionCode") == "IN" ? "0" : "1"
                ] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)send_otp", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.dismiss(animated: true, completion: nil)
                        
                        let dataArray = responceDict["data"] as! [String:Any]
                        print(dataArray)
                        
                        let otpCode = dataArray["otp"] as! Int
                        
                        let OTPVC = mainStoryBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                        mobileNumber = "\(self.txt_mobile_no.text ?? "")"
                        OTPVC.otpVerify = otpCode
                        emailId = "\(self.txt_email_id.text ?? "")"
                        
                        //                        isVerificationUsingMobile = true
                        OTPVC.isDictFromRegistration = self.registrationDataDict
                        self.pushViewController(OTPVC)
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ()")
        return String(text.filter {okayChars.contains($0) })
    }
    
    
    //    func verifyEmail() {
    //        if Reachability.isConnectedToNetwork() {
    //
    //            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
    //
    //            let ParamDict = ["email": "\(txt_email_id.text ?? "")",
    //                            "verifyEmail": 0] as NSDictionary
    //
    //            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)send_otp_email", parameters: ParamDict) { (APIResponce, Responce, error1) in
    //
    //                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
    //                {
    //                    isFromRegistration = true
    //
    //                    let responceDict = Responce as! [String:Any]
    //
    //                    if responceDict["ResponseCode"] as! String == "1"
    //                    {
    //                        let otpDict = responceDict["data"] as? [String:Any]
    //
    //                        let OTPVC = mainStoryBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
    //
    //                        //store otp for further verification
    //                        strOTPForEmailVerification = "\(otpDict?["otp"] ?? "")"
    //                        emailId = "\(self.txt_email_id.text ?? "")"
    ////                        isVerificationUsingMobile = false
    //                        OTPVC.isDictFromRegistration = self.registrationDataDict
    //                        self.pushViewController(OTPVC)
    //                    }
    //                    else
    //                    {
    //                        self.topMostViewController().view.makeToast("\(responceDict["ResponseMessage"] ?? "")", duration: 3.0, position: .bottom)
    //                    }
    //                }
    //                else
    //                {
    //                    self.topMostViewController().view.makeToast((error1?.localizedDescription ?? ""), duration: 2.0, position: .bottom)
    //                }
    //                self.RemoveLoader()
    //            }
    //        }
    //        else
    //        {
    //            self.RemoveLoader()
    //
    //            self.topMostViewController().view.makeToast("Internet is slow. Please check internet connection.", duration: 2.0, position: .bottom)
    //        }
    //    }
}
