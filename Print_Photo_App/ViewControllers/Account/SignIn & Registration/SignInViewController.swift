//
//  SignInViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 30/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController
import Firebase
import CoreTelephony
import DropDown
import GoogleMobileAds
import FirebaseAuth

fileprivate enum AMDropDownType {
    case phoneCode
    case region
}

class SignInViewController: BaseViewController,UITextFieldDelegate,UITabBarControllerDelegate,GADBannerViewDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var txtMobileNo: KTextField!
    @IBOutlet weak var txtPassword: KTextField!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var mobile_no_icon_view: UIView!
    @IBOutlet weak var password_icon_view: UIView!
    @IBOutlet var forgot_password_view: UIView!
    @IBOutlet weak var txt_forgot_password_mobile_no: KTextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var mobileNumber_borderView: UIView!
    @IBOutlet weak var phoneCode_borderView: UIView!
    @IBOutlet weak var lblPhoneCode: UILabel!
    @IBOutlet var emailVerificationView: UIView!
    @IBOutlet weak var txtEmail: KTextField!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var constraintsBannerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomScrollview: NSLayoutConstraint!
    @IBOutlet weak var selectRegionBorderView: UIView!
    @IBOutlet weak var iconRegionBorderView: UIView!
    @IBOutlet weak var lblRegion: UILabel!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var getKeyBoardHeight : CGFloat!
    fileprivate var selectedDropDown : AMDropDownType?
    var dropDown = DropDown()
    var regionCode : String?

    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Sign In", EndString: "")
        
        regionCode = userDefault.string(forKey: "RegionCode")
        
        Initialize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        navigationController?.setNavigationBarHidden(true, animated: false)

//        set_banner()
//        bannerView.isHidden = true

        self.tabBarController?.tabBar.isHidden = (tabBarController?.selectedIndex == 2 || tabBarController?.selectedIndex == 3 || isLoginOrNot == "cart_login" || isLoginOrNot == "order_login") ? false : true
        
        self.navigationItem.setHidesBackButton(false, animated:false)
        self.navigationController?.navigationBar.isHidden = false

        Reset_TextField()

        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            // Edit region code when user is login
            userDefault.removeObject(forKey: "RegionCode")
            userDefault.set(regionCode, forKey: "RegionCode")
            userDefault.synchronize()
            
            let myAccountVC = mainStoryBoard.instantiateViewController(withIdentifier: "MyAccountViewController") as! MyAccountViewController
            self.navigationController?.pushViewController(myAccountVC, animated: false)
        }
        ShowDropDown()
    }
 
    override func viewWillDisappear(_ animated: Bool) {
        isFromMugModule = false
        dropDown.hide()
    }
    
    override func viewDidDisappear(_ animated: Bool) {

        self.stopAnimating()

        //        navigationController?.setNavigationBarHidden(false, animated: false)
        txt_forgot_password_mobile_no.endEditing(true)
//        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize()
    {
 
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)

        buttonShadow()
        ViewBorder()
        TextFiled_Corner_Readius()
        forgot_password_view_size()
        addBackButton()
        removeBackButton()
        
        btnSignIn.isExclusiveTouch = true
        btnRegister.isExclusiveTouch = true
        txtMobileNo.delegate = self
        txtPassword.delegate = self
        txt_forgot_password_mobile_no.delegate = self

        txtMobileNo.returnKeyType = .next
        txtPassword.returnKeyType = .done
        
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow),name: UIResponder.keyboardWillShowNotification,object: nil)

        self.hideKeyboardWhenTappedAround()
        
    }
    
    func Reset_TextField() {
        self.txtMobileNo.text = ""
        self.txtPassword.text = ""
        self.txt_forgot_password_mobile_no.text = ""
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    @IBAction func press_btnRegion(_ sender: UIButton) {
        view.endEditing(true)
        selectedDropDown = AMDropDownType.region
        dropDown.anchorView = self.lblRegion
        
        let arrRegion = NSMutableArray()
        
        // Remove Country code from string of array
        for i in 0..<phoneCodeArray.count
        {
            let selectRegion = phoneCodeArray[i] as? String
            var region = selectRegion?.components(separatedBy: " ")
            region!.removeFirst()
            let strRegion = region!.joined(separator: " ")
            arrRegion.add(strRegion)
            
            dropDown.dataSource = arrRegion as! [String]
        }
        dropDown.show()
    }
    
    @IBAction func press_btn_send_email(_ sender: UIButton) {
        
        // Now, Disable Email Verification functionality
        if isSendOTPInEmail()
        {
            verifyMobileNumber(isSimAvailable: true)
        }
    }
    
    @IBAction func press_btn_phoneCode(_ sender: UIButton) {
        view.endEditing(true)
        selectedDropDown = AMDropDownType.phoneCode
        dropDown.anchorView = self.lblPhoneCode
        dropDown.dataSource = ["+91"]
        dropDown.show()
    }
    
    @IBAction func press_btn_forgot_password(_ sender: UIButton) {
        
        if regionCode == "IN" || regionCode == "SA"
        {
            txt_forgot_password_mobile_no.text = ""
            txt_forgot_password_mobile_no.becomeFirstResponder()
            let TFPopup = MZFormSheetPresentationViewController(contentView: forgot_password_view)
            TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
            TFPopup.presentationController?.shouldCenterVertically = false
            TFPopup.contentViewControllerTransitionStyle = .slideAndBounceFromBottom
            self.present(TFPopup, animated: true)
        }else
        {
            txtEmail.text = ""
            txtEmail.becomeFirstResponder()
            let TFPopup = MZFormSheetPresentationViewController(contentView: emailVerificationView)
            TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
            TFPopup.presentationController?.shouldCenterVertically = false
            TFPopup.contentViewControllerTransitionStyle = .slideAndBounceFromBottom
            self.present(TFPopup, animated: true)
        }
      
    }
    
    @IBAction func press_btn_sign_in(_ sender: UIButton) {
        
        if isReadyToLogin()
        {
            Call_Sign_In_API()
        }
    }
    
    @IBAction func press_btn_register(_ sender: UIButton) {
        // Edit region code when user is login
        userDefault.removeObject(forKey: "RegionCode")
        userDefault.set(regionCode, forKey: "RegionCode")
        userDefault.synchronize()
        
        let RegisterVC = mainStoryBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        pushViewController(RegisterVC)
    }
    
    @IBAction func press_btn_SEND_OTP(_ sender: UIButton) {
        
        if isSendOTP()
        {
//            verifyMobileNumber()
            check_SIM_Available_OR_Not()
        }
        
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
        if tabBarController?.selectedIndex == 3 || tabBarController?.selectedIndex == 4
        {
            ((tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)
            self.tabBarController?.selectedIndex = 0
        }
        else if isLoginOrNot == "cart_login"
        {
            let VCarray = self.navigationController?.viewControllers
            for Controller in VCarray! {
                if Controller is CartViewController {
                    self.navigationController?.popToViewController(Controller, animated: true)
                }
            }
        }else if isLoginOrNot == "order_login"
        {
            let VCarray = self.navigationController?.viewControllers
            for Controller in VCarray! {
                if Controller is OrderViewController {
                    self.navigationController?.popToViewController(Controller, animated: true)
                }
            }
        }else if isLoginOrNot == "edit_login"
        {
            if isFromMugModule
            {
                // Managed orientation go to Edit Mug module
                //                                (UIApplication.shared.delegate as! AppDelegate).allowOrientations = 1
                //                                let value = UIInterfaceOrientation.landscapeRight.rawValue
                //                                UIDevice.current.setValue(value, forKey: "orientation")
                
                AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
                
            }
            
//            self.tabBarController?.selectedIndex = 0
            
            self.popViewController()
           
        }else if isLoginOrNot == "mobile_company_login"
        {
            if isLogin
            {
                ((tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)
                self.tabBarController?.selectedIndex = 0
                
                isLogin = false
            }else
            {
                let VCarray = self.navigationController?.viewControllers
                for Controller in VCarray! {
                    if Controller is RequestBrandViewController {
                        self.navigationController?.popToViewController(Controller, animated: true)
                    }
                }
            }
           
        }else if isLoginOrNot == "mobile_model_login"
        {
            let VCarray = self.navigationController?.viewControllers
            for Controller in VCarray! {
                if Controller is MobileModelsViewController {
                    self.navigationController?.popToViewController(Controller, animated: true)
                }
            }
        }else if isLoginOrNot == "mall_add_to_cart_login"
        {
            if isFromMallCategory
            {
                // Come from MallHomeViewController
                let VCarray = self.navigationController?.viewControllers
                for Controller in VCarray! {
                    if Controller is MallHomeViewController {
                        self.navigationController?.popToViewController(Controller, animated: true)
                    }
                }
            }else
            {
                let VCarray = self.navigationController?.viewControllers
                for Controller in VCarray! {
                    if Controller is MallAddToCartViewController {
                        self.navigationController?.popToViewController(Controller, animated: true)
                    }
                }
            }
        }else if isLoginOrNot == "categoryList_wishlist_icon"
        {
            if isFromMallCategory
            {
                // Come from MallHomeViewController
                let VCarray = self.navigationController?.viewControllers
                for Controller in VCarray! {
                    if Controller is MallHomeViewController {
                        self.navigationController?.popToViewController(Controller, animated: true)
                    }
                }
            }else
            {
                let VCarray = self.navigationController?.viewControllers
                for Controller in VCarray! {
                    if Controller is CategoryListViewController {
                        self.navigationController?.popToViewController(Controller, animated: true)
                    }
                }
            }
          
        }else if isLoginOrNot == "seller_information_login"
        {
            let VCarray = self.navigationController?.viewControllers
            for Controller in VCarray! {
                if Controller is SellerInformationViewController {
                    self.navigationController?.popToViewController(Controller, animated: true)
                }
            }
        }else if isLoginOrNot == "multiple_photo_frame_login"
        {
            let VCarray = self.navigationController?.viewControllers
            for Controller in VCarray! {
                if Controller is MultiplePhotoFrameVC {
                    self.navigationController?.popToViewController(Controller, animated: true)
                }
            }
        }else
        {
             self.popViewController()
        }
    }
 
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** BUTTON SHADOW **********************
    
    func buttonShadow() {
        btnSignIn.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        btnRegister.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
    }
    
    //MARK:- ********************** UIVIEW BORDER **********************
    
    func ViewBorder() {
        
        mobile_no_icon_view.setBorder()
        password_icon_view.setBorder()
        phoneCode_borderView.setBorder()
        selectRegionBorderView.setBorder()
        iconRegionBorderView.setBorder()
    }
    
    //MARK:- ********************** TEXTFIELD CORNER RADIUS  **********************
    
    func TextFiled_Corner_Readius() {
        txt_forgot_password_mobile_no.layer.cornerRadius = 3
    }
    
    //MARK:- ********************** FORGOT PASSWORD UIVIEW SIZE **********************
    
    func forgot_password_view_size() {
        if DeviceType.IS_IPAD
        {
            forgot_password_view.frame.size = CGSize(width: 450, height: 265)
            emailVerificationView.frame.size = CGSize(width: 450, height: 255)
        }
        else
        {
            forgot_password_view.frame.size = CGSize(width: 300, height: 180)
            emailVerificationView.frame.size = CGSize(width: 300, height: 170)
        }
    }
    
    //MARK:- ********************** DROP DOWN **********************
    
    func ShowDropDown() {
                
        self.txtMobileNo.text = ""
        
//        if !isSetIndex
//        {
//            // User without select region from dropdown
//            self.lblRegion.text = "\(self.removeSpecialCharsFromString(text: phoneCodeArray[0] as! String))"
//        }else
//        {
            // User select region from dropdown
        
        if regionCode == "" || regionCode == nil
        {
            self.lblRegion.text = (self.removeSpecialCharsFromString(text: phoneCodeArray[0] as! String)) //strSelectedRegion

        }else
        {
            // get region from select RegionVC for perticular index
            for i in 0..<phoneCodeArray.count
            {
                if regionCode == ((self.removeSpecialCharsFromString(text: phoneCodeArray[i] as! String)).slice(from: "(", to: ")") ?? "")
                {
                    self.lblRegion.text = (self.removeSpecialCharsFromString(text: phoneCodeArray[i] as! String)) //strSelectedRegion
                    
                }
            }
        }
        
        //        }

        // Set default first index without select index in drop down
        regionCode = "\(self.lblRegion.text!.slice(from: "(", to: ")") ?? "")"

        // Set Help Video URL
        videoURL = (regionCode == "IN") ? indiaVideoURl : otherCountryVideoURl

        self.lblPhoneCode.text = (regionCode == "SA") ? "+966" : "+91" //"+\((phoneCodeArray[0] as! String).digitsOnly())"
        phoneCode = (regionCode == "SA") ? "+966" : "+91"
        
        if regionCode == "IN" || regionCode == "SA"
        {
            self.txtMobileNo.keyboardType = .numberPad
            self.txtMobileNo.placeholder = "Enter Mobile No."
            
        }else
        {
            self.txtMobileNo.keyboardType = .asciiCapable
            self.txtMobileNo.placeholder = "Enter Email ID"
        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            print("Selected item: \(item) at index: \(index)")
           
            if self.selectedDropDown == AMDropDownType.phoneCode {
                
                // Get Phonecode
                self.lblPhoneCode.text = "+\(item.digitsOnly())"
                
                phoneCode = self.lblPhoneCode.text

            }else if self.selectedDropDown == AMDropDownType.region {
                
                isFromMallCategory = true
                isLogin = true
                
                self.txtMobileNo.text = ""
                self.txtPassword.text = ""
                
                let strRegion = self.removeSpecialCharsFromString(text: item)
                
                self.lblRegion.text = "\(strRegion)"
                
                // store selected region 
//                strSelectedRegion = self.lblRegion.text
//                isSetIndex = true

                self.regionCode = "\(self.lblRegion.text!.slice(from: "(", to: ")") ?? "")"

                // Edit region code when user is login
                userDefault.removeObject(forKey: "RegionCode")
                userDefault.set(self.regionCode, forKey: "RegionCode")
                
                self.lblPhoneCode.text = (self.regionCode == "SA") ? "+966" : "+91"
                
                // Set Help Video URL
                videoURL = (self.regionCode == "IN") ? indiaVideoURl : otherCountryVideoURl

                if self.regionCode == "IN" || self.regionCode == "SA"
                {
                    self.txtMobileNo.keyboardType = .numberPad
                    self.txtMobileNo.placeholder = "Enter Mobile No."
                }else
                {
                    self.txtMobileNo.keyboardType = .asciiCapable
                    self.txtMobileNo.placeholder = "Enter Email ID"
                }
            }
        }
    
    }
    
    //MARK:- ********************** VALIDATION **********************
    
    func isReadyToLogin()-> Bool {
        
        // Managed Validation When Sign in with mobile number and Email Id

        if regionCode == "IN"
        {
            // Mobile Number
            if Validation.isStringEmpty(txtMobileNo.text!) || txtMobileNo.text?.trim() == ""{
                
                presentAlertWithTitle(title: "Please enter mobile number", message: "", options: "OK") { (Int) in
                    self.txtMobileNo.becomeFirstResponder()
                }
            }else if Int(txtMobileNo.text!.count) < 10
            {
                presentAlertWithTitle(title: "Please enter 10 digit mobile number", message: "", options: "OK") { (Int) in
                    self.txtMobileNo.becomeFirstResponder()
                }
            }
            else if Validation.isStringEmpty(txtPassword.text!) || txtPassword.text?.trim() == ""{
                
                presentAlertWithTitle(title: "Please enter password", message: "", options: "OK") { (Int) in
                    self.txtPassword.becomeFirstResponder()
                }
            }
            else{
                return true
            }
        }
        else if regionCode == "SA"
        {
            // Mobile Number
            if Validation.isStringEmpty(txtMobileNo.text!) || txtMobileNo.text?.trim() == ""{
                
                presentAlertWithTitle(title: "Please enter mobile number", message: "", options: "OK") { (Int) in
                    self.txtMobileNo.becomeFirstResponder()
                }
            }else if Int(txtMobileNo.text!.count) != 9
            {
                presentAlertWithTitle(title: "Please enter 9 digit mobile number", message: "", options: "OK") { (Int) in
                    self.txtMobileNo.becomeFirstResponder()
                }
            }
            else if Validation.isStringEmpty(txtPassword.text!) || txtPassword.text?.trim() == ""{
                
                presentAlertWithTitle(title: "Please enter password", message: "", options: "OK") { (Int) in
                    self.txtPassword.becomeFirstResponder()
                }
            }
            else{
                return true
            }
        }
        else
        {
            // Email Id
            if Validation.isStringEmpty(txtMobileNo.text!) {
                presentAlertWithTitle(title: "Please enter email id", message: "", options: "OK") { (Int) in
                }
            }else if !Validation.isValidEmail(emailString: txtMobileNo.text!)
            {
                presentAlertWithTitle(title: "Please enter valid email id", message: "", options: "OK") { (Int) in
                }
            }
            else if Validation.isStringEmpty(self.txtPassword.text!) {

                presentAlertWithTitle(title: "Please enter password", message: "", options: "OK") { (Int) in
                    print("Password")
                }
            }
            else{
                return true
            }
        }
        return false
    }
    
    func isSendOTP()-> Bool {
        
        if Validation.isStringEmpty(txt_forgot_password_mobile_no.text!) || txt_forgot_password_mobile_no.text?.trim() == ""{
            
            self.topMostViewController().view.makeToast("Please enter mobile number", duration: 2.0, position: .bottom)
            
        }else if regionCode == "IN" && Int(txt_forgot_password_mobile_no.text!.count) < 10
        {
            self.topMostViewController().view.makeToast("Please enter 10 digit mobile number", duration: 2.0, position: .bottom)
        }
        else if regionCode == "SA" && Int(txt_forgot_password_mobile_no.text!.count) != 9
        {
            self.topMostViewController().view.makeToast("Please enter 9 digit mobile number", duration: 2.0, position: .bottom)
        }
        else{
            return true
        }
        
        return false
    }
    
    // Now, Disable Email Verification functionality

    func isSendOTPInEmail()-> Bool {
        
        if Validation.isStringEmpty(txtEmail.text!) || txtEmail.text?.trim() == ""{
            self.topMostViewController().view.makeToast("Please enter email id", duration: 2.0, position: .bottom)
        }else if !Validation.isValidEmail(emailString: txtEmail.text!)
        {
            self.topMostViewController().view.makeToast("Please enter valid email id", duration: 2.0, position: .bottom)
        }
        else{
            return true
        }
        
        return false
    }
    
    //MARK:- **********************  SHOW KEY-BOARD  **********************
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            getKeyBoardHeight = keyboardHeight
 
        }
    }
    
    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtMobileNo
        {
            textField.resignFirstResponder()
            txtPassword.becomeFirstResponder()
        }
        else{
            txtPassword.resignFirstResponder()
        }
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        textField.autocorrectionType = .no
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        // Set character limitation when Sign in with Mobile number
        if regionCode == "IN"
        {
            if textField.tag == 1
            {
                switch string
                {
                case  "0","1","2","3","4","5","6","7","8","9":
                    
                    if regionCode == "IN"
                    {
                        if newLength <= 10
                        {
                            return true
                        } else {
                            return false
                        }
                    }
                    
                case "":
                    return true && newLength <= 50
                    
                default:
                    let array = [string]
                    if array.count == 0
                    {
                        return true
                    }
                    return false && newLength <= 4
                }
            }
        } else if regionCode == "SA" {
            if textField.tag == 1
            {
                switch string
                {
                case  "0","1","2","3","4","5","6","7","8","9":
                    
                    if regionCode == "SA"
                    {
                        if newLength <= 9
                        {
                            return true
                        } else {
                            return false
                        }
                    }
                    
                case "":
                    return true && newLength <= 50
                    
                default:
                    let array = [string]
                    if array.count == 0
                    {
                        return true
                    }
                    return false && newLength <= 4
                }
            }
        }
        
        return true
    }
    
        //MARK:- ********************** CHECK SIM CARD AVAILABLE OR NOT **********************

            func check_SIM_Available_OR_Not()
            {
                    let networkInfo = CTTelephonyNetworkInfo()
                    let carrier: CTCarrier? = networkInfo.subscriberCellularProvider
                    let code: String? = carrier?.mobileNetworkCode
                
                    txt_forgot_password_mobile_no.resignFirstResponder()

                    if (code != nil) {
                    
                        verifyMobileNumber(isSimAvailable: true)
                    }
                    else {
                        /*
                        txtEmail.text = ""
                        txtEmail.becomeFirstResponder()
                        let TFPopup = MZFormSheetPresentationViewController(contentView: emailVerificationView)
                        TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
                        TFPopup.presentationController?.shouldCenterVertically = false
                        TFPopup.contentViewControllerTransitionStyle = .slideAndBounceFromBottom
                        
                        self.present(TFPopup, animated: true)
                        txtEmail.resignFirstResponder()
                        */
                        
                        verifyMobileNumber(isSimAvailable: false)

                        
//                        self.topMostViewController().view.makeToast("Please Insert SIM card to get OTP", duration: 3.0, position: .bottom)
                    }
            }
    
   
    
    //MARK:- ********************** API CALLING **********************
    
    
    func Call_Sign_In_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)

            // Sign in usign mobile number or email id, same as "mobile" parameter
            
            let ParamDict = ["mobile": "\(self.txtMobileNo.text ?? "")",
                            "password": txtPassword.text ?? "",
                             "device_token": deviceToken ?? "",
                             "device_type": "ios"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)login", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        
                        self.navigationController?.navigationBar.isHidden = false
                        
                        let dataArray = responceDict["data"] as! [String:Any]
                        print(dataArray)
                        
                        let userID = dataArray["user_id"] as! Int
                        userDefault.set(userID, forKey: "USER_ID")
                        
                        // Pass Dictionary to Edit Account View Controller
                        let editAccountDict = ["name" : dataArray["name"] ?? "",
                                               "mobileno" : dataArray["mobile_1"] ?? "",
                                               "email" : dataArray["email"] ?? "",
                                               "sellerID" : dataArray["seller_id"] ?? "",
                                               "token" : dataArray["token"] ?? "",
                                               "countryCode" : dataArray["country_code"] ?? "",
                                               "currencyID" : dataArray["currency_id"] ?? ""] as [String:Any]
                        
                        sellerID = "\(dataArray["seller_id"] ?? "")"
                        
                        // Edit region code when user is login
                        let countryCode = dataArray["country_code"] ?? ""
                        userDefault.removeObject(forKey: "RegionCode")
                        userDefault.set(countryCode, forKey: "RegionCode")
                        
                        // Edit Country ID when user is login
                        let countryID = "\(dataArray["country_id"] ?? 0)"
                        userDefault.removeObject(forKey: "countryId")
                        userDefault.set(countryID, forKey: "countryId")
                        
                        userDefault.set(sellerID, forKey: "sellerid")
                        
//                        let isApprove = "\(dataArray["is_approve"] ?? "")"
//
//                        userDefault.removeObject(forKey: "sellerTitle")

                        if sellerID != ""
                        {
                            strSeller = "Seller Wallet"
                        }else
                        {
                            strSeller = "Want to become a seller?"
                        }
                        
                        userDefault.set(strSeller, forKey: "sellerTitle")
                        
                        userDefault.set(editAccountDict, forKey: "EditAccountDict")
                        userDefault.synchronize()
                        
//                        if(self.regionCode == "SA" && self.tabBarController!.viewControllers!.count == 5)
                        if(self.tabBarController!.viewControllers!.count == 5)
                        {
//                            let indexToRemove = 1
//                            if indexToRemove <  self.tabBarController!.viewControllers!.count {
//                                var viewControllers =  self.tabBarController!.viewControllers
//                                viewControllers?.remove(at: indexToRemove)
//                                self.tabBarController!.viewControllers = viewControllers
//                            }
                        }
                        /*else if(self.regionCode != "SA" && self.tabBarController!.viewControllers!.count == 4)
                        {
                            let mallVC = self.storyboard?.instantiateViewController(withIdentifier: "myMallNavigation") as! UINavigationController
                            var viewControllers =  self.tabBarController!.viewControllers
                            viewControllers?.insert(mallVC, at: 1)
                            self.tabBarController!.viewControllers = viewControllers
                        }
                         */
                        
                        /*
                        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "homeVC") as! UINavigationController
                        if(self.tabBarController!.viewControllers!.count > 0)
                        {
                            self.tabBarController!.viewControllers![0] = homeVC
                        }
                        */
                        
                        // Set Badge Icon for Cart
                        appDelegate.SetBadgeIcon()

                        if isLoginOrNot == "cart_login"
                        {
                            ((self.tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
                            self.tabBarController?.selectedIndex = (self.regionCode == "SA") ? 1 : 2
                            
//                            let VCarray = self.navigationController?.viewControllers
//                            for Controller in VCarray! {
//                                if Controller is CartViewController {
//                                    self.navigationController?.popToViewController(Controller, animated: true)
//                                }
//                            }
                        }else if isLoginOrNot == "order_login"
                        {
                            ((self.tabBarController!.viewControllers! as NSArray)[1] as! UINavigationController).popToRootViewController(animated: false)
                            self.tabBarController?.selectedIndex = (self.regionCode == "SA") ? 0 : 1
                            
//                            let VCarray = self.navigationController?.viewControllers
//                            for Controller in VCarray! {
//                                if Controller is OrderViewController {
//                                    self.navigationController?.popToViewController(Controller, animated: true)
//                                }
//                            }
                        }else if isLoginOrNot == "edit_login"
                        {
                            if isFromMugModule
                            {
                                // Managed orientation go to Edit Mug module
//                                (UIApplication.shared.delegate as! AppDelegate).allowOrientations = 1
//                                let value = UIInterfaceOrientation.landscapeRight.rawValue
//                                UIDevice.current.setValue(value, forKey: "orientation")
                                
                                AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
                                
                            }
                            
//                            self.tabBarController?.selectedIndex = 0
                            self.popViewController()

                        }else if isLoginOrNot == "mobile_company_login"
                        {
                            if isLogin
                            {
                                ((self.tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)
                                self.tabBarController?.selectedIndex = 0
                                
                                isLogin = false
                            }else
                            {
                                let VCarray = self.navigationController?.viewControllers
                                for Controller in VCarray! {
                                    if Controller is RequestBrandViewController {
                                        self.navigationController?.popToViewController(Controller, animated: true)
                                    }
                                }
                            }
                        }else if isLoginOrNot == "mobile_model_login"
                        {
                            let VCarray = self.navigationController?.viewControllers
                            for Controller in VCarray! {
                                if Controller is MobileModelsViewController {
                                    self.navigationController?.popToViewController(Controller, animated: true)
                                }
                            }
                        }else if isLoginOrNot == "mall_add_to_cart_login"
                        {
                            if isFromMallCategory
                            {
                                // Come from MallHomeViewController
                                let VCarray = self.navigationController?.viewControllers
                                for Controller in VCarray! {
                                    if Controller is MallHomeViewController {
                                        self.navigationController?.popToViewController(Controller, animated: true)
                                    }
                                }
                            }else
                            {
                                let VCarray = self.navigationController?.viewControllers
                                for Controller in VCarray! {
                                    if Controller is MallAddToCartViewController {
                                        self.navigationController?.popToViewController(Controller, animated: true)
                                    }
                                }
                            }
                        }else if isLoginOrNot == "categoryList_wishlist_icon"
                        {
                            if isFromMallCategory
                            {
                                // Come from MallHomeViewController
                                let VCarray = self.navigationController?.viewControllers
                                for Controller in VCarray! {
                                    if Controller is MallHomeViewController {
                                        self.navigationController?.popToViewController(Controller, animated: true)
                                    }
                                }
                            }else
                            {
                                let VCarray = self.navigationController?.viewControllers
                                for Controller in VCarray! {
                                    if Controller is CategoryListViewController {
                                        self.navigationController?.popToViewController(Controller, animated: true)
                                    }
                                }
                            }
                           
                        }else if isLoginOrNot == "seller_information_login"
                        {
                            let VCarray = self.navigationController?.viewControllers
                            for Controller in VCarray! {
                                if Controller is SellerInformationViewController {
                                    self.navigationController?.popToViewController(Controller, animated: true)
                                }
                            }
                        }else if isLoginOrNot == "multiple_photo_frame_login"
                        {
                            let VCarray = self.navigationController?.viewControllers
                            for Controller in VCarray! {
                                if Controller is MultiplePhotoFrameVC {
                                    self.navigationController?.popToViewController(Controller, animated: true)
                                }
                            }
                        }else
                        {
                            // Edit region code when user is login
                            userDefault.removeObject(forKey: "RegionCode")
                            userDefault.set(self.regionCode, forKey: "RegionCode")
                            userDefault.synchronize()
                            
                            let myaccountVC = mainStoryBoard.instantiateViewController(withIdentifier: "MyAccountViewController") as! MyAccountViewController
                            self.pushViewController(myaccountVC)
                        }
                        
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "Log In", message: "\(responceDict["ResponseMessage"]!)", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
              
            })
        }
        
    }
    
    func verifyMobileNumber(isSimAvailable : Bool) {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            var strUrl : String?
            
            if regionCode == "IN" || regionCode == "SA"
            {
                // Used for india country
                strUrl = "\(BaseURL)can_send_otp?mobile=\(self.txt_forgot_password_mobile_no.text ?? "")&email="
                
            }else
            {
                // Used for out side of india country
               strUrl = "\(BaseURL)can_send_otp?mobile=&email=\(self.txtEmail.text ?? "")"

            }
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: strUrl ?? "", parameters: nil) { (APIResponce, Responce, error1) in

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["status"] as! Int == 1
                    {
                        isFromRegistration = false
                        
                        // HIDE KEY-BOARD
                        self.txt_forgot_password_mobile_no.resignFirstResponder()
                        self.dismiss(animated: true, completion: nil)
                        
                        print(isBulk_SMS)
                        
                        if isBulk_SMS == "1" || !isSimAvailable
                        {
                            isBulk_SMS = "1"
                            
                            // OTP verify using Bulk SMS
                            
                            if self.regionCode == "IN" || self.regionCode == "SA"
                            {
                                self.Bulk_SMS_Mobile_Verify_API(mobileno: self.txt_forgot_password_mobile_no.text ?? "", email: "")
                            }else
                            {
                                self.Bulk_SMS_Mobile_Verify_API(mobileno: "", email: self.txtEmail.text ?? "")
                            }
                            
                        }
                        else if isBulk_SMS == "0"
                        {
                            // OTP verify using Firebase
                            
                            if self.regionCode == "IN"
                            {
                                self.Firebase_Mobile_Verify_API()
                            }else
                            {
                                self.Bulk_SMS_Mobile_Verify_API(mobileno: "", email: self.txtEmail.text ?? "")
                            }
                        }
                    }
                    else
                    {
                        self.RemoveLoader()
                        self.topMostViewController().view.makeToast(responceDict["message"] as? String, duration: 3.0, position: .bottom)
                    }
                }
                else
                {
                    self.RemoveLoader()
                    self.topMostViewController().view.makeToast((error1?.localizedDescription ?? ""), duration: 2.0, position: .bottom)
                }
//                self.RemoveLoader()
            }
        }
        else
        {
            self.RemoveLoader()

            self.topMostViewController().view.makeToast("Internet is slow. Please check internet connection.", duration: 2.0, position: .bottom)
        }
    }
    
    
    //MARK:- ********************** FIREBASE MOBILE NUMBER VERIFY **********************
    
    func Firebase_Mobile_Verify_API()
    {
        showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)

        PhoneAuthProvider.provider().verifyPhoneNumber("\(self.lblPhoneCode.text ?? "")"+""+"\(self.txt_forgot_password_mobile_no.text!)", uiDelegate: nil) { (verificationID, error) in
            
            
            if ((error) != nil) {
                // Verification code not sent.
                //                print(error)
                
                self.RemoveLoader()

                self.presentAlertWithTitle(title: (error?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                })
                
            } else {
                // Successful. -> it's sucessfull here
                print("VERIFICATION ID\(verificationID ?? "")")
                
                userDefault.set(verificationID!, forKey: "VarificationId")
                
                // Edit region code when user is login
                userDefault.removeObject(forKey: "RegionCode")
                userDefault.set(self.regionCode, forKey: "RegionCode")
                userDefault.synchronize()
                
                let OTPVC = mainStoryBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                mobileNumber = "\(self.txt_forgot_password_mobile_no.text ?? "")"
                emailId = "\(self.txtEmail.text ?? "")"
//                isVerificationUsingMobile = true
                self.pushViewController(OTPVC)
            }
        }
    }
    
    //MARK:- ********************** BULK SMS MOBILE NUMBER VERIFY **********************
    
    func Bulk_SMS_Mobile_Verify_API(mobileno: String, email: String)
    {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = [
                "mobile_number": mobileno,
                "email": email,
                "is_international": regionCode == "IN" ? "0" : "1"
            ] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)send_otp", parameters: ParamDict) { (APIResponce, Responce, error1) in
                

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        let dataArray = responceDict["data"] as! [String:Any]
                        print(dataArray)
                        
                        let otpCode = dataArray["otp"] as! Int
                        
                        // Edit region code when user is login
                        userDefault.removeObject(forKey: "RegionCode")
                        userDefault.set(self.regionCode, forKey: "RegionCode")
                        userDefault.synchronize()
                        
                        let OTPVC = mainStoryBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                        mobileNumber = "\(self.txt_forgot_password_mobile_no.text ?? "")"
                        emailId = "\(self.txtEmail.text ?? "")"

//                        isVerificationUsingMobile = true
                        OTPVC.otpVerify = otpCode
                        self.pushViewController(OTPVC)
                    }
                    else
                    {
                        self.RemoveLoader()
                        self.topMostViewController().view.makeToast("\(responceDict["ResponseMessage"]!)", duration: 2.0, position: .bottom)
                    }
                }
                else
                {
                    self.RemoveLoader()
                    self.topMostViewController().view.makeToast("\(error1?.localizedDescription ?? "")", duration: 2.0, position: .bottom)
   
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
              
            })
        }
    }
    
    //MARK:- ********************** EMAIL THROUGH SEND OTP REQUEST **********************

    
//    func verifyEmail() {
//        if Reachability.isConnectedToNetwork() {
//
//            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
//
//
//            let ParamDict = ["email": "\(self.txtEmail.text ?? "")",
//                            "verifyEmail": 1] as NSDictionary
//
//            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)send_otp_email", parameters: ParamDict) { (APIResponce, Responce, error1) in
//
//                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
//                {
//                    let responceDict = Responce as! [String:Any]
//
//                    if responceDict["ResponseCode"] as! String == "1"
//                    {
//                        isFromRegistration = false
//
//                        // HIDE KEY-BOARD
//                        self.txtEmail.resignFirstResponder()
//                        self.dismiss(animated: true, completion: nil)
//
//                        let otpDict = responceDict["data"] as? [String:Any]
//
//                        let OTPVC = mainStoryBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
//
//                        //store otp for further verification
//                        strOTPForEmailVerification = "\(otpDict?["otp"] ?? "")"
//                        emailId = "\(self.txtEmail.text ?? "")"
////                        isVerificationUsingMobile = false
//                        self.pushViewController(OTPVC)
//                    }
//                    else
//                    {
//                        self.topMostViewController().view.makeToast("\(responceDict["ResponseMessage"] ?? "")", duration: 3.0, position: .bottom)
//                    }
//                }
//                else
//                {
//                    self.topMostViewController().view.makeToast((error1?.localizedDescription ?? ""), duration: 2.0, position: .bottom)
//                }
//                self.RemoveLoader()
//            }
//        }
//        else
//        {
//            self.RemoveLoader()
//
//            self.topMostViewController().view.makeToast("Internet is slow. Please check internet connection.", duration: 2.0, position: .bottom)
//        }
//    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ()")
        return String(text.filter {okayChars.contains($0) })
    }
    
//    //MARK:- ********************** BANNER AD **********************
//
//    func set_banner()
//    {
//        self.bannerView.adUnitID = banner_ID
//        self.bannerView.rootViewController = self
//        let request = GADRequest()
//        request.testDevices = ["ce0c8d1e1aa2d1ebcf636d56c2aa6571","eb0e2074bacb6396cd12bd015b847075","06d24ef8a171e533169c992149f07817","d14f6feee5647c7beae207686b72c6d6","4fb88b739b81cca87ff7060574247a87","97f940130f7863cd056bbff53204f9f2","dfa3bf19497c465906a7b6b7ff782f4d","f4fc2c1165675652c987515f68292322","78af841c2188dbd6256ecd991434e7a2",kGADSimulatorID]
//
//        self.bannerView.delegate = self
//        self.bannerView.load(request)
//    }
//
//    //MARK:- ********************** BANNER AD DELEGATE **********************
//
//    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
//        bannerView.isHidden = false
//
//        DispatchQueue.main.async {
//            if DeviceType.IS_IPAD
//            {
//                self.constraintsBannerViewHeight.constant = 75.0
//                self.bottomScrollview.constant = 75.0
//
//            }else{
//                self.constraintsBannerViewHeight.constant = 50.0
//                self.bottomScrollview.constant = 50.0
//
//            }
//        }
//
//    }
//
//    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
//        self.constraintsBannerViewHeight.constant = 0.0
//        self.bottomScrollview.constant = 0.0
//        bannerView.isHidden = true
//
//    }
}
