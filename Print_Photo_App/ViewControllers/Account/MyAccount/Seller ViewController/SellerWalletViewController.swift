//
//  SellerWalletViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 11/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import Social
import SystemConfiguration

class SellerWalletViewController: BaseViewController {

    //MARK:- ********************** OUTLATE DECLARATION **********************
    @IBOutlet weak var lblHeaderPromocode: UILabel!
    @IBOutlet weak var wallet_balance_shadowView: UIView!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var transactionHistory_shadowView: UIView!
    @IBOutlet weak var promocode_shadowView: UIView!
    @IBOutlet weak var lblPromocodeWithDiscount: UILabel!
    @IBOutlet weak var coupan_shadowView: UIView!
    @IBOutlet weak var dashedLineView: UIView!
    @IBOutlet weak var codeDashedView: UIView!
    @IBOutlet weak var btnShareNow: UIButton!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var share_promocode_shadowView: UIView!
    @IBOutlet weak var term_condition_shadowView: UIView!
    @IBOutlet weak var lblFlatRs: UILabel!
    @IBOutlet weak var lblSharePromocodeWithDiscount: UILabel!
    
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var transationDict = NSArray()
    var code : String?
    
    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()

        SetNavigationBarTitle(Startstring: "Seller Wallet", EndString: "")
//        btnShareNow.isExclusiveTouch = true

        Initialize()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
        addBackButton()
        addPrivacyButton()
        Set_View_Shadow()
        Call_Seller_Wallet_API()
    }
    
    //MARK:- ********************** VIEW SHADOW **********************

    func Set_View_Shadow() {
        wallet_balance_shadowView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        wallet_balance_shadowView.cornerRadius = 5.0
        
        transactionHistory_shadowView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        transactionHistory_shadowView.cornerRadius = 5.0
     
        promocode_shadowView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        promocode_shadowView.cornerRadius = 5.0
        
        coupan_shadowView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        coupan_shadowView.cornerRadius = 5.0
        
        share_promocode_shadowView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        share_promocode_shadowView.cornerRadius = 5.0
        
        term_condition_shadowView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        term_condition_shadowView.cornerRadius = 5.0
        
        btnShareNow.cornerRadius = 8.0

    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    @IBAction func press_header_btn_share(_ sender: UIButton) {
        
        let link = "https://itunes.apple.com/us/app/print-photo-phone-case-maker/id1458325325?ls=1&mt=8"
        let objectsToShare = ["Download this amazing print photo app from App Store \n\n Also apply this Promocode : \(self.code ??  "") and get amazing discount on your every order!!! \(link)"] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.setValue(appName, forKey: "subject")

        if DeviceType.IS_IPHONE
        {
            self.present(activityVC, animated: true, completion: nil)
        }else
        {
            let popController: UIPopoverPresentationController = activityVC.popoverPresentationController!
            popController.permittedArrowDirections = .any
            popController.sourceView = sender.superview
            popController.sourceRect = sender.frame
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func press_btn_TransactionHistory(_ sender: UIButton) {
        let TransactionHistoryVC = mainStoryBoard.instantiateViewController(withIdentifier: "TransactionHistoryViewController") as! TransactionHistoryViewController
        TransactionHistoryVC.transationDataArray = transationDict
        self.pushViewController(TransactionHistoryVC)
    }
    
    @IBAction func press_btn_shareNow(_ sender: UIButton) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            let coupanImage = self.coupan_shadowView.takeScreenshot()
            let objectsToShare = [coupanImage] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            if DeviceType.IS_IPHONE
            {
                self.present(activityVC, animated: true, completion: nil)
            }else
            {
                let popController: UIPopoverPresentationController = activityVC.popoverPresentationController!
                popController.permittedArrowDirections = .any
                popController.sourceView = sender.superview
                popController.sourceRect = sender.frame
                self.present(activityVC, animated: true, completion: nil)
            }
        }

    }
    
    @IBAction func press_btn_share_promocode(_ sender: UIButton) {
        UIApplication.shared.open(NSURL(string: "https://itunes.apple.com/us/app/print-photo-phone-case-maker/id1458325325?ls=1&mt=8")! as URL)
    }
    
    @IBAction func press_btn_Term_Condition(_ sender: UIButton) {

        UIApplication.shared.open(NSURL(string: sellerPrivacyPilicyURL)! as URL, options: [:], completionHandler: nil)

        
//        let PrivacyPolicyVC = mainStoryBoard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
//        self.pushViewController(PrivacyPolicyVC)
    }
    
    func addBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addPrivacyButton() {
        let privacyButton = UIButton(type: .custom)
        privacyButton.setImage(UIImage(named: "ic_term_condition2"), for: .normal)
        privacyButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        privacyButton.addTarget(self, action: #selector(self.privacyButtonAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: privacyButton)
    }
    
    @objc func privacyButtonAction(_ sender: UIButton) {

        UIApplication.shared.open(NSURL(string: sellerPrivacyPilicyURL)! as URL, options: [:], completionHandler: nil)
        
//        let PrivacyPolicyVC = mainStoryBoard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
//        self.pushViewController(PrivacyPolicyVC)
    }
    
    //MARK:- ********************** API CALLING **********************

    func Call_Seller_Wallet_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
//            let EditDict = userDefault.value(forKey: "EditAccountDict")! as! [String : Any]

            let ParamDict = ["seller_id": "\(userDefault.value(forKey: "sellerid") ?? "")"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)seller_wallet", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
 
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        let cash : Double = responceDict["cash"] as! Double
                        let withdraw : Double = responceDict["withdraw"] as! Double
                        
                        self.lblBalance.text = "₹ \(cash - withdraw)"
                        self.transationDict = responceDict["data"] as! NSArray

                    }else
                    {
                        self.lblBalance.text = "₹ 0.00"
                    }
                    
                    self.code = "\(responceDict["promo_code"]!)"
                    let promocode = "Promocode : \(self.code ?? "")"
                    self.lblHeaderPromocode.text = promocode
                    let dict = responceDict["user_price"] as! NSDictionary
                    let dicountAmt = "\(dict.value(forKey: "phone") ?? "")"
                    self.lblSharePromocodeWithDiscount.text = "Share Your Promocode & Get ₹"+"\(dicountAmt)"+" in Wallet"
                    
                    // Promocode with Discount Label
                    self.lblPromocodeWithDiscount.text = "Use This Promocode & Get ₹"+"\(dicountAmt)"+" INR Instant Discount : \(self.code ?? "")"
                    self.lblPromocodeWithDiscount.boldSubstring(self.code ?? "")
                    
                    // Flat Rs. Label
                    let stringValue = "Flat ₹\(dicountAmt) off"
                    let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
                    attributedString.setColorForText(textForAttribute: "₹50", withColor: UIColor.red)
                    self.lblFlatRs.attributedText = attributedString
                    
                    self.lblCode.text = "CODE : \(self.code ?? "")         "
                    
                    DispatchQueue.main.async {
                        self.dashedLineView.addDashedBorder()
                        self.codeDashedView.addDashedBorder()
                        self.lblCode.boldSubstring(self.code ?? "")
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in

                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
 
            })
        }
    }
}
