//
//  PrivacyPolicyViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 16/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: BaseViewController,UIWebViewDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var webView: UIWebView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    
    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Privacy Policy", EndString: "")

        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
        removeBackButton()
        addBackButton()
        Call_Privacy_Policy_API()
    }
    
    //MARK:- ********************** BUTTON EVENT **********************

    func addBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** API CALLING **********************

    func Call_Privacy_Policy_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)PrivacyPolicy", parameters: nil) { (APIResponce, Responce, error1) in
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        let decodedData = NSData(base64Encoded: responceDict["privacy_policy"] as? String ?? "", options:NSData.Base64DecodingOptions(rawValue: 0))
                        let decodedString = NSString(data: decodedData! as Data, encoding: String.Encoding.utf8.rawValue)
                        self.webView.loadHTMLString(decodedString! as String, baseURL: nil)

                    }else{
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Call_Privacy_Policy_API()
                        }
                    })
                }
                self.RemoveLoader()

            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Call_Privacy_Policy_API()
                }
            })
        }
    }
    
    //MARK:- ********************** WEBVIEW METHOD **********************

    func webViewDidStartLoad(_ webView: UIWebView) {
        showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.RemoveLoader()
    }
}
