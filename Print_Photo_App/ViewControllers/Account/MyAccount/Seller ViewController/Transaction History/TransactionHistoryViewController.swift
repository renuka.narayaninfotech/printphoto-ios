//
//  TransactionHistoryViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 18/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class TransactionHistoryViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var tblTransactionHistory: UITableView!
   
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var transactionHistoryArray = NSMutableArray()
    var transationDataArray = NSArray()
    var lblNoTransaction = UILabel()
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Transaction History", EndString: "")

        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
        addBackButton()
        removeBackButton()

        if transationDataArray.count > 0
        {
            tblTransactionHistory.isHidden = false
            lblNoTransaction.isHidden = true
        }
        else
        {
            tblTransactionHistory.isHidden = true
            lblNoTransaction.isHidden = false
             No_Transaction_Label()
        }
    }

    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    func No_Transaction_Label() {
        
        // Create Bottom Default Label
//        lblNoTransaction.translatesAutoresizingMaskIntoConstraints = false
        
//        lblNoTransaction.topAnchor.constraint(equalTo: (self.navigationController?.navigationBar.bottomAnchor)!, constant: 0).isActive = true
//        lblNoTransaction.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
//        lblNoTransaction.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        
        lblNoTransaction.textAlignment = .center
        lblNoTransaction.numberOfLines = 0
        lblNoTransaction.textColor = UIColor.gray
        lblNoTransaction.frame = CGRect(x: 0, y: (navigationController?.navigationBar.frame.height ?? 0.0) + 50, width: self.view.Getwidth, height: 50.0)
        
        if DeviceType.IS_IPAD
        {
            lblNoTransaction.font = UIFont(name: CustomFontWeight.medium, size: 22.0)
        }else
        {
            lblNoTransaction.font = UIFont(name: CustomFontWeight.medium, size: 16.0)
        }
        
        lblNoTransaction.text = "No Transaction"
        self.view.addSubview(lblNoTransaction)

    }
    
    //MARK:- ********************** BUTTON EVENT **********************

    func addBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transationDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionHistoryTableViewCell", for: indexPath) as! TransactionHistoryTableViewCell
        
        cell.shadowView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        cell.shadowView.cornerRadius = 3.0
        
        let dict = transationDataArray.object(at: indexPath.row) as! NSDictionary
        
        cell.lblDate.text = "Date : \(dict["date_time"] ?? "")"
        
        let status = "\(dict["status"] ?? "")"
        
        if status == "cash"
        {
            cell.lblPrice.text = "+ ₹\(dict["price"] ?? "")"
        }else{
            cell.lblPrice.text = "- ₹\(dict["price"] ?? "")"
        }
        
        cell.lblOrderID.text = "Order ID : \(dict["order_id"] ?? "")"

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
