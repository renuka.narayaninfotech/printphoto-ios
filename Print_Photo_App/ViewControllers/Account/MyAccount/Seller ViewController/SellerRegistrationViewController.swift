//
//  SellerRegistrationViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 09/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import DropDown

fileprivate enum AMDropDownType {
    case userType
}

class SellerRegistrationViewController: BaseViewController,UITextFieldDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var paytm_mobile_icon_borderview: UIView!
    @IBOutlet weak var txtPaytmMobileNo: KTextField!
    @IBOutlet weak var adhaar_number_icon_borderview: UIView!
    @IBOutlet weak var txtAdhaarNumber: KTextField!
    @IBOutlet weak var user_type_icon_borderview: UIView!
    @IBOutlet weak var lblUserType: UILabel!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var user_type_borderview: UIView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    fileprivate var selectedDropDown : AMDropDownType?
    var dropDown = DropDown()

    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Seller Registration", EndString: "")
        
        Initialize()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dropDown.hide()
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
        
        EditDict = userDefault.value(forKey: "EditAccountDict")! as! [String : Any]
        print(EditDict)
        
        addBackButton()
        ViewBorder()
        addResetButton()

        txtPaytmMobileNo.returnKeyType = .next
        txtAdhaarNumber.returnKeyType = .done
        btnRegister.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)

        ShowDropDown()
        
        self.hideKeyboardWhenTappedAround()
    }
    
    //MARK:- ********************** UIVIEW BORDER **********************
    
    func ViewBorder() {
        
        paytm_mobile_icon_borderview.setBorder()
        adhaar_number_icon_borderview.setBorder()
        user_type_icon_borderview.setBorder()
        user_type_borderview.setBorder()
    }
    
    //MARK:- ********************** BUTTON EVENT **********************

    @IBAction func press_btn_Register(_ sender: UIButton) {
        
        if isReadyToSellerRegister()
        {
            Call_Seller_Registration_API()
        }
        
    }
    
    @IBAction func press_btn_select_user_type(_ sender: UIButton) {
        view.endEditing(true)
        selectedDropDown = AMDropDownType.userType
        dropDown.anchorView = self.lblUserType
        dropDown.dataSource = ["Merchant","Business"]
        dropDown.show()
    }
    
    func addBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addResetButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_reset"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.resetAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func resetAction(_ sender: UIButton) {
        txtPaytmMobileNo.text = ""
        txtAdhaarNumber.text = ""
        lblUserType.text = "Select User Type"
    }
    
    //MARK:- ********************** DROP DOWN **********************
    
    func ShowDropDown() {
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            print("Selected item: \(item) at index: \(index)")
            
            if self.selectedDropDown == AMDropDownType.userType {
                
                // Get User Type
                self.lblUserType.text = item
            }
        }
    }
    
    //MARK:- ********************** VALIDATION **********************

    func isReadyToSellerRegister()-> Bool {
        if Validation.isStringEmpty(txtPaytmMobileNo.text!) || txtPaytmMobileNo.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter paytm mobile number", message: "", options: "OK") { (Int) in
                self.txtPaytmMobileNo.becomeFirstResponder()
            }
        }else if Int(txtPaytmMobileNo.text!.count) < 10
        {
            self.presentAlertWithTitle(title: "Please enter valid mobile number", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txtPaytmMobileNo.becomeFirstResponder()
            })
            
        }else if Validation.isStringEmpty(txtAdhaarNumber.text!) || txtAdhaarNumber.text?.trim() == ""{
            
            presentAlertWithTitle(title: "Please enter adhaar number", message: "", options: "OK") { (Int) in
                self.txtAdhaarNumber.becomeFirstResponder()
            }
        }else if Int(txtAdhaarNumber.text!.count) < 12
        {
            self.presentAlertWithTitle(title: "Please enter valid adhaar number", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txtAdhaarNumber.becomeFirstResponder()
            })
            
        }else if lblUserType.text == "Select User Type" {
            presentAlertWithTitle(title: "Please select user type", message: "", options: "OK") { (Int) in
            }
        }
        else{
            
            return true
        }
        
        return false
    }
    
    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************

    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtPaytmMobileNo
        {
            textField.resignFirstResponder()
            txtAdhaarNumber.becomeFirstResponder()
        }
        else if textField == txtAdhaarNumber
        {
            txtAdhaarNumber.resignFirstResponder()
        }
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        textField.autocorrectionType = .no
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if textField.tag == 1
        {
            switch string
            {
            case  "0","1","2","3","4","5","6","7","8","9":
                
                if userDefault.string(forKey: "RegionCode") == "IN"
                {
                    if newLength <= 10
                    {
                        return true
                    } else {
                        return false
                    }
                }
                
            case "":
                return true && newLength <= 50
                
            default:
                let array = [string]
                if array.count == 0
                {
                    return true
                }
                return false && newLength <= 4
            }
        }else if textField.tag == 3
        {
            switch string
            {
            case  "0","1","2","3","4","5","6","7","8","9":
                
                if newLength <= 12
                {
                    return true
                } else {
                    return false
                }
                
            case "":
                return true && newLength <= 50
                
            default:
                let array = [string]
                if array.count == 0
                {
                    return true
                }
                return false && newLength <= 4
            }
        }
        return true
    }
    
    //MARK:- ********************** API CALLING **********************

    func Call_Seller_Registration_API() {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["name": EditDict["name"] as? String ?? "",
                             "email": EditDict["email"] as? String ?? "",
                             "mobile": txtPaytmMobileNo.text ?? "",
                             "aadhar_no": txtAdhaarNumber.text ?? "",
                             "business_type": lblUserType.text ?? "",
                             "user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)seller", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {

                        let dict = responceDict["data"] as? [String:Any]
                        
                        self.presentAlertWithTitle(title: "Seller Registered Successfully", message: "You have registered Successfully, You will get Confirmation in 24 hours. Thank You.", options: "OK", completion: { (ButtonIndex) in
                        
                            sellerID = "\(dict?["id"] ?? "")"
                            
                            userDefault.set(sellerID, forKey: "sellerid")

                            let isApprove = "\(dict?["is_approve"] ?? "")"
                            
                            if isApprove == "1"
                            {
                                strSeller = "Seller Wallet"
                            }else
                            {
                                strSeller = "Want to become a seller?"
                            }
                            
                            userDefault.set(strSeller, forKey: "sellerTitle")

                            self.popViewController()
                        })
                        
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
}
