//
//  ChangePasswordViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 07/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class ChangePasswordViewController: BaseViewController,UITextFieldDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var txtOldPassword: KTextField!
    @IBOutlet weak var txtNewPassword: KTextField!
    @IBOutlet weak var txtRetypeNewPassword: KTextField!
    @IBOutlet weak var old_password_icon_view: UIView!
    @IBOutlet weak var new_password_icon_view: UIView!
    @IBOutlet weak var retype_new_password_icon_view: UIView!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var changePasswordView: UIView!
    
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SetNavigationBarTitle(Startstring: "Change Password", EndString: "")

        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true

    }
    
    //MARK:- ********************** INITIALIZE **********************

     func Initialize() {
        
        addBackButton()
        ViewBorder()
        addResetButton()

        changePasswordView.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)

        txtOldPassword.returnKeyType = .next
        txtNewPassword.returnKeyType = .next
        txtRetypeNewPassword.returnKeyType = .done
        
        self.hideKeyboardWhenTappedAround()
    }
    
    //MARK:- ********************** UIVIEW BORDER **********************
    
    func ViewBorder() {
        
        old_password_icon_view.setBorder()
        new_password_icon_view.setBorder()
        retype_new_password_icon_view.setBorder()

    }
    
    //MARK:- ********************** BUTTON EVENT **********************

    @IBAction func press_button_Change_Password(_ sender: UIButton) {
       
        if isReadyToChangePassword()
        {
            Call_Change_Password_API()
        }
    }
    
    func addBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addResetButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_reset"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.resetAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func resetAction(_ sender: UIButton) {
        txtOldPassword.text = ""
        txtNewPassword.text = ""
        txtRetypeNewPassword.text = ""
    }
    
    //MARK:- ********************** VALIDATION **********************
    
    func isReadyToChangePassword()-> Bool {
        
        if Validation.isStringEmpty(txtOldPassword.text!) || txtOldPassword.text?.trim() == "" {
            
            presentAlertWithTitle(title: "Please enter old password", message: "", options: "OK") { (Int) in
                self.txtOldPassword.becomeFirstResponder()
            }
        }else if Validation.isStringEmpty(txtNewPassword.text!) || txtNewPassword.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter new password", message: "", options: "OK") { (Int) in
                self.txtNewPassword.becomeFirstResponder()
            }
        }else if Int(txtNewPassword.text!.count) < 6
        {
            self.presentAlertWithTitle(title: "Password must be more than 6 digit", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txtNewPassword.becomeFirstResponder()
            })
        }else if Validation.isStringEmpty(txtRetypeNewPassword.text!) || txtRetypeNewPassword.text?.trim() == "" {
            presentAlertWithTitle(title: "Please enter confirm password", message: "", options: "OK") { (Int) in
                self.txtRetypeNewPassword.becomeFirstResponder()
            }
        }else if Validation.isPassWordMatch(pass: txtNewPassword.text!, confPass: txtRetypeNewPassword.text!)
        {
            presentAlertWithTitle(title: "New Password and Confirm Password are not same", message: "", options: "OK") { (Int) in
                self.txtRetypeNewPassword.becomeFirstResponder()
            }
        }
        else{
            return true
        }
        
        return false
    }
    
    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtOldPassword
        {
            textField.resignFirstResponder()
            txtNewPassword.becomeFirstResponder()
        }
        else if textField == txtNewPassword
        {
            textField.resignFirstResponder()
            txtRetypeNewPassword.becomeFirstResponder()
        }else
        {
            txtRetypeNewPassword.resignFirstResponder()
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        if (string == " ") {
            return false
        }
        return true
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Call_Change_Password_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                            "old_password": "\(txtOldPassword.text?.trim() ?? "")",
                            "new_password": "\(txtNewPassword.text?.trim() ?? "")"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)new_password", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        
                        self.presentAlertWithTitle(title: "Password Changed Successfully", message: "", options: "OK", completion: { (ButtonIndex) in
                            self.popViewController()
                        })
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
    }
}
