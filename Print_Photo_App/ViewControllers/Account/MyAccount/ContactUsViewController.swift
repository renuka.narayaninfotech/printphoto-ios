//
//  ContactUsViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 14/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import Social
import MessageUI
import SystemConfiguration
import MapKit
import CoreTelephony

class ContactUsViewController: BaseViewController,MFMailComposeViewControllerDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var ContactView: UIView!
    @IBOutlet weak var supportTimingView: UIView!
    @IBOutlet weak var lblEmailID: UILabel!
    @IBOutlet weak var lblPhoneNo: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnMobileNo: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var lblSupportTime: UILabel!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var hightViewLogo: NSLayoutConstraint!
    @IBOutlet weak var heightViewAddress: NSLayoutConstraint!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var contactDict = [String:Any]()
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Contact Us", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //        self.lblAddress.superview?.layoutIfNeeded()
        //        self.lblAddress.superview?.setNeedsLayout()
        
    }
    
    override func viewDidLayoutSubviews() {
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        addBackButton()

//        if userDefault.string(forKey: "RegionCode") == "IN"
//        {
            addHelpButton()
//        }else
//        {
//            removeBackButton()
//        }
        
        btnEmail.isExclusiveTouch = true
        btnMobileNo.isExclusiveTouch = true
        btnAddress.isExclusiveTouch = true
        
        self.ContactView.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        self.supportTimingView.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        
        // Get Contactus Dictionary from HomeVC
        contactDict = userDefault.value(forKey: "contactus") as? [String : Any] ?? [String : Any]()
        
        lblEmailID.text = "\(contactDict["email"] ?? "")"
        lblPhoneNo.text = "\(contactDict["mobile"] ?? "")"
        lblAddress.text = "\(contactDict["address"] ?? "")"
        lblSupportTime.text = "\(contactDict["time"] ?? "")"
        
        // Design layout in different screen with managed multiple lable line
        Design_Layout()
        
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    @IBAction func press_btn_email(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail()
        {
            if Reachability.isConnectedToNetwork() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients([lblEmailID.text ?? ""])
                present(mail, animated: true)
            }else{
                
                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                })
            }
            
        } else
        {
            let url = URL(string: "mailto:\(lblEmailID.text ?? "")")
            UIApplication.shared.open(url!)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func press_btn_Mobile_no(_ sender: UIButton) {
        
        sender.isUserInteractionEnabled = false
        check_SIM_Available_OR_Not(sender)
        
        
        // Open Whatsappp
        
        //        let whatsappURL = URL(string: "https://api.whatsapp.com/send?phone=919773055773")
        //        if UIApplication.shared.canOpenURL(whatsappURL!) {
        //            if Reachability.isConnectedToNetwork() {
        //                if #available(iOS 10.0, *) {
        //                    UIApplication.shared.open(whatsappURL!, options: [:], completionHandler: nil)
        //                } else {
        //                    UIApplication.shared.openURL(whatsappURL!)
        //                }
        //
        //            }else{
        //                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
        //                })
        //            }
        //        }else{
        //            self.presentAlertWithTitle(title: "Whatsapp has not been installed", message: "", options: "OK", completion: { (ButtonIndex) in
        //            })
        //        }
        
    }
    
    @IBAction func press_btn_Address(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() {

        //        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
        //            if Reachability.isConnectedToNetwork() {
        ////                let urlString = URL(string: "comgooglemaps://?center=\(21.223878),\(72.827486)&zoom=14&q=")
        //
            var urlString: URL!
            if(userDefault.string(forKey: "RegionCode") == "SA") {
                urlString = URL(string: ("https://maps.google.com/?q=" + (lblAddress.text?.replacingOccurrences(of: "", with: "+"))!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!))
            } else {
                urlString = URL(string:"https://www.google.com/maps/place/Printphoto.in+-+Customize+Mobile+Cover,+T-Shirt,+Mug/@21.2241708,72.8253772,17z/data=!4m8!1m2!2m1!1s205-208,+Oasis+Corner,+Dhanmora,+Surat!3m4!1s0x3be04fbe03eb2c4f:0x1a3824b4f04fe35c!8m2!3d21.2240621!4d72.8275245")
            }
        
        UIApplication.shared.open(urlString!, options: [:], completionHandler: nil)
        //            }else{
        //
        //                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
        //                })
        //            }
        //        } else {
        //            let url  = NSURL(string: "http://maps.apple.com/?q=21.223878,72.827486")
        //            if UIApplication.shared.canOpenURL(url! as URL) == true
        //            {
        //                UIApplication.shared.open(url! as URL)
        //            }
        //        }
            
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
               
            })
        }
    }
    
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
        isOrderBtnBack = false
        
        self.popViewController()
    }
    
    func addHelpButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_help"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.helpAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func helpAction(_ sender: UIButton) {
        Help_Video()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    @IBAction func btnWhatsapp(_ sender: UIButton) {
        let whatsappURL = "whatsapp://send?phone=\((userDefault.string(forKey: "contactusWhatsapp")) ?? "")&abid=12354"
        if let urlString = whatsappURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.openURL(whatsappURL)
                } else {
                    print("Install Whatsapp")
                    self.topMostViewController().view.makeToast("Whatsapp has been not installed.", duration: 2.0, position: .bottom)
                }
            }
        }
    }
    
    //MARK:- ********************** CHECK SIM CARD AVAILABLE OR NOT **********************
    func check_SIM_Available_OR_Not(_ sender: UIButton)
    {
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier: CTCarrier? = networkInfo.subscriberCellularProvider
        let code: String? = carrier?.mobileNetworkCode
        if (code != nil) {
            if let url = URL(string: "tel://\(lblPhoneNo.text ?? "")") {
                UIApplication.shared.open(url)
            }
        }
        else {
            self.topMostViewController().view.makeToast("SIM card is not inserted", duration: 3.0, position: .bottom)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
            sender.isUserInteractionEnabled = true
        })
        
    }
    
    func Design_Layout()
    {
        // managed design layout in different screen size and Managed multiple line label
        var topPadding: CGFloat = 20.0
        var bottomPadding: CGFloat = 0.0
        
        if #available(iOS 11.0, *) {
            topPadding = UIApplication.shared.keyWindow!.safeAreaInsets.top
            bottomPadding = UIApplication.shared.keyWindow!.safeAreaInsets.bottom
        }
        
        let heightView : CGFloat = (self.navigationController!.navigationBar.frame.height + topPadding + bottomPadding)
        
        let strAddress = "\(contactDict["address"] ?? "")"
        let heightAddress = strAddress.height(withConstrainedWidth: ScreenSize.SCREEN_WIDTH-116.0, font: UIFont.init(name: "HelveticaNeue", size: DeviceType.IS_IPAD ? 24.0 : 16.0)!)
        
        if DeviceType.IS_IPAD
        {
            heightViewAddress.constant = ((ScreenSize.SCREEN_HEIGHT - heightView) - ((ScreenSize.SCREEN_HEIGHT - heightView) * CGFloat(0.3)) - 45.0) / 4.0
            hightViewLogo.constant = (ScreenSize.SCREEN_HEIGHT - heightView) * CGFloat(0.3)
        }
        else
        {
            heightViewAddress.constant = heightAddress + 57.0
            
            let minHeightLogo : CGFloat = 80.0
            let remainHeight = ScreenSize.SCREEN_HEIGHT - (4.0 * (heightAddress + 57.0)) - 30.0 - heightView
            if remainHeight < 165.0 // if under default height
            {
                hightViewLogo.constant = (remainHeight > minHeightLogo ? remainHeight : minHeightLogo) - 5.0
            }
            else
            {
                hightViewLogo.constant = remainHeight - 5.0
            }
        }
    }
}
