//
//  BulkOrderViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 06/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import DropDown

fileprivate enum AMDropDownType {
    case phoneCode
    case product
    case quantity
}

class BulkOrderViewController: BaseViewController, UITextViewDelegate, UITextFieldDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var fullNameIconBorderView: UIView!
    @IBOutlet weak var txtFullName: KTextField!
    @IBOutlet weak var emailIconBorderView: UIView!
    @IBOutlet weak var txtEmail: KTextField!
    @IBOutlet weak var lblMobileCode: UILabel!
    @IBOutlet weak var txtMobileNo: KTextField!
    @IBOutlet weak var productIconBorderView: UIView!
    @IBOutlet weak var lblProduct: UILabel!
    @IBOutlet weak var quantityIconBorderView: UIView!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var mobileIconBorderView: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var productBorderView: UIView!
    @IBOutlet weak var quantityBorderView: UIView!
    @IBOutlet weak var txtBusinessType: KTextField!
    @IBOutlet weak var iconBusinessTypeBorderView: UIView!
    
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    fileprivate var selectedDropDown : AMDropDownType?
    var dropDown = DropDown()
    var quantityArray = ["5 - 10",
                        "10 - 25",
                        "25 - 50",
                        "50 - 75",
                        "75 - 100",
                        "100 or above"] as [String]
    var productId : Int?
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SetNavigationBarTitle(Startstring: "Bulk Order", EndString: "")
        
        Initialize()

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dropDown.hide()
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
        
        Reset()
        addBackButton()
        addResetButton()
        ViewBorder()
        addResetButton()
        Fill_TextField()
        btnSubmit.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        
        txtFullName.returnKeyType = .next
        txtEmail.returnKeyType = .next
        txtMobileNo.returnKeyType = .next
        txtDescription.returnKeyType = .done

        ShowDropDown()
    }
    
    // Get Dictionary from Login Screen
    func Fill_TextField() {
        
        EditDict = userDefault.value(forKey: "EditAccountDict")! as! [String : Any]
        print(EditDict)
        
        txtFullName.text = (EditDict["name"] as? String)
        txtMobileNo.text = EditDict["mobileno"] as? String
        txtEmail.text = EditDict["email"] as? String
    }
    
    //MARK:- ********************** UIVIEW BORDER **********************

    func ViewBorder() {
        
        fullNameIconBorderView.setBorder()
        emailIconBorderView.setBorder()
        mobileIconBorderView.setBorder()
        productIconBorderView.setBorder()
        productBorderView.setBorder()
        quantityIconBorderView.setBorder()
        quantityBorderView.setBorder()
        txtDescription.setBorder()
        iconBusinessTypeBorderView.setBorder()
       
    }
    
    //MARK:- ********************** RESET TEXTFIELD **********************

    func Reset()
    {
        txtFullName.text = ""
        txtEmail.text = ""
        txtMobileNo.text = ""
        txtBusinessType.text = ""
        lblProduct.text = "Select Product"
        lblQuantity.text = "Select Quantity"
        txtDescription.text = "Description..."
        txtDescription.textColor = UIColor.lightGray
    }
    
    //MARK:- ********************** BUTTON EVENT **********************

    @IBAction func press_btn_mobile_code(_ sender: UIButton) {
//        view.endEditing(true)
//        selectedDropDown = AMDropDownType.phoneCode
//        dropDown.anchorView = self.lblMobileCode
//        dropDown.dataSource = phoneCodeArray as! [String] //["+91"]
//        dropDown.show()
    }
    
    @IBAction func press_btn_product(_ sender: UIButton) {
        view.endEditing(true)
        selectedDropDown = AMDropDownType.product
        dropDown.anchorView = self.lblProduct
        dropDown.dataSource = categoryArray as! [String]
        dropDown.show()
    }
    
    @IBAction func press_btn_quantity(_ sender: UIButton) {
        if lblProduct.text == "Select Product"
        {
            self.presentAlertWithTitle(title: "Please select product", message: "", options: "OK", completion: { (ButtonIndex) in
            })
        }else
        {
            view.endEditing(true)
            selectedDropDown = AMDropDownType.quantity
            dropDown.anchorView = self.lblQuantity
            dropDown.dataSource = quantityArray
            dropDown.show()
        }
    }
    
    @IBAction func press_btn_submit(_ sender: UIButton) {
    
        if isSubmitBulkOrder()
        {
            if txtDescription.text == "Description..." || txtDescription.text.trim() == ""
            {
                txtDescription.text = ""
            }
            
            self.Call_Bulk_Order_API()
        }
    }
    
    func addBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addResetButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_reset"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.resetAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func resetAction(_ sender: UIButton) {
        
        Reset()
        
    }
    
    //MARK:- ********************** DROP DOWN **********************

    func ShowDropDown() {
        
        // Managed region from SignInVC and RegionVC get selected region and managed stateArray for perticular Country
        for i in 0..<phoneCodeArray.count
        {
            let regionCode = ((self.removeSpecialCharsFromString(text: phoneCodeArray[i] as! String)).slice(from: "(", to: ")") ?? "")
            
            if userDefault.string(forKey: "RegionCode") == regionCode
            {
                let selectRegion = phoneCodeArray[i] as? String
                let region = selectRegion?.components(separatedBy: " ")
                
                self.lblMobileCode.text = region?.first
            
                
                //                userDefault.removeObject(forKey: "RegionCode")
                //                userDefault.set(regionCode, forKey: "RegionCode")
            }
        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            print("Selected item: \(item) at index: \(index)")
            
            if self.selectedDropDown == AMDropDownType.phoneCode {
                
                // Get Phonecode
                self.lblMobileCode.text = "+\(item.digitsOnly())"
                
            }else if self.selectedDropDown == AMDropDownType.product {
               
                if self.lblProduct.text != item
                {
                    self.lblQuantity.text = "Select Quantity"
                }
                
                // Get country name
                self.lblProduct.text = item
                
                self.productId = categoryIdArray.object(at: index) as? Int
    
            }else if self.selectedDropDown == AMDropDownType.quantity {
              
                // Get state name
                self.lblQuantity.text = item
                
            }
        }
    }
    //MARK:- ********************** VALIDATION **********************

    func isSubmitBulkOrder() -> Bool {
        
        if Validation.isStringEmpty(txtFullName.text!) || txtFullName.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter full name", message: "", options: "OK") { (Int) in
                self.txtFullName.becomeFirstResponder()
            }
        }
        else if Validation.isStringEmpty(txtEmail.text!) || txtEmail.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter email id", message: "", options: "OK") { (Int) in
                self.txtEmail.becomeFirstResponder()
            }
        }else if !Validation.isValidEmail(emailString: txtEmail.text!)
        {
            presentAlertWithTitle(title: "Please enter valid email id", message: "", options: "OK") { (Int) in
                self.txtEmail.becomeFirstResponder()
            }
        }else if Validation.isStringEmpty(txtBusinessType.text!) || txtBusinessType.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter valid business name", message: "", options: "OK") { (Int) in
                self.txtBusinessType.becomeFirstResponder()
            }
        }
        else if Validation.isStringEmpty(txtMobileNo.text!) || txtMobileNo.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter mobile number", message: "", options: "OK") { (Int) in
                self.txtMobileNo.becomeFirstResponder()
            }
        }else if userDefault.string(forKey: "RegionCode") == "IN" && Int(txtMobileNo.text!.count) < 10
        {
            self.presentAlertWithTitle(title: "Please enter 10 digit mobile number", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txtMobileNo.becomeFirstResponder()
            })
        }else if userDefault.string(forKey: "RegionCode") == "SA" && Int(txtMobileNo.text!.count) != 9
        {
            self.presentAlertWithTitle(title: "Please enter 9 digit mobile number", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txtMobileNo.becomeFirstResponder()
            })
        }else if lblProduct.text == "Select Product" {
            presentAlertWithTitle(title: "Please select product", message: "", options: "OK") { (Int) in
            }
        }else if lblQuantity.text == "Select Quantity" {
            presentAlertWithTitle(title: "Please select quantity", message: "", options: "OK") { (Int) in
            }
        }
        else if txtDescription.text == "Description..." || txtDescription.text.trim() == "" || Int(txtDescription.text!.count) <= 50
        {
            presentAlertWithTitle(title: "Please enter description proper and enter minimum 50 character!!", message: "", options: "OK") { (Int) in
                self.txtDescription.becomeFirstResponder()
            }
        }
//        else if Int(txtDescription.text!.count) <= 50
//        {
//            self.presentAlertWithTitle(title: "Please enter 10 digit mobile number", message: "", options: "OK", completion: { (ButtonIndex) in
//                self.txtMobileNo.becomeFirstResponder()
//            })
//
//        }
        else{
            
            return true
        }
        
        return false
    }
    
    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************

    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtFullName
        {
            textField.resignFirstResponder()
            txtEmail.becomeFirstResponder()
        }else if textField == txtEmail
        {
            textField.resignFirstResponder()
            txtMobileNo.becomeFirstResponder()
        }else if textField == txtMobileNo
        {
            textField.resignFirstResponder()
            txtDescription.becomeFirstResponder()
        }else
        {
            txtDescription.resignFirstResponder()
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
    
        textField.autocorrectionType = .no
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if textField.tag == 1
        {
            switch string
            {
            case  "0","1","2","3","4","5","6","7","8","9":
                
                if userDefault.string(forKey: "RegionCode") == "IN"
                {
                    if newLength <= 10
                    {
                        return true
                    } else {
                        return false
                    }
                }
                
            case "":
                return true && newLength <= 50
                
            default:
                let array = [string]
                if array.count == 0
                {
                    return true
                }
                return false && newLength <= 4
            }
        }else if textField.tag == 2
        {
            if newLength <= 25
            {
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS_OnlyAlphabet).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                
                return (string == filtered)
            } else {
                return false
            }
        }
        return true
    }
    
    //MARK:- **********************  TEXTVIEW DELEGATE METHOD **********************
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtDescription.textColor == UIColor.lightGray {
            txtDescription.text = nil
            txtDescription.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtDescription.text.isEmpty {
            txtDescription.text = "Description..."
            txtDescription.textColor = UIColor.lightGray
        }
    }
    
    //MARK:- ********************** API CALLING **********************

    func Call_Bulk_Order_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                            "name": "\(txtFullName.text?.trim() ?? "")",
                            "email": "\(txtEmail.text?.trim() ?? "")",
                            "mobile": "\(txtMobileNo.text?.trim() ?? "")",
                            "category_id": self.productId ?? 0,
                            "qty": "\(lblQuantity.text ?? "")",
                            "description": "\(txtDescription.text ?? "")",
                            "bussiness" : "\(txtBusinessType.text ?? "")"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)bulk_order", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        
                        self.presentAlertWithTitle(title: "Alert", message: "Our executive will call you within 24 hours!!", options: "OK", completion: { (ButtonIndex) in
                        
                            self.popViewController()
                        
                        })
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ()")
        return String(text.filter {okayChars.contains($0) })
    }
}
