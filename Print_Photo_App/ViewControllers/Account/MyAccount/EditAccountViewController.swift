//
//  EditAccountViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 09/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import DropDown

fileprivate enum AMDropDownType {
    case CurrencyCode
}

class EditAccountViewController: BaseViewController,UITextFieldDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var txtFirstName: KTextField!
    @IBOutlet weak var firstName_Icon_Borderview: UIView!
    @IBOutlet weak var txtLastName: KTextField!
    @IBOutlet weak var lastName_Icon_Borderview: UIView!
    @IBOutlet weak var txtMobileNo: KTextField!
    @IBOutlet weak var mobileNo_Icon_Borderview: UIView!
    @IBOutlet weak var email_icon_borderview: UIView!
    @IBOutlet weak var txtEmail: KTextField!
    @IBOutlet weak var CurrencyCodeBorderView: UIView!
    @IBOutlet weak var iconCurrencyCodeView: UIView!
    @IBOutlet weak var lblCurrencyCode: UILabel!
    @IBOutlet weak var heightCurrencyCodeView: NSLayoutConstraint!
    @IBOutlet weak var bottomCurrencyCodeView: NSLayoutConstraint!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var firstName : String!
    var lastName : String!
    
    fileprivate var selectedDropDown : AMDropDownType?
    var dropDown = DropDown()
    
    var currencyID : String?
    
    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    
        SetNavigationBarTitle(Startstring: "Edit Account", EndString: "")
 
        Initialize()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dropDown.hide()
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
        
        addBackButton()
        ViewBorder()
        Fill_TextField()
        addResetButton()
        
        txtFirstName.returnKeyType = .next
        txtLastName.returnKeyType = .next
        txtEmail.returnKeyType = .done
        
        self.hideKeyboardWhenTappedAround()
        
//        // Do not select currency when user current location at India
//        let userDict = userDefault.value(forKey: "EditAccountDict") as! NSDictionary

        if userDefault.string(forKey: "RegionCode") == "IN"
        {
            heightCurrencyCodeView.constant = 0.0
            bottomCurrencyCodeView.constant = 0.0
            txtEmail.isEnabled = true
            txtEmail.textColor = UIColor.black
            
        }else
        {
            heightCurrencyCodeView.constant = DeviceType.IS_IPAD ? 75.0 : 50.0
            bottomCurrencyCodeView.constant = DeviceType.IS_IPAD ? 24.0 : 16.0
            txtEmail.isEnabled = false
            txtEmail.textColor = UIColor.lightGray
            
        }

        // Remove India Code from currenciesArray
        let arr = currenciesArray.filter { ((($0 as! NSDictionary)["code"] as! String) != "INR")}
        currenciesArray = NSMutableArray(array: arr)
        
        ShowDropDown()
    }

   // Get Dictionary from Login Screen
    func Fill_TextField() {
        
        EditDict = userDefault.value(forKey: "EditAccountDict")! as! [String : Any]
        print(EditDict)
        
        var fullName = (EditDict["name"] as? String)?.components(separatedBy: " ")
        
        if fullName!.count > 0
        {
            txtFirstName.text = fullName?[0]
          
            if fullName!.count > 1
            {
                txtLastName.text = fullName?[1]
            }
        }

        txtMobileNo.text = EditDict["mobileno"] as? String
        txtEmail.text = EditDict["email"] as? String
        let currencyid = "\(EditDict["currencyID"] ?? "")"

            // Set currency code depend on user is register in which country
            
            for i in 0..<currenciesArray.count
            {
                let arrCurrencyId = (currenciesArray.object(at: i) as! NSDictionary).value(forKey: "id") as? Int ?? 0
                
                if Int(currencyid) == arrCurrencyId
                {
                    self.currencyID = String(arrCurrencyId)
                    
                    if userDefault.string(forKey: "Currency") == "" || userDefault.string(forKey: "Currency") == nil
                    {
                        self.lblCurrencyCode.text = (currenciesArray.object(at: i) as! NSDictionary).value(forKey: "code") as? String
                    }else
                    {
                        self.lblCurrencyCode.text = userDefault.string(forKey: "Currency")
                    }
                }
            }
        
    }
    

    //MARK:- ********************** BUTTON EVENT **********************

    @IBAction func press_btnCurrencyCode(_ sender: UIButton) {
        view.endEditing(true)
        selectedDropDown = AMDropDownType.CurrencyCode
        dropDown.anchorView = self.lblCurrencyCode
        dropDown.dataSource = currenciesArray.value(forKey: "code") as! [String]
        dropDown.show()
    }
    
    @IBAction func press_btn_profile(_ sender: UIButton) {
        
        if isReadyToEditAccount()
        {
            Call_Edit_Account_API()
        }
    }
    
    func addBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addResetButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_reset"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.resetAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func resetAction(_ sender: UIButton) {
        txtFirstName.text = ""
        txtLastName.text = ""
        txtEmail.text = (EditDict["email"] as? String)

//        txtEmail.text = (userDefault.string(forKey: "RegionCode") == "IN") ? "" : (EditDict["email"] as? String)
    }
    
    //MARK:- ********************** UIVIEW BORDER **********************
    
    func ViewBorder() {
        
        firstName_Icon_Borderview.setBorder()
        lastName_Icon_Borderview.setBorder()
        mobileNo_Icon_Borderview.setBorder()
        email_icon_borderview.setBorder()
        CurrencyCodeBorderView.setBorder()
        iconCurrencyCodeView.setBorder()
    }
    
    //MARK:- ********************** DROP DOWN **********************
    
    func ShowDropDown() {
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            print("Selected item: \(item) at index: \(index)")
            
            if self.selectedDropDown == AMDropDownType.CurrencyCode {
            
                // Get User Type
                self.lblCurrencyCode.text = item

                let currencyDict = currenciesArray.object(at: index) as! NSDictionary

                self.currencyID = String(currencyDict.value(forKey: "id") as? Int ?? 0)
                
            }
        }
    }
    
    //MARK:- ********************** VALIDATION **********************

    func isReadyToEditAccount()-> Bool {
        
        if Validation.isStringEmpty(txtFirstName.text!) || txtFirstName.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter first name", message: "", options: "OK") { (Int) in
                self.txtFirstName.becomeFirstResponder()
            }
        }
        else if Validation.isStringEmpty(txtLastName.text!) || txtLastName.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter last name", message: "", options: "OK") { (Int) in
                self.txtLastName.becomeFirstResponder()
            }
        }else if Validation.isStringEmpty(txtEmail.text!) || txtEmail.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter email id", message: "", options: "OK") { (Int) in
                self.txtEmail.becomeFirstResponder()
            }
        }else if !Validation.isValidEmail(emailString: txtEmail.text!)
        {
            presentAlertWithTitle(title: "Please enter valid email id", message: "", options: "OK") { (Int) in
                self.txtEmail.becomeFirstResponder()
            }
        }
        else{
            return true
        }
        
        return false
    }
    
    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************

    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFirstName
        {
            textField.resignFirstResponder()
            txtLastName.becomeFirstResponder()
        }
        else if textField == txtLastName
        {
            textField.resignFirstResponder()
            txtEmail.becomeFirstResponder()
        }else
        {
            txtEmail.resignFirstResponder()
        }
        
        return false
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
    
        textField.autocorrectionType = .no
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if textField.tag == 1
        {
            return true
            
        }else if textField.tag == 2
        {
            if newLength <= 25
            {
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS_OnlyAlphabet).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                
                return (string == filtered)
            } else {
                return false
            }
        }
        return true

    }
    
    //MARK:- ********************** API CALLING **********************

    func Call_Edit_Account_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
//            let userDict = userDefault.value(forKey: "EditAccountDict") as! NSDictionary
            
            currencyID = userDefault.string(forKey: "RegionCode") == "IN" ? "" : currencyID
            
            let ParamDict = ["id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                            "name": "\(txtFirstName.text?.trim() ?? "")"+" "+"\(txtLastName.text?.trim() ?? "")",
                            "email": "\(txtEmail.text?.trim() ?? "")",
//                            "update": 0,
                            "currency_id" : currencyID ?? ""] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)update", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        
                        let editName = (responceDict["data"] as! NSDictionary)["name"] as? String
                        let editEmail = (responceDict["data"] as! NSDictionary)["email"] as? String
                        
                        EditDict.updateValue(editName ?? "", forKey: "name")
                        EditDict.updateValue(editEmail ?? "", forKey: "email")
                        
                        userDefault.removeObject(forKey: "Currency")
                        userDefault.set(self.lblCurrencyCode.text, forKey: "Currency")
                        
                        userDefault.set(EditDict, forKey: "EditAccountDict")
                        userDefault.synchronize()
                        
                        self.presentAlertWithTitle(title: "Update", message: "Update Successfully", options: "OK", completion: { (ButtonIndex) in
                            self.popViewController()
                        })
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
    }
    
}
