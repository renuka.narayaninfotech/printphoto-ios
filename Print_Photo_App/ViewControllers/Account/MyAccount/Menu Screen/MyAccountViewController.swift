//
//  MyAccountViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 30/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class MyAccountViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var myAccountTableView: UITableView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var iconArray = [[UIImage(named: "ic_my_order"),UIImage(named: "ic_bulk_order"),UIImage(named: "ic_mall_profile_offer"),UIImage(named: "ic_delivery")],
                     [UIImage(named: "ic_my_accoutn"),UIImage(named: "ic_seller")],
                     [UIImage(named: "ic_change_password")],
                     [UIImage(named: "ic_private_policy"),UIImage(named: "ic_term_condition"),UIImage(named: "ic_refund")],
                     [UIImage(named: "ic_visit_Website"),UIImage(named: "ic_video"),UIImage(named: "icon_contact"),UIImage(named: "ic_compliant"),UIImage(named: "ic_my_accoutn")]]
    
    var mainArray = [["My Order","Bulk Order","New Offers","Delivery Address"],
                     ["Personal Details","\(userDefault.string(forKey: "sellerTitle") ?? "")"],
                     ["Change Password"],
                     ["Privacy & Policy","Terms & Conditions","Refund & Cancellation Policy"],
                     ["Visit Our Website","Learn How To Edit Mobile Cover","Contact Us","Feedback/Complain","Delete account"],
                     [""]
                    ]
    
    var headerArray = ["Shopping","Account Settings","Security & Settings","Policies","Need Help?", ""]
    
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        SetNavigationBarTitle(Startstring: "My Account", EndString: "")
        
        Initialize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationController?.navigationBar.isHidden = false
        
        if userDefault.value(forKey: "USER_ID") == nil || userDefault.integer(forKey: "USER_ID") == 0
        {
            //            isLoginOrNot = "myaccount_login"
            //            let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
            //            self.navigationController?.pushViewController(signinVC, animated: false)
            
            let VCarray = self.navigationController?.viewControllers
            for Controller in VCarray! {
                if Controller is SignInViewController {
                    self.navigationController?.popToViewController(Controller, animated: true)
                }
            }
        }
        
        // Display "New Offers" and "Want to become seller" when user current location in india
        if userDefault.string(forKey: "RegionCode") != "IN"
        {
//            // "New Offers"
//            let arrOffer = mainArray[0].filter { $0 != "New Offers" }
//            let arrIconOffer = iconArray[0].filter { $0 != UIImage(named: "ic_mall_profile_offer") }
//
//            mainArray[0] = NSMutableArray(array: arrOffer) as! [String]
//            iconArray[0] = NSMutableArray(array: arrIconOffer as [Any]) as! [UIImage?]

            // "Want to become seller"
            
            let arrSeller = mainArray[1].filter { $0 != "\(userDefault.string(forKey: "sellerTitle") ?? "")" }
            let arIconSeller = iconArray[1].filter { $0 != UIImage(named: "ic_seller") }
            
            mainArray[1] = NSMutableArray(array: arrSeller) as! [String]
            iconArray[1] = NSMutableArray(array: arIconSeller as [Any]) as! [UIImage?]
            
            // "Refund & Cancellation Policy"
            
            let arrRefundPolicy = mainArray[3].filter { $0 != "Refund & Cancellation Policy" }
            let arIconRefund = iconArray[3].filter { $0 != UIImage(named: "ic_refund") }
            
            mainArray[3] = NSMutableArray(array: arrRefundPolicy) as! [String]
            iconArray[3] = NSMutableArray(array: arIconRefund as [Any]) as! [UIImage?]
            
//            // "Learn How to edit mobile cover"
//            
//            let arrVideo = mainArray[4].filter { $0 != "Learn How To Edit Mobile Cover" }
//            let arIconVideo = iconArray[4].filter { $0 != UIImage(named: "ic_video") }
//            
//            mainArray[4] = NSMutableArray(array: arrVideo) as! [String]
//            iconArray[4] = NSMutableArray(array: arIconVideo as [Any]) as! [UIImage?]
            
            
        }else
        {
            let strCurrent = mainArray[1][1]
            let strUpdated = "\(userDefault.string(forKey: "sellerTitle") ?? "")"
            if strCurrent != strUpdated
            {
                mainArray = [["My Order","Bulk Order","New Offers","Delivery Address"],
                             ["Personal Details","\(userDefault.string(forKey: "sellerTitle") ?? "")"],
                             ["Change Password"],
                             ["Privacy & Policy","Terms & Conditions","Refund & Cancellation Policy"],
                             ["Visit Our Website","Learn How To Edit Mobile Cover","Contact Us","Feedback/Complain","Delete account"]]
                
                myAccountTableView.reloadData()
            }
        }
        
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        registerTableViewCell(cellName: "MyAccountTableViewCell", to: myAccountTableView)
        
        addLogoutButton()
        addBackButton()
        myAccountTableView.estimatedRowHeight = 50.0
        myAccountTableView.estimatedSectionHeaderHeight = 50.0
        
//        // Do not select currency when user current location at India
//        let userDict = userDefault.value(forKey: "EditAccountDict") as! NSDictionary
        
    }

    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
    
        ((tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)
          self.tabBarController?.selectedIndex = 0

    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addLogoutButton() {
        let logoutButton = UIButton(type: .custom)
        logoutButton.setImage(UIImage(named: "ic_logout"), for: .normal)
        logoutButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        logoutButton.addTarget(self, action: #selector(self.logoutAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: logoutButton)
    }
    
    @objc func logoutAction(_ sender: UIButton) {
        
        self.presentAlertWithTitle(title: "Logout", message: "Do you want to logout?", options: "NO","YES") { (ButtonIndex) in
            if ButtonIndex == 0
            {
                self.dismissViewController()
            }else
            {
                let userDict = userDefault.value(forKey: "EditAccountDict") as! NSDictionary
                let token = userDict["token"] as? String ?? ""
              
                self.Logout_API(Token:token)

            }
        }
        
    }
    
    //MARK:- ********************** TABLEVIEW METHOD **********************
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if((userDefault.string(forKey: "contactusWhatsapp") as? String) == nil)
        {
            return mainArray.count - 1
        }
        return mainArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainArray[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(mainArray.count == indexPath.section + 1)
        {
            let myCell = tableView.dequeueReusableCell(withIdentifier: "WhatsappCell", for: indexPath)
            myCell.selectionStyle = .none
            return myCell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAccountTableViewCell", for: indexPath) as! MyAccountTableViewCell
        
        cell.lblName.text = mainArray[indexPath.section][indexPath.row]
        cell.iconImageView.image = iconArray[indexPath.section][indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if headerArray[indexPath.section] == "Shopping"
        {
            if mainArray[indexPath.section][indexPath.row] == "My Order"
            {
                isFromMyAccount = true
                isOrderBtnBack = true
                self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 0 : 1
                
            }else if mainArray[indexPath.section][indexPath.row] == "Bulk Order"
            {
                self.Check_BulkOrder_Register()
                
            }else if mainArray[indexPath.section][indexPath.row] == "New Offers"
            {
                let OfferVC = mainStoryBoard.instantiateViewController(withIdentifier: "OffersViewController") as! OffersViewController
                self.pushViewController(OfferVC)
            }else if mainArray[indexPath.section][indexPath.row] == "Delivery Address"
            {
                isFromSaveAddress = true
                isDeliveryAddress = true
                let DeliveryAddressVC = mainStoryBoard.instantiateViewController(withIdentifier: "DeliveryAddressViewController") as! DeliveryAddressViewController
                self.pushViewController(DeliveryAddressVC)
            }
        }else if headerArray[indexPath.section] == "Account Settings"
        {
            if mainArray[indexPath.section][indexPath.row] == "Personal Details"
            {
                let EditAccountVC = mainStoryBoard.instantiateViewController(withIdentifier: "EditAccountViewController") as! EditAccountViewController
                self.pushViewController(EditAccountVC)
                
            }else if mainArray[indexPath.section][indexPath.row] == "\(userDefault.string(forKey: "sellerTitle") ?? "")"
            {
                Call_Check_Seller_Registration_Status_API()
            }
        }else if headerArray[indexPath.section] == "Security & Settings"
        {
            if mainArray[indexPath.section][indexPath.row] == "Change Password"
            {
                let ChangePasswordVC = mainStoryBoard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                self.pushViewController(ChangePasswordVC)
            }
        }else if headerArray[indexPath.section] == "Policies"
        {
            if mainArray[indexPath.section][indexPath.row] == "Privacy & Policy"
            {
                UIApplication.shared.open(NSURL(string: privacyPolicyURL)! as URL, options: [:], completionHandler: nil)
            }else if mainArray[indexPath.section][indexPath.row] == "Terms & Conditions"
            {
                UIApplication.shared.open(NSURL(string: termsConditionURL)! as URL, options: [:], completionHandler: nil)
            }else if mainArray[indexPath.section][indexPath.row] == "Refund & Cancellation Policy"
            {
                UIApplication.shared.open(NSURL(string: refundURL)! as URL, options: [:], completionHandler: nil)
            }
        }else if headerArray[indexPath.section] == "Need Help?"
        {
            if mainArray[indexPath.section][indexPath.row] == "Visit Our Website"
            {
//                UIApplication.shared.open(NSURL(string: "https://printphoto.in")! as URL, options: [:], completionHandler: nil)
                
                DispatchQueue.main.async {
                    self.presentAlertWithTitle(title: "Alert", message: "Please visit printphoto.in website on laptop or desktop for more phone case design.", options: "OK", completion: { (ButtonIndex) in
                        
                        //                    UIApplication.shared.open(NSURL(string: "https://itunes.apple.com/us/app/print-photo-phone-case-maker/id1458325325?ls=1&mt=8")! as URL)
                        
                    })
                }
            
            }else if mainArray[indexPath.section][indexPath.row] == "Learn How To Edit Mobile Cover"
            {
                Help_Video()

            }else if mainArray[indexPath.section][indexPath.row] == "Contact Us"
            {
                let contactUsVC = mainStoryBoard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
                self.pushViewController(contactUsVC)
            }else if mainArray[indexPath.section][indexPath.row] == "Feedback/Complain"
            {
                let trackOrderVC = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderViewController") as! TrackOrderViewController
                trackOrderVC.isFromOrder = "MyAccount_Complain"
                pushViewController(trackOrderVC)
            }
            else if mainArray[indexPath.section][indexPath.row] == "Delete account"
            {
                let deleteProfileVC = mainStoryBoard.instantiateViewController(withIdentifier: "DeleteProfileController") as! DeleteProfileController
                pushViewController(deleteProfileVC)
            }
        }else if indexPath.section+1 == mainArray.count
        {
            let whatsappURL = "whatsapp://send?phone=\((userDefault.string(forKey: "contactusWhatsapp")) ?? "")&abid=12354"
            if let urlString = whatsappURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                if let whatsappURL = URL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL) {
                        UIApplication.shared.openURL(whatsappURL)
                    } else {
                        print("Install Whatsapp")
                        self.topMostViewController().view.makeToast("Whatsapp has been not installed.", duration: 2.0, position: .bottom)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headerArray[section]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if DeviceType.IS_IPAD {
            return 45.0
        }
        return 30.0
    }
        
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
//        header.backgroundView?.backgroundColor = UIColor.white
        
        view.tintColor = .white
        
        if DeviceType.IS_IPAD {
            header.textLabel?.font = UIFont(name: CustomFontWeight.bold, size: 26.0)
        }else
        {
            header.textLabel?.font = UIFont(name: CustomFontWeight.bold, size: 18.0)
        }
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Call_Check_Seller_Registration_Status_API() {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)check_seller_status", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        let SellerWalletVC = mainStoryBoard.instantiateViewController(withIdentifier: "SellerWalletViewController") as! SellerWalletViewController
                        self.pushViewController(SellerWalletVC)
                    }
                    else if responceDict["ResponseCode"] as! String == "2"
                    {
                        let SellerRegistrationVC = mainStoryBoard.instantiateViewController(withIdentifier: "SellerRegistrationViewController") as! SellerRegistrationViewController
                        self.pushViewController(SellerRegistrationVC)
                      
                    }else if responceDict["ResponseCode"] as! String == "0"
                    {
                        self.presentAlertWithTitle(title: "Status", message: "Pending", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    func Logout_API(Token:String) {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)logout?token=\(Token)", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
//                    let responceDict = Responce as! [String:Any]
                    
//                    if responceDict["ResponseCode"] as! String == "1"
//                    {
                    
                        userDefault.removeObject(forKey: "USER_ID")
                        userDefault.removeObject(forKey: "VarificationId")
                        userDefault.removeObject(forKey: "EditAccountDict")
                        userDefault.removeObject(forKey: "BadgeNumber")
                        userDefault.removeObject(forKey: "sellerTitle")
                        userDefault.removeObject(forKey: "sellerid")
                        userDefault.removeObject(forKey: "ShippingCharges")
                        userDefault.removeObject(forKey: "AuthToken")
//                        userDefault.removeObject(forKey: "RegionCode")
                        userDefault.removeObject(forKey: "Currency")
                        userDefault.removeObject(forKey: "countryId")

                        userDefault.synchronize()
                    
                        deliveryAddressArray.removeAll()

                        isGift = false
                        isFromSelect = ""
                        isLoginOrNot = ""
                    
                        let VCarray = self.navigationController?.viewControllers
                        for Controller in VCarray! {
                            if Controller is SignInViewController {
                                self.navigationController?.popToViewController(Controller, animated: true)
                                
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    // In this case we want to modify the badge number of the third tab:
                                    let tabItem = tabItems[(userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3]
                                    tabItem.badgeValue = nil
                                }
                            }
                        }
                        
//                    }
//                    else
//                    {
//                        self.presentAlertWithTitle(title: "\(responceDict["ResponseCode"] ?? "")", message: "", options: "OK", completion: { (ButtonIndex) in
//                        })
//                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: "Something went wrong!!", message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
        
    }
    
    
    func Check_BulkOrder_Register() {
        
        if Reachability.isConnectedToNetwork() {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)bulk_order_status?user_id=\(userDefault.value(forKey: "USER_ID") ?? "")", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["can_place_order"] as! Int == 1
                    {
                       
                        let BulkOrderVC = mainStoryBoard.instantiateViewController(withIdentifier: "BulkOrderViewController") as! BulkOrderViewController
                        self.pushViewController(BulkOrderVC)
                        
                    }else{
                        
                        self.presentAlertWithTitle(title: "\(responceDict["message"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
            
        }
        else
        {
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
        
    }
    
}

