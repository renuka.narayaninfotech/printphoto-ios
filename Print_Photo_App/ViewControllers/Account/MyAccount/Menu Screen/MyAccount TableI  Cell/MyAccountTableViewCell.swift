//
//  MyAccountTableViewCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 22/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class MyAccountTableViewCell: UITableViewCell {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblName.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
