//
//  DeleteProfileController.swift
//
//  Created by Hardik's Mac on 19/07/22.

//

import UIKit
import SkyFloatingLabelTextField
import MessageUI


class DeleteProfileController: BaseViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblGetInTouch: UILabel!
    @IBOutlet weak var lblAlwaysInyourReach: UILabel!
    @IBOutlet weak var txtSubject: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    //MARK: - Variable
    
    //MARK: - View controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        txtMessage.text = "Type your message here"
        txtMessage.textColor = UIColor.lightGray
    }
    
    //MARK: - Functions
    func isDataValid() -> Bool{
        var msg = ""
        
        if (self.txtMessage.text?.trim() == "") || (self.txtMessage.text?.trim() == "Type your message here")
        {
            msg = "Please enter message"
        }
        
        if (self.txtEmail.text?.trim() == "")
        {
            msg = "Please enter email"
        }
        else if !Validation.isValidEmail(emailString: self.txtEmail.text?.trim() ?? ""){
            msg = "Please enter valid email"
        }
        
        if (self.txtSubject.text?.trim() == "")
        {
            msg = "Please enter subject"
        }
        
        if msg != ""
        {
            presentAlertWithTitle(title: msg, message: "", options: "OK") { index in
                
            }
            return false
        }
        else
        {
            return true
        }
    }
    func Logout_API(Token:String) {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)logout?token=\(Token)", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
//                    let responceDict = Responce as! [String:Any]
                    
//                    if responceDict["ResponseCode"] as! String == "1"
//                    {
                    
                        userDefault.removeObject(forKey: "USER_ID")
                        userDefault.removeObject(forKey: "VarificationId")
                        userDefault.removeObject(forKey: "EditAccountDict")
                        userDefault.removeObject(forKey: "BadgeNumber")
                        userDefault.removeObject(forKey: "sellerTitle")
                        userDefault.removeObject(forKey: "sellerid")
                        userDefault.removeObject(forKey: "ShippingCharges")
                        userDefault.removeObject(forKey: "AuthToken")
//                        userDefault.removeObject(forKey: "RegionCode")
                        userDefault.removeObject(forKey: "Currency")
                        userDefault.removeObject(forKey: "countryId")

                        userDefault.synchronize()
                    
                        deliveryAddressArray.removeAll()

                        isGift = false
                        isFromSelect = ""
                        isLoginOrNot = ""
                    
                        let VCarray = self.navigationController?.viewControllers
                        for Controller in VCarray! {
                            if Controller is SignInViewController {
                                self.navigationController?.popToViewController(Controller, animated: true)
                                
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    // In this case we want to modify the badge number of the third tab:
                                    let tabItem = tabItems[(userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3]
                                    tabItem.badgeValue = nil
                                }
                            }
                        }
                        
//                    }
//                    else
//                    {
//                        self.presentAlertWithTitle(title: "\(responceDict["ResponseCode"] ?? "")", message: "", options: "OK", completion: { (ButtonIndex) in
//                        })
//                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: "Something went wrong!!", message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
        
    }
    
    //MARK: - button click
    
    @IBAction func btnPhoneNumberClicked(_ sender: Any) {
        let number = "+91-9712128928".components(separatedBy: .whitespaces).joined()
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func btnEmailClicked(_ sender: Any) {
        self.openMail(emailid: "printphoto44@gmail.com")
    }
    @IBAction func btnSubmitClicked(_ sender: Any) {
        if isDataValid(){
            let userDict = userDefault.value(forKey: "EditAccountDict") as! NSDictionary
            let token = userDict["token"] as? String ?? ""
          
            self.Logout_API(Token:token)
        }
    }
}

extension DeleteProfileController : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type your message here"
            textView.textColor = UIColor.lightGray
        }
    }
}
extension DeleteProfileController : MFMailComposeViewControllerDelegate{
    func openMail(emailid : String?) -> Void {
        guard let theEmail = emailid else { return }
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setToRecipients([theEmail])
        self.present(mc, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case .cancelled:
            break
        case .saved:
            break
        case .sent:
            break
        case .failed:
            break
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
