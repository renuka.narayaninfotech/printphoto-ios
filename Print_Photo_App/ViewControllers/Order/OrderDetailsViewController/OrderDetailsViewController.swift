//
//  OrderDetailsViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 19/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import SKPhotoBrowser

protocol BackOrder {
    func backToOrder (isFromCancel : Bool)
}

class OrderDetailsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,SKPhotoBrowserDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var tblProductInformation: UITableView!
    @IBOutlet weak var DeliveryStatusView: UIView!
    @IBOutlet weak var lblDeliveryStatus: UILabel!
    @IBOutlet weak var btnTrackOrder: UIButton!
    @IBOutlet weak var orderDetailsView: UIView!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblDateView: UIView!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblDiscountAmount: UILabel!
    @IBOutlet weak var lblGiftWrapAmount: UILabel!
    @IBOutlet weak var lblPaidAmount: UILabel!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var paymentMethodView: UIView!
    @IBOutlet weak var btnComplaint: UIButton!
    @IBOutlet weak var constrainHeightTblProductInfo: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnTrackOrderWidth: NSLayoutConstraint!
    @IBOutlet weak var lblShippingAmount: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var constraintdBtnCancelWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintsBtnRightPading: NSLayoutConstraint!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var heightGiftAmount: NSLayoutConstraint!
    @IBOutlet weak var heightOrderDetailsView: NSLayoutConstraint!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var productArray = NSArray()
    
    // arrOrder Pass to next view
    var itemDataDict = [String:Any]()
    var currencySymbol : String?
    var itemID : Int!

    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Order Details", EndString: "")
//        print(arrOrder)
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
       
    }
 
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize()
    {
        registerTableViewCell(cellName: "ProductInformationTableViewCell", to: tblProductInformation)
        self.constraintdBtnCancelWidth.constant = 0.0
        self.constraintsBtnRightPading.constant = 0.0
        Get_data()
        ViewBorder()
        addBackButton()
        addComplaintButton()
    }
    
    //MARK:- ********************** UIVIEW BORDER **********************
    
    func ViewBorder() {
        DeliveryStatusView.setBorder()
        orderDetailsView.setBorder()
        lblDateView.setBorder()
        paymentMethodView.setBorder()
    }
    
    //MARK:- **********************  BUTTON EVENT **********************
    
    @IBAction func btnTrackOrder(_ sender: UIButton) {
        
//        let itemDict = productArray.object(at: sender.tag) as! NSDictionary
        let orderID = "\(itemDataDict["order_id"] ?? "")"
        let trakingLink = "\(itemDataDict["tracking_link"] ?? "")"

        let trackOrderVC = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderViewController") as! TrackOrderViewController
        trackOrderVC.isFromOrder = "Track Order"
        orderIdFromOrder = orderID
        trackingURL = trakingLink
        pushViewController(trackOrderVC)
    }
    
    @IBAction func btnComplaint(_ sender: UIButton) {

        let orderID = "\(itemDataDict["order_id"] ?? "")"

        let trackOrderVC = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderViewController") as! TrackOrderViewController
        orderIdFromOrder = orderID
        trackOrderVC.isFromOrder = "Order_Complain"
        pushViewController(trackOrderVC)
        
//        let ComplainChatVC = mainStoryBoard.instantiateViewController(withIdentifier: "ComplainChatViewController") as! ComplainChatViewController
//        pushViewController(ComplainChatVC)
        
    }
    
    @IBAction func press_brn_cancel_order(_ sender: UIButton) {
     
        self.presentAlertWithTitle(title: "Order Cancellation", message: "Are you sure you want to cancel this order?", options: "NO","YES") { (ButtonIndex) in
            if ButtonIndex == 0
            {
                self.dismissViewController()
            }else
            {
               // self.Cancel_Order_API()
                let cancelViewController = mainStoryBoard.instantiateViewController(withIdentifier: "CancelViewController") as! CancelViewController
                cancelViewController.itemDataDict = self.itemDataDict
                cancelViewController.backOrderDelegate = self
                self.pushViewController(cancelViewController)
            }
        }

    }
    
    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addComplaintButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_complaint"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.complaintAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func complaintAction(_ sender: UIButton) {
        
        let orderID = "\(itemDataDict["order_id"] ?? "")"
        
        let trackOrderVC = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderViewController") as! TrackOrderViewController
        orderIdFromOrder = orderID
        trackOrderVC.isFromOrder = "Order_Complain"
        pushViewController(trackOrderVC)
        
//        let complaintVC = mainStoryBoard.instantiateViewController(withIdentifier: "ComplaintViewController") as! ComplaintViewController
//        pushViewController(complaintVC)
    }
    

    @objc func TaponImage(_ sender : UITapGestureRecognizer)
    {
        
        let imageView = sender.view as? UIImageView
        
        if imageView?.image != nil
        {
            SKPhotoBrowserOptions.displayAction = false   // action button will be hidden
            SKPhotoBrowserOptions.displayCloseButton = true   // Close Button will be hidden
            SKPhotoBrowserOptions.disableVerticalSwipe = true  // Disable Verticle swipe
            SKPhotoBrowserOptions.displayBackAndForwardButton = false // Disable Back and Forward button
            SKPhotoBrowserOptions.displayPagingHorizontalScrollIndicator = false
            SKPhotoBrowserOptions.displayCounterLabel = false
            SKPhotoBrowserOptions.enableSingleTapDismiss = true
            
            var images = [SKPhoto]()
            let photo = SKPhoto.photoWithImage(imageView?.image ?? UIImage())// add some UIImage
            images.append(photo)
            
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(0)
            present(browser, animated: true, completion: {})
        }
        
//        print("Done")
//        let imageView = sender.view as? UIImageView
//        let img = imageView?.image
//
//        var widthImg: CGFloat = 0.0
//        var heightImg: CGFloat = 0.0
//
//        if img != nil
//        {
//            self.navigationController?.isNavigationBarHidden = true
//
//            widthImg = img!.size.width
//            heightImg = img!.size.height
//
//            if widthImg > heightImg
//            {
//                // landscape image
//                if widthImg > ScreenSize.SCREEN_WIDTH - 40.0
//                {
//                    widthImg = ScreenSize.SCREEN_WIDTH - 40.0
//                    heightImg = widthImg / (widthImg/heightImg)
//                }
//            }
//            else
//            {
//                // portrait image
//                if heightImg > ScreenSize.SCREEN_HEIGHT - 60.0
//                {
//                    if (heightImg/widthImg) < (ScreenSize.SCREEN_HEIGHT/ScreenSize.SCREEN_WIDTH)
//                    {
//                        widthImg = ScreenSize.SCREEN_WIDTH - 40.0
//                        heightImg = widthImg / (widthImg/heightImg)
//                    }
//                    else
//                    {
//                        heightImg = ScreenSize.SCREEN_HEIGHT - 60.0
//                        widthImg = heightImg * (widthImg/heightImg)
//                    }
//                }
//                else if widthImg > ScreenSize.SCREEN_WIDTH - 40.0
//                {
//                    widthImg = ScreenSize.SCREEN_WIDTH - 40.0
//                    heightImg = widthImg / (widthImg/heightImg)
//                }
//
//            }
//
//
//            let newView = UIView()
//            newView.frame = self.view.frame
//            newView.backgroundColor = .black
//            newView.isUserInteractionEnabled = true
//
//            let newImageView = UIImageView()
//            newImageView.frame = CGRect.init(x: 0.0, y: 0.0, width: widthImg, height: heightImg)
//            newImageView.center = self.view.center
//            newImageView.backgroundColor = .black
//            newImageView.contentMode = .scaleAspectFit
//            newImageView.image = img
//            newImageView.isUserInteractionEnabled = false
//
//            newView.addSubview(newImageView)
//
//            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage(_:)))
//            newView.addGestureRecognizer(tap)
//            self.view.addSubview(newView)
//
//        }
        
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer)
    {
        self.navigationController?.isNavigationBarHidden = false
        sender.view?.removeFromSuperview()
    }
    
    @objc func pressButtonDeleteOrder(_ sender: UIButton?) {

        itemID = sender?.tag

        self.presentAlertWithTitle(title: "Order Cancellation", message: "Are you sure you want to cancel this order?", options: "NO","YES") { (ButtonIndex) in
            if ButtonIndex == 0
            {
                self.dismissViewController()
            }else
            {
                self.Cencel_Item_API()
            }
        }
        
    }
    
    //MARK:- ********************** TABLEVIEW  METHOD **********************
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductInformationTableViewCell") as! ProductInformationTableViewCell
        
//        let lastRowIndex = tableView.numberOfRows(inSection: tableView.numberOfSections-1)

        // Managed view border in Tableview cell First and Last index
        
        cell.productImageView.isExclusiveTouch = true
        cell.btnDelete.isExclusiveTouch = true

        let lastRowIndex = self.productArray.count - 1

        DispatchQueue.main.async {
            if self.productArray.count > 1
            {
                if indexPath.row == lastRowIndex
                {
                    self.clearBorder(view: cell.productInformationView)
                    cell.productInformationView.setBorder()
                }else
                {
                    self.clearBorder(view: cell.productInformationView)
                    cell.productInformationView.addTopBorderWithColor(color: hexStringToUIColor(hex: "0ABAD4"), width: 2)
                    cell.productInformationView.addLeftBorderWithColor(color: hexStringToUIColor(hex: "0ABAD4"), width: 2)
                    cell.productInformationView.addRightBorderWithColor(color: hexStringToUIColor(hex: "0ABAD4"), width: 2)
                }
            }else if self.productArray.count == 1
            {
                self.clearBorder(view: cell.productInformationView)
                cell.productInformationView.setBorder()
            }
        }
        
        let itemDict = productArray.object(at: indexPath.row) as? [String:Any]

        // CONVERT URL STRING TO UIIMAGE
        
        let arrDisplayImage = itemDict?["display_images"] as? [Any]
        
        let imageString = (arrDisplayImage?[0] as! [String:Any])["image"] as? String ?? ""
        
        let imgRotateDict = itemDict?["display"] as? [String:Any]
        
        let isRotate = "\(imgRotateDict?["rotate"] ?? "")"
        
        if isRotate == "1"
        {
            cell.productImageView.setImageWithRotation(indicatorStyle: .gray, imageURL: imageString)
        }else
        {
            cell.productImageView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL : imageString)
        }
        
        cell.lblProductName.text = itemDict?["model_name"] as? String
        cell.lblPrice.text = "Amount : \(currencySymbol ?? "")\(String(format: "%.2f", (itemDict?["subtotal"] as? Double ?? 0.0)))" //\(itemDict?["subtotal"] ?? "")"
        cell.lblQuantity.text = "Quantity : \(itemDict?["quantity"] ?? "")"

        cell.productImageView.contentMode = .scaleAspectFit

        DispatchQueue.main.async {
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.TaponImage(_:)))
            tap.delegate = self
            tap.numberOfTouchesRequired = 1
            cell.productImageView.addGestureRecognizer(tap)
        }
        cell.productImageView?.layoutIfNeeded()
        cell.productImageView.setNeedsLayout()
        
        itemID = itemDict?["id"] as? Int ?? 0
      
        // Managed Delete button is hide or not when cancel all order.
        let isCancel = "\(itemDict?["is_cancellable"] ?? "")"
        DispatchQueue.main.async {
            
            if isCancel == "0"
            {
                cell.btnDelete.isHidden = true
                cell.widthDeleteButton.constant = 0
            }else
            {
                cell.btnDelete.isHidden = false
                cell.widthDeleteButton.constant = DeviceType.IS_IPAD ? 33.0 : 22.0
            }
            
        }
        
        // For delete perticular record
        cell.btnDelete.tag = itemID
        cell.btnDelete.addTarget(self, action: #selector(self.pressButtonDeleteOrder(_:)), for: .touchDown)
        
        return cell
    }
    
    // Check if there is border of view
    func clearBorder(view: UIView)
    {
        if view.layer.sublayers!.last!.backgroundColor == hexStringToUIColor(hex: "0ABAD4").cgColor
        {
            view.layer.sublayers!.last!.backgroundColor = UIColor.clear.cgColor
        }
        else if view.layer.borderColor == UIColor(red:10/255, green:186/255, blue:212/255, alpha: 1).cgColor
        {
            view.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        print("content size = \(tableView.contentSize.height)")
        
        DispatchQueue.main.async {
            self.constrainHeightTblProductInfo.constant = tableView.contentSize.height
            self.tblProductInformation.layoutIfNeeded()
        }
    }
    
    //MARK:- ********************** API CALLING **********************

    func Get_data() {

//        showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)

        self.productArray = itemDataDict["order_items"] as! NSArray

        let isCancel = "\(itemDataDict["is_cancellable"] ?? "")"
        
        DispatchQueue.main.async {
            if isCancel == "0"
            {
                self.constraintdBtnCancelWidth.constant = 0.0
                self.constraintsBtnRightPading.constant = 0.0
            }else
            {
                if DeviceType.IS_IPAD
                {
                    self.constraintdBtnCancelWidth.constant = self.btnView.bounds.width/2 - 15
                    self.constraintsBtnRightPading.constant = 15.0
                }else{
                    self.constraintdBtnCancelWidth.constant = self.btnView.bounds.width/2 - 10
                    self.constraintsBtnRightPading.constant = 10.0
                }
            }
            
            self.btnCancel.setNeedsLayout()
            self.btnCancel.layoutIfNeeded()
        }
        
//        var dict = NSDictionary()
        
        let trackNumber = "\(itemDataDict["show_tracking"] ?? "")"

        if trackNumber == "0"
        {
            constraintBtnTrackOrderWidth.constant = 0
            btnTrackOrder.isHidden = true
        }else
        {
            btnTrackOrder.isHidden = false

            if DeviceType.IS_IPAD
            {
                constraintBtnTrackOrderWidth.constant = 150.0
            }else{
                constraintBtnTrackOrderWidth.constant = 100.0
            }
        }
//        var totalAmount = 0
//        var discountAmt : String?
//
//        for i in 0..<self.productArray.count
//        {
//             dict = self.productArray[i] as! NSDictionary
//
//            let orderStatus = "\(itemDataDict["order_status"] ?? "")"
//            lblDeliveryStatus.text = orderStatus
//
//            lblOrderDate.text = "Order Date : \(convertDateFormater("\(itemDataDict["date"] ?? "")"))"
//
//            discountAmt = "\(dict.value(forKey: "code_discount") ?? "")"
//
//            let price = "\(dict.value(forKey: "price") ?? "")"
//            totalAmount = totalAmount + Int(price)!
//        }
//
//        totalAmount = totalAmount + Int(userDefault.string(forKey: "ShippingCharges")!)! - Int(discountAmt ?? "")!
//
//        lblTotalAmount.text = "\u{20B9}\(totalAmount)"
//        lblDiscountAmount.text = "-\u{20B9}\(Int(discountAmt ?? "")!)"
//        lblPaidAmount.text = "\u{20B9}\(dict.value(forKey: "paid_amount") as! Int)"
//        lblPaymentMethod.text = "\(dict.value(forKey: "transaction_type") as! String)"
        
        lblDeliveryStatus.text = "\(itemDataDict["order_status"] ?? "")"
        lblOrderDate.text = "Order Date : \("\(itemDataDict["date"] ?? "")")"
        lblShippingAmount.text = "+ \(currencySymbol ?? "")\(String(format: "%.2f", (itemDataDict["shipping"] as? Double ?? 0.0)))"//\(itemDataDict["shipping"] ?? "")"
        lblTotalAmount.text = "\(currencySymbol ?? "")\(String(format: "%.2f", (itemDataDict["total_amount"] as? Double ?? 0.0)))"//\(itemDataDict["total_amount"] ?? "")"
        
        let discountAmt = "\(String(format: "%.2f", (itemDataDict["discount_amount"] as? Double ?? 0.0)))"//"\(itemDataDict["discount_amount"] ?? "")"
       
        lblDiscountAmount.text = discountAmt == "0.00" ? "\(currencySymbol ?? "")\(discountAmt)" : "- \(currencySymbol ?? "")\(discountAmt)"

//        lblDiscountAmount.text = discountAmt == "0.00" ? "\(currencySymbol ?? "")\(itemDataDict["discount_amount"] ?? "")" : "- \(currencySymbol ?? "") \(itemDataDict["discount_amount"] ?? "")"
       
        let amtGift = "\(itemDataDict["is_gift"] ?? "")"
        
        if amtGift == "0"
        {
            lblGiftWrapAmount.text = "\(currencySymbol ?? "")\(String(format: "%.2f", (itemDataDict["gift_charge"] as? Double ?? 0.0)))"//\(itemDataDict["gift_charge"] ?? "")"
            heightGiftAmount.constant = 0.0
            heightOrderDetailsView.constant = DeviceType.IS_IPAD ? 292.5 - 48.0 : 195.0 - lblGiftWrapAmount.frame.height
        }else
        {
            lblGiftWrapAmount.text = "+ \(currencySymbol ?? "")\(String(format: "%.2f", (itemDataDict["gift_charge"] as? Double ?? 0.0)))" //\(itemDataDict["gift_charge"] ?? "")"
            heightGiftAmount.constant = DeviceType.IS_IPAD ? 48.0 : 32.5
            heightOrderDetailsView.constant = DeviceType.IS_IPAD ? 292.5 : 195.0
        }
        
        self.orderDetailsView.setNeedsDisplay()
        self.orderDetailsView.layoutIfNeeded()
        
        lblPaidAmount.text = "\(currencySymbol ?? "")\(String(format: "%.2f", (itemDataDict["paid_amount"] as? Double ?? 0.0)))"//\(itemDataDict["paid_amount"] ?? "")"
        lblPaymentMethod.text = "\(itemDataDict["transaction_type"] ?? "")"
        
        tblProductInformation.reloadData()
        tblProductInformation.layoutIfNeeded()
        
//        self.RemoveLoader()
    }
    
    func Cancel_Order_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            // Get App Version
            
            let ParamDict = ["order_id" : "\(itemDataDict["order_id"] ?? "")"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)cancel_order", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        let dict = responceDict["data"] as? [String:Any]

                        self.presentAlertWithTitle(title: "Order Cancellation", message: "\(dict?["message"] ?? "")", options: "OK", completion: { (ButtonIndex) in
                            self.popViewController()
                        })
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    
    func Cencel_Item_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            // Get App Version
            
            let ParamDict = ["order_item_id" : itemID] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)cancel_order_item", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        
                        self.presentAlertWithTitle(title: "Order Cancellation", message: "Item Cancelled", options: "OK", completion: { (ButtonIndex) in
                            self.popViewController()
                        })
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
}

extension OrderDetailsViewController : BackOrder{
    func backToOrder(isFromCancel: Bool) {
        if isFromCancel {
            self.popViewController()
        }
    }
}

