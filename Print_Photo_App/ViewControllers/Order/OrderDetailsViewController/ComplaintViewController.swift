//
//  ComplaintViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 21/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import DropDown
import Photos

fileprivate enum AMDropDownType {
    case subject
}

class ComplaintViewController: BaseViewController,UIGestureRecognizerDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var selectSubjectBorderView: UIView!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var addImageBorderView: UIView!
    @IBOutlet weak var lblImageName: UILabel!
    @IBOutlet weak var txtComplaint: UITextView!
    
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var dropDown = DropDown()
    fileprivate var selectedDropDown : AMDropDownType?
    var picker = UIImagePickerController()
    var imgBrowse : UIImage!
    var imgIndex = 0

    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()

        SetNavigationBarTitle(Startstring: "Complaint", EndString: "")

        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dropDown.hide()
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        ViewBorder()
        addBackButton()
        addSendButton()
        ShowDropDown()
        Reset()
        
//        txtComplaint.returnKeyType = .done
        txtComplaint.enablesReturnKeyAutomatically = false
        hideKeyboardWhenTappedAround()
    }
    
    //MARK:- ********************** UIVIEW BORDER **********************

    func ViewBorder() {
        selectSubjectBorderView.setBorder()
        addImageBorderView.setBorder()
        txtComplaint.setBorder()
    }
    
    //MARK:- ********************** RESET TEXTFIELD **********************

    func Reset()
    {
        lblSubject.text = "Select Subject"
        lblImageName.text = "Select Image"
        txtComplaint.text = "Please provide your complaint"
        txtComplaint.textColor = UIColor.lightGray
    }
    
    //MARK:- ********************** BUTTON EVENT **********************

    @IBAction func pressBtn_SelectSubject(_ sender: UIButton) {
        view.endEditing(true)
        selectedDropDown = AMDropDownType.subject
        dropDown.anchorView = self.lblSubject
        dropDown.dataSource = categoryArray as! [String]
        dropDown.show()
    }
    
    @IBAction func pressBtn_selectImage(_ sender: UIButton) {
        
        let alert:UIAlertController=UIAlertController(title: "Add Photo!", message: nil, preferredStyle: .alert)
        let cameraAction = UIAlertAction(title: "Take Photo", style: .default)
        {
            UIAlertAction in
            
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
            switch cameraAuthorizationStatus {
            case .notDetermined: self.requestCameraPermission()
            case .authorized: self.openCamera()
            case .restricted, .denied: self.alertCameraAccessNeeded()
           
            }
        }
        let gallaryAction = UIAlertAction(title: "Choose from Gallery", style: .default)
        {
            UIAlertAction in
            
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    self.openGallary()
                    break
                case .denied, .restricted:
                    self.alertGalleryAccessNeeded()
                    break
                case .notDetermined:
                    self.requestGalleryPermission()
                    break
                    
                case .limited:
                    self.alertGalleryAccessPermissionChangeNeeded()
                 default:
                    break
                }
            }
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        picker.delegate = self
        picker.modalPresentationStyle = .overCurrentContext
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
        
//        OpenImagePicker()
    }
    
    @IBAction func pressBtn_submit(_ sender: UIButton) {
        if isReadyToComplaint()
        {
            Submit_Complaint_API()
        }
        
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addSendButton() {
        let sendButton = UIButton(type: .custom)
        sendButton.setImage(UIImage(named: "ic_send"), for: .normal)
        sendButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        sendButton.addTarget(self, action: #selector(self.sendAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: sendButton)
    }
    
    @objc func sendAction(_ sender: UIButton) {
        if isReadyToComplaint()
        {
            Submit_Complaint_API()
        }
    }
    
    
    //MARK:- ********************** Don't Allowe Camera & Gallry Permission **********************
    
    // Camera USed
    func alertCameraAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        
        let alert = UIAlertController(
            title: "Need Camera Access",
            message: "Camera access is required to make full use of this app.",
            preferredStyle: UIAlertController.Style.alert
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func requestCameraPermission() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
            self.openCamera()
        })
    }
    
    // Gallery Used
    func alertGalleryAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        
        let alert = UIAlertController(
            title: "Need Gallery Access",
            message: "Gallery access is required to make full use of this app.",
            preferredStyle: UIAlertController.Style.alert
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Gallery Access", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func requestGalleryPermission() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
            self.openGallary()
        })
    }
    func alertGalleryAccessPermissionChangeNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Change Gallery Access",
                message: " Change Gallery access from Selected Photos to All Photos is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) -> Void in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Change Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- ********************** DROP DOWN **********************

    func ShowDropDown() {
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            print("Selected item: \(item) at index: \(index)")
            
            if self.selectedDropDown == AMDropDownType.subject {
                
                // Get subject name
                self.lblSubject.text = item
            }
        }
    }
    
    //MARK:- ********************** IMAGEPICKERVIEW DELEGATE **********************

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if info[.imageURL] != nil && !((info[.imageURL] as? URL)?.absoluteString.uppercased().hasSuffix("GIF"))!{
            if let asset = info[.phAsset] as? PHAsset {
                PHImageManager.default().requestImageData(for: asset, options: nil) { (dataObj, uti, orientation, information) in
                    
                    self.imgBrowse = info[.originalImage] as? UIImage
                    
                    let imageUrl = info[.imageURL] as! NSURL
                    let imageName = imageUrl.lastPathComponent
                    self.lblImageName.text = imageName
                    print("Image Name :- \(imageName ?? "")")
                    
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        else
        {
                    self.imgBrowse = info[.originalImage] as? UIImage
            
                    imgIndex += 1
                    let imageName = UIDevice.current.identifierForVendor!
                    self.lblImageName.text = "\(imageName)_\(imgIndex).jpeg"
                    print("Image Name :- \(imageName)_\(imgIndex).jpeg")
            
                    self.dismiss(animated: true, completion: nil)

        }
    }
    
    
    //MARK:- ********************** IMAGE SELECTION **********************

    func OpenImagePicker() {

        self.openGallary()
        picker.delegate = self
        picker.modalPresentationStyle = .overCurrentContext

    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(.camera))
        {
            picker.sourceType = .camera
            
            AVCaptureDevice.authorizationStatus(for: .video)
            
//            let imageName = picker.sourceType
//            self.lblImageName.text = imageName
//            print("Image Name :- \(imageName ?? "")")
            
            self .present(picker, animated: true, completion: nil)
        }
        
    }

    func openGallary()
    {
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    
    //MARK:- ********************** VALIDATION **********************

    func isReadyToComplaint()-> Bool {
        if lblSubject.text == "Select Subject" {
            presentAlertWithTitle(title: "Please select subject", message: "", options: "OK") { (Int) in
            }
        }else if txtComplaint.text == "Please provide your complaint" || txtComplaint.text.trim() == ""
        {
            presentAlertWithTitle(title: "Please enter your complaint", message: "", options: "OK") { (Int) in
            }
        }
        else{
            
            return true
        }
        
        return false
    }
    
    //MARK:- **********************  TEXTVIEW DELEGATE METHOD **********************

//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if text == "\n"  // Recognizes enter key in keyboard
//        {
//            textView.resignFirstResponder()
//            return false
//        }
//        return true
//    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtComplaint.textColor == UIColor.lightGray {
            txtComplaint.text = nil
            txtComplaint.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtComplaint.text.isEmpty {
            txtComplaint.text = "Please provide your complaint"
            txtComplaint.textColor = UIColor.lightGray
        }
    }
    
    //MARK:- ********************** API CALLING **********************

    func Submit_Complaint_API()
    {
        self.view.endEditing(true)
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            var ParamDict = NSDictionary()
            
            if imgBrowse != nil
            {
                // Call API with Image
                
                 ParamDict = ["user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                                 "message": "\(lblSubject.text ?? "") : \(txtComplaint.text ?? "")"] as NSDictionary
                
                AFNetworkingAPIClient.sharedInstance.postAPICallWithImage(url: "\(BaseURL)new_complains", parameters: ParamDict, image: imgBrowse!, imgKey: "image", mimeType: "image/jpeg") {(APIResponce, Responce, error1) in
                    
                    self.RemoveLoader()
                    
                    if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                    {
                        
                        let responceDict = Responce as! [String:Any]
                        
                        if responceDict["ResponseCode"] as! String == "1"
                        {
                            let vc = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - 2]
                            vc.view.makeToast("Success", duration: 2.0, position: .bottom)
                            self.popViewController()

                        }
                        else
                        {
                            self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            })
                        }
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                        
                    }
                }
            }else
            {
                // Call API without Image

                ParamDict = ["user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                             "message": "\(lblSubject.text ?? "") : \(txtComplaint.text ?? "")",
                             "image" : nil] as NSDictionary
                
                AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)new_complains", parameters: ParamDict) {(APIResponce, Responce, error1) in
                    
                    self.RemoveLoader()
                    
                    if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                    {
                        
                        let responceDict = Responce as! [String:Any]
                        
                        if responceDict["ResponseCode"] as! String == "1"
                        {
                            let vc = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - 2]
                            vc.view.makeToast("Success", duration: 2.0, position: .bottom)
                            self.popViewController()
                            
                        }
                        else
                        {
                            self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            })
                        }
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                          
                        })
                        
                    }
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
               
            })
        }
    }
}
