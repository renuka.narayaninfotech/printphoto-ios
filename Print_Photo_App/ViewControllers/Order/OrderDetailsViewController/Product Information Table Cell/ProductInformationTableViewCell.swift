//
//  ProductInformationTableViewCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 19/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class ProductInformationTableViewCell: UITableViewCell {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var productInformationView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var widthDeleteButton: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblProductName.sizeToFit()
        lblPrice.sizeToFit()
        lblQuantity.sizeToFit()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
