//
//  ComplainChatViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 23/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class ComplainChatViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UIGestureRecognizerDelegate //, UITableViewDragDelegate
{
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var txtTyping: UITextView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet var contrainBottomBorderView: NSLayoutConstraint!
    
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var chatMessageArray = [[String:Any]]()
    var messageArray = NSMutableArray()
    var tempArray = NSMutableArray()
    var strDefault : String!
    var currentTime : String!
    var sendMessageArray = NSMutableArray()
    let strTemp = NSMutableDictionary()
    var initialConstantValueOfcontrainBottomBorderView : CGFloat?

    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Complain Chat", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
           IQKeyboardManager.shared.enable = false
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize()
    {
        registerTableViewCell(cellName: "tblChatReceiverCell", to: tblChat)
        registerTableViewCell(cellName: "tblChatSenderCell", to: tblChat)
        
        removeBackButton()
        addBackButton()
        borderView.setBorder()
        Receive_Message_API()
        Get_Current_Time()
        
//        tblChat.dragDelegate = self
        txtTyping.delegate = self
        txtTyping.text = "Start Typing...."
        txtTyping.textColor = UIColor.lightGray
        
//        txtTyping.returnKeyType = .done
        txtTyping.enablesReturnKeyAutomatically = false
        
        
        //set observer to get height of keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboardNotification(notif:)), name: UIResponder.keyboardDidShowNotification , object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboardNotification(notif:)), name: UIResponder.keyboardDidHideNotification , object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
         initialConstantValueOfcontrainBottomBorderView = contrainBottomBorderView.constant
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        
    }
    
    
     //MARK:- **********************  KEYBOARD EVENT **********************
    @objc func showKeyboardNotification(notif: NSNotification) -> Void {
        guard let userInfo = notif.userInfo else {return}
        
        if let myData = userInfo["UIKeyboardFrameBeginUserInfoKey"] as? CGRect {
            
//            //set bottom of border view
//            contrainBottomBorderView.constant = myData.height + borderView.frame.size.height - 30
//
//            //set scroll to bottom
//            self.tblChat.scroll(to: .bottom, animated: false)
            
        }
    }
    
    @objc func hideKeyboardNotification(notif: NSNotification) -> Void {
        guard let userInfo = notif.userInfo else {return}
        
        if (userInfo["UIKeyboardFrameEndUserInfoKey"] as? CGRect) != nil {
            
            //set bottom of border view
            contrainBottomBorderView.constant = initialConstantValueOfcontrainBottomBorderView ?? 0.0
        }
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        
        if let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("keyboard height = \(keyboardRect.height)")
            
            
            // let window = UIApplication.shared.keyWindow
            
            //let bottomPadding = window?.safeAreaInsets.bottom
            
            //set bottom of border view
            contrainBottomBorderView.constant = keyboardRect.height //+ borderView.frame.size.height - 75
            
            //set scroll to bottom
            self.tblChat.scroll(to: .bottom, animated: false)
        }
    }

    
    //MARK:- **********************  GET CURRENT TIME **********************
    
    func Get_Current_Time() {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        
        currentTime = formatter.string(from: Date())
    }
   
    //MARK:- **********************  BUTTON EVENT **********************
    
    @IBAction func btnSend(_ sender: UIButton) {
        if isReadyToSendComplain()
        {
            contrainBottomBorderView.constant = initialConstantValueOfcontrainBottomBorderView ?? 0.0
            self.txtTyping.endEditing(true)
            Send_Message_API()
        }
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    @objc func TaponImage(_ sender : UITapGestureRecognizer)
    {
        print("Done")
        let imageView = sender.view as? UIImageView
        let img = imageView?.image
        
        var widthImg: CGFloat = 0.0
        var heightImg: CGFloat = 0.0
        
        if img != nil
        {
            self.navigationController?.isNavigationBarHidden = true
            self.tabBarController?.tabBar.isHidden = true
            
            widthImg = img!.size.width
            heightImg = img!.size.height
            
            if widthImg > heightImg
            {
                // landscape image
                if widthImg > ScreenSize.SCREEN_WIDTH - 40.0
                {
                    widthImg = ScreenSize.SCREEN_WIDTH - 40.0
                    heightImg = widthImg / (widthImg/heightImg)
                }
            }
            else
            {
                // portrait image
                if heightImg > ScreenSize.SCREEN_HEIGHT - 60.0
                {
                    if (heightImg/widthImg) < (ScreenSize.SCREEN_HEIGHT/ScreenSize.SCREEN_WIDTH)
                    {
                        widthImg = ScreenSize.SCREEN_WIDTH - 40.0
                        heightImg = widthImg / (widthImg/heightImg)
                    }
                    else
                    {
                        heightImg = ScreenSize.SCREEN_HEIGHT - 60.0
                        widthImg = heightImg * (widthImg/heightImg)
                    }
                }
                else if widthImg > ScreenSize.SCREEN_WIDTH - 40.0
                {
                    widthImg = ScreenSize.SCREEN_WIDTH - 40.0
                    heightImg = widthImg / (widthImg/heightImg)
                }
                
            }
            
            
            let newView = UIView()
            newView.frame = self.view.frame
            newView.backgroundColor = .black
            newView.isUserInteractionEnabled = true
            
            let newImageView = UIImageView()
            newImageView.frame = CGRect.init(x: 0.0, y: 0.0, width: widthImg, height: heightImg)
            newImageView.center = self.view.center
            newImageView.backgroundColor = .black
            newImageView.contentMode = .scaleAspectFit
            newImageView.image = img
            newImageView.isUserInteractionEnabled = false
            
            newView.addSubview(newImageView)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage(_:)))
            newView.addGestureRecognizer(tap)
            self.view.addSubview(newView)
            
        }
        
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
    //MARK:- ********************** VALIDATION **********************
    
    func isReadyToSendComplain()-> Bool {
        print("a\(txtTyping.text ?? "")b")
        if txtTyping.text == "Start Typing...." || txtTyping.text == "" || txtTyping.text.trim() == ""
            {
                presentAlertWithTitle(title: "Please enter text", message: "", options: "OK") { (Int) in
            }
        }
        else{
            
            return true
        }
        
        return false
    }
    
   
    //MARK:- ********************** TABLEVIEW  METHOD **********************
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tempArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let messageDict = tempArray.object(at: indexPath.row) as! NSDictionary
        
        if messageDict.value(forKey: "replay") != nil
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "tblChatReceiverCell", for: indexPath) as! tblChatReceiverCell
            
            cell.lblMessage.text = "\(messageDict.value(forKey: "replay") as? String ?? "")"
            
            if (messageDict.value(forKey: "image") as? String) != ""
            {
//                let imageString = "\(messageDict.value(forKey: "image") as? String ?? "")"

//                cell.imgVIew.frame.size.width = self.view.frame.width
                cell.constraintHeightImage.constant = self.view.frame.width
                cell.imgVIew.contentMode = .scaleAspectFit

//                cell.imgVIew.frame =  cell.imgVIew.contentClippingRect
                
                cell.imgVIew.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: messageDict.value(forKey: "image") as! String)

//                let url = URL(string:imageString)
//                if let data = try? Data(contentsOf: url!)
//                {
//                    cell.imgVIew.image = UIImage(data: data)
//                }
                DispatchQueue.main.async {
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.TaponImage(_:)))
                    tap.delegate = self
                    tap.numberOfTouchesRequired = 1
                    cell.imgVIew.addGestureRecognizer(tap)
                }
            }
            else
            {
//                    cell.imageView?.frame.size.width = cell.lblMessage.bounds.size.width
                cell.constraintHeightImage.constant = 0.0
//                cell.imgVIew.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: "")
            }
            cell.imgVIew?.layoutIfNeeded()
            cell.imgVIew.setNeedsLayout()
            
//            let replayDate = (messageDict.value(forKey: "replayDate") as? String)?.toDate(inputDateFormat: "yyyy-MM-dd HH:mm:ss")
//            let time = replayDate?.toString(dateFormat: "h:mm a")
            
            cell.lblTime.text = "\(messageDict.value(forKey: "replayDate") ?? "")"
            
            return cell
        }else if messageDict.value(forKey: "complain") != nil
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "tblChatSenderCell", for: indexPath) as! tblChatSenderCell
            cell.lblMessage.text = "\(messageDict.value(forKey: "complain") as? String ?? "")"
//            let replayDate = (messageDict.value(forKey: "complainDate") as? String)?.toDate(inputDateFormat: "yyyy-MM-dd HH:mm:ss")
//            let time = replayDate?.toString(dateFormat: "h:mm a")
            cell.lblTime.text = "\(messageDict.value(forKey: "complainDate") ?? "")"
            return cell
        }else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "tblChatReceiverCell", for: indexPath) as! tblChatReceiverCell
            cell.lblMessage.text = "\(messageDict.value(forKey: "strDefault") as? String ?? "")"
            cell.lblTime.text = "\(messageDict.value(forKey: "time") as? String ?? "")"
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
    //        return UITableView.automaticDimension
    //    }

//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//
//        contrainBottomBorderView.constant = initialConstantValueOfcontrainBottomBorderView ?? 0.0
//
//    }
//
//    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
//        return [UIDragItem]()
//    }
    
    //MARK:- **********************  TEXTVIEW DELEGATE METHOD **********************
    
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//
//        super.touchesBegan(touches, with: event)
//
//        let touch: UITouch = (touches.first)!
//
//        if (touch.view == self.view){
//            self.txtTyping.endEditing(true)
//        }
//
//    }
//
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if isReadyToSendComplain()
        {
            txtTyping.resignFirstResponder()

        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if txtTyping.tag == 111
        {
//            txtTyping.text.removingWhitespaces()
            
            if txtTyping.text.removingWhitespaces() == ""
            {
                    presentAlertWithTitle(title: "Please enter text", message: "", options: "OK") { (Int) in
                    }
            }
        }
    }
    
//
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if text == "\n"  // Recognizes enter key in keyboard
//        {
//            txtTyping.resignFirstResponder()
//            return false
//        }
//        return true
//    }
//
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if txtTyping.textColor == UIColor.lightGray {
            txtTyping.text = ""
            txtTyping.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        contrainBottomBorderView.constant = initialConstantValueOfcontrainBottomBorderView ?? 0.0

        if txtTyping.text.isEmpty {
            txtTyping.text = "Start Typing...."
            txtTyping.textColor = UIColor.lightGray
        }
    }

    //MARK:- **********************  API CALLING **********************
    
    func Receive_Message_API() {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                             "order_id": orderIdFromOrder] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)get_response", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.chatMessageArray = responceDict["data"] as! [[String:Any]]
                        
                        for i in 0..<self.chatMessageArray.count
                        {
                            let dict = self.chatMessageArray[i]
                            let tempCompalinDict = NSMutableDictionary()
                            
                            self.tempArray.remove("strDefault")

                            if dict["complain"] as? String != ""
                            {
                                tempCompalinDict.setValue(dict["complain"], forKey: "complain")
                                tempCompalinDict.setValue(dict["date"], forKey: "complainDate")
                                self.tempArray.add(tempCompalinDict)
                                
                            }
                            //                                print(tempCompalinDict)
                            //                                print(dict)
                            
                            let dictReplayArray = dict["response"] as! NSArray
                            //                                print(dictReplayArray)
                            
                            for j in 0..<dictReplayArray.count
                            {
                                let dictReplay = dictReplayArray[j] as! NSDictionary
                                let tempReplayDict = NSMutableDictionary()
                                
                                if dictReplay.value(forKey: "replay") as? String != ""
                                {
                                    tempReplayDict.setValue(dictReplay.value(forKey: "replay"), forKey: "replay")
                                    tempReplayDict.setValue(dictReplay.value(forKey: "date"), forKey: "replayDate")
                                    tempReplayDict.setValue(dictReplay.value(forKey: "n_image"), forKey: "image")
                                    self.tempArray.add(tempReplayDict)
                                    
                                }
                                  print(tempReplayDict)
                            }
                            
                        }
                        
                    }
                    else
                    {
                        self.strTemp.setValue("Provide Your Complain", forKey: "strDefault")
//                        self.strTemp.setValue(self.currentTime, forKey: "time")
                        self.tempArray.add(self.strTemp)
                    }
                    print(self.tempArray)
                    self.tblChat.reloadData()
                    self.tblChat.scroll(to: .bottom, animated: false)

                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Receive_Message_API()
                        }
                    })
                }
                
            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Receive_Message_API()
                }
            })
        }
    }
    
    func Send_Message_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)

            let ParamDict = ["order_id": orderIdFromOrder,
                             "user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                             "complain" : txtTyping.text] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)add_complain", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.txtTyping.text = "Start Typing...."
                        self.txtTyping.textColor = UIColor.lightGray
                        
                        self.topMostViewController().view.makeToast("Complain send successfully", duration: 2.0, position: .bottom)

                        let dict = responceDict["data"] as! NSDictionary
                        let tempSendDict = NSMutableDictionary()
                        
                        self.tempArray.remove("strDefault")
                        
                        if dict.value(forKey: "complain") as? String != ""
                        {
                            tempSendDict.setValue(dict.value(forKey: "complain"), forKey: "complain")
                            tempSendDict.setValue(dict.value(forKey: "date"), forKey: "complainDate")
                            self.tempArray.add(tempSendDict)
                            
                        }
                        print(self.tempArray)
                        self.tblChat.reloadData()
                        self.tblChat.scroll(to: .bottom, animated: false)
                        
                    }else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Send_Message_API()
                        }
                    })
                }
            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Send_Message_API()
                }
            })
        }
    }
}
