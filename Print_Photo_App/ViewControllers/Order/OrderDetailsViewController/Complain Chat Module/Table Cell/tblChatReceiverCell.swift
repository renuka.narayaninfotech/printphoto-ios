//
//  tblChatReceiverCell.swift
//  Prontopegno
//
//  Created by V3 on 26/12/18.
//  Copyright © 2018 Vasundhara. All rights reserved.
//

import UIKit

class tblChatReceiverCell: UITableViewCell {

    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet weak var imgVIew: UIImageView!
    @IBOutlet weak var constraintHeightImage: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblMessage.sizeToFit()
        lblTime.sizeToFit()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
