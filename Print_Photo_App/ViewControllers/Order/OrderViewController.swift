//
//  OrderViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 09/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import GoogleMobileAds

class OrderViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var orderTableView: UITableView!
    @IBOutlet weak var signIn_signUp_View: UIView!
    @IBOutlet weak var btnSignUP: UIButton!
    @IBOutlet weak var btnLogIn: UIButton!
   
    @IBOutlet weak var nativeAdPlaceholder: UIView!
    
    @IBOutlet var viewMainPopup: UIView!
    @IBOutlet var viewPopup: UIView!
    @IBOutlet weak var btnSignupVertical: NSLayoutConstraint!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var refreshControl = UIRefreshControl()
    var orderArray = [[String:Any]]()
    
    // For Native Advanced ads
    var heightConstraint : NSLayoutConstraint?
    var adLoader: GADAdLoader!
    var nativeAdView: GADNativeAdView!
    var currencySymbol : String?
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SetNavigationBarTitle(Startstring: "Order", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(isLoginOrNot == "order_signup") {
            isLoginOrNot = ""
            ((self.tabBarController!.viewControllers! as NSArray)[(userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3] as! UINavigationController).popToRootViewController(animated: false)
            self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3
        }
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationController?.navigationBar.isHidden = false

        self.orderTableView.isHidden = true
        
//        set_banner()
        
        viewMainPopup.isHidden = true
        nativeAdPlaceholder.isHidden = true
        btnSignupVertical.constant = 0.0
        
        if userDefault.value(forKey: "USER_ID") == nil || userDefault.integer(forKey: "USER_ID") == 0
        {
//            bannerView.isHidden = false
            
            signIn_signUp_View.isHidden = false
            self.nodataLbl.isHidden = true
            nativeAdPlaceholder.isHidden = false
            
            // Load Native Ad
          /*  adLoader = GADAdLoader(adUnitID: Native_Ad_ID, rootViewController: self,
                                   adTypes: [ .unifiedNative ], options: nil)
            adLoader.delegate = self
            adLoader.load(GADRequest())
           */
            
        }else
        {
//            bannerView.isHidden = true
            signIn_signUp_View.isHidden = true

            registerTableViewCell(cellName: "OrderVCTableViewCell", to: orderTableView)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.Call_Order_API()
            })
            
//            self.nodataLbl.isHidden = false
//            self.nodataLbl.text = "No order available"
//
//            Pull_To_Refresh()
        }
        
//        if isOrderBtnBack
//        {
            addBackButton()
//            isOrderBtnBack = false
//        }
//        else
//        {
//            removeBackButton()
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        isOrderBtnBack = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize()
    {
        buttonShadow()
        addContactUsButton()
        
        // App Review Popup design
        App_Rate()
    
        // Native ads View
    /*    let nibObjects = Bundle.main.loadNibNamed("UnifiedNativeAdView", owner: nil, options: nil)
        let adView = nibObjects!.first as! GADUnifiedNativeAdView
        setAdView(adView)
     */
        initNativeAds()
        
    }
    
    func buttonShadow() {
        btnSignUP.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
    }
    
    //MARK:- ********************** NATIVE ADVANCED ADS **********************

    /*
    func setAdView(_ view: GADUnifiedNativeAdView) {
        // Remove the previous ad view.
        nativeAdView = view
        nativeAdPlaceholder.addSubview(nativeAdView)
        nativeAdView.translatesAutoresizingMaskIntoConstraints = false
        
        // Layout constraints for positioning the native ad view to stretch the entire width and height
        // of the nativeAdPlaceholder.
        let viewDictionary = ["_nativeAdView": nativeAdView!]
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[_nativeAdView]|",
                                                                options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[_nativeAdView]|",
                                                                options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary))
    }
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
        
        btnSignupVertical.constant = -150.0

        nativeAdPlaceholder.isHidden = false
        nativeAdView.nativeAd = nativeAd
        
        // Set ourselves as the native ad delegate to be notified of native ad events.
        nativeAd.delegate = self
        
        // Deactivate the height constraint that was set when the previous video ad loaded.
        heightConstraint?.isActive = false
        
        // Populate the native ad view with the native ad assets.
        // The headline and mediaContent are guaranteed to be present in every native ad.
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
        nativeAdView.mediaView?.frame.size.height = (nativeAdView.mediaView?.frame.size.width)! / nativeAd.mediaContent.aspectRatio
        
        
        // Some native ads will include a video asset, while others do not. Apps can use the
        // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
        // UI accordingly.
        if let controller = nativeAd.videoController, controller.hasVideoContent() {
            // By acting as the delegate to the GADVideoController, this ViewController receives messages
            // about events in the video lifecycle.
//            controller.delegate = self
        }
        else {
        }
        
        // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
        // ratio of the media it displays.
//        if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
//            heightConstraint = NSLayoutConstraint(item: mediaView,
//                                                  attribute: .height,
//                                                  relatedBy: .equal,
//                                                  toItem: mediaView,
//                                                  attribute: .width,
//                                                  multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
//                                                  constant: 0)
//            heightConstraint?.isActive = true
//        }
        
        // These assets are not guaranteed to be present. Check that they are before
        // showing or hiding them.
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        
        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
        
        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
        
//        (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(from:nativeAd.starRating)
//        nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil
        
//        (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
//        nativeAdView.storeView?.isHidden = nativeAd.store == nil
        
//        (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
//        nativeAdView.priceView?.isHidden = nativeAd.price == nil
        
        (nativeAdView.starRatingView as? UIImageView)?.isHidden = true
        nativeAdView.storeView?.isHidden = true
        nativeAdView.priceView?.isHidden = true
        
        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
        
        // In order for the SDK to process touch events properly, user interaction should be disabled.
        nativeAdView.callToActionView?.isUserInteractionEnabled = false
    }
    
    func imageOfStars(from starRating: NSDecimalNumber?) -> UIImage? {
        guard let rating = starRating?.doubleValue else {
            return nil
        }
        if rating >= 5 {
            return UIImage(named: "stars_5")
        } else if rating >= 4.5 {
            return UIImage(named: "stars_4_5")
        } else if rating >= 4 {
            return UIImage(named: "stars_4")
        } else if rating >= 3.5 {
            return UIImage(named: "stars_3_5")
        } else {
            return nil
        }
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
        
        btnSignupVertical.constant = 0.0
        
        print("\(adLoader) failed with error: \(error.localizedDescription)")
    }
    
    func nativeAdDidRecordClick(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }
    
    func nativeAdDidRecordImpression(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }
    
    func nativeAdWillPresentScreen(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }
    
    func nativeAdWillDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }
    
    func nativeAdDidDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }
    
    func nativeAdWillLeaveApplication(_ nativeAd: GADUnifiedNativeAd) {
        print("\(#function) called")
    }

    */
    //MARK:- ********************** BUTTON EVENT **********************
    
    @IBAction func press_btn_signUp(_ sender: UIButton) {

        let RegisterVC = mainStoryBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        isLoginOrNot = "order_signup"
        pushViewController(RegisterVC)
    }
    
    @IBAction func press_btn_Login(_ sender: UIButton) {
        
        isLoginOrNot = "order_login"
        let lastIndex = (self.tabBarController?.viewControllers?.count ?? 1) - 1

        ((tabBarController!.viewControllers! as NSArray)[lastIndex] as! UINavigationController).popToRootViewController(animated: false)
        self.tabBarController?.selectedIndex = lastIndex
        
//        let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
//        pushViewController(signinVC)
    }
    
    func addContactUsButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_contact"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.contactUsAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func contactUsAction(_ sender: UIButton) {
        let contactUsVC = mainStoryBoard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        pushViewController(contactUsVC)
    }
    
    @objc func pressButtonTrackOrder(_ sender: UIButton?) {
        
        let orderDict = orderArray[(sender?.tag ?? 0)]
//        let arrItems = orderDict["order_items"] as! [Any]
//        let itemDict = arrItems[(sender?.tag)] as! [String:Any]

        let orderID = "\(orderDict["order_id"] ?? "")"
        let trakingLink = "\(orderDict["tracking_link"] ?? "")"

        let trackOrderVC = mainStoryBoard.instantiateViewController(withIdentifier: "TrackOrderViewController") as! TrackOrderViewController
        trackOrderVC.isFromOrder = "Track Order"
        orderIdFromOrder = orderID
        trackingURL = trakingLink
        pushViewController(trackOrderVC)
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    func removeBackButton() {
      //add nil view to remove back button
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    @objc func backAction(_ sender: UIButton) {
        
        ((tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)

        self.tabBarController?.selectedIndex = 0

        
//        if isFromMyAccount
//        {
//            isFromMyAccount = false
//            self.tabBarController?.selectedIndex = 4
//        }
//        else
//        {
//            self.popViewController()
//        }
    }
    
    //MARK:- ********************** TABLEVIEW  METHOD **********************
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return orderArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let orderDict = orderArray[section]
        let arrItems = orderDict["order_items"] as! [Any]
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderVCTableViewCell") as? OrderVCTableViewCell
        
        let orderDict = orderArray[indexPath.section]
        let arrItems = orderDict["order_items"] as? [Any]
        let itemDict = arrItems?[indexPath.row] as? [String:Any]
        
        cell?.totalAmountViewHeight.constant = 0
        cell?.orderNumberViewHeight.constant = 0
        
//        cell.constraintMainViewTopMargin.constant = 10.0
//        cell.constraintMainViewBottomMargin.constant = 10.0
        
        var orderString = "Order ID : \((orderDict["order_id"] as! String).dropFirst(5))"
        
        let lastRowIndex = (arrItems?.count ?? 0) - 1
        
        if (arrItems?.count ?? 0) > 1
        {
            if indexPath.row == 0
            {
                DispatchQueue.main.async {
                    cell?.cornerRadiusAndShadowView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
                    cell?.mainContentView.roundCorners([.topLeft, .topRight], radius: 10)
                }
                
                cell?.constraintMainViewBottomMargin.constant = 0.0
                if DeviceType.IS_IPAD
                {
                    cell?.orderNumberViewHeight.constant = 79.0
                    cell?.constraintMainViewTopMargin.constant = 15.0

                }else
                {
                    cell?.orderNumberViewHeight.constant = 53.0
                    cell?.constraintMainViewTopMargin.constant = 10.0
                }
                
            }else if indexPath.row == lastRowIndex
            {
                DispatchQueue.main.async {
                    cell?.cornerRadiusAndShadowView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
                    cell?.mainContentView.roundCorners([.bottomLeft, .bottomRight], radius: 10)
                }
                
                cell?.constraintMainViewTopMargin.constant = 0.0
                if DeviceType.IS_IPAD
                {
                    cell?.totalAmountViewHeight.constant = 64.0
                    cell?.constraintMainViewBottomMargin.constant = 15.0

                }else
                {
                    cell?.totalAmountViewHeight.constant = 43.0
                    cell?.constraintMainViewBottomMargin.constant = 10.0
                }
                
            }else
            {
                DispatchQueue.main.async {
                    cell?.cornerRadiusAndShadowView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
                    cell?.mainContentView.roundCorners([.topLeft, .topRight,.bottomLeft,.bottomRight], radius: 0.0)
                }
                
                cell?.constraintMainViewTopMargin.constant = 0.0
                cell?.constraintMainViewBottomMargin.constant = 0.0
            }
        }else if arrItems?.count ?? 0 == 1
        {
            DispatchQueue.main.async {
                cell?.cornerRadiusAndShadowView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
                cell?.mainContentView.roundCorners([.topLeft, .topRight,.bottomLeft,.bottomRight], radius: 10.0)
            }

            if DeviceType.IS_IPAD
            {
                cell?.orderNumberViewHeight.constant = 79.0
                cell?.totalAmountViewHeight.constant = 64.0
                cell?.constraintMainViewTopMargin.constant = 15.0
                cell?.constraintMainViewBottomMargin.constant = 15.0

            }else
            {
                cell?.orderNumberViewHeight.constant = 53.0
                cell?.totalAmountViewHeight.constant = 43.0
                cell?.constraintMainViewTopMargin.constant = 10.0
                cell?.constraintMainViewBottomMargin.constant = 10.0

            }
        }
        
        // Track Order Button Visible Or Not
        let trackNumber = "\(orderDict["show_tracking"] ?? "")"
        
        if trackNumber == "0"
        {
            cell?.btntrackOrderWidth.constant = 0
            cell?.btnTrackOrder.isHidden = true
        }else
        {
            cell?.btnTrackOrder.isHidden = false

            if DeviceType.IS_IPAD
            {
                cell?.btntrackOrderWidth.constant = 127.0
            }else{
                cell?.btntrackOrderWidth.constant = 85.0
            }
        }

        let orderStatus = orderDict["order_status"] as? String
        cell?.lblWaitingForDispatch.text = orderStatus
        
        if DeviceType.IS_IPAD
        {
            cell?.lblWaitingForDispatchWidth.constant = 217.0
        }else{
            cell?.lblWaitingForDispatchWidth.constant = 145.0
        }
        
        if orderStatus == "Confirmation Pending"
        {
            cell?.lblWaitingForDispatch.textColor = hexStringToUIColor(hex: "11AA18")
        }else if orderStatus == "Order Confirmed"
        {
            cell?.lblWaitingForDispatch.textColor = hexStringToUIColor(hex: "0ABAD4")
            
        }else if orderStatus == "Dispatched"
        {
            orderString = "\(orderString) (Dispatch Order)"
            
            // Hide Status Label for Only for Order Dispatch
            cell?.lblWaitingForDispatchWidth.constant = 0

        }else if orderStatus == "Order Cancelled"
        {
            cell?.lblWaitingForDispatch.textColor = UIColor.red
           
        }else if orderStatus == "Delivered"
        {
            cell?.lblWaitingForDispatch.textColor = hexStringToUIColor(hex: "11AA18")
            
        }else if orderStatus == "Order Received"
        {
            cell?.lblWaitingForDispatch.textColor = hexStringToUIColor(hex: "0ABAD4")
        }else if orderStatus == "Out for Delivery"
        {
            cell?.lblWaitingForDispatch.textColor = hexStringToUIColor(hex: "11AA18")
        }
        
        // CONVERT URL STRING TO UIIMAGE

        let arrDisplayImage = itemDict?["display_images"] as? [Any]
        
        let imageString = (arrDisplayImage?[0] as! [String:Any])["image"] as? String ?? ""
        
        let imgRotateDict = itemDict?["display"] as? [String:Any]
        
        let isRotate = "\(imgRotateDict?["rotate"] ?? "")"
        
        if isRotate == "1"
        {
            cell?.imgProductView.setImageWithRotation(indicatorStyle: .gray, imageURL: imageString)
        }else
        {
            cell?.imgProductView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL : imageString)
        }
        
        
        // Get Only date From datetime
//        let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let date: NSDate = dateFormatter.date(from: orderDict["date"] as? String ?? "")! as NSDate
//        dateFormatter.dateFormat = "yyyy-MM-dd"
//        let onlyDate = dateFormatter.string(from: date as Date)
        
        let onlyDate = "\(orderDict["date"] as? String ?? "")"

        cell?.lblOrderNumber.text = orderString
        cell?.lblProductName.text = itemDict?["model_name"] as? String
        cell?.lblPlaceOnDate.text = "Placed on : \(onlyDate)"
        cell?.lblTotalPrice.text = "Amount : \(orderDict["currency_symbol"] as? String ?? "")\(String(format: "%.2f", (itemDict?["subtotal"] as? Double ?? 0.0)))" //"\(itemDict?["subtotal"] ?? 0)"
        cell?.lblTotalAmount.text = "\(orderDict["currency_symbol"] ?? "")\(String(format: "%.2f", (orderDict["paid_amount"] as? Double ?? 0.0)))" //"\(orderDict["paid_amount"] ?? "")"
        
        cell?.btnTrackOrder.tag = indexPath.section
        cell?.btnTrackOrder.addTarget(self, action: #selector(self.pressButtonTrackOrder(_:)), for: .touchUpInside)
        
        return (cell ?? UITableViewCell())
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let orderDict = orderArray[indexPath.section]
//        let itemDict = arrItems[indexPath.row] as! [String:Any]
        let orderID = "\(orderDict["order_id"] ?? "")"
        let OrderDetailsVC = mainStoryBoard.instantiateViewController(withIdentifier: "OrderDetailsViewController") as! OrderDetailsViewController
        OrderDetailsVC.itemDataDict = orderDict
        OrderDetailsVC.currencySymbol = "\(orderDict["currency_symbol"] ?? "")"
        orderIdFromOrder = orderID
        pushViewController(OrderDetailsVC)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Call_Order_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)get_orders", parameters: ParamDict) { (APIResponce, Responce, error1) in
  
                self.orderTableView.isHidden = false
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.orderArray = responceDict["data"] as! [[String:Any]]
   
                        let arrOrders = (self.orderArray as NSArray).value(forKeyPath: "order_status") as? NSArray
                      
                        if (self.orderArray.count == 0)
                        {
                            self.orderTableView.isHidden = true
                            self.nodataLbl.isHidden = false
                            self.nodataLbl.text = "No order available"
                        }
                        else
                        {
                            // Open Rate popup when Order is delivered
                            
                            if (arrOrders?.contains("Delivered"))!
                            {
                                if userDefault.value(forKey: "OrderRate") == nil
                                {
                                    appDelegate.window?.addSubview(self.viewMainPopup)
                                    self.viewMainPopup.frame = UIScreen.main.bounds
                                    self.viewMainPopup.isHidden = false
                                    self.openRatePopup()
                                }
                            }
                            
                            self.orderTableView.isHidden = false
                            self.nodataLbl.isHidden = true
                            
                            self.orderTableView.reloadData()
                            self.orderTableView.scroll(to: .top, animated: true)

                        }
                        
                    }
                    else
                    {
                        self.orderTableView.isHidden = true
                        self.nodataLbl.isHidden = false
                        self.nodataLbl.text = "No order available"
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Call_Order_API()
                        }
                    })
                    
                }
                self.RemoveLoader()

            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Call_Order_API()
                }
            })
        }
        
    }
    
    //MARK:- ********************** APP REVIEW POPUP  **********************

    func App_Rate() {
        viewMainPopup.alpha = 0.0
        
        let col = UIColor.init(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        viewPopup.backgroundColor = col
        viewPopup.layer.cornerRadius = 8.0
        
        let gradient : CAGradientLayer = CAGradientLayer()
        gradient.frame = viewPopup.bounds
        gradient.frame.size.width = 290.0
        gradient.frame.size.height = 220.0
        gradient.colors = [col.cgColor, col.cgColor]
        gradient.startPoint = CGPoint.init(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint.init(x: 1.0, y: 0.5)
        gradient.opacity = 0.5
        gradient.cornerRadius = 8.0
        gradient.shadowColor = UIColor.darkGray.cgColor
        gradient.shadowOpacity = 1.0
        gradient.shadowRadius = 10.0
        gradient.shadowOffset = CGSize.init(width: 0.0, height: 0.0)
        viewPopup.layer.insertSublayer(gradient, at: 0)
    }
    
    func openRatePopup()
    {
        UIView.animate(withDuration: 0.3) {
            self.viewMainPopup.alpha = 1.0
        }
    }
   
    @IBAction func btnRate(_ sender: Any) {
        
        userDefault.set(0, forKey: "OrderRate")
        
        guard let writeReviewURL = URL(string: "https://itunes.apple.com/app/id1458325325?action=write-review")
            else { fatalError("Expected a valid URL") }
        UIApplication.shared.open(writeReviewURL, options: [:], completionHandler: nil)
        closeRatePopup()
    }
   
    @IBAction func btnLater(_ sender: Any) {
        closeRatePopup()
    }
    
    @IBAction func btnNoThanks(_ sender: Any) {
        closeRatePopup()
    }
    
    func closeRatePopup()
    {
        UIView.animate(withDuration: 0.3, animations: {
            self.viewMainPopup.alpha = 0.0

        }, completion: { (bool) in
            self.viewMainPopup.removeFromSuperview()
        })
    }
    
    
//    //MARK:- ********************** BANNER AD **********************
//
//    func set_banner()
//    {
//        self.bannerView.adUnitID = banner_ID
//        self.bannerView.rootViewController = self
//        let request = GADRequest()
//        request.testDevices = ["ce0c8d1e1aa2d1ebcf636d56c2aa6571","eb0e2074bacb6396cd12bd015b847075","06d24ef8a171e533169c992149f07817","d14f6feee5647c7beae207686b72c6d6","4fb88b739b81cca87ff7060574247a87","97f940130f7863cd056bbff53204f9f2","dfa3bf19497c465906a7b6b7ff782f4d","f4fc2c1165675652c987515f68292322","78af841c2188dbd6256ecd991434e7a2",kGADSimulatorID]
//
//        self.bannerView.delegate = self
//        self.bannerView.load(request)
//    }
    
//    //MARK:- ********************** OTHER FUNCTION **********************
//
//    func Pull_To_Refresh() {
//        refreshControl.attributedTitle = NSAttributedString(string: "Loading...")
//        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
//        orderTableView.addSubview(refreshControl)
//    }
//
//    @objc func refresh(sender:AnyObject) {
//        // Code to refresh table view
//
//        orderTableView.reloadData()
//        refreshControl.endRefreshing()
//    }
}

extension OrderViewController : GADAdLoaderDelegate, GADNativeAdDelegate,GADVideoControllerDelegate,GADNativeAdLoaderDelegate{
    
    func initNativeAds() {
        if let nibObjects = Bundle.main.loadNibNamed("UnifiedNativeAdView", owner: nil, options: nil),
           let adView = nibObjects.first as? GADNativeAdView {
           setAdView(adView)
           refreshAd(nil)
       }
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        print("error:",error)
    }
 
    
//    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
//
//      nativeAd.delegate = self
//      heightConstraint?.isActive = false
//
//      (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
//      nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
//
//      // Some native ads will include a video asset, while others do not. Apps can use the
//      // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
//      // UI accordingly.
//      let mediaContent = nativeAd.mediaContent
//      if mediaContent.hasVideoContent {
//        // By acting as the delegate to the GADVideoController, this ViewController receives messages
//        // about events in the video lifecycle.
//        mediaContent.videoController.delegate = self
//        videoStatusLabel.text = "Ad contains a video asset."
//      } else {
//        videoStatusLabel.text = "Ad does not contain a video."
//      }
//
//      if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
//        heightConstraint = NSLayoutConstraint(
//          item: mediaView,
//          attribute: .height,
//          relatedBy: .equal,
//          toItem: mediaView,
//          attribute: .width,
//          multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
//          constant: 0)
//        heightConstraint?.isActive = true
//      }
//
//      (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
//      nativeAdView.bodyView?.isHidden = nativeAd.body == nil
//
//      (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
//      nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
//
//      (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
//      nativeAdView.iconView?.isHidden = nativeAd.icon == nil
//
//
//     (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(from: nativeAd.starRating)
//      nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil
//
//      (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
//      nativeAdView.storeView?.isHidden = nativeAd.store == nil
//
//      (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
//      nativeAdView.priceView?.isHidden = nativeAd.price == nil
//
//      (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
//      nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
//
//      // In order for the SDK to process touch events properly, user interaction should be disabled.
//      nativeAdView.callToActionView?.isUserInteractionEnabled = true
//
//      // Associate the native ad view with the native ad object. This is
//      // required to make the ad clickable.
//      // Note: this should always be done after populating the ad views.
//      nativeAdView.nativeAd = nativeAd
//
//    }
    
    // Mark: - GADNativeAdLoaderDelegate
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        btnSignupVertical.constant = -150.0

        nativeAdPlaceholder.isHidden = false
        nativeAdView.nativeAd = nativeAd
        
      print("Received native ad: \(nativeAd)")
     // refreshAdButton.isEnabled = true
      // Create and place ad in view hierarchy.
      let nibView = Bundle.main.loadNibNamed("UnifiedNativeAdView", owner: nil, options: nil)?.first
      guard let nativeAdView = nibView as? GADNativeAdView else {
        return
      }
      setAdView(nativeAdView)

      // Set ourselves as the native ad delegate to be notified of native ad events.
      nativeAd.delegate = self

      // Populate the native ad view with the native ad assets.
      // The headline and mediaContent are guaranteed to be present in every native ad.
      (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
       nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
     
//        if let mediaContent = nativeAd.mediaContent, let mediaView = nativeAdView.adMediaView as? GADMediaView {
//                        mediaContent.videoController.delegate = self
//                        mediaContent.videoController.setVideoLifecycleDelegate(self)
//                        mediaContent.videoController.setAspectRatio(mediaView.bounds.size)
//                        mediaView.mediaContent = mediaContent
//                    }
//
//                    // Call the ad view's register method to enable click and impression tracking
//                    nativeAdView.register(nativeAdView, with: nativeAd)
        
        let mediaContent = nativeAd.mediaContent
             if mediaContent.hasVideoContent {
               // By acting as the delegate to the GADVideoController, this ViewController receives messages
               // about events in the video lifecycle.
               mediaContent.videoController.delegate = self
                 print("Ad contains a video asset.")}

      // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
      // ratio of the media it displays.
//      if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
//        let heightConstraint = NSLayoutConstraint(
//          item: mediaView,
//          attribute: .height,
//          relatedBy: .equal,
//          toItem: mediaView,
//          attribute: .width,
//          multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
//          constant: 0)
//        heightConstraint.isActive = true
//      }

      // These assets are not guaranteed to be present. Check that they are before
      // showing or hiding them.
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        
//        nativeAdView.bodylblView?.text = nativeAd.body
//        nativeAdView.bodylblView?.isHidden = nativeAd.body == nil
        
      (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
      nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil

      (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
      nativeAdView.iconView?.isHidden = nativeAd.icon == nil

      (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(
        from: nativeAd.starRating)
      nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil

      (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
      nativeAdView.storeView?.isHidden = nativeAd.store == nil

      (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
      nativeAdView.priceView?.isHidden = nativeAd.price == nil

      (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
      nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
    

      // In order for the SDK to process touch events properly, user interaction should be disabled.
      nativeAdView.callToActionView?.isUserInteractionEnabled = false

      // Associate the native ad view with the native ad object. This is
      // required to make the ad clickable.
      // Note: this should always be done after populating the ad views.
      nativeAdView.nativeAd = nativeAd
    }
    
    func setAdView(_ view: GADNativeAdView) {
      // Remove the previous ad view.
      nativeAdView = view
      nativeAdPlaceholder.addSubview(nativeAdView)
      nativeAdView.translatesAutoresizingMaskIntoConstraints = false

      // Layout constraints for positioning the native ad view to stretch the entire width and height
      // of the nativeAdPlaceholder.
      let viewDictionary = ["_nativeAdView": nativeAdView!]
      self.view.addConstraints(
        NSLayoutConstraint.constraints(
          withVisualFormat: "H:|[_nativeAdView]|",
          options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
      )
      self.view.addConstraints(
        NSLayoutConstraint.constraints(
          withVisualFormat: "V:|[_nativeAdView]|",
          options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
      )
    }
    
    func imageOfStars(from starRating: NSDecimalNumber?) -> UIImage? {
      guard let rating = starRating?.doubleValue else {
        return nil
      }
      if rating >= 5 {
        return UIImage(named: "stars_5")
      } else if rating >= 4.5 {
        return UIImage(named: "stars_4_5")
      } else if rating >= 4 {
        return UIImage(named: "stars_4")
      } else if rating >= 3.5 {
        return UIImage(named: "stars_3_5")
      } else {
        return UIImage(named: "stars_3_5")
      }
    }
    
    func refreshAd(_ sender: AnyObject!) {
        let videoOptions = GADVideoOptions()
        let multipleAdsOptions = GADMultipleAdsAdLoaderOptions()
                multipleAdsOptions.numberOfAds = 1
      adLoader = GADAdLoader(
        adUnitID: Native_Ad_ID, rootViewController: self,
        adTypes: [GADAdLoaderAdType.native], options:[multipleAdsOptions])
      adLoader.delegate = self
      adLoader.load(GADRequest())
    }
    
    // GADVideoControllerDelegate methods
     func videoControllerDidPlayVideo(_ videoController: GADVideoController) {
       // Implement this method to receive a notification when the video controller
       // begins playing the ad.
     }

     func videoControllerDidPauseVideo(_ videoController: GADVideoController) {
       // Implement this method to receive a notification when the video controller
       // pauses the ad.
     }

     func videoControllerDidEndVideoPlayback(_ videoController: GADVideoController) {
       // Implement this method to receive a notification when the video controller
       // stops playing the ad.
     }

     func videoControllerDidMuteVideo(_ videoController: GADVideoController) {
       // Implement this method to receive a notification when the video controller
       // mutes the ad.
     }

     func videoControllerDidUnmuteVideo(_ videoController: GADVideoController) {
       // Implement this method to receive a notification when the video controller
       // unmutes the ad.
     }
}
