//
//  CancelViewController.swift
//  Print_Photo_App
//
//  Created by atul patil on 13/09/23.
//  Copyright © 2023 des. All rights reserved.
//

import UIKit
import DropDown

    fileprivate enum AMDropDownType {
        case reason
        
    }

    class CancelViewController: BaseViewController,UIGestureRecognizerDelegate,UITextViewDelegate {

        //MARK:- ********************** OUTLATE DECLARATION **********************

        @IBOutlet weak var selectSubjectBorderView: UIView!
        @IBOutlet weak var mainView: UIView!
        @IBOutlet weak var lblSubject: UILabel!
        @IBOutlet weak var lblFeedback: UILabel!
        @IBOutlet weak var btnClose: UIButton!
        @IBOutlet weak var btnCancel: UIButton!
    
        @IBOutlet weak var txtCancelFeedback: UITextView!
        
        
        //MARK:- ********************** VARIABLE DECLARATION **********************

        var dropDown = DropDown()
        fileprivate var selectedDropDown : AMDropDownType?
        var itemDataDict = [String:Any]()
        var selectedIndex : String = ""
        var backOrderDelegate : BackOrder?

        //MARK:- ********************** VIEW LIFECYLE **********************

        override func viewDidLoad() {
            super.viewDidLoad()
            lblFeedback.isHidden = false
           
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(labelTapped))
                    
                    // Add the tap gesture to your label
            lblFeedback.isUserInteractionEnabled = true
            lblFeedback.addGestureRecognizer(tapGesture)

           // SetNavigationBarTitle(Startstring: "Complaint", EndString: "")
            if #available(iOS 16.0, *) {
                self.navigationItem.rightBarButtonItem?.isHidden = true
            } else {
                // Fallback on earlier versions
            }
            if #available(iOS 16.0, *) {
                self.navigationItem.leftBarButtonItem?.isHidden = true
            } else {
                // Fallback on earlier versions
            }

            Initialize()
            
            // Do any additional setup after loading the view.
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.tabBarController?.tabBar.isHidden = true
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            dropDown.hide()
        }
        
        //MARK:- ********************** INITIALIZE **********************
        
        func Initialize() {
            ViewBorder()
            addButtonAction()
            ShowDropDown()
            Reset()
            
    //        txtComplaint.returnKeyType = .done
            txtCancelFeedback.enablesReturnKeyAutomatically = false
            hideKeyboardWhenTappedAround()
        }
        
        //MARK:- ********************** UIVIEW BORDER **********************

        func ViewBorder() {
            mainView.setBorder()
           
         selectSubjectBorderView.setBorder()

            txtCancelFeedback.setBorder()
        }
        
        //MARK:- ********************** RESET TEXTFIELD **********************

        func Reset()
        {
            lblSubject.text = "Select Reason"
            lblFeedback.text = "Please provide your FeedBack"
            txtCancelFeedback.text = ""
            lblFeedback.textColor = UIColor.lightGray
            lblFeedback.isHidden = false
        }
        
        //MARK:- ********************** BUTTON EVENT **********************

        @IBAction func pressBtn_SelectSubject(_ sender: UIButton) {
            view.endEditing(true)
            selectedDropDown = AMDropDownType.reason
            dropDown.anchorView = self.lblSubject
            if let cancelReasons = cancel_reson_Array as? [[String: Any]] {
                let reasons = cancelReasons.compactMap { $0["reason"] as? String }
                dropDown.dataSource = reasons
            }
           
            dropDown.show()
        }
        
        
        func addButtonAction() {
        
            btnClose.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
            btnCancel.addTarget(self, action: #selector(self.sendAction(_:)), for: .touchUpInside)
           
        }
        
        @objc func backAction(_ sender: UIButton) {
            self.popViewController()
        }
        
        
        @objc func labelTapped() {
            lblFeedback.isHidden = true
           }
      
        
        @objc func sendAction(_ sender: UIButton) {
            if isReadyToComplaint()
            {
                Cancel_Order_API()
            }
        }
        
        //MARK:- ********************** DROP DOWN **********************

        func ShowDropDown() {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                
                print("Selected item: \(item) at index: \(index)")
                
                if self.selectedDropDown == AMDropDownType.reason {
                    
                    // Get subject name
                    self.lblSubject.text = item
                    self.selectedIndex = "\(index)"
                }
            }
        }
        
       
        //MARK:- ********************** VALIDATION **********************

        func isReadyToComplaint()-> Bool {
            if lblSubject.text == "Select Reason" {
                presentAlertWithTitle(title: "Please select Reason", message: "", options: "OK") { (Int) in
                }
            }else if txtCancelFeedback.text.trim() == ""
            {
                presentAlertWithTitle(title: "Please enter your feedback", message: "", options: "OK") { (Int) in
                }
            }
            else{
                
                return true
            }
            
            return false
        }
        
        //MARK:- **********************  TEXTVIEW DELEGATE METHOD **********************

        
        func textViewDidBeginEditing(_ textView: UITextView) {
            if txtCancelFeedback.text == "" {
                txtCancelFeedback.text = nil
                txtCancelFeedback.textColor = UIColor.black
                lblFeedback.isHidden = true
            }
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            if txtCancelFeedback.text.isEmpty {
                lblFeedback.isHidden = false
            } else {
                lblFeedback.isHidden = true
            }
            
        }
        
        //MARK:- ********************** API CALLING **********************

      
        
        func Cancel_Order_API() {
            
            if Reachability.isConnectedToNetwork() {
                
                showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                
                // Get App Version
                
                let ParamDict = ["order_id" : "\(itemDataDict["order_id"] ?? "")",
                                 "cancel_reason_id" :   self.selectedIndex ,
                                 "cancel_description" : lblSubject.text ?? ""
                ] as NSDictionary
                
                AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)cancel_order", parameters: ParamDict) { (APIResponce, Responce, error1) in
                    
                    self.RemoveLoader()
                    
                    if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                    {
                        let responceDict = Responce as! [String:Any]
                        
                        if responceDict["ResponseCode"] as! String == "1"
                        {
                            let dict = responceDict["data"] as? [String:Any]

                            self.presentAlertWithTitle(title: "Order Cancellation", message: "\(dict?["message"] ?? "")", options: "OK", completion: { (ButtonIndex) in
                                self.backOrderDelegate?.backToOrder(isFromCancel: true)
                                self.popViewController()
                            })
                        }
                        else
                        {
                            self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            })
                        }
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
            }
            else
            {
                self.RemoveLoader()

                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                })
            }
        }
        
    }

