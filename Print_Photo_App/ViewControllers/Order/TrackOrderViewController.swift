//
//  TrackOrderViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 21/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import AFNetworking
import Alamofire
import AVKit
import Photos
class TrackOrderViewController: BaseViewController,UIWebViewDelegate,UIImagePickerControllerDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var trackOrderWebView: UIWebView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var isFromOrder : String?
    
    var picker = UIImagePickerController()

    var isFromBack: Bool = false
    
    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()

        if isFromOrder == "Track Order"
        {
            SetNavigationBarTitle(Startstring: "Track Order", EndString: "")
        }else
        {
            SetNavigationBarTitle(Startstring: "Complain", EndString: "")
        }
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
//        guard !isFromBack else {
//            return
//        }
//
//        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
//        switch cameraAuthorizationStatus {
//        case .notDetermined:
//            print("")
//        case .restricted, .denied: self.alertCameraAccessNeeded()
//        case .authorized:
//            print("")
//        }
    }

    // Camera USed
    func alertCameraAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        
        let alert = UIAlertController(
            title: "Need Camera Access",
            message: "Camera access is required to make full use of this app.",
            preferredStyle: UIAlertController.Style.alert
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        }))
        
        self.navigationController!.presentedViewController!.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize()
    {
        removeBackButton()
        addBackButton()
        Track_Order_WebView()
    }
    
    //MARK:- ********************** BUTTON EVENT **********************

    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.isFromBack = true
        self.popViewController()
    }
    
    func Track_Order_WebView() {
        
        trackOrderWebView.scrollView.showsHorizontalScrollIndicator = false
        trackOrderWebView.scrollView.showsVerticalScrollIndicator = false
        
        let userDict = userDefault.value(forKey: "EditAccountDict") as! NSDictionary
        let token = userDict["token"] as? String ?? ""
        
        let isTicketURL : String?
        
        if isFromOrder == "MyAccount_Complain"
        {
            isTicketURL = "\(ticketsURL ?? "")?token=\(token)"
        }else
        {
            isTicketURL = "\(ticketsURL ?? "")?token=\(token)&order_id=\(orderIdFromOrder ?? "")"
        }
        
        let orderURl = isFromOrder == "Track Order" ? URL(string: trackingURL ?? "") : URL(string: isTicketURL ?? "")
    
        let orderURlRequest = URLRequest(url: orderURl!)
        self.trackOrderWebView.loadRequest(orderURlRequest)

    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** WEBVIEW METHOD **********************

    func webViewDidStartLoad(_ webView: UIWebView) {
        showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.RemoveLoader()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        print(request)

        return true
        
//        if request.url!.absoluteString != "https://printphoto.in/Photo_case/tickets/create#fake"
//        {
//            return true
//        }
//
//        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
//        switch cameraAuthorizationStatus {
//        case .notDetermined:
//            DispatchQueue.main.asyncAfter(deadline: .now()+1.0, execute: {
//                self.navigationController!.presentedViewController!.dismiss(animated: false, completion: nil)
//                self.requestCameraPermission()
//            })
//        case .restricted, .denied:
//            DispatchQueue.main.asyncAfter(deadline: .now()+1.0, execute: {
//                self.navigationController!.presentedViewController!.dismiss(animated: false) {
//                    self.alertCameraAccessNeeded()
//                }
//            })
//        case .authorized:
//
//            PHPhotoLibrary.requestAuthorization { status in
//                switch status {
//                case .authorized:
//
//                    break
//                case .denied, .restricted:
//                    DispatchQueue.main.asyncAfter(deadline: .now()+1.0, execute: {
//                        self.navigationController!.presentedViewController!.dismiss(animated: false) {
//                            self.alertGalleryAccessNeeded()
//                        }
//                    })
//                    break
//                case .notDetermined:
//                    DispatchQueue.main.asyncAfter(deadline: .now()+1.0, execute: {
//                        self.navigationController!.presentedViewController!.dismiss(animated: false) {
//                            self.requestGalleryPermission()
//                        }
//                    })
//                    break
//                }
//            }
//        }
//
//
//        return true
    }
    
    func requestCameraPermission() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
        })
    }
    
    // Gallery Used
    func alertGalleryAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        
        let alert = UIAlertController(
            title: "Need Gallery Access",
            message: "Gallery access is required to make full use of this app.",
            preferredStyle: UIAlertController.Style.alert
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Gallery Access", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func requestGalleryPermission() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
        })
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {

        self.RemoveLoader()

        if !(error.localizedDescription).contains("-999")
        {
            self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
                
                if ButtonIndex == 0
                {
                    self.popViewController()
                }
            })
        }else{
            print("error")
        }
      
    }
    
    //MARK:- ********************** IMAGEPICKERVIEW DELEGATE **********************

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(info[.imageURL]!)
    }

}
