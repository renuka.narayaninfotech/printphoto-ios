//
//  OrderVCTableViewCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 13/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class OrderVCTableViewCell: UITableViewCell {

    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var orderNumberView: UIView!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblWaitingForDispatch: UILabel!
    @IBOutlet weak var centerContentView: UIView!
    @IBOutlet weak var imgProductView: UIImageView!
    @IBOutlet weak var productDetailsView: UIView!
    @IBOutlet weak var imgNextArrow: UIImageView!
    @IBOutlet weak var btnTrackOrder: UIButton!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblPlaceOnDate: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var totalAmountPaybleView: UIView!
    @IBOutlet weak var lblTotalAmountPayble: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblWaitingForDispatchWidth: NSLayoutConstraint!
    @IBOutlet weak var btntrackOrderWidth: NSLayoutConstraint!
    @IBOutlet weak var orderNumberViewHeight: NSLayoutConstraint!
    @IBOutlet weak var totalAmountViewHeight: NSLayoutConstraint!
    @IBOutlet weak var orderNumberViewTopMargin: NSLayoutConstraint!
    @IBOutlet weak var TotalAmountViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var shadowViewTopMargin: NSLayoutConstraint!
    @IBOutlet weak var shadowViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var constraintMainViewTopMargin: NSLayoutConstraint!
    
    @IBOutlet weak var constraintMainViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var constraintCenterViewTopMargin: NSLayoutConstraint!
    @IBOutlet weak var constraintCenterViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var lblSaprater: UILabel!
    @IBOutlet weak var cornerRadiusAndShadowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblOrderNumber.sizeToFit()
        lblWaitingForDispatch.sizeToFit()
        lblProductName.sizeToFit()
        lblPlaceOnDate.sizeToFit()
        lblTotalPrice.sizeToFit()
        lblTotalAmountPayble.sizeToFit()
        lblTotalAmount.sizeToFit()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
