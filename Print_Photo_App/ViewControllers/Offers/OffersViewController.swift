//
//  OffersViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 05/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController
import SDWebImage
import Alamofire

class OffersViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var OfferesTableView: UITableView!
    @IBOutlet var terms_condition_View: UIView!
    @IBOutlet weak var lblTermsOfUse: UILabel!
    @IBOutlet weak var lblTitleTermsOfUse: UILabel!
    @IBOutlet weak var btnOkTermsOfUse: UIButton!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var badgeValue : Int!
    var orderID : String?
    var internetSpeed : Double = 0
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Offer Code", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //        addCartButtonWithBadgeIcon()
        self.tabBarController?.tabBar.isHidden = true
        
        self.OfferesTableView.scroll(to: .top, animated: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.OfferesTableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        addBackButton()
        removeBackButton()
        self.OfferesTableView.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.CallApi()
        }
        
        //        checkRechabilityStatus()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** CHECK RECHABILITY STATUS **********************
    
    func checkRechabilityStatus()
    {
        let reachabilityManager = NetworkReachabilityManager()
    
        reachabilityManager?.startListening(onUpdatePerforming: { _ in
            if let isNetworkReachable = reachabilityManager?.isReachable,
                isNetworkReachable == true {
                
                print(self.internetSpeed)
                
                //Internet Available
                print("Internet Available")
                if self.internetSpeed < 0.06
                {
                    self.RemoveLoader()
                    
                    self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                        self.CallApi()
                    })
                }else
                {
                    self.CallApi()
                }
                
            } else {
                //Internet Not Available"
                print("Internet Not Available")
                
                self.RemoveLoader()
                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                    self.CallApi()
                })
                
            }
        })
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    //    @IBAction func press_button_termsOfUse_ok(_ sender: UIButton) {
    //        self.dismiss(animated: true, completion: nil)
    //    }
    
    @objc func pressButtonTermsOfUse(_ sender: UIButton?) {
        
        let dict = OffersArray.object(at: (sender?.tag)!) as! [String:Any]
        
        //        lblTermsOfUse.Getwidth = self.view.Getwidth/1.10 - 40.0
        
        //        lblTermsOfUse.text = dict["terms_condition"] as? String
        
        self.presentAlertWithTitle(title: "Terms of Use", message: "\((dict["terms_condition"]) ?? "")", options: "OK", completion: { (ButtonIndex) in
        })
        
        //        lblTermsOfUse.sizeToFit()
        //
        //        if DeviceType.IS_IPAD
        //        {
        //            terms_condition_View.Getheight = lblTermsOfUse.Getheight + 180
        //        }
        //        else
        //        {
        //            terms_condition_View.Getheight = lblTermsOfUse.Getheight + 90
        //        }
        //
        //        // Present Pop-up view of Terms of use item
        //        let TFPopup = MZFormSheetPresentationViewController(contentView: terms_condition_View)
        //        TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
        //        TFPopup.presentationController?.shouldCenterVertically = true
        //        TFPopup.contentViewControllerTransitionStyle = .none
        //        TFPopup.presentationController?.contentViewSize = CGSize(width: self.view.Getwidth/1.10, height: terms_condition_View.Getheight)
        //        self.present(TFPopup, animated: true)
        
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
        //        if isOfferBtnBack
        //        {
        isBackFromOffer = true
        isPromocode = false
        self.popViewController()
        
        //        }else
        //        {
        //            ((tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)
        //            self.tabBarController?.selectedIndex = 0
        //        }
        
        
    }
    
    //    func addCartButtonWithBadgeIcon() {
    //
    //        var badgelabel = UILabel()
    //
    //        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
    //        {
    //            // badge label
    //            badgelabel = UILabel(frame: CGRect(x: 20, y: -5, width: 17, height: 17))
    //            badgelabel.layer.borderColor = UIColor.clear.cgColor
    //            badgelabel.layer.borderWidth = 2
    //            badgelabel.layer.cornerRadius = badgelabel.bounds.size.height / 2
    //            badgelabel.textAlignment = .center
    //            badgelabel.layer.masksToBounds = true
    //            badgelabel.font = UIFont(name: CustomFontWeight.medium, size: 10)
    //            badgelabel.textColor = .white
    //            badgelabel.backgroundColor = hexStringToUIColor(hex: "0ABAD4")
    //
    //            if (userDefault.integer(forKey: "BadgeNumber")) != 0
    //            {
    //                badgelabel.isHidden = false
    //                badgelabel.text = "\(userDefault.integer(forKey: "BadgeNumber"))"
    //            }else
    //            {
    //                badgelabel.isHidden = true
    //            }
    //        }
    //
    //        // Button
    //        let cartButton = UIButton(type: .custom)
    //        cartButton.setImage(UIImage(named: "ic_cart"), for: .normal)
    //        cartButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
    //        cartButton.addTarget(self, action: #selector(self.cartAction(_:)), for: .touchUpInside)
    //        cartButton.addSubview(badgelabel)
    //
    //        if isIconCart
    //        {
    //            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cartButton)
    //
    ////            isIconCart = false
    //        }else
    //        {
    //            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    //            isIconCart = true
    //        }
    //
    //    }
    
    //    @objc func cartAction(_ sender: UIButton) {
    //
    //        isCartBtnBack = true
    //
    //        ((tabBarController!.viewControllers! as NSArray)[3] as! UINavigationController).popToRootViewController(animated: false)
    //        self.tabBarController?.selectedIndex = 3
    //
    ////        isFromOffer = true
    ////        let cartVC = mainStoryBoard.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
    ////        cartVC.isFromOffer = true
    ////        self.pushViewController(cartVC)
    //    }
    
    //MARK:- ********************** TABLEVIEW DELEGATE **********************
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OffersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OffersVCTableViewCell") as! OffersVCTableViewCell
        cell.contentsView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        
        //        if DeviceType.IS_IPAD
        //        {
        //            cell.imageviewWidthRatio.constant = OfferesTableView.Getwidth/2
        //
        //            cell.contentsView.translatesAutoresizingMaskIntoConstraints = false
        //
        //            let widthConstraint = NSLayoutConstraint(item: cell.offersImage, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cell.contentsView, attribute: NSLayoutConstraint.Attribute.width, multiplier: 0.5, constant: 100)
        //
        //            NSLayoutConstraint.activate([widthConstraint])
        //        }
        
        let dict = OffersArray.object(at: indexPath.row) as! [String:Any]
        
        if dict["n_offer_new_image"] as! String != ""
        {
            // Managed image size in iPad
            if DeviceType.IS_IPAD
            {
                if imgTemp != nil
                {
                    cell.imageviewWidthRatio.constant = 226.0 * (((imgTemp?.size.width)!)/(imgTemp?.size.height)!)
                    cell.offersImage.sd_setImage(with: URL(string: (dict["n_offer_new_image"] as! String)), placeholderImage: nil, options: .refreshCached){ (img, error, catchType, url) in
                        cell.offersImage.sd_removeActivityIndicator()
                    }
                }
                else
                {
                    cell.offersImage.sd_setShowActivityIndicatorView(true)
                    cell.offersImage.sd_setIndicatorStyle(.gray)
                    
                }
                
            }else
            {
                cell.offersImage.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: (dict["n_offer_new_image"] as! String))
            }
        }
        
        cell.lblOffers.text = "\(dict["description"] ?? "")"
        
        //        // Check get offer code is blank, - or not
        //        var offerCode = "\(dict["offer_code"] as? String ?? "")"
        //        offerCode = (offerCode == "-" || offerCode == "") ? "" : "Use Code : \(dict["offer_code"] as? String ?? "")"
        //        cell.lblCode.text = offerCode
        
        cell.lblCode.text = "\(dict["display_message"] ?? "")"
        
        cell.lblExpiredDate.text = "\(dict["expiry_text"] ?? "")"
        cell.btnTermsOfUse.tag = indexPath.row
        cell.btnTermsOfUse.addTarget(self, action: #selector(self.pressButtonTermsOfUse(_:)), for: .touchUpInside)
        
        //        cell.heightImgView.constant = ScreenSize.SCREEN_HEIGHT / 5.0 //150.0
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = OffersArray.object(at: indexPath.row) as! [String:Any]
        copyToClipBoard(textToCopy: "\(dict["offer_code"] as? String ?? "")")
        print(dict["offer_code"] as? String ?? "")
        let offerCode = "\(dict["offer_code"] as? String ?? "")"
        //        let offerCode_amount = "\(dict["amount"] ?? "")"
        
        if isPromocode
        {
            Check_Validation(code: offerCode)
            
        }else
        {
            self.view.makeToast("Offer Code Copied", duration: 2.0, position: .center)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
        
    //MARK:- ********************** COPY TEXT FROM CLIPBOARD **********************
    
    private func copyToClipBoard(textToCopy: String) {
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = textToCopy
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func CallApi() {
        
        if Reachability.isConnectedToNetwork() {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            // Managed Banner of Offer in India and Other Country
            var isInternational = userDefault.string(forKey: "RegionCode") == "IN" ? "0" : "1"
            
            if(userDefault.string(forKey: "RegionCode") == "SA")
            {
                isInternational = "1&country_code=SA"
            }
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)get_offers?for_mall=&is_international=\(isInternational)", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.OfferesTableView.isHidden = false

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        OffersArray.removeAllObjects()
                        
                        OffersArray.addObjects(from: responceDict["offer"] as! [Any])
                        
                        if let strImg = (OffersArray.object(at: 0) as! [String:Any])["n_offer_new_image"] as? String
                        {
                            let url = URL.init(string: strImg)
                            let data = try? Data(contentsOf: url!)
                            
                            if Reachability.isConnectedToNetwork() {
                                
                                if data != nil || imgTemp != nil
                                {
                                    imgTemp = UIImage.init(data: data!)
                                }else
                                {
                                    self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                                        if ButtonIndex == 0
                                        {
                                            self.CallApi()
                                        }
                                    })
                                }
                                
                            }
                            else
                            {
                                self.RemoveLoader()
                                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                                    if ButtonIndex == 0
                                    {
                                        self.CallApi()
                                    }
                                })
                            }
                            
                        }
                        
                        self.OfferesTableView.reloadData()
                        
                        self.RemoveLoader()
                        
                    }else{
                        self.RemoveLoader()
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                    
                }
                else
                {
                    self.RemoveLoader()
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.CallApi()
                        }
                    })
                    
                }
            }
            
        }
        else
        {
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.CallApi()
                }
            })
        }
        
    }
    
    
    func Check_Validation(code : String) {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["order_id" : orderID ?? "",
                             "discount_code" : code,
                             "gift" : isGift] as NSDictionary
            
            //            ParamDict = ["user_id": userID,
            //                             "code" : code,
            //                             "status" : 1 ] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)order/apply_discount", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        // Send to PlaceOrder Screen
                        
                        let dict = responceDict["data"] as! [String:Any]
                        
                        if let isApplied = dict["discount_applied"] as? Int, isApplied > 0
                        {
                            let discount = "\(dict["discount_amount"] ?? "")"
                            
                            isBackFromOffer = false
                            
                            userDefault.set(code, forKey: "OfferCode")
                            userDefault.set(discount, forKey: "OfferCodeAmount")
                            
                            userDefault.set(dict, forKey: "OfferCodeDict")
                            self.popViewController()
                            
                        }else
                        {
                            let msg = dict["discount_message"] as? String ?? ""
                            
                            self.presentAlertWithTitle(title: msg, message: "", options: "OK", completion: { (ButtonIndex) in
                            })
                        }
                        
                    }else
                    {
                        self.view.makeToast("\(responceDict["ResponseMessage"] ?? "")", duration: 2.0, position: .bottom)
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
}
