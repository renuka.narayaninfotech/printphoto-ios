//
//  OffersVCTableViewCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 05/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class OffersVCTableViewCell: UITableViewCell {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var contentsView: UIView!
    @IBOutlet weak var offersImage: UIImageView!
    @IBOutlet weak var lblOffers: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblExpiredDate: UILabel!
    @IBOutlet weak var btnTermsOfUse: UIButton!
//    @IBOutlet weak var heightImgView: NSLayoutConstraint!
    @IBOutlet weak var imageviewWidthRatio: NSLayoutConstraint!
    
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
            
        lblOffers.sizeToFit()
        lblCode.sizeToFit()
        lblExpiredDate.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
