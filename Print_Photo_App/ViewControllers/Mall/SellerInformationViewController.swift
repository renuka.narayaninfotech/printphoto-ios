//
//  SellerInformationViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 21/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import Social
import MessageUI
import SystemConfiguration
import MapKit
import CoreTelephony

class SellerInformationViewController: BaseViewController,MFMailComposeViewControllerDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var sellerInformationView: UIView!
    @IBOutlet weak var lblSellerName: UILabel!
    @IBOutlet weak var lblEmailId: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var heightViewAddress: NSLayoutConstraint!
    @IBOutlet weak var heightLogoView: NSLayoutConstraint!
    @IBOutlet weak var btnSellerName: UIButton!
    @IBOutlet weak var btnEmailId: UIButton!
    @IBOutlet weak var btnMobileNo: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var selerInformationDict : NSDictionary!
    
    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Seller Information", EndString: "")
        
        Initialize()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        addBackButton()
//        OpenWishList()
        removeBackButton()
        
        btnSellerName.isExclusiveTouch = true
        btnEmailId.isExclusiveTouch = true
        btnMobileNo.isExclusiveTouch = true
        btnAddress.isExclusiveTouch = true
        
        self.sellerInformationView.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
   
        lblSellerName.text = "\(selerInformationDict.value(forKey: "name") as? String ?? "")"
        lblEmailId.text = "\(selerInformationDict.value(forKey: "email") as? String ?? "")"
        lblMobileNo.text = "\(selerInformationDict.value(forKey: "mobile") as? String ?? "")"
        lblAddress.text = "\(selerInformationDict.value(forKey: "address") as? String ?? "")"
        
        // Design layout in different screen with managed multiple lable line
        Design_Layout()
        
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    @IBAction func press_btn_SellerName(_ sender: UIButton) {
        
    }
    
    @IBAction func press_btn_EmailId(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail()
        {
            if Reachability.isConnectedToNetwork() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients([lblEmailId.text ?? ""])
                present(mail, animated: true)
            }else{
                
                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                })
            }
            
        } else
        {
            let url = URL(string: "mailto:\(lblEmailId.text ?? "")")
            UIApplication.shared.open(url!)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func press_btn_MobileNo(_ sender: UIButton) {
        sender.isUserInteractionEnabled = false
        check_SIM_Available_OR_Not(sender)
    }
    
    @IBAction func press_btn_Address(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork() {

            let strURL = ("https://maps.google.com/?q=" + (lblAddress.text?.replacingOccurrences(of: "", with: "+"))!).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            let urlString = URL(string: strURL)

            UIApplication.shared.open(urlString!, options: [:], completionHandler: nil)
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
//    func OpenWishList() {
//        let wishListButton = UIButton()
//        wishListButton.setImage(UIImage(named: "ic_mall_header_fav_list"), for: .normal)
//        wishListButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
//        wishListButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0, bottom: 0, right: -10)
//        wishListButton.addTarget(self, action: #selector(self.wishListButtonAction(_:)), for: .touchUpInside)
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: wishListButton)
//    }
//
//    @objc func wishListButtonAction(_ sender: UIButton) {
//
//        if userDefault.value(forKey: "USER_ID") == nil || userDefault.integer(forKey: "USER_ID") == 0
//        {
//            self.presentAlertWithTitle(title: "Log In", message: "Need to log in first", options: "CANCEL","OK") { (ButtonIndex) in
//                if ButtonIndex == 0
//                {
//                    self.dismissViewController()
//                }else
//                {
//                    isLoginOrNot = "seller_information_login"
//
//                    let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
//                    self.pushViewController(signinVC)
//
//                }
//            }
//        }else
//        {
//            let WishListCV = self.storyboard?.instantiateViewController(withIdentifier: "WishListViewController") as! WishListViewController
//            self.pushViewController(WishListCV)
//        }
//
//    }
    
    //MARK:- ********************** CHECK SIM CARD AVAILABLE OR NOT **********************

    func check_SIM_Available_OR_Not(_ sender: UIButton)
    {
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier: CTCarrier? = networkInfo.subscriberCellularProvider
        let code: String? = carrier?.mobileNetworkCode
        if (code != nil) {
            if let url = URL(string: "tel://\(lblMobileNo.text ?? "")") {
                UIApplication.shared.open(url)
            }
        }
        else {
            self.topMostViewController().view.makeToast("SIM card is not inserted", duration: 3.0, position: .bottom)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
            sender.isUserInteractionEnabled = true
        })
    }
    
    func Design_Layout()
    {
        // managed design layout in different screen size and Managed multiple line label
        var topPadding: CGFloat = 20.0
        var bottomPadding: CGFloat = 0.0
        
        if #available(iOS 11.0, *) {
            topPadding = UIApplication.shared.keyWindow!.safeAreaInsets.top
            bottomPadding = UIApplication.shared.keyWindow!.safeAreaInsets.bottom
        }
        
        let heightView : CGFloat = (self.navigationController!.navigationBar.frame.height + bottomPadding)
        
//        let strAddress = "\(contactDict["address"] ?? "")"
        let heightAddress = lblAddress.text?.height(withConstrainedWidth: ScreenSize.SCREEN_WIDTH-116.0, font: UIFont.init(name: "HelveticaNeue", size: DeviceType.IS_IPAD ? 24.0 : 16.0)!)
        
        if DeviceType.IS_IPAD
        {
            heightViewAddress.constant = ((ScreenSize.SCREEN_HEIGHT - heightView) - ((ScreenSize.SCREEN_HEIGHT - heightView) * CGFloat(0.3)) - 42.0) / 4.0
            heightLogoView.constant = (ScreenSize.SCREEN_HEIGHT - heightView) * CGFloat(0.3)
        }
        else
        {
            heightViewAddress.constant = heightAddress! + 57.0
            
            let minHeightLogo : CGFloat = 80.0
            let remainHeight = ScreenSize.SCREEN_HEIGHT - (4.0 * (heightAddress! + 57.0)) - 30 - heightView
            if remainHeight < 165.0 // if under default height
            {
                heightLogoView.constant = (remainHeight > minHeightLogo ? remainHeight : minHeightLogo) - 5.0
            }
            else
            {
                heightLogoView.constant = remainHeight - 5.0
            }
        }
    }
}
