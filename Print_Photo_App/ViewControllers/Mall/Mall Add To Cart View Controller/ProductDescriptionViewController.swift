//
//  ProductDescriptionViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 04/07/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import TYCyclePagerView
import SKPhotoBrowser

class ProductDescriptionViewController: BaseViewController,TYCyclePagerViewDelegate, TYCyclePagerViewDataSource {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var pagerView: TYCyclePagerView!
    @IBOutlet weak var pageControl: TYPageControl!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductDescription: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnZoomImage: UIButton!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var productDict : [String:Any]!
    var pagerViewCurrentIndex : Int?
    var arrProductImage = [Any]()
    let link = "https://itunes.apple.com/us/app/print-photo-phone-case-maker/id1458325325?ls=1&mt=8"
    var strProductSharing : String?
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()

         Initialize()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        
        self.scrollView.isHidden = true
        self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let strTitle = "\(productDict?["name"] as? String ?? "")"
        SetNavigationBarTitle(Startstring: strTitle, EndString: "")
        
        Pager_Control()
        loadData()
        
        pagerViewArray = productDict?["product_images"] as? NSArray ?? NSArray()

        arrProductImage.removeAll()
        
        // Convert URl to UIImage and add to productArray
        for i in 0..<pagerViewArray.count
        {
            let imageURL = (pagerViewArray.object(at: i) as! NSDictionary).value(forKey: "thumb_image") as? String ?? ""
            
            let url = URL(string:imageURL)
            if let data = try? Data(contentsOf: url!)
            {
                let image: UIImage = UIImage(data: data)!
                
                arrProductImage.append(image)
            }
        }
        
        // Get data from itemDict
        lblProductName.text = "\(productDict?["name"] as? String ?? "")"
        lblProductDescription.text = "\(productDict?["description"] as? String ?? "")"
        
        self.scrollView.isHidden = false
        self.RemoveLoader()
        
        
        strProductSharing = "\n\n Buy this amazing \(productDict?["name"] as? String ?? "") from print photo app from App Store \n\n \(link) \n\n \(productDict?["description"] as? String ?? "")"
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {

        addBackButton()
        addSharingButton()
    }
    
    //MARK:- ********************** PAGER CONTROL **********************

    func Pager_Control() {
        
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.register(TYCyclePagerViewCell.classForCoder(), forCellWithReuseIdentifier: "cellId")
        pagerView.cornerRadius = 10.0
        pagerView.isInfiniteLoop = true
        pagerView.autoScrollInterval = 2.0
        
        pageControl.currentPageIndicatorSize = CGSize(width: 8, height: 8)
        pageControl.pageIndicatorTintColor = .gray
        pageControl.currentPageIndicatorTintColor = UIColor(red:11/255, green:175/255, blue:205/255, alpha: 1)

    }
    
    func loadData() {
        
        var i = 0
        while i < pagerViewArray.count {
            i += 1
        }
        pageControl.numberOfPages = pagerViewArray.count
        pagerView.reloadData()
    }
    
    //MARK:- ********************** BUTTON EVENT **********************

    @IBAction func press_btn_zoom_image(_ sender: UIButton) {
        
        let ZoomImageVC = storyboard?.instantiateViewController(withIdentifier: "ZoomImageViewController") as! ZoomImageViewController
        ZoomImageVC.selectedIndex = pagerViewCurrentIndex
        ZoomImageVC.strSharing = strProductSharing
        ZoomImageVC.arrProductZoomImage = arrProductImage
        self.pushViewController(ZoomImageVC)
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
      self.popViewController()
    }
    
    func addSharingButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_share_Mall"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.sharingButtonAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func sharingButtonAction(_ sender: UIButton) {
        
        arrProductImage.append(strProductSharing ?? "")
        
        let objectsToShare = arrProductImage as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.setValue(appName, forKey: "subject")
        
        if DeviceType.IS_IPHONE
        {
            self.present(activityVC, animated: true, completion: nil)
        }else
        {
            let popController: UIPopoverPresentationController = activityVC.popoverPresentationController!
            popController.permittedArrowDirections = .any
            popController.sourceView = sender.superview
            popController.sourceRect = sender.frame
            self.present(activityVC, animated: true, completion: nil)
        }
        
        arrProductImage.removeLast()
    }
    
    //MARK:- ********************** TYCyclePagerView Datasource Method **********************

    func numberOfItems(in pageView: TYCyclePagerView) -> Int {
        return pagerViewArray.count
    }
    
    func pagerView(_ pagerView: TYCyclePagerView, cellForItemAt index: Int) -> UICollectionViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cellId", for: index) as! TYCyclePagerViewCell
        
        cell.imageView.contentMode = .scaleAspectFit
        let imageURL = (pagerViewArray[index] as! [String:Any])["thumb_image"] as? String ?? ""
        cell.imageView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: imageURL)
        
        return cell
    }
    
    func layout(for pageView: TYCyclePagerView) -> TYCyclePagerViewLayout {
        let layout = TYCyclePagerViewLayout()
        layout.itemSize = CGSize(width: pagerView.frame.width, height: pagerView.frame.height)
        layout.itemSpacing = 0
        layout.itemHorizontalCenter = true
        return layout
    }
    
    func pagerView(_ pageView: TYCyclePagerView, didScrollFrom fromIndex: Int, to toIndex: Int) {
        pageControl.currentPage = toIndex
        pagerViewCurrentIndex = toIndex
    }
    
    func pagerView(_ pageView: TYCyclePagerView, didSelectedItemCell cell: UICollectionViewCell, at index: Int) {
        
        let ZoomImageVC = storyboard?.instantiateViewController(withIdentifier: "ZoomImageViewController") as! ZoomImageViewController
        ZoomImageVC.selectedIndex = index
        ZoomImageVC.strSharing = strProductSharing
        ZoomImageVC.arrProductZoomImage = arrProductImage
        self.pushViewController(ZoomImageVC)
    }
}
