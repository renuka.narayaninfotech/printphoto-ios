//
//  MallAddToCartViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 22/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import TYCyclePagerView
import SKPhotoBrowser

// Get Slider PagerView image Array
var pagerViewArray = NSArray()

// Create delegate method push from SearchCategoryViewController and CategorylistViewController for Favorite or Unfavorite image
protocol MallAddCartDelegate : class {
    func backFromMallCart()
}

class MallAddToCartViewController: BaseViewController,TYCyclePagerViewDelegate, TYCyclePagerViewDataSource,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pagerView: TYCyclePagerView!
    @IBOutlet weak var pageControl: TYPageControl!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblOriginalPrice: UILabel!
    @IBOutlet weak var lblDummyPrice: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var pincodeBorderView: UIView!
    @IBOutlet weak var txtPincode: UITextField!
    @IBOutlet weak var tblProductDetils: UITableView!
    @IBOutlet weak var lblSellerName: UILabel!
    @IBOutlet weak var constraintsTblProductDetilsHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var btnSellerInformation: UIButton!
    @IBOutlet weak var constraintsBtnAddToCartBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnZoomImage: UIButton!
    @IBOutlet weak var lblYouSave: UILabel!
    @IBOutlet weak var heightlblSave: NSLayoutConstraint!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var heightProductDescriptionView: NSLayoutConstraint!
    @IBOutlet weak var ProductDescriptionView: UIView!
    @IBOutlet weak var heightPincodeView: NSLayoutConstraint!
    @IBOutlet weak var pincodeView: UIView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var productDetailsArray = NSArray()
    weak var delegate: MallAddCartDelegate?
    var itemDict : [String:Any]!
    var getKeyBoardHeight : CGFloat!

    var pagerViewCurrentIndex : Int?
    
    var arrProductImage = [Any]()
    var strProductSharing : String?
    let link = "https://itunes.apple.com/us/app/print-photo-phone-case-maker/id1458325325?ls=1&mt=8"
    var currencySymbol : String?
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        DispatchQueue.main.async {
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
        }

        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        
        self.scrollView.isHidden = true
        
        // Pincode(Check Delivery) is hide when user is out of India
        if userDefault.string(forKey: "RegionCode") == "IN"
        {
            pincodeView.isHidden = false
            heightPincodeView.constant = DeviceType.IS_IPAD ? 102.0 : 68.0
        }else
        {
            pincodeView.isHidden = true
            heightPincodeView.constant = 0.0
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let strTitle = "\(itemDict?["name"] as? String ?? "")"
        SetNavigationBarTitle(Startstring: strTitle, EndString: "")
        
        pagerViewArray = itemDict?["product_images"] as? NSArray ?? NSArray()
        
        arrProductImage.removeAll()
        
        // Convert URl to UIImage and add to productArray
        for i in 0..<pagerViewArray.count
        {
            let imageURL = (pagerViewArray.object(at: i) as! NSDictionary).value(forKey: "thumb_image") as? String ?? ""
            
            let url = URL(string:imageURL)
            if let data = try? Data(contentsOf: url!)
            {
                let image: UIImage = UIImage(data: data)!
                
                arrProductImage.append(image)
            }
        }
        
        productDetailsArray = itemDict?["product_details"] as? NSArray ?? NSArray()
        Pager_Control()
        loadData()
        
        // Get data from itemDict
        lblProductName.text = "\(itemDict?["name"] as? String ?? "")"
        lblOriginalPrice.text = "\(currencySymbol ?? "")\(itemDict?["price"] as? String ?? "")"
        
        if itemDict["dummy_price"] as? String ?? "" != "" || itemDict["discount"] as? String ?? "" != ""
        {
            lblDummyPrice.attributedText = "\(currencySymbol ?? "")\(itemDict?["dummy_price"] as? String ?? "")".strikeThrough()
            lblDiscount.text = "\(itemDict?["discount"] as? String ?? "")"
        }
     
        if itemDict["you_save"] as? String ?? "" != ""
        {
            lblYouSave.text = itemDict["you_save"] as? String ?? ""
            heightlblSave.constant = DeviceType.IS_IPAD ? 39.0 : 26.0
        }else
        {
            heightlblSave.constant = 0
        }
        
        lblDescription.text = itemDict?["description"] as? String ?? ""
        lblSellerName.text = (itemDict?["seller_details"] as! [String:Any])["name"] as? String ?? ""
        
        // Resize Product description view depend on lblDescription label content size
        lblDescription.sizeToFit()
        heightProductDescriptionView.constant = lblDescription.Getheight
        ProductDescriptionView.setNeedsLayout()
        ProductDescriptionView.layoutIfNeeded()
        
        if itemDict?["in_wishlist"] as? Bool == true
        {
            self.btnFavorite.setBackgroundImage(UIImage(named: "ic_mall_favorites"), for: .normal)
        }else{
            self.btnFavorite.setBackgroundImage(UIImage(named: "ic_mall_unfavorites"), for: .normal)
        }
        
        tblProductDetils.reloadData()
        
        self.scrollView.isHidden = false
        self.RemoveLoader()
        
        strProductSharing = "\n\n Buy this amazing \(itemDict?["name"] as? String ?? "") from print photo app from App Store \n\n \(link) \n\n \(itemDict?["description"] as? String ?? "")"
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        self.txtPincode.isUserInteractionEnabled = true
        txtPincode.returnKeyType = .done
        
        btnFavorite.isExclusiveTouch = true
        btnSubmit.isExclusiveTouch = true
        btnAddToCart.isExclusiveTouch = true
        btnSellerInformation.isExclusiveTouch = true
        btnShare.isExclusiveTouch = true
        
        addBackButton()
//        OpenWishList()
        addContactUsButton()
        
        pincodeBorderView.setBorder(borderWidth: 2, borderColor: UIColor(red:69/255, green:69/255, blue:69/255, alpha: 1).cgColor)
        
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow),name: UIResponder.keyboardWillShowNotification,object: nil)
        
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillHide),name: UIResponder.keyboardWillHideNotification,object: nil)

    }
    
    //MARK:- ********************** PAGER CONTROL **********************
    
    func Pager_Control() {
        
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.register(TYCyclePagerViewCell.classForCoder(), forCellWithReuseIdentifier: "cellId")
        pagerView.cornerRadius = 10.0
        pagerView.isInfiniteLoop = true
        pagerView.autoScrollInterval = 2.0
        pageControl.currentPageIndicatorSize = CGSize(width: 8, height: 8)
        pageControl.pageIndicatorTintColor = .gray
        pageControl.currentPageIndicatorTintColor = UIColor(red:11/255, green:175/255, blue:205/255, alpha: 1)

    }
    
    func loadData() {
        
        var i = 0
        while i < pagerViewArray.count {
            i += 1
        }
        if(pagerViewArray.count == 0 || pagerViewArray.count == 1)
        {
            pagerView.isInfiniteLoop = false
            pagerView.autoScrollInterval = 0.0
        }
        pageControl.numberOfPages = pagerViewArray.count
        pagerView.reloadData()
    }
    
    //MARK:- **********************  SHOW KEY-BOARD  **********************
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        // return keyboard height when come from Login screen
        guard ((self.view?.window) != nil) else {
            return
        }
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            getKeyBoardHeight = keyboardHeight
            
            constraintsBtnAddToCartBottomMargin.constant = getKeyBoardHeight - view.safeAreaInsets.bottom

            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        
        constraintsBtnAddToCartBottomMargin.constant = DeviceType.IS_IPAD ? 9.0 : 6.0

        self.view.layoutIfNeeded()

    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    @IBAction func press_btn_share(_ sender: UIButton) {
        
        arrProductImage.append(strProductSharing ?? "")
        
        let objectsToShare = arrProductImage as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.setValue(appName, forKey: "subject")
        
        if DeviceType.IS_IPHONE
        {
            self.present(activityVC, animated: true, completion: nil)
        }else
        {
            let popController: UIPopoverPresentationController = activityVC.popoverPresentationController!
            popController.permittedArrowDirections = .any
            popController.sourceView = sender.superview
            popController.sourceRect = sender.frame
            self.present(activityVC, animated: true, completion: nil)
        }
        
        arrProductImage.removeLast()
        
    }
    
    @IBAction func press_btn_zoom_image(_ sender: UIButton) {
        
//        let strCurrentImage = (pagerViewArray[pagerViewCurrentIndex!] as! [String:Any])["image"] as? String ?? ""

        let ZoomImageVC = storyboard?.instantiateViewController(withIdentifier: "ZoomImageViewController") as! ZoomImageViewController
        ZoomImageVC.selectedIndex = pagerViewCurrentIndex
        ZoomImageVC.strSharing = strProductSharing
        ZoomImageVC.arrProductZoomImage = arrProductImage
        self.pushViewController(ZoomImageVC)
    }
    
    @IBAction func press_btn_Favorite(_ sender: UIButton) {
        
        if userDefault.value(forKey: "USER_ID") == nil || userDefault.integer(forKey: "USER_ID") == 0
        {
            self.presentAlertWithTitle(title: "Log In", message: "Need to log in first", options: "CANCEL","OK") { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.dismissViewController()
                }else
                {
                    isLoginOrNot = "mall_add_to_cart_login"
                    
                    let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                    self.pushViewController(signinVC)
                }
            }
        }else
        {
            if itemArrayGlobal.count > 0
            {
                let id = itemDict["id"] as? Int ?? 0
                
                let objTemp = itemArrayGlobal.first { (obj) -> Bool in
                    if let dict = obj as? NSDictionary
                    {
                        let idTemp = dict["id"] as? Int ?? 0
                        
                        return id == idTemp
                    }
                    return false
                }
                
                if let objUpdate = objTemp as? NSDictionary
                {
                    let objDict = NSMutableDictionary(dictionary: objUpdate)
                    
                    let idx = itemArrayGlobal.index(of: objUpdate)
                    
                    if objDict["in_wishlist"] as? Bool == true
                    {
                        objDict["in_wishlist"] = false
                    }
                    else
                    {
                        objDict["in_wishlist"] = true
                    }
                    
                    itemArrayGlobal.replaceObject(at: idx, with: objDict)

                }
                
                if itemDict?["in_wishlist"] as? Bool == true
                {
                    itemDict?["in_wishlist"] = false
                }
                else
                {
                    itemDict?["in_wishlist"] = true
                }

            }
            
           Add_To_Favorite()
        }
    }
    
    @IBAction func press_btn_Submit_Pincode(_ sender: UIButton) {
        
        if sender.titleLabel?.text == "Submit"
        {
            if isCheckPincode()
            {
                self.Check_Pincode_API()
            }
        }else
        {
            self.txtPincode.isUserInteractionEnabled = true
            self.btnSubmit.setTitle("Submit", for: .normal)
            self.txtPincode.text = ""
            self.txtPincode.textColor = UIColor(red:69/255, green:69/255, blue:69/255, alpha: 1)
        }
        
    }
    
    @IBAction func press_btn_product_description(_ sender: UIButton) {
        let ProductDescriptionVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductDescriptionViewController") as! ProductDescriptionViewController
        ProductDescriptionVC.productDict = itemDict
        self.pushViewController(ProductDescriptionVC)
    }
    
    @IBAction func press_btn_SellerInformation(_ sender: UIButton) {
        let SellerInformationVC = self.storyboard?.instantiateViewController(withIdentifier: "SellerInformationViewController") as! SellerInformationViewController
        SellerInformationVC.selerInformationDict = itemDict?["seller_details"] as? NSDictionary
        self.pushViewController(SellerInformationVC)
    }
    
    @IBAction func press_btn_Add_To_Cart(_ sender: UIButton) {
        
        if userDefault.value(forKey: "USER_ID") == nil || userDefault.integer(forKey: "USER_ID") == 0
        {
            self.presentAlertWithTitle(title: "Log In", message: "Need to log in first", options: "CANCEL","OK") { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.dismissViewController()
                }else
                {
                    isLoginOrNot = "mall_add_to_cart_login"
                    
                    let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                    self.pushViewController(signinVC)
                    
                }
            }
        }else
        {
            self.presentAlertWithTitle(title: "Add to cart?", message: "", options: "NO","YES") { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.dismissViewController()
                }else
                {
                    DispatchQueue.main.async{
                        self.Add_To_Cart()
                    }
                }
            }
        }
       
    }

    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
 
            let VCarray = self.navigationController?.viewControllers
            
            for Controller in VCarray! {
                if Controller is MallAddToCartViewController {
                    (Controller as! MallAddToCartViewController).delegate?.backFromMallCart()
                    break
                }
            }
        
            if isFromSelect == "isFromCategory"
            {
                for Controller in VCarray! {
                    if Controller is CategoryListViewController {
                        self.navigationController?.popToViewController(Controller, animated: true)
                    }
                }
            }else  if isFromSelect == "isFromSearch"
            {
                for Controller in VCarray! {
                    if Controller is SearchCategoryViewController {
                        self.navigationController?.popToViewController(Controller, animated: true)
                    }
                }
            }else  if isFromSelect == "isFromWishlist"
            {
                for Controller in VCarray! {
                    if Controller is WishListViewController {
                        self.navigationController?.popToViewController(Controller, animated: true)
                    }
                }
            }
    }
    
    func addContactUsButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_contact"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.contactUsAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func contactUsAction(_ sender: UIButton) {
        let contactUsVC = mainStoryBoard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        pushViewController(contactUsVC)
    }
    
//    func OpenWishList() {
//        let wishListButton = UIButton()
//        wishListButton.setImage(UIImage(named: "ic_mall_header_fav_list"), for: .normal)
//        wishListButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
//        wishListButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0, bottom: 0, right: -10)
//        wishListButton.addTarget(self, action: #selector(self.wishListButtonAction(_:)), for: .touchUpInside)
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: wishListButton)
//    }
//
//    @objc func wishListButtonAction(_ sender: UIButton) {
//
//        if userDefault.value(forKey: "USER_ID") == nil || userDefault.integer(forKey: "USER_ID") == 0
//        {
//            self.presentAlertWithTitle(title: "Log In", message: "Need to log in first", options: "CANCEL","OK") { (ButtonIndex) in
//                if ButtonIndex == 0
//                {
//                    self.dismissViewController()
//                }else
//                {
//                    isLoginOrNot = "mall_add_to_cart_login"
//
//                    let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
//                    self.pushViewController(signinVC)
//
//                }
//            }
//        }else
//        {
//            let WishListCV = self.storyboard?.instantiateViewController(withIdentifier: "WishListViewController") as! WishListViewController
//            self.pushViewController(WishListCV)
//        }
//
//    }
    
    //MARK:- ********************** TYCyclePagerView Datasource Method **********************
    
    func numberOfItems(in pageView: TYCyclePagerView) -> Int {
        return pagerViewArray.count
    }
    
    func pagerView(_ pagerView: TYCyclePagerView, cellForItemAt index: Int) -> UICollectionViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cellId", for: index) as! TYCyclePagerViewCell
        
        cell.imageView.contentMode = .scaleAspectFit

        let imageURL = (pagerViewArray[index] as! [String:Any])["thumb_image"] as? String ?? ""
        
        if imageURL != ""
        {
            cell.imageView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: imageURL)
            
        }
//        else{
//            // Temp string is a setlement
//            cell.imageView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: "tempString")
//        }
        
        return cell
    }
    
    func layout(for pageView: TYCyclePagerView) -> TYCyclePagerViewLayout {
        let layout = TYCyclePagerViewLayout()
        layout.itemSize = CGSize(width: pagerView.frame.width, height: pagerView.frame.height)
        layout.itemSpacing = 0
        layout.itemHorizontalCenter = true
        return layout
    }
    
    func pagerView(_ pageView: TYCyclePagerView, didScrollFrom fromIndex: Int, to toIndex: Int) {
//        if(pagerViewArray.count == 0 || pagerViewArray.count == 1)
//        {
//            return
//        }
        pageControl.currentPage = toIndex;
        pagerViewCurrentIndex = toIndex
    }
    
    func pagerView(_ pageView: TYCyclePagerView, didSelectedItemCell cell: UICollectionViewCell, at index: Int) {
        
//        let strCurrentImage = (pagerViewArray[index] as! [String:Any])["image"] as? String ?? ""
        
        let ZoomImageVC = storyboard?.instantiateViewController(withIdentifier: "ZoomImageViewController") as! ZoomImageViewController
//        ZoomImageVC.strSelectedImage = strCurrentImage
        ZoomImageVC.selectedIndex = index
        ZoomImageVC.strSharing = strProductSharing
        ZoomImageVC.arrProductZoomImage = arrProductImage
        self.pushViewController(ZoomImageVC)
    }

    //MARK:- ********************** TABLEVIEW METHODS **********************

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailsTableViewCell", for: indexPath) as! ProductDetailsTableViewCell
        
        cell.lblProductDetails.text = ((itemDict["product_details"] as! NSArray)[indexPath.row] as! [String:Any])["name"] as? String ?? ""
        cell.lblCurrentProductDetails.text = ((itemDict["product_details"] as! NSArray)[indexPath.row] as! [String:Any])["value"] as? String ?? ""

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        print("content size = \(tableView.contentSize.height)")
        
        DispatchQueue.main.async {
            self.constraintsTblProductDetilsHeight.constant = tableView.contentSize.height
            self.tblProductDetils.layoutIfNeeded()
        }
    }
    
    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        constraintsBtnAddToCartBottomMargin.constant = DeviceType.IS_IPAD ? 9.0 : 6.0
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      
        if textField == txtPincode
        {
            txtPincode.resignFirstResponder()
        }

        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        textField.autocorrectionType = .no
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if textField.tag == 2
        {
            switch string
            {
            case  "0","1","2","3","4","5","6","7","8","9":
                
                if newLength <= 6
                {
                    return true
                } else {
                    return false
                }
            case "":
                return true && newLength <= 50
                
            default:
                let array = [string]
                if array.count == 0
                {
                    return true
                }
                return false && newLength <= 4
            }
        }
        return true
    }
    
    //MARK:- ********************** VALIDATION **********************
    
    func isCheckPincode()-> Bool {
        
        if Validation.isStringEmpty(txtPincode.text!) {
            presentAlertWithTitle(title: "Please enter pincode", message: "", options: "OK") { (Int) in
                self.txtPincode.becomeFirstResponder()
            }
        }else if Int(txtPincode.text!.count) < 6
        {
            self.presentAlertWithTitle(title: "Pincode must be 6 digit", message: "", options: "OK", completion: { (ButtonIndex) in
                self.txtPincode.becomeFirstResponder()
            })
        }
        else{
            return true
        }
        
        return false
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Check_Pincode_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)pincode_serviceability?pincode=\(txtPincode.text ?? "")", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    self.view.endEditing(true)
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        let pincodeDict = responceDict["data"] as! [String:Any]
                        
                        self.btnSubmit.setTitle("Change", for: .normal)
                        self.txtPincode.isUserInteractionEnabled = false
                        
                        let pincodeServiceable  = pincodeDict["prepaid_serviceable"] as! Bool
                        
                        if pincodeServiceable == true
                        {
                            self.txtPincode.text = "Delivery available"
                            self.txtPincode.textColor = .green
                            
                        }else
                        {
                            self.txtPincode.text = "Delivery not available"
                            self.txtPincode.textColor = .red
                            
                            self.topMostViewController().view.makeToast("\(pincodeDict["prepaid_message"] as? String ?? "")", duration: 2.0, position: .bottom)
                        }
                        
                    }else{
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
            
        }
        else
        {
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
               
            })
        }
    }

    func Add_To_Cart() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                            "model_id": "\(itemDict?["id"] as? Int ?? 0)",
                            "quantity": "1"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)add_to_cart", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        isCartBtnBack = false
                        
                        self.dismissViewController()
                        ((self.tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
                        
                        self.tabBarController?.selectedIndex = 2
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
//                        self.Add_To_Cart()
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
//                self.Add_To_Cart()
            })
        }
    }
    
    func Add_To_Favorite() {
        
        if Reachability.isConnectedToNetwork() {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
       
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)add_wishlist?product_id=\(itemDict?["id"] as? Int ?? 0)&user_id=\(userDefault.value(forKey: "USER_ID") ?? "")", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    self.view.endEditing(true)
                    
                    let responceDict = Responce as! [String:Any]

                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.btnFavorite.setBackgroundImage(UIImage(named: "ic_mall_favorites"), for: .normal)
                    }else{
                        self.btnFavorite.setBackgroundImage(UIImage(named: "ic_mall_unfavorites"), for: .normal)
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
            
        }
        else
        {
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
        
    }
}
