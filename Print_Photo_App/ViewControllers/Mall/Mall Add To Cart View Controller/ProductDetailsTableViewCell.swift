//
//  ProductDetailsTableViewCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 24/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class ProductDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblProductDetails: UILabel!
    @IBOutlet weak var lblCurrentProductDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblProductDetails.sizeToFit()
        lblCurrentProductDetails.sizeToFit()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
