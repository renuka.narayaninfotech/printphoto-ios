//
//  ZoomImageViewController.swift
//  Print_Photo_App
//
//  Created by Mac Os on 13/07/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import Alamofire

class ZoomImageViewController: BaseViewController,SKPhotoBrowserDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var zoomImageView: UIView!

    //MARK:- ********************** VARIABLE DECLARATION **********************

    var strSelectedImage : String?
    var selectedIndex :Int?
    var arrProductZoomImage = [Any]()
    var strSharing : String?
    
    let reachabilityManager = NetworkReachabilityManager()

    //MARK:- ********************** VIEW LIFECYCLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()

        Initialize()

        // Do any additional setup after loading the view.
    }
 
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
     
        addBackButton()
        addSharingButton()
        
        checkRechabilityStatus()
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        reachabilityManager?.stopListening()
    }
    
    //MARK:- ********************** CHECK RECHABILITY STATUS **********************

    func checkRechabilityStatus()
    {
        reachabilityManager?.startListening(onUpdatePerforming: { _ in
            if let isNetworkReachable = self.reachabilityManager?.isReachable,
                isNetworkReachable == true {
                
                //Internet Available
                print("Internet Available")
                
                var images = [SKPhoto]()
                for i in (0 ..< pagerViewArray.count)
                {
                    let strCurrentImage = (pagerViewArray[i] as! [String:Any])["image"] as? String ?? ""
                    let URL = NSURL(string: strCurrentImage)
                    let photo = SKPhoto.photoWithImageURL(URL?.absoluteString! ?? "")
                    photo.shouldCachePhotoURLImage = false
                    images.append(photo)
                }
                
                SKPhotoBrowserOptions.displayAction = false   // action button will be hidden
                SKPhotoBrowserOptions.displayCloseButton = false   // Close Button will be hidden
                SKPhotoBrowserOptions.disableVerticalSwipe = true  // Disable Verticle swipe
                SKPhotoBrowserOptions.displayBackAndForwardButton = false // Disable Back and Forward button
                SKPhotoBrowserOptions.displayPagingHorizontalScrollIndicator = false
                SKPhotoBrowserOptions.displayCounterLabel = false
                
                let browser = SKPhotoBrowser(photos: images)
                browser.initializePageIndex(self.selectedIndex ?? 0)
                browser.delegate = self
                self.addChild(browser)
                browser.view.frame = CGRect(x: 0, y: 0, width: self.zoomImageView.frame.size.width, height: self.zoomImageView.frame.size.height)
                self.zoomImageView.addSubview(browser.view)
                browser.didMove(toParent: self)
                
            } else {
                //Internet Not Available"
                print("Internet Not Available")
               self.popViewController()

                    self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                    })
            }
        })
        
//        reachabilityManager?.stopListening()
    }
    //MARK:- ********************** BUTTON EVENT **********************

    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
      self.popViewController()
    }
    
    func addSharingButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ic_share_Mall"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(self.sharingButtonAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func sharingButtonAction(_ sender: UIButton) {
        
        arrProductZoomImage.append(strSharing ?? "")
        
        let objectsToShare = arrProductZoomImage as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.setValue(appName, forKey: "subject")
        
        if DeviceType.IS_IPHONE
        {
            self.present(activityVC, animated: true, completion: nil)
        }else
        {
            let popController: UIPopoverPresentationController = activityVC.popoverPresentationController!
            popController.permittedArrowDirections = .any
            popController.sourceView = sender.superview
            popController.sourceRect = sender.frame
            self.present(activityVC, animated: true, completion: nil)
        }
        
        arrProductZoomImage.removeLast()
    }
    
    //MARK:- ********************** SKPhotoBrowserDelegate **********************

    func captionViewForPhotoAtIndex(index: Int) -> SKCaptionView? {
        
        var totalcount = String()
        var imagenumb = String()
        
        if pagerViewArray.count <= 9
        {
            totalcount = String(format: "%02d", pagerViewArray.count)
        }
        else
        {
            totalcount = String(pagerViewArray.count)
        }
        
        if selectedIndex!+1 <= 9
        {
            imagenumb = String(format: "%02d", index+1)
        }
        else
        {
            imagenumb = String(index+1)
        }
        
        SetNavigationBarTitle(Startstring: "\(imagenumb) / \(totalcount)", EndString: "")
        
        return nil
    }

}

