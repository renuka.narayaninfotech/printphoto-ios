//
//  CategoryTableViewCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 18/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var imgSubCategory: UIImageView!
    @IBOutlet weak var lblSubCategory: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        lblCategory.sizeToFit()
//        lblDescription.sizeToFit()
//        lblSubCategory.sizeToFit()
//        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
