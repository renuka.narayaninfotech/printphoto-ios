//
//  SearchCategoryViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 20/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import Firebase

class SearchCategoryViewController: BaseViewController,UITableViewDelegate, UITableViewDataSource,MallAddCartDelegate,UITextFieldDelegate,UISearchBarDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblCategory: UITableView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var search = ""
    var timer: Timer? = nil
    var currencySymbol : String?

    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Shopping Mall", EndString: "")
        
        Initialize()

        
        // Do any additional setup after loading the view.
    }
  
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        searchBar.becomeFirstResponder()
    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
        
        addBackButton()
        addCartButtonWithBadgeIcon()
        
        mallSearchCategoryFilteredArr.removeAll()
        
//        DispatchQueue.main.async {
//            self.searchBarView.cornerRadius = self.searchBarView.Getheight/2
//        }
//
//        txtSearchBar.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)

    }
    
    //MARK:- ********************** BUTTON EVENT **********************

    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addCartButtonWithBadgeIcon() {
        
        var badgelabel = UILabel()
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            // badge label
            badgelabel = UILabel(frame: CGRect(x: 20, y: -5, width: 17, height: 17))
            badgelabel.layer.borderColor = UIColor.clear.cgColor
            badgelabel.layer.borderWidth = 2
            badgelabel.layer.cornerRadius = badgelabel.bounds.size.height / 2
            badgelabel.textAlignment = .center
            badgelabel.layer.masksToBounds = true
            badgelabel.font = UIFont(name: CustomFontWeight.medium, size: 10)
            badgelabel.textColor = .white
            badgelabel.backgroundColor = hexStringToUIColor(hex: "0ABAD4")
            
            if (userDefault.integer(forKey: "BadgeNumber")) != 0
            {
                badgelabel.isHidden = false
                badgelabel.text = "\(userDefault.integer(forKey: "BadgeNumber"))"
            }else
            {
                badgelabel.isHidden = true
            }
        }
        
        // Button
        let cartButton = UIButton(type: .custom)
        cartButton.setImage(UIImage(named: "ic_cart"), for: .normal)
        cartButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cartButton.addTarget(self, action: #selector(self.cartAction(_:)), for: .touchUpInside)
        cartButton.addSubview(badgelabel)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cartButton)
        
    }
    
    @objc func cartAction(_ sender: UIButton) {
        
        isCartBtnBack = false
        ((tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
        self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 1 : 2
        
    }

    //MARK:- ********************** API CALLING **********************

    func Search_Category_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            var userId : Int?
            
            var strURL : String?

            let strSearchBar = searchBar.text?.lowercased().trim().addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

            // Get userID when user is login other wise userId is take 0
            if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
            {
                userId = userDefault.value(forKey: "USER_ID") as? Int ?? 0
                strURL = "\(BaseURL)mall_products?search=\(strSearchBar ?? "")&user_id=\(userId ?? 0)&country_code="
            }else
            {
                userId = 0
                strURL = "\(BaseURL)mall_products?search=\(strSearchBar ?? "")&user_id=\(userId ?? 0)&country_code=\(userDefault.string(forKey: "RegionCode") ?? "")"
            }

            
            if searchBar.text?.trim() != "" && (searchBar.text?.count ?? 0) >= 3
             {
                AFNetworkingAPIClient.sharedInstance.getAPICall(url: strURL ?? "", parameters: nil) { (APIResponce, Responce, error1) in
                    
                    if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                    {
                        let responceDict = Responce as! [String:Any]
                        
                        if responceDict["ResponseCode"] as! String == "1"
                        {
                            self.currencySymbol = responceDict["currency_symbol"] as? String ?? ""

                            mallSearchCategoryArray = responceDict["data"] as! [[String:Any]]
                            
                            itemArrayGlobal = NSMutableArray(array: responceDict["data"] as! NSArray)
                            
                            let tempDict = ["name":"Clear History"]
                            mallSearchCategoryArray.append(tempDict)
                            
                            mallSearchCategoryFilteredArr = mallSearchCategoryArray
                            
                            self.tblCategory.reloadData()
                            
                        }
                        else
                        {
                            self.topMostViewController().view.makeToast("No Products Found", duration: 2.0, position: .center)
                            
                            mallSearchCategoryFilteredArr.removeAll()
                            self.tblCategory.reloadData()

                        }
                        
                    }
                    else
                    {
                        self.topMostViewController().view.makeToast("No Products Found", duration: 2.0, position: .center)
                        
                        mallSearchCategoryFilteredArr.removeAll()
                        self.tblCategory.reloadData()
                    }
                }
            }
          
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
               
            })
        }
        
    }
    
    //MARK:- ********************** TABLEVIEW METHODS **********************

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mallSearchCategoryFilteredArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCategoryTableViewCell", for: indexPath) as! SearchCategoryTableViewCell
        
            cell.lblCategory.text = ((mallSearchCategoryFilteredArr[indexPath.row])["name"] as! String)
            cell.iconArrow.image = UIImage(named: "ArrowIcon")
        
            if ((mallSearchCategoryFilteredArr[indexPath.row])["name"] as! String) == "Clear History"
            {
                cell.iconArrow.image = UIImage(named: "ic_delte")
            }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedItemDict = mallSearchCategoryFilteredArr[indexPath.row] as [String:Any]
        
        if indexPath.row == mallSearchCategoryFilteredArr.count - 1 {
            
            // Clear data when clear history
            searchBar.text = ""
            mallSearchCategoryFilteredArr.removeAll()
            tblCategory.reloadData()
        }else
        {
            // Mall Category Product Open Analytics of Firebase
            Analytics.logEvent("mall_category_product_open", parameters: [:])
            
            let MallAddToCartVC = mainStoryBoard.instantiateViewController(withIdentifier: "MallAddToCartViewController") as! MallAddToCartViewController
            MallAddToCartVC.itemDict = selectedItemDict
            MallAddToCartVC.currencySymbol = currencySymbol
            MallAddToCartVC.delegate = self
            isFromSelect = "isFromSearch"
            self.pushViewController(MallAddToCartVC)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // Create Mall Add to cart delegate to manage Favorite or Unfavorite image in Wishlist View controller
    func backFromMallCart()
    {
        updateMainArray()
    }
    
    func updateMainArray()
    {
        for i in 0..<mallSearchCategoryFilteredArr.count
        {
            let dict0 = mallSearchCategoryFilteredArr[i]
            let id = dict0["id"] as? Int ?? 0
            
            let objTemp = itemArrayGlobal.first { (obj) -> Bool in
                if let dict = obj as? NSDictionary
                {
                    let idTemp = dict["id"] as? Int ?? 0
                    
                    return id == idTemp
                }
                return false
            }
            
            if let objUpdate = objTemp as? NSDictionary
            {
                mallSearchCategoryFilteredArr[i] = objUpdate as! [String:Any]
            }
        }
    }
    
    // MARK: - ********************** TEXTFIELD METHOD **********************

//    @objc func textFieldDidChange(textField: UITextField){
//
//        if textField.text!.count >= 3
//        {
//            Search_Category_API()
//        }
//    }
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        return true
//
//    }
    
    // MARK: - ********************** TOUCHES LIFECYCLE **********************

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.endEditing(true)
    }

    //MARK:- ********************** SEARCH METHODS **********************

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }

    //    //MARK:- ********************** SEARCHBAR METHODS **********************
  
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    
        if searchText.trim() != ""
        {
            if searchText.count >= 3
            {
                print("In")
                
                timer?.invalidate()
                timer = nil
                timer = Timer.scheduledTimer(timeInterval: 1.0,target: self,selector: #selector(self.getHints),userInfo: nil, repeats: false)
                
            }
            else
            {
                print("Out")
            }
        }
        
    }
    
    @objc func getHints(timer: Timer) {

        print("Call Method")
        
        Search_Category_API()

    }
}

