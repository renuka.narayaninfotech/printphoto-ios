//
//  MallHomeSubCategoryVC.swift
//  Print_Photo_App
//
//  Created by Mac Os on 17/08/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import Firebase

class MallHomeSubCategoryVC: BaseViewController, UITableViewDelegate,UITableViewDataSource //,UISearchBarDelegate
{
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
//    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblProduct: UITableView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var selectedArray = [[String:Any]]()
    var filteredSelectedArray = [[String:Any]]()
    var categoryId : Int?
    var strTitle : String?
    
    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()

        SetNavigationBarTitle(Startstring: "\(strTitle ?? "")", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = false
        
        addCartButtonWithBadgeIcon()
        
        self.RemoveLoader()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        searchBar.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {

    }
    
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
        
        addBackButton()
        
        filteredSelectedArray = selectedArray
        
    }
    
    //MARK:- ********************** BUTTON EVENT **********************

    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addCartButtonWithBadgeIcon() {
        
        var badgelabel = UILabel()
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            // badge label
            badgelabel = UILabel(frame: CGRect(x: 20, y: -5, width: 17, height: 17))
            badgelabel.layer.borderColor = UIColor.clear.cgColor
            badgelabel.layer.borderWidth = 2
            badgelabel.layer.cornerRadius = badgelabel.bounds.size.height / 2
            badgelabel.textAlignment = .center
            badgelabel.layer.masksToBounds = true
            badgelabel.font = UIFont(name: CustomFontWeight.medium, size: 10)
            badgelabel.textColor = .white
            badgelabel.backgroundColor = hexStringToUIColor(hex: "0ABAD4")
            
            if (userDefault.integer(forKey: "BadgeNumber")) != 0
            {
                badgelabel.isHidden = false
                badgelabel.text = "\(userDefault.integer(forKey: "BadgeNumber"))"
            }else
            {
                badgelabel.isHidden = true
            }
        }
        
        // Button
        let cartButton = UIButton(type: .custom)
        cartButton.setImage(UIImage(named: "ic_cart"), for: .normal)
        cartButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cartButton.addTarget(self, action: #selector(self.cartAction(_:)), for: .touchUpInside)
        cartButton.addSubview(badgelabel)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cartButton)
        
    }
    
    @objc func cartAction(_ sender: UIButton) {
        
        isCartBtnBack = false
        ((tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
        
        self.tabBarController?.selectedIndex = 2
        
    }
    
    //MARK:- ********************** TABLEVIEW DELEGATE AND DATASOURCE METHOD **********************

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredSelectedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath) as! CategoryTableViewCell
        
        let categoryDict = filteredSelectedArray[indexPath.row] as NSDictionary
        
        let imgUrl = "\(categoryDict["app_image"] as? String ?? "")"
        
        if imgUrl != ""
        {
            cell.imgSubCategory.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: imgUrl)
        }
        else{
            cell.imgSubCategory.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: "tempString")
        }
        
        cell.lblSubCategory.text = "\(categoryDict["name"] as? String ?? "")"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.filteredSelectedArray.count > 0
        {
            let catID = (self.filteredSelectedArray[indexPath.row])["id"] as? Int

            if catID != nil
            {
                let childArray = (self.filteredSelectedArray[indexPath.row])["all_childs"] as? [[String:Any]]
                
                if childArray!.count > 0
                {
                    // One or More than data in Child Array
                    
                    let MallSubCategoryVC = mainStoryBoard.instantiateViewController(withIdentifier: "MallHomeSubCategoryVC") as! MallHomeSubCategoryVC
                    MallSubCategoryVC.selectedArray = childArray!
                    MallSubCategoryVC.strTitle = ((filteredSelectedArray[indexPath.row] as NSDictionary)["name"] as? String)
                    self.pushViewController(MallSubCategoryVC)
                }else
                {
                    
                    // Reset Array and Value in Filter Screen
                    selectedButtonIndexArray.removeAllObjects()
                    expandedSectionHeaderNumbers.removeAllObjects()
                    maxValue = 0.0
                    sliderCurrentMinValue = 0.0
                    sliderCurrentMaxValue = 0.0
                    
                    // Mall Category Open Analytics of Firebase
                    Analytics.logEvent("mall_category_open", parameters: [:])
                    
                    // Child Array is nil
                    
                    let SelectCategoryVC = mainStoryBoard.instantiateViewController(withIdentifier: "CategoryListViewController") as! CategoryListViewController
                    SelectCategoryVC.selectedDict = filteredSelectedArray[indexPath.row] as NSDictionary
                    SelectCategoryVC.categoryId = ((filteredSelectedArray[indexPath.row] as NSDictionary)["id"] as? Int)
                    SelectCategoryVC.strTitle = ((filteredSelectedArray[indexPath.row] as NSDictionary)["name"] as? String)
                    self.pushViewController(SelectCategoryVC)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    

//    //MARK:- ********************** SEARCHBAR METHODS **********************
//
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        searchBar.endEditing(true)
//    }
//
//    //    //MARK:- ********************** SEARCHBAR METHODS **********************
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//
////        let arrMain = selectedArray.sorted {(($0["name"] as? String)!) < ($1["name"] as! String)}
//
//        filteredSelectedArray = searchText.isEmpty ? selectedArray : selectedArray.filter { team in
//            return (team["name"] as! String).lowercased().contains(searchText.lowercased())
//        }
//
//        if (self.filteredSelectedArray.count <= 0)
//        {
//            self.tblProduct.isHidden = true
//            self.nodataLbl.isHidden = false
//        }
//        else
//        {
//            self.tblProduct.isHidden = false
//            self.nodataLbl.isHidden = true
//        }
//
//        self.tblProduct.reloadData()
//
//    }
}
