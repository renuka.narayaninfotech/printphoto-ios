//
//  CategoryListCollectionViewCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 19/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class CategoryListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var itemShadowView: UIView!
    @IBOutlet weak var imageItem: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblOriginalPrice: UILabel!
    @IBOutlet weak var lblDummyPrice: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var iconFavorite: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
