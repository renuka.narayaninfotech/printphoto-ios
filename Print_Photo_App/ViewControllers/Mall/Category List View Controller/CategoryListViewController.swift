//
//  CategoryListViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 19/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import Firebase

// It's used in SearchCategoruViewController,WishlistViewController,MallAddToCartViewController managed category list Array
var itemArrayGlobal = NSMutableArray()

class CategoryListViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource, MallAddCartDelegate, WishListDelegate {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var sortShadowUIView: UIView!
    @IBOutlet weak var filterShadowUIView: UIView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet var sortBlurView: UIView!
    @IBOutlet weak var tblSortList: UITableView!
    @IBOutlet weak var btnSort: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnCloseSort: UIButton!
    @IBOutlet weak var btnClearSort: UIButton!
    @IBOutlet weak var btnApplySort: UIButton!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var categoryId : Int?
    var strTitle : String?
    
    var itemArray = [[String:Any]]()
    
    var iconArray = [UIImage(named: "ic_mall_whats_new"), UIImage(named: "ic_mall_low_to_high"),UIImage(named: "ic_mall_high_to_low")] //,UIImage(named: "ic_mall_popularity"),UIImage(named: "ic_mall_best_seller"),UIImage(named: "ic_mall_rating")
    
    var sortListArray = ["What's New","Price Low To High","Price High To Low"]
    
//    var passAPIArrry : NSMutableArray = ["price_low_high","price_high_low","popularity","whats_new","best_seller","rating"]
    
    var passAPIArrry : NSMutableArray = ["whats_new","price_low_high","price_high_low"]
    
//    var selectedSortName : String?
    var selectedSortIndex = 0
    var selectedSortIndexFinal = 0

    var selectedDict = NSDictionary()
    var isApplySortList = false
    var currencySymbol : String?
    
    //MARK:- ********************** VIEW LIFECYCLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()

        SetNavigationBarTitle(Startstring: "\(strTitle ?? "")", EndString: "")

        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        
        // Set Sorting Data View
        
        appDelegate.window?.addSubview(sortBlurView)
        sortBlurView.frame = UIScreen.main.bounds
        
        sortBlurView.isHidden = true
        sortBlurView.alpha = 0.0

        // Get Notification center for Reload Collectionview from FilterViewController
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("ReloadCategoryCollectionView"), object: nil)
        
        registerCollectionViewCell(cellName: "CategoryListCollectionViewCell", to: categoryCollectionView)
        

    }

    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
        
        addBackButton()
        OpenWishList()

        filterShadowUIView.isHidden = true
        sortShadowUIView.isHidden = true

        // Reset Category array
        filterURL = ""

        sortShadowUIView.addShadow(color: UIColor.darkGray, opacity: 0.5, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        filterShadowUIView.addShadow(color: UIColor.darkGray, opacity: 0.5, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        
        DispatchQueue.main.async {
            self.sortShadowUIView.cornerRadius = self.sortShadowUIView.Getheight/2.0
            self.filterShadowUIView.cornerRadius = self.filterShadowUIView.Getheight/2.0
        }
      
        btnSort.isExclusiveTouch = true
        btnFilter.isExclusiveTouch = true
        btnCloseSort.isExclusiveTouch = true
        btnClearSort.isExclusiveTouch = true
        btnApplySort.isExclusiveTouch = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.Get_Category_API()
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification)
    {
        self.itemArray = filterArray
        self.categoryCollectionView.reloadData()
        self.categoryCollectionView.resetScrollPositionToTop()

    }

    //MARK:- ********************** BUTTON EVENT **********************

    @IBAction func press_btn_Sort(_ sender: UIButton) {
        
        self.sortBlurView.isHidden = false
        self.sortBlurView.alpha = 1.0
        
        let idx = selectedSortIndexFinal
        selectedSortIndex = idx
        
        self.tblSortList.reloadData()
    }
    
    @IBAction func press_btn_close_Sortlist(_ sender: UIButton) {
        
//        if !isApplySortList
//        {
////            selectedSortIndex = 0
//            self.tblSortList.reloadData()
//        }
        
        let idx = selectedSortIndexFinal
        selectedSortIndex = idx
    
        self.sortBlurView.isHidden = true
        self.sortBlurView.alpha = 0.0
     
    }
    
    @IBAction func press_btn_Clear_Sortlist(_ sender: UIButton) {
        
        isApplySortList = false
        selectedSortIndex = 0
        
        selectedSortIndexFinal = 0

        // Default set zero index for sort data
        sortURL = "\(self.passAPIArrry.object(at: 0) as? String ?? "")"
        
        Sort_Data_API(clickButton: "clear")
        self.tblSortList.reloadData()
    }
    
    @IBAction func press_btn_Apply_Sortlist(_ sender: UIButton) {
        
        isApplySortList = true
//        selectedSortIndex = sender.tag
        
        let idx = selectedSortIndex
        selectedSortIndexFinal = idx
        
        sortURL = "\(passAPIArrry.object(at: selectedSortIndex) as? String ?? "")"

        Sort_Data_API(clickButton: "Apply")
    }
  
    @IBAction func press_btn_Filter(_ sender: UIButton) {
        let FilterVC = mainStoryBoard.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        FilterVC.GetFilterDict = selectedDict
        FilterVC.currencySymbol = currencySymbol
        self.pushViewController(FilterVC)
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func OpenWishList() {
        let wishListButton = UIButton()
        wishListButton.setImage(UIImage(named: "ic_mall_header_fav_list"), for: .normal)
        wishListButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        wishListButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0, bottom: 0, right: -10)
        wishListButton.addTarget(self, action: #selector(self.wishListButtonAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: wishListButton)
    }
    
    @objc func wishListButtonAction(_ sender: UIButton) {
        
        if userDefault.value(forKey: "USER_ID") == nil || userDefault.integer(forKey: "USER_ID") == 0
        {
            self.presentAlertWithTitle(title: "Log In", message: "Need to log in first", options: "CANCEL","OK") { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.dismissViewController()
                }else
                {
                    isLoginOrNot = "categoryList_wishlist_icon"
                    
                    let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                    self.pushViewController(signinVC)
                    
                }
            }
        }else
        {
            let WishListCV = self.storyboard?.instantiateViewController(withIdentifier: "WishListViewController") as! WishListViewController
            WishListCV.delegate = self
            isFromSelect = "isFromWishlist"
            self.pushViewController(WishListCV)
        }
        
    }
    
    @objc func pressBtnRadio(_ sender: UIButton?) {
        
        selectedSortIndex = sender!.tag
        
        sortURL = "\(passAPIArrry.object(at: sender!.tag) as? String ?? "")"

        self.tblSortList.reloadData()
    }
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryListCollectionViewCell", for: indexPath) as! CategoryListCollectionViewCell
        
        let itemDict = itemArray[indexPath.row] as [String:Any]
        
        cell.itemShadowView.addShadow(color: UIColor.darkGray, opacity: 0.5, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        cell.lblDescription.text = "\(itemDict["name"] as? String ?? "")"

        let imageArray = itemDict["product_images"] as! [Any]
        if imageArray.count > 0
        {
            let imageDict = imageArray[0] as! [String:Any]
            
            if (imageDict["thumb_image"] as? String ?? "") != ""
            {
                DispatchQueue.global(qos: .background).async {
                    cell.imageItem.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: imageDict["thumb_image"] as? String ?? "")
                }
            }
        }else
        {
            cell.imageItem.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: "tempString")
        }

        cell.lblOriginalPrice.font = cell.lblOriginalPrice.font.withSize(getScreenFontSize(fontSize: 11.5))
        cell.lblDummyPrice.font = cell.lblDummyPrice.font.withSize(getScreenFontSize(fontSize: 11.5))
        cell.lblDiscount.font = cell.lblDiscount.font.withSize(getScreenFontSize(fontSize: 11.5))

        cell.lblOriginalPrice.text = "\(currencySymbol ?? "")\(itemDict["price"] as? String ?? "")"
        
        if itemDict["dummy_price"] as? String ?? "" != "" && itemDict["discount"] as? String ?? "" != ""
        {
            cell.lblDummyPrice.attributedText = "\(currencySymbol ?? "")\(itemDict["dummy_price"] as? String ?? "")".strikeThrough()
            cell.lblDiscount.text = "\(itemDict["discount"] as? String ?? "")"
        }else
        {
            cell.lblDummyPrice.text = ""
            cell.lblDiscount.text = ""
        }
        
        cell.iconFavorite.isHidden = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if DeviceType.IS_IPAD
        {
            if itemArray.count % 2 == 0 && indexPath.row >= (itemArray.count - 3)
            {
                return CGSize(width: collectionView.Getwidth * 0.33, height: collectionView.Getwidth * 0.33 + 90)
            }
            else if indexPath.row == itemArray.count - 1
            {
                return CGSize(width: collectionView.Getwidth * 0.33, height: collectionView.Getwidth * 0.33 + 90)
            }

            return CGSize(width: collectionView.Getwidth * 0.33, height: collectionView.Getwidth * 0.33 + 90)
        }else
        {
            if itemArray.count % 2 == 0 && indexPath.row >= (itemArray.count - 2)
            {
                return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.5 + 60)
            }
            else if indexPath.row == itemArray.count - 1
            {
                return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.5 + 60)
            }

            return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.5 + 60)

        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedItemDict = itemArray[indexPath.row] as [String:Any]
        
        // Mall Category Product Open Analytics of Firebase
        Analytics.logEvent("mall_category_product_open", parameters: [:])

        let MallAddToCartVC = mainStoryBoard.instantiateViewController(withIdentifier: "MallAddToCartViewController") as! MallAddToCartViewController
        MallAddToCartVC.itemDict = selectedItemDict
        MallAddToCartVC.currencySymbol = currencySymbol
        MallAddToCartVC.delegate = self
        isFromSelect = "isFromCategory"
        self.pushViewController(MallAddToCartVC)
    }
    
    // Create Mall Add to cart delegate to manage Favorite or Unfavorite image in Wishlist View controller

    func backFromWishList()
    {
        updateMainArray()
    }
    
    func backFromMallCart()
    {
        updateMainArray()
    }
    
    func updateMainArray()
    {
        for i in 0..<self.itemArray.count
        {
            let dict0 = self.itemArray[i]
            let id = dict0["id"] as? Int ?? 0
            
            let objTemp = itemArrayGlobal.first { (obj) -> Bool in
                if let dict = obj as? NSDictionary
                {
                    let idTemp = dict["id"] as? Int ?? 0
                    
                    return id == idTemp
                }
                return false
            }
            
            if let objUpdate = objTemp as? NSDictionary
            {
                self.itemArray[i] = objUpdate as! [String:Any]
            }
        }
    }
    
    //MARK:- ********************** API CALLING **********************

    func Get_Category_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)

            var userId : Int?
            var strURL : String?
            
            // Get userID when user is login other wise userId is take 0
            if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
            {
                userId = userDefault.value(forKey: "USER_ID") as? Int ?? 0
                strURL = "\(BaseURL)mall_products?category_id=\(categoryId ?? 0)&user_id=\(userId ?? 0)&country_code="
            }else
            {
                userId = 0
                strURL = "\(BaseURL)mall_products?category_id=\(categoryId ?? 0)&user_id=\(userId ?? 0)&country_code=\(userDefault.string(forKey: "RegionCode") ?? "")"
            }
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: strURL ?? "", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.filterShadowUIView.isHidden = false
                self.sortShadowUIView.isHidden = false
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
//                        self.itemArray = responceDict["data"] as! [[String:Any]]
                        
                        itemArrayGlobal = NSMutableArray(array: responceDict["data"] as! NSArray)
                        
                        // First time add Slider max value
                        let arrItem = self.selectedDict.value(forKey: "available_filter") as! NSArray
                        let arr = (arrItem.value(forKey: "value") as! NSArray)[0] as! NSArray
                        sliderCurrentMaxValue = Double(arr[0] as! String)!
                        
                        // Default set zero index for sort data
                        sortURL = "\(self.passAPIArrry.object(at: 0) as? String ?? "")"
                        self.Sort_Data_API(clickButton: "Apply")
                        
//                        self.categoryCollectionView.reloadData()
//                        self.categoryCollectionView.resetScrollPositionToTop()
                        
//                        self.RemoveLoader()
                        
                    }
                    else
                    {
                        self.RemoveLoader()

                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"] ?? "")", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                    

                }
                else
                {
                    self.RemoveLoader()

                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Get_Category_API()
                        }
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()

            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Get_Category_API()
                }
            })
        }
    }
    
    func Sort_Data_API(clickButton:String) {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)

            // Filter is remove
            if filterURL == ""
            {
                let arrItem = self.selectedDict.value(forKey: "available_filter") as! NSArray
                let arr = (arrItem.value(forKey: "value") as! NSArray)[0] as! NSArray
//                sliderCurrentMaxValue = Double(arr[0] as! String)!
            }
            
//            // Set Slider max value when without select slider max value from filter screen
//            let arrItem = selectedDict.value(forKey: "available_filter") as! NSArray
//            let sliderMaxValue = ((arrItem.value(forKey: "value") as! NSArray)[0] as! NSArray)[0]
            
//            let sliderMaxValue = (itemArray[0] as NSDictionary).value(forKey: "price") as? Int ?? 0
            
            var userId : Int?
            // Get userID when user is login other wise userId is take 0
            var strURL : String?
            
            if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
            {
                userId = userDefault.value(forKey: "USER_ID") as? Int ?? 0
                strURL = "\(BaseURL)mall_products?user_id=\(userId ?? 0)&category_id=\(categoryId ?? 0)&max_price=\(sliderCurrentMaxValue)&min_price=\(sliderCurrentMinValue)\(filterURL)&sort=\(sortURL)&country_code="
            }else
            {
                userId = 0
                strURL = "\(BaseURL)mall_products?user_id=\(userId ?? 0)&category_id=\(categoryId ?? 0)&max_price=\(sliderCurrentMaxValue)&min_price=\(sliderCurrentMinValue)\(filterURL)&sort=\(sortURL)&country_code=\(userDefault.string(forKey: "RegionCode") ?? "")"

            }
            
//            let urlString = str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: strURL ?? "", parameters: nil) { (APIResponce, Responce, error1) in
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    self.itemArray.removeAll()
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.currencySymbol = responceDict["currency_symbol"] as? String ?? ""
                        self.itemArray = responceDict["data"] as! [[String:Any]]

                        self.sortBlurView.isHidden = true

                        self.categoryCollectionView.reloadData()
                        self.categoryCollectionView.resetScrollPositionToTop()

                        self.RemoveLoader()

                    }
                    else
                    {
                        self.sortBlurView.isHidden = true
                        self.sortBlurView.alpha = 0.0
                        self.RemoveLoader()
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"] ?? "")", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                
                }
                else
                {
                    self.sortBlurView.isHidden = true
                    self.sortBlurView.alpha = 0.0
                    self.RemoveLoader()
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        self.Sort_Data_API(clickButton:clickButton)
                    })
                    
                }
            }
        }
        else
        {
            self.sortBlurView.isHidden = true
            self.sortBlurView.alpha = 0.0
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                self.Sort_Data_API(clickButton:clickButton)

            })
        }
        
    }
    
    //MARK:- ********************** SORT TABLEVIEW METHOD **********************
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortTableViewCell", for: indexPath) as! SortTableViewCell
        
        cell.iconImage.image = iconArray[indexPath.row]
        cell.lblSort.text = sortListArray[indexPath.row]
        
        // select Radio Button
        cell.btnRadio.tag = indexPath.row
        cell.btnRadio.addTarget(self, action: #selector(self.pressBtnRadio(_:)), for: .touchUpInside)
        
        if selectedSortIndex == indexPath.row
        {
            cell.btnRadio.setImage(UIImage(named: "ic_mall_check"), for: .normal)
        }else
        {
            cell.btnRadio.setImage(UIImage(named: "ic_mall_uncheck"), for: .normal)
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        selectedSortIndex = indexPath.row
        
        for i in 0..<sortListArray.count
        {
            
            let cell = tableView.cellForRow(at: IndexPath(row: i, section: 0)) as! SortTableViewCell
            
            cell.btnRadio.setImage(UIImage(named: "ic_mall_uncheck"), for: .normal)
            
            if i == indexPath.row
            {
                
                cell.btnRadio.setImage(UIImage(named: "ic_mall_check"), for: .normal)
                
//                btnApplySort.tag = indexPath.row
                selectedSortIndex = indexPath.row

            }
            
//            tblSortList.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if DeviceType.IS_IPAD
        {
            return 75.0
        }else
        {
            return 50.0
        }
    }
    
}
