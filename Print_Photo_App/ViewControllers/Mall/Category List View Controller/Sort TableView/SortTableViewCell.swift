//
//  SortTableViewCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 20/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class SortTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var lblSort: UILabel!
    @IBOutlet weak var btnRadio: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
