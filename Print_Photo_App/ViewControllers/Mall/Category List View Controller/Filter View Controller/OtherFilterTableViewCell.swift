//
//  OtherFilterTableViewCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 21/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class OtherFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var btnRadio: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewBottomLine: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
