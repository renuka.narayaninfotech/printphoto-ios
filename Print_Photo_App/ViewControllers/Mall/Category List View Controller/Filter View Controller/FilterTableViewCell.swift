//
//  FilterTableViewCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 20/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import RangeSeekSlider

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var rangeSlider: RangeSeekSlider!
    
    @IBOutlet weak var lblMinPrice: UILabel!
    @IBOutlet weak var lblMaxPrice: UILabel!
    @IBOutlet weak var viewBottomLine: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rangeSlider.minValue = 0.0
        rangeSlider.maxValue = CGFloat(maxValue)
        rangeSlider.selectedMaxValue = CGFloat(sliderCurrentMaxValue)
        rangeSlider.selectedMinValue = CGFloat(sliderCurrentMinValue)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
