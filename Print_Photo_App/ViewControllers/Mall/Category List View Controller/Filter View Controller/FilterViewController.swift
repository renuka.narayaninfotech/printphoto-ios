//
//  FilterViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 20/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import RangeSeekSlider

// Reset value in Filter screen
var maxValue = 0.0
var selectedButtonIndexArray = NSMutableArray()
var sliderCurrentMinValue = 0.0
var sliderCurrentMaxValue = 0.0
var expandedSectionHeaderNumbers = NSMutableArray()

class FilterViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,RangeSeekSliderDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var tblFilter: UITableView!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnApply: UIButton!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var sectionNames = NSArray()
    var sectionItems = NSArray()
    
    let kHeaderSectionTag: Int = 6900;
    let kLineSectionTag: Int = 5900;
    
    var expandedSectionHeader: UITableViewHeaderFooterView!
    
    var GetFilterDict : NSDictionary!
    var categoryId : Int?
    var userId : Int?
    var currencySymbol : String?

    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Filter", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        
        // Get userID when user is login other wise userId is take 0
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            userId = userDefault.value(forKey: "USER_ID") as? Int ?? 0
        }else
        {
            userId = 0
        }
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        addBackButton()
        addCartButtonWithBadgeIcon()
        
        btnApply.isExclusiveTouch = true
        btnClear.isExclusiveTouch = true
        
        // Add index 0 or 1 in selectedButtonIndexArray
        
        categoryId = GetFilterDict.value(forKey: "id") as? Int ?? 0
        
        sectionNames = GetFilterDict.value(forKey: "available_filter") as! NSArray
        
        sectionItems = sectionNames.value(forKeyPath: "value")  as! NSArray
        
        if selectedButtonIndexArray.count == 0
        {
            for i in 0..<sectionItems.count
            {
                let arrMute = NSMutableArray()
                let arrSub = sectionItems[i] as? NSArray ?? NSArray()
                for _ in 0..<arrSub.count
                {
                    arrMute.add(0)
                }
                
                selectedButtonIndexArray.add(arrMute)
            }
        }
        
        if sliderCurrentMaxValue == 0.0
        {
            sliderCurrentMaxValue = (Double((sectionItems.object(at: 0) as! NSArray)[0] as? String ?? "0")!).rounded(.up)
        }
        
        if maxValue == 0.0
        {
            maxValue = (Double((sectionItems.object(at: 0) as! NSArray)[0] as? String ?? "0")!).rounded(.up)
        }
        
        if expandedSectionHeaderNumbers.count == 0
        {
            expandedSectionHeaderNumbers = NSMutableArray.init(array: Array.init(repeating: -1, count: sectionNames.count))
        }
        
        
        self.tblFilter!.tableFooterView = UIView()
        
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    @IBAction func press_btn_Clear(_ sender: UIButton) {
        selectedButtonIndexArray.removeAllObjects()
        expandedSectionHeaderNumbers.removeAllObjects()
//        maxValue = 0.0
        sliderCurrentMinValue = 0.0
        let val = maxValue
        sliderCurrentMaxValue = val
        
        filterURL = ""
        
        var strURL : String?
        
        // Get userID when user is login other wise userId is take 0
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            userId = userDefault.value(forKey: "USER_ID") as? Int ?? 0
            strURL = "\(BaseURL)mall_products?user_id=\(userId ?? 0)&category_id=\(categoryId ?? 0)&sort=\(sortURL)&country_code="
        }else
        {
            userId = 0
            strURL = "\(BaseURL)mall_products?user_id=\(userId ?? 0)&category_id=\(categoryId ?? 0)&sort=\(sortURL)&country_code=\(userDefault.string(forKey: "RegionCode") ?? "")"
        }
        
        Filter_API(strURL: strURL ?? "")
    }
    
    @IBAction func press_btn_Apply(_ sender: UIButton) {
        
        // Manage selected value in perticular section in perticular value
        let arrMain = NSMutableArray()
        
        for i in 0..<selectedButtonIndexArray.count
        {
            let arrSub = selectedButtonIndexArray[i] as! NSArray
            
            if i != 0
            {
                let strName = (sectionNames[i] as! NSDictionary)["name"] as? String ?? ""
                var strURL : String = "Filter_\(strName)" + "="
                
                for j in 0..<arrSub.count
                {
                    if (arrSub[j] as! Int) == 1
                    {
                        let value = (sectionItems[i] as! NSArray)[j] as? String ?? ""
                        
                        strURL = strURL + value + ","
                        
                    }
                }
                
                arrMain.add(strURL)
                
            }
        }
        
        var strMainURL: String = ""
        for i in 0..<arrMain.count
        {
            let arrComponents = (arrMain[i] as? String ?? "").components(separatedBy: "=")
            if arrComponents.count > 1 && arrComponents[1] != ""
            {
                let strUrl = (arrMain[i] as! String).dropLast()
                
                let allowedCharacterSet = (CharacterSet(charactersIn: " & ").inverted)
                let strEncoding = strUrl.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)!
                
                strMainURL += "&" + strEncoding
            }
        }
        
        filterURL = strMainURL
        
        var strURL : String?
        
        // Get userID when user is login other wise userId is take 0
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            userId = userDefault.value(forKey: "USER_ID") as? Int ?? 0
            strURL = "\(BaseURL)mall_products?user_id=\(userId ?? 0)&category_id=\(categoryId ?? 0)&max_price=\(sliderCurrentMaxValue)&min_price=\(sliderCurrentMinValue)\(filterURL)&sort=\(sortURL)&country_code="
        }else
        {
            userId = 0
            strURL = "\(BaseURL)mall_products?user_id=\(userId ?? 0)&category_id=\(categoryId ?? 0)&max_price=\(sliderCurrentMaxValue)&min_price=\(sliderCurrentMinValue)\(filterURL)&sort=\(sortURL)&country_code=\(userDefault.string(forKey: "RegionCode") ?? "")"
        }
        
        Filter_API(strURL: strURL ?? "")
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
        selectedButtonIndexArray.removeAllObjects()
        expandedSectionHeaderNumbers.removeAllObjects()
//        maxValue = 0.0
        sliderCurrentMinValue = 0.0
        let val = maxValue
        sliderCurrentMaxValue = val
        
        filterURL = ""
        
        var strURL : String?
        
        // Get userID when user is login other wise userId is take 0
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            userId = userDefault.value(forKey: "USER_ID") as? Int ?? 0
            strURL = "\(BaseURL)mall_products?user_id=\(userId ?? 0)&category_id=\(categoryId ?? 0)&sort=\(sortURL)&country_code="
        }else
        {
            userId = 0
            strURL = "\(BaseURL)mall_products?user_id=\(userId ?? 0)&category_id=\(categoryId ?? 0)&sort=\(sortURL)&country_code=\(userDefault.string(forKey: "RegionCode") ?? "")"
        }
        
        Filter_API(strURL: strURL ?? "")
        
    }
    
    func addCartButtonWithBadgeIcon() {
        
        var badgelabel = UILabel()
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            // badge label
            badgelabel = UILabel(frame: CGRect(x: 20, y: -5, width: 17, height: 17))
            badgelabel.layer.borderColor = UIColor.clear.cgColor
            badgelabel.layer.borderWidth = 2
            badgelabel.layer.cornerRadius = badgelabel.bounds.size.height / 2
            badgelabel.textAlignment = .center
            badgelabel.layer.masksToBounds = true
            badgelabel.font = UIFont(name: CustomFontWeight.medium, size: 10)
            badgelabel.textColor = .white
            badgelabel.backgroundColor = hexStringToUIColor(hex: "0ABAD4")
            
            if (userDefault.integer(forKey: "BadgeNumber")) != 0
            {
                badgelabel.isHidden = false
                badgelabel.text = "\(userDefault.integer(forKey: "BadgeNumber"))"
            }else
            {
                badgelabel.isHidden = true
            }
        }
        
        // Button
        let cartButton = UIButton(type: .custom)
        cartButton.setImage(UIImage(named: "ic_cart"), for: .normal)
        cartButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cartButton.addTarget(self, action: #selector(self.cartAction(_:)), for: .touchUpInside)
        cartButton.addSubview(badgelabel)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cartButton)
        
    }
    
    @objc func cartAction(_ sender: UIButton) {
        ((tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
        self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 1 : 2
    }
    
    @objc func pressRadioButton(_ sender: UIButton?) {
        
        let cell = sender?.superview?.superview as! OtherFilterTableViewCell
        let indexPath = tblFilter.indexPath(for: cell)
        
        let arrSub = NSMutableArray(array: selectedButtonIndexArray.object(at: indexPath!.section) as! NSArray)
        
        var state = arrSub.object(at: indexPath!.row) as? Int ?? 0
        state = state == 0 ? 1 : 0
        arrSub.replaceObject(at: indexPath!.row, with: state)
        
        selectedButtonIndexArray.replaceObject(at: indexPath!.section, with: arrSub)
        
        tblFilter.reloadData()
        
    }
    
    //MARK:- ********************** TABLEVIEW DELEGATE AND DATASOURCE METHOD **********************
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if sectionNames.count > 0 {
            tableView.backgroundView = nil
            return sectionNames.count
        } else {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = "Retrieving data.\nPlease wait."
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = .center;
            messageLabel.font = UIFont(name: "HelveticaNeue", size: 20.0)!
            messageLabel.sizeToFit()
            self.tblFilter.backgroundView = messageLabel;
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if ((expandedSectionHeaderNumbers.object(at: section) as! Int) == section)
        {
            if section == 0
            {
                // First static Price section
                return 1
            }
            else
            {
                // Other section are Dynamic
                return (sectionItems[section] as! NSArray).count
            }
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (self.sectionNames.count != 0) {
            
            let sectionDict = self.sectionNames[section] as! NSDictionary
            
            return sectionDict.value(forKeyPath: "name") as? String
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if DeviceType.IS_IPAD {
            return 67.0
        }
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
            
            cell.lblMinPrice.text = "Min Price \(currencySymbol ?? "")\(sliderCurrentMinValue)"
            cell.lblMaxPrice.text = "Max Price \(currencySymbol ?? "")\(sliderCurrentMaxValue)"
            
            cell.rangeSlider.delegate = self
            
            // Seprator Line hide or not for only section not all row.
            if ((expandedSectionHeaderNumbers.object(at: indexPath.section) as! Int) == -1) {
                cell.viewBottomLine.isHidden = true
            } else {
                cell.viewBottomLine.isHidden = false
            }
            
            return cell
            
        }else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherFilterTableViewCell", for: indexPath) as! OtherFilterTableViewCell
            
            //            let strName = sectionItems.object(at: indexPath.row)
            
            cell.lblName.text = (self.sectionItems[indexPath.section] as! NSArray)[indexPath.row] as? String ?? ""
            
            // Seprator Line hide or not for only section not all row.
            
            if ((expandedSectionHeaderNumbers.object(at: indexPath.section) as! Int) == -1) {
                cell.viewBottomLine.isHidden = true
            } else {
                cell.viewBottomLine.isHidden = indexPath.row != (self.sectionItems[indexPath.section] as! NSArray).count-1
            }
            
            // Selecte or Unselect radio button
            let state = (selectedButtonIndexArray.object(at: indexPath.section) as! NSArray)[indexPath.row] as? Int ?? 0
            
            let imgRadio = state == 0 ? UIImage(named: "ic_mall_uncheckbox") : UIImage(named: "ic_mall_checkbox")
            cell.btnRadio.setImage(imgRadio, for: .normal)
            
            // For select index
            cell.btnRadio.tag = state
            cell.btnRadio.addTarget(self, action: #selector(self.pressRadioButton(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let arrSub = NSMutableArray(array: selectedButtonIndexArray.object(at: indexPath.section) as! NSArray)
        
        var state = arrSub.object(at: indexPath.row) as? Int ?? 0
        state = state == 0 ? 1 : 0
        arrSub.replaceObject(at: indexPath.row, with: state)
        
        selectedButtonIndexArray.replaceObject(at: indexPath.section, with: arrSub)
        
        tblFilter.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        // Recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
//        header.backgroundView?.backgroundColor = UIColor.white
        
        view.tintColor = .white
        
        // Managed Arrow Animation and Seprator line when didselect any row.
        for vieww in view.subviews
        {
            if vieww is UIButton
            {
                vieww.removeFromSuperview()
            }
            else if vieww.frame.height == 1
            {
                vieww.removeFromSuperview()
            }
        }
        
        //        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
        //            viewWithTag.removeFromSuperview()
        //        }
        //
        //        if let viewWithTag = self.view.viewWithTag(kLineSectionTag + section) {
        //            viewWithTag.removeFromSuperview()
        //        }
        
        let headerFrame = self.view.frame.size
        
        var theImageView = UIButton()
        
        if DeviceType.IS_IPAD {
            header.textLabel?.font = UIFont(name: CustomFontWeight.bold, size: 26.0)
            theImageView = UIButton(frame: CGRect(x: headerFrame.width - 47, y: 21, width: 20, height: 25));
            
        }else
        {
            header.textLabel?.font = UIFont(name: CustomFontWeight.bold, size: 18.0)
            theImageView = UIButton(frame: CGRect(x: headerFrame.width - 32, y: 14, width: 8, height: 15));
        }
        
        theImageView.setImage(UIImage(named: "next"), for: .normal)
        theImageView.contentMode = .scaleAspectFit
        theImageView.tag = kHeaderSectionTag + section
        
        if (expandedSectionHeaderNumbers.object(at: section) as! Int) == section
        {
            // Open section
            theImageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 360.0)
        }
        else
        {
            // Close Section
            theImageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 360.0)
        }
        
        header.addSubview(theImageView)
        
        // Make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(self.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
        
        let viewLine = UIView.init(frame: CGRect.init(x: 0.0, y: header.frame.height-1, width: header.frame.width, height: 1.0))
        viewLine.tag = kLineSectionTag + section
        viewLine.backgroundColor = .lightGray
        header.addSubview(viewLine)
        
        // Seprator Line hide or not for only section not all row.
        
        if ((expandedSectionHeaderNumbers.object(at: section) as! Int) == -1) {
            viewLine.isHidden = false
        } else {
            viewLine.isHidden = true
        }
    }
    
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIButton
        
        if ((expandedSectionHeaderNumbers.object(at: section) as! Int) == -1) {
            
            if let viewWithTag = self.view.viewWithTag(kLineSectionTag + section) {
                viewWithTag.isHidden = true
            }
            expandedSectionHeaderNumbers.replaceObject(at: section, with: section)
            tableViewExpandSection(section, imageView: eImageView!)
            
        } else {
            
            if let viewWithTag = self.view.viewWithTag(kLineSectionTag + section) {
                viewWithTag.isHidden = false
            }
            if ((expandedSectionHeaderNumbers.object(at: section) as! Int) == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            }
            else
            {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + (expandedSectionHeaderNumbers.object(at: section) as! Int)) as? UIButton
                tableViewCollapeSection((expandedSectionHeaderNumbers.object(at: section) as! Int), imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
        
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIButton) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        expandedSectionHeaderNumbers.replaceObject(at: section, with: -1)
        if (sectionData.count == 0 && section != 0) {
            return;
        } else {
            
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 360.0)
            })
            
            var indexesPath = [IndexPath]()
            
            if section == 0
            {
                let index = IndexPath(row: 0, section: section)
                indexesPath.append(index)
            }
            else
            {
                for i in 0 ..< sectionData.count {
                    let index = IndexPath(row: i, section: section)
                    indexesPath.append(index)
                }
            }
            self.tblFilter!.beginUpdates()
            self.tblFilter!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tblFilter!.endUpdates()
            
            // Managed Arrow Animation and Seprator line when didselect any row.
            
            DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
                self.tblFilter.reloadData()
            })
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIButton) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0 && section != 0) {
            expandedSectionHeaderNumbers.replaceObject(at: section, with: -1)
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 360.0)
            })
            var indexesPath = [IndexPath]()
            
            if section == 0
            {
                let index = IndexPath(row: 0, section: section)
                indexesPath.append(index)
            }
            else
            {
                for i in 0 ..< sectionData.count {
                    let index = IndexPath(row: i, section: section)
                    indexesPath.append(index)
                }
            }
            
            expandedSectionHeaderNumbers.replaceObject(at: section, with: section)
            self.tblFilter!.beginUpdates()
            self.tblFilter!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tblFilter!.endUpdates()
            
            // Managed Arrow Animation and Seprator line when didselect any row.
            
            DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
                self.tblFilter.reloadData()
            })
        }
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Filter_API(strURL:String) {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: strURL, parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        filterArray = responceDict["data"] as! [[String:Any]]
                        NotificationCenter.default.post(name: Notification.Name("ReloadCategoryCollectionView"), object: nil)
                        self.popViewController()
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"] ?? "")", message: "", options: "OK", completion: { (ButtonIndex) in
                        })
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                }
            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
        
    }
    
    //MARK:- ********************** RANGE SEEK SLIDER DELEGATE METHOD **********************
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
        sliderCurrentMinValue = (Double.init(minValue)).rounded(.up)
        sliderCurrentMaxValue = (Double.init(maxValue)).rounded(.up)
        
        self.tblFilter.reloadData()
    }
    
}
