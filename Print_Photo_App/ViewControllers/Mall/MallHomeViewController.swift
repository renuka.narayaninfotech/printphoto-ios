//
//  MallHomeViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 18/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import TYCyclePagerView
import Alamofire
import Firebase

class MallHomeViewController: BaseViewController, TYCyclePagerViewDelegate, TYCyclePagerViewDataSource, UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var pagerView: TYCyclePagerView!
    @IBOutlet weak var pagerControl: TYPageControl!
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var heightMallOfferSlider: NSLayoutConstraint!
    @IBOutlet weak var bottomMallOfferSlider: NSLayoutConstraint!
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var categoryArray = [[String:Any]]()
    var pagerViewArray = NSMutableArray()
    var OffersArray = NSMutableArray()
    let reachabilityManager = NetworkReachabilityManager()
    var userId : Int?
    var orderID : String?

    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "New Offers", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        self.PagerView_API()
//    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
        
        self.nodataLbl.isHidden = true
        
//        if userDefault.string(forKey: "RegionCode") == "IN"
//        {
            DispatchQueue.main.async {
                self.PagerView_API()
            }
//        }else
//        {
//            DispatchQueue.main.async {
//                self.Get_Data_API()
//            }
//        }
      
//        asyncAfter(deadline: .now() + 0.05, execute: {
//            self.PagerView_API()
//        })
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = false
        
        addCartButtonWithBadgeIcon()
        
        self.categoryTableView.isHidden = true
        self.pagerView.isHidden = true
        self.pagerControl.isHidden = true
        self.searchBar.isHidden = true
        self.pagerControl.pageIndicatorTintColor = .clear
        self.pagerControl.currentPageIndicatorTintColor = .clear
        
        // managed Mall offer code slider when user is out of india
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            // When user is Login
//            if userDefault.string(forKey: "RegionCode") == "IN"
//            {
                heightMallOfferSlider.constant = DeviceType.IS_IPAD ? 400.0 : 200.0
                bottomMallOfferSlider.constant = DeviceType.IS_IPAD ? 15.0 : 10.0
                pagerControl.isHidden = false
//            }else
//            {
//                heightMallOfferSlider.constant = 0.0
//                bottomMallOfferSlider.constant = 0.0
//                pagerControl.isHidden = true
//            }
        }else
        {
            // When user is Logout
//            if userDefault.string(forKey: "RegionCode") ?? "" == "IN"
//            {
                heightMallOfferSlider.constant = DeviceType.IS_IPAD ? 400.0 : 200.0
                bottomMallOfferSlider.constant = DeviceType.IS_IPAD ? 15.0 : 10.0
                pagerControl.isHidden = false
//            }else
//            {
//                heightMallOfferSlider.constant = 0.0
//                bottomMallOfferSlider.constant = 0.0
//                pagerControl.isHidden = true
//            }
        }
        
        //        self.RemoveLoader()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        reachabilityManager?.stopListening()
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        addBackButton()
        
        self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
        
        self.categoryTableView.isHidden = true
        self.pagerView.isHidden = true
        self.pagerControl.isHidden = true
        self.searchBar.isHidden = true
        
        checkRechabilityStatus()
        
    }
    
    //MARK:- ********************** CHECK RECHABILITY STATUS **********************
    
    func checkRechabilityStatus()
    {
        reachabilityManager?.startListening(onUpdatePerforming: { _ in
            if let isNetworkReachable = self.reachabilityManager?.isReachable,
                isNetworkReachable == true {
                //Internet Available
                print("Internet Available")
                
                self.dismissViewController()

                DispatchQueue.main.asyncAfter(deadline: .now() + 0.05, execute: {
                    self.PagerView_API()
                })
                
            } else {
                //Internet Not Available"
                print("Internet Not Available")
                
                self.RemoveLoader()
                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05, execute: {
                        self.PagerView_API()
                    })
                    
                })
                
            }
        })
    }
    
    //MARK:- ********************** PAGER CONTROL **********************
    
    func Pager_Control() {
        
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.register(TYCyclePagerViewCell.classForCoder(), forCellWithReuseIdentifier: "cellId")
        pagerView.cornerRadius = 10.0
        pagerView.isInfiniteLoop = true
        pagerView.autoScrollInterval = 2.0
        pagerControl.currentPageIndicatorSize = CGSize(width: 8, height: 8)
        pagerControl.pageIndicatorTintColor = .gray
        pagerControl.currentPageIndicatorTintColor = UIColor(red:11/255, green:175/255, blue:205/255, alpha: 1)
        
    }
    
    func loadData() {
        
        var i = 0
        while i < pagerViewArray.count {
            i += 1
        }
        pagerControl.numberOfPages = pagerViewArray.count
        pagerView.reloadData()
    }
    
    //MARK:- ********************** COPY TEXT FROM CLIPBOARD **********************
    
    private func copyToClipBoard(textToCopy: String) {
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = textToCopy
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        ((tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)
        self.tabBarController?.selectedIndex = 0
    }
    
    @objc func pressButtonTermsOfUse(_ sender: UIButton?) {
        
        let dict = OffersArray.object(at: (sender?.tag)!) as! [String:Any]
        
        self.presentAlertWithTitle(title: "Terms of Use", message: "\((dict["terms_condition"]) ?? "")", options: "OK", completion: { (ButtonIndex) in
        })
    }
    
    
    func addCartButtonWithBadgeIcon() {
        
        var badgelabel = UILabel()
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            // badge label
            badgelabel = UILabel(frame: CGRect(x: 20, y: -5, width: 17, height: 17))
            badgelabel.layer.borderColor = UIColor.clear.cgColor
            badgelabel.layer.borderWidth = 2
            badgelabel.layer.cornerRadius = badgelabel.bounds.size.height / 2
            badgelabel.textAlignment = .center
            badgelabel.layer.masksToBounds = true
            badgelabel.font = UIFont(name: CustomFontWeight.medium, size: 10)
            badgelabel.textColor = .white
            badgelabel.backgroundColor = hexStringToUIColor(hex: "0ABAD4")
            
            if (userDefault.integer(forKey: "BadgeNumber")) != 0
            {
                badgelabel.isHidden = false
                badgelabel.text = "\(userDefault.integer(forKey: "BadgeNumber"))"
            }else
            {
                badgelabel.isHidden = true
            }
        }
        
        // Button
        let cartButton = UIButton(type: .custom)
        cartButton.setImage(UIImage(named: "ic_cart"), for: .normal)
        cartButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cartButton.addTarget(self, action: #selector(self.cartAction(_:)), for: .touchUpInside)
        cartButton.addSubview(badgelabel)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cartButton)
        
    }
    
    @objc func cartAction(_ sender: UIButton) {
        
        isCartBtnBack = false
        ((tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
        
        self.tabBarController?.selectedIndex = 2
        
    }
    
    //MARK:- ********************** TYCyclePagerView Datasource Method **********************
    
    func numberOfItems(in pageView: TYCyclePagerView) -> Int {
        return pagerViewArray.count
    }
    
    func pagerView(_ pagerView: TYCyclePagerView, cellForItemAt index: Int) -> UICollectionViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cellId", for: index) as! TYCyclePagerViewCell
        
        cell.imageView.contentMode = .scaleToFill
        
        if pagerViewArray[index] as? UIImage != nil
        {
            cell.imageView.image = pagerViewArray[index] as? UIImage
        }else{
            // Temp string is a setlement
            cell.imageView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: "tempString")
        }
        
        return cell
    }
    
    func layout(for pageView: TYCyclePagerView) -> TYCyclePagerViewLayout {
        let layout = TYCyclePagerViewLayout()
        layout.itemSize = CGSize(width: pagerView.frame.width, height: pagerView.frame.height)
        layout.itemSpacing = 0
        layout.itemHorizontalCenter = true
        return layout
    }
    
    func pagerView(_ pageView: TYCyclePagerView, didScrollFrom fromIndex: Int, to toIndex: Int) {
        pagerControl.currentPage = toIndex;
    }
    
    //MARK:- ********************** TABLEVIEW DELEGATE AND DATASOURCE METHOD **********************
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return  categoryArray.count > 0 ? categoryArray.count : OffersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if categoryArray.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath) as! CategoryTableViewCell
            
            let categoryDict = self.categoryArray[indexPath.row] as NSDictionary
            
            let imgUrl = "\(categoryDict["app_image"] as? String ?? "")"
            
            if imgUrl != ""
            {
                cell.imageCategory.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: imgUrl)
            }
            else{
                cell.imageCategory.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: "tempString")
            }
            
            cell.lblCategory.text = "\(categoryDict["name"] as? String ?? "")"
            cell.lblDescription.text = "\(categoryDict["description"] as? String ?? "")"
            
            return cell
        } else if (OffersArray.count > 0) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "OffersVCTableViewCell") as! OffersVCTableViewCell
            cell.contentsView.addShadow(color: UIColor.gray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
            
            let dict = OffersArray.object(at: indexPath.row) as! [String:Any]
            
            if dict["n_offer_new_image"] as! String != ""
            {
                // Managed image size in iPad
                if DeviceType.IS_IPAD
                {
                    if imgTemp != nil
                    {
                        cell.imageviewWidthRatio.constant = 226.0 * (((imgTemp?.size.width)!)/(imgTemp?.size.height)!)
                        cell.offersImage.sd_setImage(with: URL(string: (dict["n_offer_new_image"] as! String)), placeholderImage: nil, options: .refreshCached){ (img, error, catchType, url) in
                            cell.offersImage.sd_removeActivityIndicator()
                        }
                    }
                    else
                    {
                        cell.offersImage.sd_setShowActivityIndicatorView(true)
                        cell.offersImage.sd_setIndicatorStyle(.gray)
                        
                    }
                    
                }else
                {
                    cell.offersImage.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: (dict["n_offer_new_image"] as! String))
                }
            }
            
            cell.lblOffers.text = "\(dict["description"] ?? "")"
        
            cell.lblCode.text = "\(dict["display_message"] ?? "")"
            
            cell.lblExpiredDate.text = "\(dict["expiry_text"] ?? "")"
            cell.btnTermsOfUse.tag = indexPath.row
            cell.btnTermsOfUse.addTarget(self, action: #selector(self.pressButtonTermsOfUse(_:)), for: .touchUpInside)
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.categoryArray.count > 0
        {
            let catID = (self.categoryArray[indexPath.row])["id"] as? Int
            
            if catID != nil
            {
                
                let childArray = (self.categoryArray[indexPath.row])["all_childs"] as? [[String:Any]]
                
                if childArray!.count > 0
                {
                    // One or More than data in Child Array
                    
                    let MallSubCategoryVC = mainStoryBoard.instantiateViewController(withIdentifier: "MallHomeSubCategoryVC") as! MallHomeSubCategoryVC
                    MallSubCategoryVC.selectedArray = childArray!
                    MallSubCategoryVC.strTitle = ((categoryArray[indexPath.row] as NSDictionary)["name"] as? String)
                    self.pushViewController(MallSubCategoryVC)
                }else
                {
                    
                    // Reset Array and Value in Filter Screen
                    selectedButtonIndexArray.removeAllObjects()
                    expandedSectionHeaderNumbers.removeAllObjects()
                    maxValue = 0.0
                    sliderCurrentMinValue = 0.0
                    sliderCurrentMaxValue = 0.0
                    
                    // Mall Category Open Analytics of Firebase
                    Analytics.logEvent("mall_category_open", parameters: [:])
                    
                    // Child Array is nil
                    
                    let SelectCategoryVC = mainStoryBoard.instantiateViewController(withIdentifier: "CategoryListViewController") as! CategoryListViewController
                    SelectCategoryVC.selectedDict = categoryArray[indexPath.row] as NSDictionary
                    SelectCategoryVC.categoryId = ((categoryArray[indexPath.row] as NSDictionary)["id"] as? Int)
                    SelectCategoryVC.strTitle = ((categoryArray[indexPath.row] as NSDictionary)["name"] as? String)
                    self.pushViewController(SelectCategoryVC)
                }
            }
        } else if self.OffersArray.count > 0 {
            let dict = OffersArray.object(at: indexPath.row) as! [String:Any]
            copyToClipBoard(textToCopy: "\(dict["offer_code"] as? String ?? "")")
            print(dict["offer_code"] as? String ?? "")
            let offerCode = "\(dict["offer_code"] as? String ?? "")"
            //        let offerCode_amount = "\(dict["amount"] ?? "")"
            
            if mall_enable_status ==  1 //isPromocode //update condition
            {
                Check_Validation(code: offerCode)
                
            }else
            {
                self.view.makeToast("Offer Code Copied", duration: 2.0, position: .center)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        print("content size = \(tableView.contentSize.height)")
        
        DispatchQueue.main.async {
            self.heightTableView.constant = tableView.contentSize.height
            self.categoryTableView.layoutIfNeeded()
        }
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func PagerView_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            // Managed Banner of Offer in India and Other Country
            let isInternational = userDefault.string(forKey: "RegionCode") == "IN" ? "0" : "1"
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)get_offers?for_mall=1&is_international=\(isInternational)", parameters: nil) { (APIResponce, Responce, error1) in
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.pagerViewArray.removeAllObjects()
                        let sliderImageArray = responceDict["offer"] as! NSArray
                        var imgTemp = UIImage()
                        
                        for i in 0..<sliderImageArray.count
                        {
                            if let strImg = (sliderImageArray[i] as! [String:Any])["n_offer_new_image"] as? String, strImg != ""
                            {
                                let url = URL.init(string: strImg)
                                let data = try? Data(contentsOf: url!)
                                
                                if data != nil
                                {
                                    self.dismissViewController()
                                    imgTemp = UIImage.init(data: data!)!
                                    self.pagerViewArray.add(imgTemp)
                                }else
                                {
                                    print("error")
                                }
                            }
                        }
                        
                        self.Pager_Control()
                        self.loadData()
                        
                        self.Get_Data_API()
                        
//                         self.RemoveLoader()
                    }else{
                         self.RemoveLoader()
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                    
                }
                else
                {
                    self.RemoveLoader()
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.PagerView_API()
                        }
                    })
                    
                }
            }
            
        }
        else
        {
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.PagerView_API()
                }
            })
        }
        
    }
    
    func Get_Data_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            //self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let strURL : String?
            
            if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
            {
                userId = userDefault.value(forKey: "USER_ID") as? Int ?? 0
                strURL = "\(BaseURL)mall_categories?user_id=\(userId ?? 0)&country_code="
            }else
            {
                userId = 0
                strURL = "\(BaseURL)mall_categories?user_id=\(userId ?? 0)&country_code=\(userDefault.string(forKey: "RegionCode") ?? "")"
            }
        
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: strURL ?? "", parameters: nil) { (APIResponce, Responce, error1) in
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.categoryArray.removeAll()
                        
                        self.categoryArray = responceDict["all_childs"] as! [[String : Any]]
                        
                        self.categoryTableView.reloadData()
                        
//                        if userDefault.string(forKey: "RegionCode") == "IN"
//                        {
                            self.heightMallOfferSlider.constant = DeviceType.IS_IPAD ? 400.0 : 200.0
                            self.bottomMallOfferSlider.constant = DeviceType.IS_IPAD ? 15.0 : 10.0
                            self.pagerControl.isHidden = false
                            self.pagerView.isHidden = false
//                        }else
//                        {
//                            self.heightMallOfferSlider.constant = 0.0
//                            self.bottomMallOfferSlider.constant = 0.0
//                            self.pagerControl.isHidden = true
//                            self.pagerView.isHidden = true
//                        }
                        
                        if self.categoryArray.count == 0 && self.pagerView.isHidden == true
                        {
                            self.categoryTableView.isHidden = true
                            self.searchBar.isHidden = true
                            self.nodataLbl.isHidden = false
                            self.nodataLbl.text = "No product available"
                        } else if self.categoryArray.count == 0 {
                            self.get_offer_Api()
                            self.categoryTableView.isHidden = false
                            self.searchBar.isHidden = false
                            self.nodataLbl.isHidden = true
                        } else
                        {
                            self.categoryTableView.isHidden = false
                            self.searchBar.isHidden = false
                            self.nodataLbl.isHidden = true
                        }
                        
                        
//                        self.categoryTableView.isHidden = false
//                        self.pagerView.isHidden = false
//                        self.pagerControl.isHidden = false
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            self.RemoveLoader()
                        })
                        
                    }else{
                        
                        self.RemoveLoader()
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                    
                }
                else
                {
                    self.RemoveLoader()
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.PagerView_API()
                        }
                    })
                    
                }
            }
            
        }
        else
        {
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.PagerView_API()
                }
            })
        }
        
    }
    
    func get_offer_Api() {
        
        if Reachability.isConnectedToNetwork() {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            // Managed Banner of Offer in India and Other Country
            var isInternational = userDefault.string(forKey: "RegionCode") == "IN" ? "0" : "1"
            
            if(userDefault.string(forKey: "RegionCode") == "SA")
            {
                isInternational = "1&country_code=SA"
            }
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)get_offers?for_mall=&is_international=\(isInternational)", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.categoryTableView.isHidden = false

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.OffersArray.removeAllObjects()
                        
                        self.OffersArray.addObjects(from: responceDict["offer"] as! [Any])
                        
                        if let strImg = (self.OffersArray.object(at: 0) as! [String:Any])["n_offer_new_image"] as? String
                        {
                            let url = URL.init(string: strImg)
                            let data = try? Data(contentsOf: url!)
                            
                            if Reachability.isConnectedToNetwork() {
                                
                                if data != nil || imgTemp != nil
                                {
                                    imgTemp = UIImage.init(data: data!)
                                }else
                                {
                                    self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                                        if ButtonIndex == 0
                                        {
                                            self.get_offer_Api()
                                        }
                                    })
                                }
                                
                            }
                            else
                            {
                                self.RemoveLoader()
                                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                                    if ButtonIndex == 0
                                    {
                                        self.get_offer_Api()
                                    }
                                })
                            }
                            
                        }
                        
                        self.categoryTableView.reloadData()
                        
                        self.RemoveLoader()
                        
                    }else{
                        self.RemoveLoader()
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                    
                }
                else
                {
                    self.RemoveLoader()
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.get_offer_Api()
                        }
                    })
                    
                }
            }
            
        }
        else
        {
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.get_offer_Api()
                }
            })
        }
        
    }
    
    func Check_Validation(code : String) {
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let ParamDict = ["order_id" : orderID ?? "",
                             "discount_code" : code,
                             "gift" : isGift] as NSDictionary
            
            //            ParamDict = ["user_id": userID,
            //                             "code" : code,
            //                             "status" : 1 ] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)order/apply_discount", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        // Send to PlaceOrder Screen
                        
                        let dict = responceDict["data"] as! [String:Any]
                        
                        if let isApplied = dict["discount_applied"] as? Int, isApplied > 0
                        {
                            let discount = "\(dict["discount_amount"] ?? "")"
                            
                            isBackFromOffer = false
                            
                            userDefault.set(code, forKey: "OfferCode")
                            userDefault.set(discount, forKey: "OfferCodeAmount")
                            
                            userDefault.set(dict, forKey: "OfferCodeDict")
                            self.popViewController()
                            
                        }else
                        {
                            let msg = dict["discount_message"] as? String ?? ""
                            
                            self.presentAlertWithTitle(title: msg, message: "", options: "OK", completion: { (ButtonIndex) in
                            })
                        }
                        
                    }else
                    {
                        self.view.makeToast("\(responceDict["ResponseMessage"] ?? "")", duration: 2.0, position: .bottom)
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    
    //MARK:- ********************** SEARCHBAR METHODS **********************
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchBar.isUserInteractionEnabled = false
        
        searchBar.resignFirstResponder()
        
        let SearchCategoryVC = mainStoryBoard.instantiateViewController(withIdentifier: "SearchCategoryViewController") as! SearchCategoryViewController
        self.pushViewController(SearchCategoryVC)
        
        searchBar.isUserInteractionEnabled = true
        
    }
    
}
