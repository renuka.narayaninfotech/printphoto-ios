//
//  TYCyclePagerViewCell.swift
//  TYCyclePagerViewDemo_swift
//
//  Created by tany on 2017/7/20.
//  Copyright © 2017年 tany. All rights reserved.
//

import UIKit

class TYCyclePagerViewCell: UICollectionViewCell {
    
    lazy var imageView = UIImageView()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addImage()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addImage()
    }

    func addImage() {
        self.addSubview(self.imageView)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.imageView.frame = self.bounds
    }
}
