//
//  WishListViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 22/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

// Create delegate method push from CategoryListViewController for Favorite or Unfavorite image
protocol WishListDelegate : class {
    func backFromWishList()
}

class WishListViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var wishListCollectionView: UICollectionView!

    //MARK:- ********************** VARIABLE DECLARATION **********************

    var itemArray = [[String:Any]]()

//    var iconArray = [UIImage(named: "ic_mall_low_to_high"),UIImage(named: "ic_mall_high_to_low"),UIImage(named: "ic_mall_popularity"),UIImage(named: "ic_mall_whats_new"),UIImage(named: "ic_mall_best_seller"),UIImage(named: "ic_mall_rating")]
    
//    var sortListArray = ["Price Low To High","Price High To Low","Popularity","What's New","Best Seller","Rating"]
    
    weak var delegate: WishListDelegate?

    var currencySymbol : String?
    
    //MARK:- ********************** VIEW LIFECYCLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        
        registerCollectionViewCell(cellName: "CategoryListCollectionViewCell", to: wishListCollectionView)
        
        Get_Wishlist_API()

    }

    func Initialize() {
        
        addBackButton()
        removeBackButton()
        
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
        self.delegate?.backFromWishList()
        self.popViewController()

    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    @objc func press_btn_favorite(_ sender:UIButton)
    {
//        let obj = itemArray.first { (obj) -> Bool in
//            return (obj["id"] as! Int) == sender.tag
//        }
//
//        if let objDict = obj
//        {
//            var objUpdate = objDict
//            let idx = itemArrayGlobal.index(of: objDict as NSDictionary)
//            objUpdate["in_wishlist"] = false
//            itemArrayGlobal.replaceObject(at: idx, with: objUpdate as NSDictionary)
//        }

        Remove_To_Favorite(selectedProductID: sender.tag)
    }
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryListCollectionViewCell", for: indexPath) as! CategoryListCollectionViewCell
     
        let itemDict = itemArray[indexPath.row] as [String:Any]

        cell.iconFavorite.isHidden = true
        cell.itemShadowView.addShadow(color: UIColor.darkGray, opacity: 0.5, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        cell.lblDescription.text = "\(itemDict["name"] as? String ?? "")"
        
        let imageArray = itemDict["product_images"] as! [Any]
        
        if imageArray.count > 0
        {
            let imageDict = imageArray[0] as! [String:Any]
            cell.imageItem.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: imageDict["thumb_image"] as? String ?? "")
        }
        
        cell.lblOriginalPrice.font = cell.lblOriginalPrice.font.withSize(getScreenFontSize(fontSize: 11.5))
        cell.lblDummyPrice.font = cell.lblDummyPrice.font.withSize(getScreenFontSize(fontSize: 11.5))
        cell.lblDiscount.font = cell.lblDiscount.font.withSize(getScreenFontSize(fontSize: 11.5))
        
        cell.lblOriginalPrice.text = "\(self.currencySymbol ?? "")\(itemDict["price"] as? String ?? "")"
        
        if itemDict["dummy_price"] as? String ?? "" != "" || itemDict["discount"] as? String ?? "" != ""
        {
            cell.lblDummyPrice.attributedText = "\(self.currencySymbol ?? "")\(itemDict["dummy_price"] as? String ?? "")".strikeThrough()
            cell.lblDiscount.text = "\(itemDict["discount"] as? String ?? "")"
        }
    
        cell.iconFavorite.tag = itemDict["id"] as? Int ?? 0
        cell.iconFavorite.addTarget(self, action: #selector(press_btn_favorite(_:)), for: .touchDown)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedItemDict = itemArray[indexPath.row] as [String:Any]

        let MallAddToCartVC = mainStoryBoard.instantiateViewController(withIdentifier: "MallAddToCartViewController") as! MallAddToCartViewController
        MallAddToCartVC.itemDict = selectedItemDict
        MallAddToCartVC.currencySymbol = currencySymbol
        self.pushViewController(MallAddToCartVC)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if DeviceType.IS_IPAD
        {
            if itemArray.count % 2 == 0 && indexPath.row >= (itemArray.count - 3)
            {
                return CGSize(width: collectionView.Getwidth * 0.33, height: collectionView.Getwidth * 0.33 + 90)
            }
            else if indexPath.row == itemArray.count - 1
            {
                return CGSize(width: collectionView.Getwidth * 0.33, height: collectionView.Getwidth * 0.33 + 90)
            }

            return CGSize(width: collectionView.Getwidth * 0.33, height: collectionView.Getwidth * 0.33 + 90)
        }else
        {
            if itemArray.count % 2 == 0 && indexPath.row >= (itemArray.count - 2)
            {
                return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.5 + 60)
            }
            else if indexPath.row == itemArray.count - 1
            {
                return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.5 + 60)
            }
        
            return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.5 + 60)
            
        }
        
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Get_Wishlist_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)mall_products?user_id=\(userDefault.value(forKey: "USER_ID") ?? "")&wishlist=1", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    self.view.endEditing(true)
                    self.wishListCollectionView.isHidden = false
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        self.currencySymbol = responceDict["currency_symbol"] as? String ?? ""
                        
                        self.itemArray.removeAll()
                        self.itemArray = responceDict["data"] as! [[String:Any]]

                        if self.itemArray.count > 0
                        {
                            self.SetNavigationBarTitle(Startstring: "Wishlist(\(self.itemArray.count))", EndString: "")

                            self.wishListCollectionView.isHidden = false
                            self.nodataLbl.isHidden = true
                            
                            self.wishListCollectionView.reloadData()
                        }else
                        {
                            self.SetNavigationBarTitle(Startstring: "Wishlist(\(0))", EndString: "")

                            self.wishListCollectionView.isHidden = true
                            self.nodataLbl.isHidden = false
                            self.nodataLbl.text = "No product available"
                        }
                        
                    }else{
                        
                        self.SetNavigationBarTitle(Startstring: "Wishlist(\(0))", EndString: "")
                        
                        self.wishListCollectionView.isHidden = true
                        self.nodataLbl.isHidden = false
                        self.nodataLbl.text = "No product available"
                        
                        self.topMostViewController().view.makeToast("\(responceDict["ResponseMessage"]!)", duration: 2.0, position: .bottom)
                   
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        self.Get_Wishlist_API()
                    })
                    
                }
            }
            
        }
        else
        {
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                self.Get_Wishlist_API()
            })
        }
    }
    
    func Remove_To_Favorite(selectedProductID:Int) {
        
        if Reachability.isConnectedToNetwork() {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)add_wishlist?product_id=\(selectedProductID)&user_id=\(userDefault.value(forKey: "USER_ID") ?? "")", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                   
                    self.Get_Wishlist_API()
                    
                    self.wishListCollectionView.reloadData()
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })
                    
                }
            }
            
        }
        else
        {
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                
            })
        }
        
    }

}
