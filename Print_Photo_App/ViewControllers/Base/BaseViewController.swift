//
//  AMBaseViewController.swift
//  Amzan
//
//  Created by AlphaVed Mac on 12/09/18.
//  Copyright © 2018 AlphaVed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import AVKit
import AVFoundation
import IQKeyboardManagerSwift

class BaseViewController: UIViewController, NVActivityIndicatorViewable {
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var nodataLbl : UILabel!
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Base_Initialize()
    }
    
    func Base_Initialize() {
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        var preferredStatusBarStyle : UIStatusBarStyle {
            return .lightContent
        }
        nodataLbl = ConfigureNodataLbl(viewController: self)
        self.view.addSubview(nodataLbl)
        
        
        // Set bottom shadow of navigation bar
        self.navigationController?.navigationBar.addShadow(color: UIColor.lightGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 3, scale: true)
        
    }
    
    func SetNavigationBarTitle(Startstring : String , EndString : String) {
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 88.0, height: 44))
        
        var txtSize = Int()
        
        if DeviceType.IS_IPAD
        {
            txtSize = 24
        } else {
            txtSize = 16
        }
        
        let Starting = [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: CGFloat(txtSize), weight: .heavy),
                         NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "129FBA")]
        let Ending = [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: CGFloat(txtSize), weight: .heavy),
                       NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "53D45C")]
        
        let myString = NSMutableAttributedString(string: Startstring, attributes: Starting)
        myString.append(NSMutableAttributedString(string: EndString, attributes: Ending))
        label.numberOfLines = 0
        label.attributedText = myString
        label.textAlignment = .center
        label.center = self.view.center
        
        self.navigationItem.titleView = label
        
    }
    
    func showLoaderwithMessage(message : String ,type : NVActivityIndicatorType ,color : UIColor) {
        
        startAnimating(CGSize(width: 60, height:60),
                       message: message,
                       messageFont: UIFont.boldSystemFont(ofSize: 12),
                       type: type,
                       color: UIColor.white,
                       padding: 10,
                       displayTimeThreshold: nil,
                       minimumDisplayTime: nil,
                       backgroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.6),
                       textColor: UIColor.white)
    }
    
    func removeLoaderWithMessage(message : String)  {
        
        NVActivityIndicatorPresenter.sharedInstance.setMessage(message)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.stopAnimating()
        }
    }
    
    func RemoveLoader() {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.stopAnimating()
        }
        
    }
    
    //MARK:- Memory Warning
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Navigation Controller Animation based Transition
    
    func pushViewController(_ viewController: UIViewController) {
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func presentViewController(_ viewController: UIViewController) {
        
        self.navigationController?.present(viewController, animated: true, completion: nil)
    }
    
    func popViewController() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func dismissViewController() {
        
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************
    
    @objc func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    //MARK:- **********************  REGISTER XIB **********************
    
    //Register TableViewCell
    func registerTableViewCell(cellName : String , to tableView: UITableView) {
        
        let cellNIB = UINib(nibName: cellName, bundle: nil)
        tableView.register(cellNIB, forCellReuseIdentifier: cellName)
    }
    
    //Register CollectionViewCell
    func registerCollectionViewCell(cellName: String , to collectionView: UICollectionView)  {
        
        let cellNIB = UINib(nibName: cellName, bundle: nil)
        collectionView.register(cellNIB, forCellWithReuseIdentifier: cellName)
    }
    
    // How to used app (Help video)
    
    func Help_Video() {
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
        //        let videoURL = URL(string: "https://printphoto.in/Photo_case/public/tutorial.mp4")
        //        let player = AVPlayer(url: videoURL!)
        //        let playerLayer = AVPlayerLayer(player: player)
        //        playerLayer.frame = self.view.bounds
        //        self.view.layer.addSublayer(playerLayer)
        //        player.play()
    }
    
    
    //********************** CHANGE DATE FORMATE **********************
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        return  dateFormatter.string(from: date!)
    }
    
    //********************** Get font size as per Device Width **********************
    
    func getScreenFontSize(fontSize: CGFloat) -> CGFloat {
        
        let currentFontSize = DeviceType.IS_IPAD ? UIScreen.main.bounds.width/640 * fontSize : UIScreen.main.bounds.width/320 * fontSize
        
        return currentFontSize
    }
    
}
