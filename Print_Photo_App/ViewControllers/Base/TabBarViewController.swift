//
//  AMTabBarViewController.swift
//  Amzan
//
//  Created by AlphaVed Mac on 19/09/18.
//  Copyright © 2018 AlphaVed. All rights reserved.
//

import UIKit
import Firebase

class TabBarViewController: UITabBarController , UITabBarControllerDelegate{
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    @IBInspectable var defaultIndex: Int = 0
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
//        if(userDefault.string(forKey: "RegionCode") == "SA" && self.viewControllers!.count == 5)
        if (self.viewControllers!.count == 5)
        {
//            let indexToRemove = 1
//            if indexToRemove < self.viewControllers!.count {
//                var viewControllers = self.viewControllers
//                viewControllers?.remove(at: indexToRemove)
//                self.viewControllers = viewControllers
//            }
        }
        
        self.delegate = self
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            // Set Badge Icon for Cart
            appDelegate.SetBadgeIcon()
        }
        
        // Set Tabbar title color and size
        let fontSize = DeviceType.IS_IPAD ? 15.0 : 11.0
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: CustomFontWeight.medium, size: CGFloat(fontSize))!, NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "545454")], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font:  UIFont(name: CustomFontWeight.medium, size: CGFloat(fontSize))!, NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "08a8c0")], for: .selected)
     
        self.tabBar.unselectedItemTintColor = hexStringToUIColor(hex: "545454")

        // Set Tabbar top border color
        tabBar.addTopBorderWithColor(color: UIColor.black, width: 0.5)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedLocalNotification(notification:)), name: Notification.Name("LocalNotification"), object: nil)
        
        if isFromLocalKilled
        {
            self.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 1 : 2
        }
        
        if isFromPushKilled
        {
            self.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 0 : 1
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification)
    {              
        self.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 0 : 1
    }
    
    //    @objc func methodOfReceivedLocalNotification(notification: Notification)
    //    {
    //        self.selectedIndex = 3
    //    }
    
    // Set Tabbar height in ipad
    override func viewWillLayoutSubviews() {
        
        //        if DeviceType.IS_IPAD
        //        {
        //            var tabFrame = self.tabBar.frame
        //            // - 40 is editable , the default value is 49 px, below lowers the tabbar and above increases the tab bar size
        //            tabFrame.size.height = 64
        //            tabFrame.origin.y = self.view.frame.size.height - 64
        //            self.tabBar.frame = tabFrame
        //            self.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: -5, right: 0)
        
        //        }else
        //        {
        //            self.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        
        //        }
        
    }
    
    //    func repositionBadge(tabIndex: Int){
    //
    //        DispatchQueue.main.async {
    //            for badgeView in self.tabBar.subviews[tabIndex].subviews {
    //
    //                print(NSStringFromClass(badgeView.classForCoder))
    //                if NSStringFromClass(badgeView.classForCoder) == "UITabBarButtonLabel" {
    //                    badgeView.layer.transform = CATransform3DIdentity
    //                    badgeView.layer.transform = CATransform3DMakeTranslation(-17, 1.0, 1.0)
    //                }
    //            }
    //        }
    //
    //    }
    
    //MARK:- ********************** TAB BAR METHOD **********************
    
    
    //    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    //
    //        if item.tag == 4 {
    //            if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
    //            {
    //                let myAccountVC = mainStoryBoard.instantiateViewController(withIdentifier: "MyAccountViewController") as! MyAccountViewController
    //                self.navigationController?.pushViewController(myAccountVC, animated: false)
    //            }else{
    //                let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
    //                self.navigationController?.pushViewController(signinVC, animated: false)
    //            }
    //        }
    //    }
    
    
    //    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    //        let tabBarIndex = tabBarController.selectedIndex
    //
    //        if tabBarIndex == 4 {
    //            if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
    //            {
    //                let myAccountVC = mainStoryBoard.instantiateViewController(withIdentifier: "MyAccountViewController") as! MyAccountViewController
    //                self.navigationController?.pushViewController(myAccountVC, animated: false)
    //            }else{
    //                let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
    //                self.navigationController?.pushViewController(signinVC, animated: false)
    //            }
    //        }
    //    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        // Home ViewController
        
        
        DispatchQueue.main.async {
            AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        }
        
        // Home screen Tableview is Enable Scroll to top
        isScroll = true
        
        if tabBarIndex != ((userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3)
        {
            if tabBarIndex == 1 && (userDefault.string(forKey: "RegionCode") != "SA")
            {
                // Mall Visit Analytics of Firebase
                Analytics.logEvent("mall_visit", parameters: [:])
            }
            
            ((tabBarController.viewControllers! as NSArray)[tabBarIndex] as! UINavigationController).popToRootViewController(animated: false)
            
        }
        else if tabBarIndex == ((userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3)
        {
            // When push to login screen from Order and Cart and click on MyAccount tab then login success
            isLoginOrNot = ""
            
            //fill an array with all the view controllers in the UITabBarController
            let arr = (tabBarController.viewControllers! as NSArray).mutableCopy() as! NSMutableArray
            
            let signInNavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "SignInNavigationController") as! SignInNavigationController
            
            arr.replaceObject(at: arr.count-1, with: signInNavigationController)
            
            //set array
            tabBarController.viewControllers = arr as? [UIViewController]
        }
        
    }
    
}
