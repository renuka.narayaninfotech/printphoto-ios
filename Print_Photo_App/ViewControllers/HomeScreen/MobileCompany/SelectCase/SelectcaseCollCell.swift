//
//  SelectcaseCollCell.swift
//  Print_Photo_App
//
//  Created by des on 26/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class SelectcaseCollCell: UICollectionViewCell {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet var CaseImage: UIImageView!
    @IBOutlet var CoverImageView: UIImageView!
    @IBOutlet var WhiteFrameImageview: UIImageView!
}
