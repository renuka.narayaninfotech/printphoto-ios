//
//  SelectCaseViewController.swift
//  Print_Photo_App
//
//  Created by des on 26/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import AssetsPickerViewController
import Photos

fileprivate enum AMDropDownType {
    case filter
}

class SelectCaseViewController: BaseViewController ,UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, URLSessionDelegate, URLSessionDataDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var selectCaseCollView: UICollectionView!
    fileprivate var selectedDropDown : AMDropDownType?
    var dropDown = DropDown()
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var mainArr = [[String:Any]]()
    var categoryId : Int!
    var mainDictFromCoverCase : [String:Any]!
    var PageNo = 1
    var isendLoad = false
    var strTitle : String!
    var strFilter : String?
    var filterButton = UIButton()
    
    // speed test
    typealias speedTestCompletionHandler = (_ megabytesPerSecond: Double? , _ error: Error?) -> Void
    
    var speedTestCompletionBlock : speedTestCompletionHandler?
    var startTime: CFAbsoluteTime!
    var stopTime: CFAbsoluteTime!
    var bytesReceived: Int!
    var isLoaded: Bool = false
    let reachabilityManager = NetworkReachabilityManager()
    var selectDict = [String:Any]()
    var getSpeed : Double!
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetNavigationBarTitle(Startstring: strTitle, EndString: "")
        
        Initialize()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        reachabilityManager?.stopListening()
        dropDown.hide()
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        selectCaseCollView.isExclusiveTouch = true
        
        checkRechabilityStatus()
        self.Call_Case_Image_API()
        
        addBackButton()
        addFilterButton()
        ShowDropDown()
        
    }
    
    //MARK:- ********************** CHECK RECHABILITY STATUS **********************
    
    func checkRechabilityStatus()
    {
        reachabilityManager?.startListening(onUpdatePerforming: { _ in
            if let isNetworkReachable = self.reachabilityManager?.isReachable,
                isNetworkReachable == true {
                //Internet Available
                print("Internet Available")
                
                self.dismissViewController()
                
            } else {
                //Internet Not Available"
                print("Internet Not Available")
                
                self.RemoveLoader()
                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                    self.Call_Case_Image_API()
                })
            }
        })
    }
    
    //MARK:- ********************** DROP DOWN **********************
    
    func ShowDropDown() {
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            print("Selected item: \(item) at index: \(index)")
            
            if self.selectedDropDown == AMDropDownType.filter {
                if index == 0
                {
                    self.strFilter = "By Popularity"
                    
                    self.PageNo = 1
                    
                    if self.mainArr.count > 0
                    {
                        self.mainArr.removeAll()
                    }
                    
                    self.topMostViewController().view.makeToast("Sort by popularity", duration: 2.0, position: .bottom)
                    
                }else
                {
                    self.strFilter = "Newest First"
                    self.PageNo = 1
                    
                    if self.mainArr.count > 0
                    {
                        self.mainArr.removeAll()
                    }
                    
                    self.topMostViewController().view.makeToast("Sort by newest first", duration: 2.0, position: .bottom)
                    
                }
                self.Call_Case_Image_API()
                self.selectCaseCollView.reloadData()
            }
        }
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addFilterButton() {
        filterButton = UIButton(type: .custom)
        filterButton.setImage(UIImage(named: "ic_filter"), for: .normal)
        filterButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        filterButton.addTarget(self, action: #selector(self.filterAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: filterButton)
    }
    
    @objc func filterAction(_ sender: UIButton) {
        view.endEditing(true)
        selectedDropDown = AMDropDownType.filter
        dropDown.backgroundColor = UIColor.darkGray
        dropDown.textColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.darkGray
        dropDown.selectedTextColor = UIColor.white
        dropDown.anchorView = filterButton
        dropDown.dataSource = ["By Popularity","Newest First"]
        
        // Resize Dropdown Box in iPad
        if DeviceType.IS_IPAD
        {
            dropDown.textFont = UIFont.boldSystemFont(ofSize: 20.0)
            dropDown.cellHeight = 60
            dropDown.width = 180
        }else
        {
            dropDown.textFont = UIFont.boldSystemFont(ofSize: 15.0)
        }
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .center
        }
        
        dropDown.show()
        
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Call_Case_Image_API() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            var ParamDict = NSDictionary()
            
            if self.strFilter == "By Popularity"
            {
                ParamDict = ["category_id":categoryId,
                             "page":PageNo,
                             "orderBy":"selling"] as NSDictionary
            }else if self.strFilter == "Newest First"{
                ParamDict = ["category_id":categoryId,
                             "page":PageNo,
                             "orderBy":"created_at"] as NSDictionary
            }else
            {
                ParamDict = ["category_id":categoryId,
                             "page":PageNo,
                             "orderBy":""] as NSDictionary
            }
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)case_images?platform=1", parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["success"] as! Int == 1
                    {
                        self.mainArr.append(contentsOf: responceDict["result"] as! [[String:Any]])
                        
                        if (self.mainArr.count <= 0)
                        {
                            self.selectCaseCollView.isHidden = true
                            self.nodataLbl.isHidden = false
                        }
                        else
                        {
                            self.selectCaseCollView.isHidden = false
                            self.nodataLbl.isHidden = true
                        }
                        
                        self.isendLoad = ((responceDict["result"] as! [[String:Any]]).count < 12) ? true : false
                        
                        self.selectCaseCollView.reloadData()
                    }
                    else
                    {
                        self.isendLoad = (responceDict["message"] as! String) == "No more data found" ? true : false
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Call_Case_Image_API()
                        }
                    })
                    
                }
            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Call_Case_Image_API()
                }
            })
        }
        
    }
    
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return mainArr.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectcaseCollCell", for: indexPath) as! SelectcaseCollCell
        
        let dict = mainArr[indexPath.row]
        let caseimageURL = "\(dict["n_image\((mainDictFromCoverCase["image_type"] as? Int ?? 0))"] as? String ?? "")"
        
        let CoverImageURL = "\(mainDictFromCoverCase["n_new_modal_image"] as! String)"
        
        cell.CaseImage.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: caseimageURL)
        
        if categoryId != 76
        {
            cell.CoverImageView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: CoverImageURL)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard !isLoaded else {
            return
        }
        
        isLoaded = true
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            // Check Internet Speed
            self.test(inSecond: 3.0) { [weak self] (speed) in
                print(speed)
                
                if speed < 0.03
                {
                    self!.RemoveLoader()
                    self!.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                    })
                    self!.isLoaded = false
                }
                else
                {
                    
                    DispatchQueue.main.async {
                        self!.selectDict = self!.mainArr[indexPath.row]
                        
                        if self!.categoryId == 76
                        {
                            self!.getSpeed = speed
                            self!.OpenGallery()
                            
                        }else
                        {
                            if (self!.mainDictFromCoverCase["n_mask_image"] as? String ?? "") != ""
                            {
                                let EditVc = mainStoryBoard.instantiateViewController(withIdentifier: "EditingViewController") as! EditingViewController
                                EditVc.mainDictFromCoverCase = self!.mainDictFromCoverCase
                                EditVc.internetSpeed = speed
                                EditVc.DefaultImgStr = "\(self!.selectDict["n_image\((self!.mainDictFromCoverCase["image_type"] as? Int ?? 0))"] as? String ?? "")"
                                self!.pushViewController(EditVc)
                            }
                            
                        }
                        
                        self!.isLoaded = false
                    }
                }
            }
            
        }
        else
        {
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
            self.isLoaded = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.Getwidth / 3, height: collectionView.Getwidth / 2)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        if indexPath.row >= self.mainArr.count - 1
        {
            if !isendLoad
            {
                PageNo += 1
                Call_Case_Image_API()
                
            }
        }
    }
    
    func OpenGallery()
    {
        DispatchQueue.main.async {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            //check user has allowed permission or not
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    
                    if self.selectDict["n_image\((self.mainDictFromCoverCase["image_type"] as? Int ?? 0))"] as? String ?? "" != ""
                    {
                        DispatchQueue.main.async {
                            let FancyCoverEditingVc = mainStoryBoard.instantiateViewController(withIdentifier: "FancyMobileCoverEditingVC") as! FancyMobileCoverEditingVC
                            FancyCoverEditingVc.coverFancyDict = self.selectDict
                            FancyCoverEditingVc.mainDictFromCoverCase = self.mainDictFromCoverCase
                            FancyCoverEditingVc.internetSpeed = self.getSpeed
                            FancyCoverEditingVc.DefaultImgStr = "\(self.selectDict["n_image\((self.mainDictFromCoverCase["image_type"] as? Int ?? 0))"] as? String ?? "")"
                            self.pushViewController(FancyCoverEditingVc)
                        }
                    }
                    
                    break
                case .denied, .restricted:
                    self.RemoveLoader()
                    self.alertGalleryAccessNeeded()
                    break
                case .notDetermined:
                    self.RemoveLoader()
                    self.requestGalleryPermission()
                    break
                case .limited:
                    self.RemoveLoader()
                    self.alertGalleryAccessPermissionChangeNeeded()
                    
                default:
                 break
                }
            }
        }
    }
    
    func requestGalleryPermission() {
        
        DispatchQueue.main.async {
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                guard accessGranted == true else { return }
            })
        }
    }
    
    func alertGalleryAccessNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Need Gallery Access",
                message: "Gallery access is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) -> Void in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Allow Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func alertGalleryAccessPermissionChangeNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Change Gallery Access",
                message: " Change Gallery access from Selected Photos to All Photos is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) -> Void in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Change Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- ********************** INTERNET SPEED TEST **********************
    
    func test(inSecond: Double, completion: @escaping((Double) -> ())) -> () {
        
        testDownloadSpeedWithTimout(timeout: inSecond) { (speed, error) in
            print("Download Speed:", speed ?? "KO")
            print("Speed Test Error:", error ?? "KO")
            completion(speed ?? 0.0)
        }
    }
    
    func testDownloadSpeedWithTimout(timeout: TimeInterval, withCompletionBlock: @escaping speedTestCompletionHandler) {
        
        guard let url = URL(string: "https://images.apple.com/v/imac-with-retina/a/images/overview/5k_image.jpg") else { return }
        
        startTime = CFAbsoluteTimeGetCurrent()
        stopTime = startTime
        bytesReceived = 0
        
        speedTestCompletionBlock = withCompletionBlock
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForResource = timeout
        let session = URLSession.init(configuration: configuration, delegate: self, delegateQueue: nil)
        session.dataTask(with: url).resume()
        
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        bytesReceived! += data.count
        stopTime = CFAbsoluteTimeGetCurrent()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        let elapsed = stopTime - startTime
        
        if let aTempError = error as NSError?, aTempError.domain != NSURLErrorDomain && aTempError.code != NSURLErrorTimedOut && elapsed == 0  {
            speedTestCompletionBlock?(nil, error)
            return
        }
        
        let speed = elapsed != 0 ? Double(bytesReceived) / elapsed / 1024.0 / 1024.0 : -1
        speedTestCompletionBlock?(speed, nil)
        
    }
}
