
//
//  RequestBrandViewController.swift
//  Print_Photo_App
//
//  Created by V3 on 20/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController

class RequestBrandViewController: BaseViewController,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate, URLSessionDelegate, URLSessionDataDelegate{
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var RequestBrandTableView: UITableView!
    @IBOutlet var SearchBar: UISearchBar!
    @IBOutlet var viewItemNotAvailable: UIView!
    @IBOutlet var viewRequestYourDevice: UIView!
    @IBOutlet var txtNewBrand: UITextView!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet weak var btnWriteYourRequest: UIButton!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    var FilteredArr = [[String:Any]]()
    var refreshControl = UIRefreshControl()
    var modelDict = [String:Any]()
    
    // speed test
    typealias speedTestCompletionHandler = (_ megabytesPerSecond: Double? , _ error: Error?) -> Void
    
    var speedTestCompletionBlock : speedTestCompletionHandler?
    
    var startTime: CFAbsoluteTime!
    var stopTime: CFAbsoluteTime!
    var bytesReceived: Int!
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        SetNavigationBarTitle(Startstring: "Request Brand/Model", EndString: "")
        
        Initialize()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.view.endEditing(true)
        
        //Apply place holder to text view
        txtNewBrand.text = "Enter Brand or Model Name"
        txtNewBrand.textColor = UIColor.lightGray
        
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        //hide view for "item not available"
        viewItemNotAvailable.isHidden = true
        
        removeBackButton()
        addBackButton()
        set_Border()
        Device_Request_View()
        txtNewBrand.enablesReturnKeyAutomatically = false
        
        if RequestBrandMainArr.count == 0
        {
            CallApi()
        }else
        {
            FilteredArr = RequestBrandMainArr
            self.RequestBrandTableView.reloadData()
        }
        
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** View Border & Button Corner Radius **********************
    
    func set_Border()
    {
        txtNewBrand.setBorder()
        btnWriteYourRequest.cornerRadius = 10.0
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func CallApi() {
        if Reachability.isConnectedToNetwork() {
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)getAllModelDetails", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        
                        let arrData = responceDict["data"] as! [[String:Any]]
                        let arrModal = (arrData as NSArray).value(forKeyPath: "modal") as! [[[String:Any]]]
                        RequestBrandMainArr = Array(arrModal.joined())
                        
                        self.FilteredArr = RequestBrandMainArr
                        
                        if (self.FilteredArr.count <= 0)
                        {
                            self.RequestBrandTableView.isHidden = true
                            self.nodataLbl.isHidden = false
                        }
                        else
                        {
                            self.RequestBrandTableView.isHidden = false
                            self.nodataLbl.isHidden = true
                        }
                        
                        self.RequestBrandTableView.reloadData()
                        
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.CallApi()
                        }
                    })
                    
                }
            }
        }
            
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.CallApi()
                }
            })
        }
    }
    
    func CallApiForSendRequest(dicModalData:[String:Any]) {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let userid = userDefault.value(forKey: "USER_ID")
            
            let parameter = ["modal_id": dicModalData["model_id"],
                             "user_id": userid,
                             "request_model":dicModalData["Model_Name"],
                             "request_model_id":dicModalData["id"]]
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)saveData", parameters: parameter as NSDictionary) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    //request send successfully
                    if responceDict["ResponseCode"] as! String == "2"
                    {
                        
                        self.presentAlertWithTitle(title: "Request send successfully", message: "", options: "OK") { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.dismissViewController()
                            }
                        }
                    }
                        //we already have modal, so show alert and redirect to editing screen
                    else if responceDict["ResponseCode"] as! String == "1"
                    {
                        //                        self.modelDict = responceDict["data"] as! [String:Any]
                        let modalDataDict   = responceDict["data"] as! [String:Any]
                        
                        // Set data in to modeldict from Send Request responce
                        self.modelDict["model_id"] = modalDataDict["id"]
                        self.modelDict["modalName"] = modalDataDict["modalName"]
                        self.modelDict["price"] = modalDataDict["price"]
                        self.modelDict["n_modal_image"] = modalDataDict["n_modal_image"]
                        self.modelDict["n_mask_image"] = modalDataDict["n_mask_image"]
                        self.modelDict["width"] = modalDataDict["width"]
                        self.modelDict["height"] = modalDataDict["height"]
                        
                        self.presentAlertWithTitle(title: "Requested Model is already available, Want to proceed for Editing ?", message: "", options: "NO","YES") { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.dismissViewController()
                            }else
                            {
                                self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)

                                // Check Internet Speed
                                self.test(inSecond: 3.0) { [weak self] (speed) in
                                    print(speed)
                                    
                                    self!.RemoveLoader()
                                    
                                    if speed < 0.03
                                    {
                                        self!.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                                        })
                                    }else
                                    {
                                        DispatchQueue.main.async {
                                            if (self!.modelDict["n_mask_image"] as? String ?? "") != ""
                                            {
                                                // Redirect to editing screen
                                                
                                                let EditVc = mainStoryBoard.instantiateViewController(withIdentifier: "EditingViewController") as! EditingViewController
                                                EditVc.categoryId = 0
                                                EditVc.internetSpeed = speed
                                                EditVc.mainDictFromCoverCase = self!.modelDict
                                                self!.pushViewController(EditVc)
                                            }
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                    else//request is already send
                    {
                        self.presentAlertWithTitle(title: "Model Request", message: "\(responceDict["ResponseMessage"]!)", options: "OK", completion: { (ButtonIndex) in
                            
                            if ButtonIndex == 0
                            {
                                self.dismissViewController()
                            }
                        })
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.dismissViewController()
                        }
                    })
                    
                }
                
            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.CallApi()
                }
            })
        }
    }
    
    func CallApiForNewModelRequest() {
        
        if Reachability.isConnectedToNetwork() {
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let userid = userDefault.value(forKey: "USER_ID")
            
            let parameter = ["user_id": userid,
                             "request_model":txtNewBrand.text
            ]
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)save_request", parameters: parameter as NSDictionary) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                                    
                    let responceDict = Responce as! [String:Any]
                    
                    //request send successfully
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        //hide view for add model
                        self.dismissViewController()
                        
                        self.presentAlertWithTitle(title: "Request send successfully", message: "", options: "OK", completion: { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.dismissViewController()
                                
                                self.FilteredArr = RequestBrandMainArr
                                
                                self.SearchBar.text = ""
                                if (self.FilteredArr.count <= 0)
                                {
                                    self.RequestBrandTableView.isHidden = true
                                    self.viewItemNotAvailable.isHidden = false
                                }
                                else
                                {
                                    self.RequestBrandTableView.isHidden = false
                                    self.viewItemNotAvailable.isHidden = true
                                }
                                
                                self.RequestBrandTableView.reloadData()
                            }
                        })
                        
                    }
                    else//error in send request
                    {

                        self.topMostViewController().view.makeToast("Something went wrong", duration: 2.0, position: .center)
                        
//                        self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
//
////                            if ButtonIndex == 0
////                            {
////                                self.dismissViewController()
////                            }
//                        })
                    }
                }
                else
                {
//                    //hide view for add model
//                    self.dismissViewController()

                self.topMostViewController().view.makeToast("Something went wrong", duration: 2.0, position: .center)
                    
//                    self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
////                        if ButtonIndex == 0
////                        {
////                            self.CallApiForNewModelRequest()
////                        }
//                    })
                }
            }
        }
        else
        {
            self.topMostViewController().view.makeToast("Internet is slow. Please check internet connection.", duration: 2.0, position: .center)
            
//            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
//                if ButtonIndex == 0
//                {
//                    self.CallApi()
//                }
//            })
        }
    }
    
    //MARK:- ********************** TABLEVIEW METHODS **********************
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FilteredArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestBrandTableCell", for: indexPath) as! RequestBrandTableCell
        
        //give shadow to view
        cell.ShadowView.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        
        //set font
        //        cell.lblBrandCompany.font = UIFont.init(name: CustomFontWeight.reguler, size: cell.lblBrandCompany.Getheight *  0.9)
        
        //set buttom alignment
        cell.btnSendRequest.titleLabel?.textAlignment = .center
        
        // Only one event call when click on two button together
        cell.btnSendRequest.isExclusiveTouch = true
        
        //set btn tag and action
        cell.btnSendRequest.tag = indexPath.row
        cell.btnSendRequest.addTarget(self, action: #selector(btnSendRequestClicked(sender:)), for: .touchDown)
        
        // set data
        cell.lblBrandCompany.text = "\((self.FilteredArr[indexPath.row])["Manufacturer"] ?? "") \((self.FilteredArr[indexPath.row])["Model_Name"] ?? "")"
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //hide keyboard
        //        SearchBar.resignFirstResponder()
    }
    
    // MARK: - ********************** TOUCHES LIFECYCLE **********************
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        SearchBar.endEditing(true)
        
    }
    
    //MARK:- ********************** SEARCH METHODS **********************
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        FilteredArr = searchText.isEmpty ? RequestBrandMainArr : RequestBrandMainArr.filter { team in
            
            let searchString = "\(team["Manufacturer"] ?? "") \(team["Model_Name"] ?? "")"
            
            return (searchString).lowercased().contains(searchText.lowercased())
        }
        
        if (self.FilteredArr.count <= 0)
        {
            self.RequestBrandTableView.isHidden = true
            viewItemNotAvailable.isHidden  = false
            
        }
        else
        {
            self.RequestBrandTableView.isHidden = false
            viewItemNotAvailable.isHidden  = true
        }
        
        self.RequestBrandTableView.reloadData()
        
    }
    
    //MARK:- ********************** TEXT VIEW DELEGATE METHOD **********************
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter Brand or Model Name"
            textView.textColor = UIColor.lightGray
        }
    }
    
    //MARK:- ********************** SEND DEVICE REQUEST UIVIEW SIZE **********************
    
    func Device_Request_View() {
        if DeviceType.IS_IPAD
        {
            viewRequestYourDevice.frame.size = CGSize(width: 450, height: 255)
        }
        else
        {
            viewRequestYourDevice.frame.size = CGSize(width: 300, height: 170)
        }
    }
    
    //MARK:- ********************** BUTTON ACTION **********************
    
    @objc func btnSendRequestClicked(sender:UIButton)
    {
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            self.view.endEditing(true)
            
            if FilteredArr.count > 0
            {
                //send request for modal
                self.CallApiForSendRequest(dicModalData: self.FilteredArr[sender.tag])
            }
        }else
        {
            isLoginOrNot = "mobile_company_login"
            let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
            self.pushViewController(signinVC)
        }
    }
    
    @IBAction func btnWriteYourRequestClicked(_ sender: Any) {
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            self.view.endEditing(true)
            
            txtNewBrand.text = "Enter Brand or Model Name"
            txtNewBrand.textColor = UIColor.lightGray
            txtNewBrand.becomeFirstResponder()
            let TFPopup = MZFormSheetPresentationViewController(contentView: viewRequestYourDevice)
            TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
            TFPopup.presentationController?.shouldCenterVertically = false
            TFPopup.contentViewControllerTransitionStyle = .slideAndBounceFromBottom
            
            self.present(TFPopup, animated: true)
        }else
        {
            isLoginOrNot = "mobile_company_login"
            let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
            self.pushViewController(signinVC)
        }
        
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            if (txtNewBrand.text == "" || txtNewBrand.text == "Enter Brand or Model Name" || txtNewBrand.text.trim() == "")
            {
                self.topMostViewController().view.makeToast("Enter Brand or Model Name", duration: 2.0, position: .center)
            }
            else
            {
                CallApiForNewModelRequest()
            }
        }else
        {
            //hide view for add model
            self.dismissViewController()
            
            isLoginOrNot = "mobile_company_login"
            let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
            self.pushViewController(signinVC)
        }
        
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        
        self.dismissViewController()
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    //MARK:- ********************** INTERNET SPEED TEST **********************
    
    func test(inSecond: Double, completion: @escaping((Double) -> ())) -> () {
        
        testDownloadSpeedWithTimout(timeout: inSecond) { (speed, error) in
            print("Download Speed:", speed ?? "KO")
            print("Speed Test Error:", error ?? "KO")
            completion(speed ?? 0.0)
        }
    }
    
    func testDownloadSpeedWithTimout(timeout: TimeInterval, withCompletionBlock: @escaping speedTestCompletionHandler) {
        
        guard let url = URL(string: "https://images.apple.com/v/imac-with-retina/a/images/overview/5k_image.jpg") else { return }
        
        startTime = CFAbsoluteTimeGetCurrent()
        stopTime = startTime
        bytesReceived = 0
        
        speedTestCompletionBlock = withCompletionBlock
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForResource = timeout
        let session = URLSession.init(configuration: configuration, delegate: self, delegateQueue: nil)
        session.dataTask(with: url).resume()
        
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        bytesReceived! += data.count
        stopTime = CFAbsoluteTimeGetCurrent()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        let elapsed = stopTime - startTime
        
        if let aTempError = error as NSError?, aTempError.domain != NSURLErrorDomain && aTempError.code != NSURLErrorTimedOut && elapsed == 0  {
            speedTestCompletionBlock?(nil, error)
            return
        }
        
        let speed = elapsed != 0 ? Double(bytesReceived) / elapsed / 1024.0 / 1024.0 : -1
        speedTestCompletionBlock?(speed, nil)
        
    }
}

