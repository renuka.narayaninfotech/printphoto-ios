//
//  RequestBrandTableCell.swift
//  Print_Photo_App
//
//  Created by V3 on 20/03/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class RequestBrandTableCell: UITableViewCell {

      //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var ShadowView: UIView!
    @IBOutlet var lblBrandCompany: UILabel!
    @IBOutlet var btnSendRequest: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
