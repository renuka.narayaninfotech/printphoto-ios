//
//  MugStickerTableViewController.swift
//  Print_Photo_App
//
//  Created by des on 02/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import Alamofire

class MugStickerTableViewController: BaseViewController ,UITableViewDataSource , UITableViewDelegate, UISearchBarDelegate, URLSessionDelegate, URLSessionDataDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var MugStickerTableView: UITableView!
    @IBOutlet var SearchBar: UISearchBar!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var MainArr = [[String:Any]]()
    var categoryId = Int()
    var strNavTitle: String = ""
    var FilteredArr = [[String:Any]]()
    var refreshControl = UIRefreshControl()
    var expandedArray = [Int]()
    var tempFilteredArr = [[String:Any]]()
    var currencySymbol : String?
    
    // speed test
    typealias speedTestCompletionHandler = (_ megabytesPerSecond: Double? , _ error: Error?) -> Void
    
    var speedTestCompletionBlock : speedTestCompletionHandler?
    var startTime: CFAbsoluteTime!
    var stopTime: CFAbsoluteTime!
    var bytesReceived: Int!
    let reachabilityManager = NetworkReachabilityManager()
    var lastContentOffset: CGFloat = 0.0
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    override func viewDidLoad() {
        super.viewDidLoad()
        SetNavigationBarTitle(Startstring: strNavTitle, EndString: "")
        
        Initialize()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        SearchBar.returnKeyType = .done
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // Reload tableview for selected perticular row index in tableview
        self.MugStickerTableView.setContentOffset(CGPoint.init(x: 0.0, y: self.lastContentOffset), animated: false)
        
        
        ////        if mainProductID == ID.Mug
        ////        {
        ////            DispatchQueue.main.async {
        ////                self.MugStickerTableView.reloadData()
        ////            }
        //
        ////            let loc = MugStickerTableView.contentOffset
        //            UIView.performWithoutAnimation {
        //
        //                MugStickerTableView.reloadData()
        //
        //                MugStickerTableView.layoutIfNeeded()
        //                MugStickerTableView.beginUpdates()
        //                MugStickerTableView.endUpdates()
        //
        //                MugStickerTableView.layer.removeAllAnimations()
        //            }
        //            MugStickerTableView.setContentOffset(CGPoint.init(x: 0.0, y: self.lastContentOffset), animated: fals)
        ////        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        
        reachabilityManager?.stopListening()
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        SearchBar.placeholder = "Search for \(strNavTitle)..."
        
        checkRechabilityStatus()
        
        addCartButtonWithBadgeIcon()
        
        addBackButton()
        
        self.FilteredArr = self.MainArr
        tempFilteredArr = self.MainArr
        
        if (self.FilteredArr.count <= 0)
        {
            self.MugStickerTableView.isHidden = true
            self.nodataLbl.text = "Model not available"
            self.nodataLbl.isHidden = false
        }
        else
        {
            self.MugStickerTableView.isHidden = false
            self.nodataLbl.isHidden = true
        }
    }
    
    func addCartButtonWithBadgeIcon() {
        
        var badgelabel = UILabel()
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            // badge label
            badgelabel = UILabel(frame: CGRect(x: 20, y: -5, width: 17, height: 17))
            badgelabel.layer.borderColor = UIColor.clear.cgColor
            badgelabel.layer.borderWidth = 2
            badgelabel.layer.cornerRadius = badgelabel.bounds.size.height / 2
            badgelabel.textAlignment = .center
            badgelabel.layer.masksToBounds = true
            badgelabel.font = UIFont(name: CustomFontWeight.medium, size: 10)
            badgelabel.textColor = .white
            badgelabel.backgroundColor = hexStringToUIColor(hex: "0ABAD4")
            
            if (userDefault.integer(forKey: "BadgeNumber")) != 0
            {
                badgelabel.isHidden = false
                badgelabel.text = "\(userDefault.integer(forKey: "BadgeNumber"))"
            }else
            {
                badgelabel.isHidden = true
            }
        }
        
        // Button
        let cartButton = UIButton(type: .custom)
        cartButton.setImage(UIImage(named: "ic_cart"), for: .normal)
        cartButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cartButton.addTarget(self, action: #selector(self.cartAction(_:)), for: .touchUpInside)
        cartButton.addSubview(badgelabel)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cartButton)
        
    }
    
    @objc func cartAction(_ sender: UIButton) {
        
        isCartBtnBack = false
        ((tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
        
        self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 1 : 2
        
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    @objc func pressButtonShareProduct(_ sender: UIButton?) {
        
        let cell = sender?.superview?.superview as! MugStickerTableCell
        
        let coupanImage = cell.ImgView.takeScreenshot()
        
        let link = "https://itunes.apple.com/us/app/print-photo-phone-case-maker/id1458325325?ls=1&mt=8"
        let objectsToShare = ["Download this amazing print photo app from App Store \n\n \(link) \n",coupanImage] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.setValue(appName, forKey: "subject")
        
        if DeviceType.IS_IPHONE
        {
            self.present(activityVC, animated: true, completion: nil)
        }else
        {
            let popController: UIPopoverPresentationController = activityVC.popoverPresentationController!
            popController.permittedArrowDirections = .any
            popController.sourceView = sender?.superview
            popController.sourceRect = sender!.frame
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @objc func pressButtonTutorialVideo(_ sender: UIButton?) {
        
        let dict1 = FilteredArr[sender!.tag]
        let videoUrl = "\(dict1["video_url"] ?? "")"
        
        UIApplication.shared.open(NSURL(string: videoUrl)! as URL, options: [:], completionHandler: nil)
    }
    
    //MARK:- ********************** CHECK RECHABILITY STATUS **********************
    
    func checkRechabilityStatus()
    {
        
        reachabilityManager?.startListening(onUpdatePerforming: { _ in
            if let isNetworkReachable = self.reachabilityManager?.isReachable,
                isNetworkReachable == true {
                //Internet Available
                print("Internet Available")
                
            } else {
                //Internet Not Available"
                print("Internet Not Available")
                
                self.RemoveLoader()
                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                    
                    if Reachability.isConnectedToNetwork()
                    {
                        self.MugStickerTableView.reloadData()
                    }else
                    {
                        self.checkRechabilityStatus()
                    }
                })
            }
        })
    }
    
    
    //MARK:- ********************** API CALLING **********************
    
    //    func CallApi() {
    //
    //        if Reachability.isConnectedToNetwork() {
    //
    //            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
    //
    //            let ParamDict = ["category_id":mainProductID!] as NSDictionary
    //
    //            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)get_companies", parameters: ParamDict) { (APIResponce, Responce, error1) in
    //
    //                self.RemoveLoader()
    //
    //                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
    //                {
    //                    let responceDict = Responce as! [String:Any]
    //
    //                    if responceDict["ResponseCode"] as! String == "1"
    //                    {
    //                        self.MainArr = responceDict["data"] as! [[String:Any]]
    //                        self.FilteredArr = self.MainArr
    //
    //                        if (self.MainArr.count <= 0)
    //                        {
    //                            self.MugStickerTableView.isHidden = true
    //                            self.nodataLbl.text = "Model not available"
    //                            self.nodataLbl.isHidden = false
    //                        }
    //                        else
    //                        {
    //                            self.MugStickerTableView.isHidden = false
    //                            self.nodataLbl.isHidden = true
    //                        }
    //
    //                        self.MugStickerTableView.reloadData()
    //                    }else
    //                    {
    //                        self.MugStickerTableView.isHidden = true
    //                        self.nodataLbl.isHidden = false
    //                    }
    //
    //                }
    //                else
    //                {
    //                    self.presentAlertWithTitle(title: "Something went wrong!!", message: "", options: "Retry", completion: { (ButtonIndex) in
    //                        if ButtonIndex == 0
    //                        {
    //                            self.CallApi()
    //                        }
    //                    })
    //
    //                }
    //            }
    //        }
    //        else
    //        {
    //            self.RemoveLoader()
    //
    //            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
    //
    //                if ButtonIndex == 0
    //                {
    //                    self.CallApi()
    //                }
    //            })
    //        }
    //
    //    }
    
    //MARK:- ********************** TABLEVIEW METHODS **********************
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FilteredArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MugStickerTableCell") as! MugStickerTableCell
        
        let dict1 = FilteredArr[indexPath.row]
        
        if !((FilteredArr[indexPath.row])["n_mug_image"] is NSNull) || !((FilteredArr[indexPath.row])["international_n_mug_image"] is NSNull)
        {
            cell.ImgView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: (userDefault.string(forKey: "RegionCode") == "IN") ? (dict1["n_mug_image"] as? String ?? "") : (dict1["international_n_mug_image"] as? String ?? ""))
            
            cell.PriceLbl.text = "\(currencySymbol ?? "")\(dict1["price"] ?? "")"
            cell.lblDummyPrice.attributedText = "\(currencySymbol ?? "")\(dict1["dummy_price"] ?? "")".strikeThrough()
            cell.Sizelbl.text = "Size : \(dict1["width"] ?? "") X \(dict1["height"] ?? "") INCH"
            
            cell.TitleLbl.text = "\(dict1["modalName"] ?? "")"
            
            let modelID = dict1["model_id"] as? Int ?? 0
            
            cell.ExpandBtn.tag = modelID
            cell.ExpandBtn.addTarget(self, action: #selector(ExpandPressed(sender:)), for: .touchUpInside)
            let angle = expandedArray.contains(modelID) ? CGFloat(Double.pi) : 0.0
            cell.ExpandBtn.imageView?.transform = CGAffineTransform(rotationAngle: angle)
            
            cell.descLbl.text = expandedArray.contains(modelID) ? "\(dict1["description"] as? String ?? "")" : nil
            
            cell.btnShareProduct.tag = modelID
            cell.btnShareProduct.addTarget(self, action: #selector(self.pressButtonShareProduct(_:)), for: .touchDown)
            
        }else{
            // Temp string is a setlement
            cell.ImgView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: "tempString")
        }
        
        // Youtube Video Tutorial (When get URL then show button)
        
        let videoUrl = "\(dict1["video_url"] ?? "")"
        cell.btnTutorialVideoWidth.constant = 0.0
        
        if videoUrl != ""
        {
            cell.btnTutorialVideo.isHidden = false
            cell.btnTutorialVideoWidth.constant = DeviceType.IS_IPAD ? 52.0 : 35.0
            cell.btnTutorialVideo.tag = indexPath.row
            cell.btnTutorialVideo.addTarget(self, action: #selector(self.pressButtonTutorialVideo(_:)), for: .touchDown)
        }else
        {
            cell.btnTutorialVideoWidth.constant = 0.0
            cell.btnTutorialVideo.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.isExclusiveTouch = true
        tableView.isMultipleTouchEnabled = false
        
        if let cellTemp = tableView.cellForRow(at: indexPath) as? MugStickerTableCell
        {
            if cellTemp.ImgView.image == nil
            {
                return
            }
        }
        
        //do not allow user to tap
        MugStickerTableView.isUserInteractionEnabled = false
        
        self.SearchBar.endEditing(true)
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            self.lastContentOffset = self.MugStickerTableView.contentOffset.y
            
            // Check Internet Speed
            self.test(inSecond: 3.0) { [weak self] (speed) in
                print(speed)
                
                //allow user to tap
                DispatchQueue.main.async {
                    self?.MugStickerTableView.isUserInteractionEnabled = true
                }
                
                self!.RemoveLoader()
                
                if speed < 0.03
                {
                    self!.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                    })
                    
                }else
                {
                    
                    DispatchQueue.main.async {
                        
                        if self!.FilteredArr.count > 0 && self!.FilteredArr.count > indexPath.row
                        {
                            if ((self!.FilteredArr[indexPath.row] as NSDictionary).value(forKey: "n_mask_image") as? String ?? "") != "" || ((self!.FilteredArr[indexPath.row] as NSDictionary).value(forKey: "n_modal_image") as? String ?? "") != ""
                            {
                                if mainProductID == ID.Mug || mainProductID == 10
                                {
                                    let MugFrameVC = mainStoryBoard.instantiateViewController(withIdentifier: "MugFancyFrameVC") as! MugFancyFrameVC
                                    MugFrameVC.mainDictFromCoverCase = self!.FilteredArr[indexPath.row]
                                    MugFrameVC.internetSpeed = speed
                                    self!.pushViewController(MugFrameVC)
                                    
                                }else if selectedMainProductId == ID.MultiImagePhotoFrames
                                {
                                    let MultiplePhotoFrameView = mainStoryBoard.instantiateViewController(withIdentifier: "MultiplePhotoFrameVC") as! MultiplePhotoFrameVC
                                    MultiplePhotoFrameView.mainDictFromCoverCase = self!.FilteredArr[indexPath.row]
                                    MultiplePhotoFrameView.internetSpeed = speed
                                    self!.pushViewController(MultiplePhotoFrameView)
                                }
                                else
                                {
                                    let EditingVC = mainStoryBoard.instantiateViewController(withIdentifier: "EditingViewController") as! EditingViewController
                                    EditingVC.mainDictFromCoverCase = self!.FilteredArr[indexPath.row]
                                    EditingVC.internetSpeed = speed
                                    self!.pushViewController(EditingVC)
                                }
                            }
                            
                        }
                    }
                }
            }
        }
        else
        {
            //allow user to tap
            self.MugStickerTableView.isUserInteractionEnabled = true
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func ExpandPressed(sender : UIButton) {
        
        if expandedArray.contains(sender.tag)
        {
            expandedArray = expandedArray.filter { (int) -> Bool in
                if int == sender.tag
                {
                    return false
                }
                return true
            }
        }
        else
        {
            expandedArray.append(sender.tag)
        }
        
        let index = FilteredArr.index{ $0["model_id"] as? Int == sender.tag } ?? 0
        
        let idx = IndexPath(row:index, section: 0)
        let dict = FilteredArr[index]
        
        let cell = MugStickerTableView.cellForRow(at: idx) as! MugStickerTableCell
        
        MugStickerTableView.beginUpdates()
        let loc = MugStickerTableView.contentOffset
        cell.descLbl.text = expandedArray.contains(sender.tag) ? "\(dict["description"] as? String ?? "")" : nil
        let angle = expandedArray.contains(sender.tag) ? CGFloat(Double.pi) : 0.0
        cell.ExpandBtn.imageView?.transform = CGAffineTransform(rotationAngle: angle)
        MugStickerTableView.endUpdates()
        self.MugStickerTableView.contentOffset = loc
    }
    
    // MARK: - ********************** TOUCHES LIFECYCLE **********************
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //        SearchBar.endEditing(true)
    }
    
    //MARK:- ********************** SEARCH METHODS **********************
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        FilteredArr = searchText.isEmpty ? MainArr : MainArr.filter { team in
            
            return (team["modalName"] as! String).lowercased().contains(searchText.lowercased())
        }
        
        if (self.FilteredArr.count <= 0)
        {
            self.MugStickerTableView.isHidden = true
            self.nodataLbl.text = "Model not available"
            self.nodataLbl.isHidden = false
        }
        else
        {
            self.MugStickerTableView.isHidden = false
            self.nodataLbl.isHidden = true
        }
        
        if (tempFilteredArr as NSArray) != (FilteredArr as NSArray)
        {
            self.tempFilteredArr = FilteredArr
            self.MugStickerTableView.reloadData()
        }
        self.MugStickerTableView.scroll(to: .top, animated: false)
        
    }
    
    //MARK:- ********************** INTERNET SPEED TEST **********************
    
    func test(inSecond: Double, completion: @escaping((Double) -> ())) -> () {
        
        testDownloadSpeedWithTimout(timeout: inSecond) { (speed, error) in
            print("Download Speed:", speed ?? "KO")
            print("Speed Test Error:", error ?? "KO")
            completion(speed ?? 0.0)
        }
    }
    
    func testDownloadSpeedWithTimout(timeout: TimeInterval, withCompletionBlock: @escaping speedTestCompletionHandler) {
        
        guard let url = URL(string: "https://images.apple.com/v/imac-with-retina/a/images/overview/5k_image.jpg") else { return }
        
        startTime = CFAbsoluteTimeGetCurrent()
        stopTime = startTime
        bytesReceived = 0
        
        speedTestCompletionBlock = withCompletionBlock
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForResource = timeout
        let session = URLSession.init(configuration: configuration, delegate: self, delegateQueue: nil)
        session.dataTask(with: url).resume()
        
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        bytesReceived! += data.count
        stopTime = CFAbsoluteTimeGetCurrent()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        let elapsed = stopTime - startTime
        
        if let aTempError = error as NSError?, aTempError.domain != NSURLErrorDomain && aTempError.code != NSURLErrorTimedOut && elapsed == 0  {
            speedTestCompletionBlock?(nil, error)
            return
        }
        
        let speed = elapsed != 0 ? Double(bytesReceived) / elapsed / 1024.0 / 1024.0 : -1
        speedTestCompletionBlock?(speed, nil)
        
    }
    
}
