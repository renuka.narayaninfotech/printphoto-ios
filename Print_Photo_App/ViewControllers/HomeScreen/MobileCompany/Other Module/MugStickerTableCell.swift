//
//  MugStickerTableCell.swift
//  Print_Photo_App
//
//  Created by des on 02/02/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class MugStickerTableCell: UITableViewCell {

    @IBOutlet var ImgView: UIImageView!
    
    @IBOutlet var TitleLbl: UILabel!
    
    @IBOutlet var Sizelbl: UILabel!
    
    @IBOutlet var PriceLbl: UILabel!
    
    @IBOutlet var descLbl: UILabel!
    
    @IBOutlet var ExpandBtn: UIButton!
    
    @IBOutlet var lblSelect: UILabel!
    @IBOutlet weak var lblDummyPrice: UILabel!
    @IBOutlet weak var btnTutorialVideo: UIButton!
    @IBOutlet weak var btnTutorialVideoWidth: NSLayoutConstraint!
    @IBOutlet weak var btnShareProduct: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblSelect.layer.shadowColor = UIColor.lightGray.cgColor
        lblSelect.layer.shadowRadius = 2.0
        lblSelect.layer.shadowOpacity = 1.0
        lblSelect.layer.shadowOffset = CGSize.init(width: 1.0, height: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
