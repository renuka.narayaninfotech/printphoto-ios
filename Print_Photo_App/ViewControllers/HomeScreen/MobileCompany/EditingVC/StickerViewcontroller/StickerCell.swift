//
//  StickerCell.swift
//  Print_Photo_App
//
//  Created by des on 31/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class StickerCell: UICollectionViewCell {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet var stickerImgView: UIImageView!
    @IBOutlet weak var lblStickerName: UILabel!
}
