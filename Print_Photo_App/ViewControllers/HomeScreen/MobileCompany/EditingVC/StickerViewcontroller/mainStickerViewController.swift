//
//  mainStickerViewController.swift
//  Print_Photo_App
//
//  Created by des on 31/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class mainStickerViewController: BaseViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var stickerCollView: UICollectionView!
    
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Add Sticker", EndString: "")
        
        Initialize()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //MARK:- ********************** Initialize **********************
    
    func Initialize() {
        
        removeBackButton()
        addBackButton()
        
        if mainstickerArr.count == 0
        {
            CallApi()
        }
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func CallApi() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)new_stickers", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        mainstickerArr = responceDict["data"] as! [[String:Any]]
                        self.stickerCollView.reloadData()
                    }else{
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.CallApi()
                        }
                    })
                    
                }
            }
            
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.popViewController()
                }
            })
        }
        
    }
    
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainstickerArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerCell", for: indexPath) as! StickerCell
        
        cell.stickerImgView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: (mainstickerArr[indexPath.row])["n_image"] as! String)
        cell.lblStickerName.text = "\((mainstickerArr[indexPath.row])["name"] ?? "")"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let subStickerVc = mainStoryBoard.instantiateViewController(withIdentifier: "subStickerViewController") as! subStickerViewController
        subStickerVc.substickerArr = (mainstickerArr[indexPath.row])["stickers"] as! [[String:Any]]
        pushViewController(subStickerVc)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.Getwidth * 0.25, height: collectionView.Getwidth * 0.25 + 20.0)
    }
    
    
}
