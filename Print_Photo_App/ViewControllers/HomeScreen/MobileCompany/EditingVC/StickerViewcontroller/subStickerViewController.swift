//
//  subStickerViewController.swift
//  Print_Photo_App
//
//  Created by des on 31/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class subStickerViewController: BaseViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout  {
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var substickerArr = [[String:Any]]()
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    
    @IBOutlet var stickerColView: UICollectionView!
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Choose Sticker", EndString: "")
        
        addBackButton()
        removeBackButton()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return substickerArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerCell", for: indexPath) as! StickerCell
        
        cell.stickerImgView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: (substickerArr[indexPath.row])["n_image"] as! String)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.Getwidth * 0.25, height: collectionView.Getwidth * 0.25)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (collectionView.cellForItem(at: indexPath) as! StickerCell).stickerImgView.image != nil {
            
            StickerImage = (collectionView.cellForItem(at: indexPath) as! StickerCell).stickerImgView.image!
            
            if isSelectSticker == "EditingVC"
            {
                let VCarray = self.navigationController?.viewControllers
                for Controller in VCarray! {
                    if Controller is EditingViewController {
                        self.navigationController?.popToViewController(Controller, animated: true)
                    }
                }
            }else if isSelectSticker == "MugEditingVC"
            {
                let VCarray = self.navigationController?.viewControllers
                for Controller in VCarray! {
                    if Controller is MugEditingViewController {
                        self.navigationController?.popToViewController(Controller, animated: true)
                    }
                }
            }else if isSelectSticker == "EditingMultiplePhotoFrame"
            {
                let VCarray = self.navigationController?.viewControllers
                for Controller in VCarray! {
                    if Controller is EditingMultiplePhotoFrameVC {
                        self.navigationController?.popToViewController(Controller, animated: true)
                    }
                }
            }
            
        }
        
    }
    
}
