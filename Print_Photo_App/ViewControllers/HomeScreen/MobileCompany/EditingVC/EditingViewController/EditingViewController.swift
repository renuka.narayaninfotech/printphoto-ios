//
//  EditingViewController.swift
//  Original_ImagesendDemo
//
//  Created by des on 12/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import AFNetworking
import Photos
import Toast_Swift
import ChromaColorPicker
import MZFormSheetPresentationController
import Alamofire
import FBSDKCoreKit
//import FBSDKMarketingKit
import FacebookCore
import Firebase
import SDWebImage

//import FLAnimatedImage
//import SDWebImageFLPlugin


class EditingViewController: BaseViewController , StickerViewDelegate , UINavigationControllerDelegate , UIImagePickerControllerDelegate , ChromaColorPickerDelegate , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    //MARK:- ********************** OUTLAT DECLARTION **********************
    
    @IBOutlet var bottomMenuCollView: UICollectionView!
    @IBOutlet var ViewforCapture: UIView!
    @IBOutlet var defaultImgView: UIImageView!
    @IBOutlet var Cover_ImgView: UIImageView!
    @IBOutlet var Mask_ImgView: UIImageView!
    @IBOutlet var CromaColorPicker: ChromaColorPicker!
    @IBOutlet var CromaVIew: UIView!
    @IBOutlet weak var captureSuperview: UIView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var FancyCoverID : Int?
    var isSetupDone = false
    var categoryId : Int!
    var picker = UIImagePickerController()
    private var _selectedStickerView:StickerView?
    var mainDictFromCoverCase : [String:Any]!
    var DefaultImgStr : String!
    var btnAddPhoto = UIButton()
    var gestureRecognizer: UITapGestureRecognizer!
    var bottomItemsArr: [UIImage?]!
    var defaultLabel = UILabel()
    var isFromViewDidLoad: Bool = true
    var internetSpeed : Double = 0
    var fancyCaseImage : UIImage!
    var isLoaded = false
    
    var selectedStickerView:StickerView? {
        get {
            return _selectedStickerView
        }
        set {
            
            // if other sticker choosed then resign the handler
            if _selectedStickerView != newValue {
                if let selectedStickerView = _selectedStickerView {
                    selectedStickerView.showEditingHandlers = false
                }
                _selectedStickerView = newValue
            }
            
            // assign handler to new sticker added
            if let selectedStickerView = _selectedStickerView {
                selectedStickerView.showEditingHandlers = true
                //                selectedStickerView.superview?.bringSubviewToFront(selectedStickerView)
            }
        }
    }
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if mainProductID == ID.Phonecase
        {
            self.SetNavigationBarTitle(Startstring: "Cover Editor", EndString: "")
        }else
        {
            self.SetNavigationBarTitle(Startstring: "\(mainDictFromCoverCase["modalName"] ?? "")", EndString: "")
        }
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        bottomItemsArr = [UIImage(named: "images"),
                          UIImage(named: "add_text"),
                          UIImage(named: "sticker"),
                          UIImage(named: "bg_color"),
                          UIImage(named: "bg_asset")]
        
        self.tabBarController?.tabBar.isHidden = true
        
        if (StickerImage != nil) {
            
            AddStickertoView(Image: StickerImage , isSticker : true)
            StickerImage = nil
            
        }
        else if (StickerTextString != nil)
        {
            if StickerTextColor == nil
            {
                StickerTextColor = UIColor.black
            }
            if stickerFontString == nil {
                stickerFontString = CustomFontWeight.reguler
            }
            AddStickertoView(TextString: StickerTextString, TextColor: StickerTextColor, FontStyle: stickerFontString)
            StickerTextString = nil
            StickerTextColor = nil
            stickerFontString = nil
            
        }
        else if (StickerBackgroung != nil)
        {
            AddStickertoView(Image: StickerBackgroung , isSticker : false)
            StickerBackgroung = nil
        }else if instagramImage != nil
        {
            AddStickertoView(Image: instagramImage , isSticker : false)
            instagramImage = nil
        }
        
        // Enable or Disable Add Photo Button
        if ViewforCapture.subviews.count > 2 && !isFromViewDidLoad
        {
            btnAddPhoto.isHidden = true
        }else
        {
            btnAddPhoto.isHidden = false
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        Add_Footer_Label()
        
        if isFromViewDidLoad
        {
            isFromViewDidLoad = false
            self.viewDidLayout()
        }
    }
    
    func viewDidLayout() {
        
        //        let CoverImageURL = "\(modelImgpath)\(mainDictFromCoverCase["modal_image"] as! String)"
        
        var CoverImageURL : String!
        
        CoverImageURL = mainDictFromCoverCase["n_modal_image"] as? String ?? ""
        Cover_ImgView.sd_setShowActivityIndicatorView(false)
        Cover_ImgView.sd_setIndicatorStyle(.gray)
        Cover_ImgView.sd_setImage(with: URL(string: CoverImageURL), placeholderImage: nil, options: .refreshCached){ (img, error, catchType, url) in
            self.reloadViews()
        }
        
        
        //        if mainProductID != ID.Phonecase && mainProductID != nil
        //        {
        //            defaultImgView.image = UIImage(color: .clear)
        //        }
        
    }
    
    func reloadViews()
    {
        self.ViewforCapture.isHidden = false
        
        showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
        
        var ratio : CGFloat = 0.5
        if let image = Cover_ImgView.image {
            ratio = image.size.width / image.size.height
        }
        
        if Mask_ImgView.frame.width > Mask_ImgView.frame.height {
            let newHeight = Mask_ImgView.frame.width / ratio
            
            if newHeight > (ScreenSize.SCREEN_HEIGHT - 100)
            {
                Mask_ImgView.frame.size = CGSize(width: (ScreenSize.SCREEN_HEIGHT - 100) * ratio, height: (ScreenSize.SCREEN_HEIGHT - 100))
            }
            else
            {
                Mask_ImgView.frame.size = CGSize(width: Mask_ImgView.frame.width, height: newHeight)
            }
            
        }
        else{
            let newWidth = Mask_ImgView.frame.height * ratio
            
            if newWidth > (UIScreen.main.bounds.width - 20)
            {
                Mask_ImgView.frame.size = CGSize(width: (UIScreen.main.bounds.width - 20), height: (UIScreen.main.bounds.width - 20)/ratio)
            }
            else
            {
                Mask_ImgView.frame.size = CGSize(width: newWidth, height: Mask_ImgView.frame.height)
            }
        }
        
        
        captureSuperview.frame = Mask_ImgView.frame
        captureSuperview.layoutSubviews()
        
        let screenCenter = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height / 2)
        captureSuperview.center = screenCenter
        captureSuperview.center.y = screenCenter.y * 0.95
        
        if categoryId == 0 || FancyCoverID == 76
        {
            btnAddPhoto = UIButton(frame: CGRect(x: 0, y: 0 , width: 100, height: 100))
            btnAddPhoto.setImage(UIImage(named: "add_photo"), for: .normal)
            
            btnAddPhoto.center = CGPoint(x: ViewforCapture.frame.size.width  / 2,
                                         y: ViewforCapture.frame.size.height / 2)
            
            btnAddPhoto.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
            self.ViewforCapture.addSubview(btnAddPhoto)
            
            defaultImgView.image = UIImage(color: .clear)
        }else
        {
            if DefaultImgStr != nil{
                defaultImgView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: DefaultImgStr)
            }
        }
        
        //        let url = URL(string: "\(maskImgpath)\(mainDictFromCoverCase["mask_image"] as! String)")
        
        
        let url = URL(string: mainDictFromCoverCase["n_mask_image"] as! String)
        
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            RenderAtTransparentPart(image: UIImage(data: imageData)!.mask(with: .white), viewformask: ViewforCapture)
            
            // Managed transparent default Image (Crystal Keychain, Glass Photo Frame) and Cover Case in set clear color
            
            if mainProductID != ID.Phonecase
            {
                defaultImgView.image = UIImage(data: imageData)
            }
            
        }
        
        if fancyCaseImage != nil
        {
            AddStickertoView(Image: fancyCaseImage , isSticker : false)
            fancyCaseImage = nil
        }
                
        RemoveLoader()
        
        self.defaultLabel.isHidden = false
        Add_Footer_Label()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.isSetupDone = true
        }
        
    }
    
    func Add_Footer_Label() {
        
        // Create Bottom Default Label
        defaultLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(defaultLabel)
        defaultLabel.topAnchor.constraint(equalTo: self.captureSuperview.bottomAnchor, constant: 0).isActive = true
        defaultLabel.bottomAnchor.constraint(equalTo: self.bottomMenuCollView.topAnchor, constant: 0).isActive = true
        defaultLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        defaultLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        defaultLabel.textAlignment = .center
        defaultLabel.numberOfLines = 0
        
        if DeviceType.IS_IPAD
        {
            defaultLabel.font = UIFont(name: CustomFontWeight.medium, size: 22.0)
        }else
        {
            defaultLabel.font = UIFont(name: CustomFontWeight.medium, size: 15.0)
        }
        
        if mainProductID == ID.Phonecase
        {
            defaultLabel.text = "Please add high resolution image to get high clarity phone cover"
        }else{
            defaultLabel.text = "Please add high resolution image to get high clarity product"
        }
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        CromaColorPicker.delegate = self
        CromaColorPicker.backgroundColor = .white
        
        addBackButton()
        addRightButtons()
        
        checkRechabilityStatus()
        
        showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
        
        self.ViewforCapture.isHidden = true
        self.defaultLabel.isHidden = true
        
        //        mainDict = userDefault.value(forKey: "MainDict") as! [String : Any]
        
    }
    
    //MARK:- ********************** CHECK RECHABILITY STATUS **********************
    
    func checkRechabilityStatus()
    {
        let reachabilityManager = NetworkReachabilityManager()
        
        reachabilityManager?.startListening(onUpdatePerforming: { _ in
            if let isNetworkReachable = reachabilityManager?.isReachable,
                isNetworkReachable == true {
                //Internet Available
                print("Internet Available")
                
                if self.internetSpeed < 0.06
                {
                    self.RemoveLoader()
                    
                    self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                        self.popViewController()
                    })
                }else
                {
                    self.captureSuperview.isHidden = false
                }
                
            } else {
                //Internet Not Available"
                print("Internet Not Available")
                
                if !self.isSetupDone
                {
                    self.RemoveLoader()
                    self.captureSuperview.isHidden = true
                    self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                        self.popViewController()
                    })
                }
                
            }
        })
    }
    
    //MARK:- ********************** TOUCHES LIFECYCLE **********************
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch: UITouch? = touches.first
        if touch?.view != selectedStickerView {
            selectedStickerView?.showEditingHandlers = false
        }
        
    }
    
    //MARK:- ********************** BUTTON EVENTS **********************
    
    @IBAction func ExportImage_Pressed(_ sender: UIButton) {
        
        selectedStickerView?.showEditingHandlers = false
        
        //        CallApi(isPng: false)
        
    }
    
    @IBAction func AddSticker_Pressed(_ sender: UIButton) {
        
        //        let SticerVC = mainStoryBoard.instantiateViewController(withIdentifier: "StickerCollectionView") as! StickerCollectionView
        //        self.navigationController?.pushViewController(SticerVC, animated: true)
        
        
    }
    
    @IBAction func AddTextSticker_Pressed(_ sender: UIButton) {
        
        //        let AddText = mainStoryBoard.instantiateViewController(withIdentifier: "AddTextViewController") as! AddTextViewController
        //        self.navigationController?.pushViewController(AddText, animated: true)
        
    }
    
    @objc func buttonClicked() {
        OpenImagePicker()
        
    }
    
    //    @objc func doubleTapped(gesture: UITapGestureRecognizer) {
    //
    //        doubleTapStickerView?.frame.size = CGSize(width: 100.0, height: 100.0)
    //
    //    }
    
    func OpenImagePicker() {
        
        DispatchQueue.main.async {
            
            let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { action in
                
                DispatchQueue.main.async {
                    self.OpenCamera()
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: { action in
                
                DispatchQueue.main.async {
                    self.OpenGallery()
                }
                
            }))
            
            //            alert.addAction(UIAlertAction(title: "Choose from Instagram", style: .default, handler: { action in
            //
            //                let InstagramVC = mainStoryBoard.instantiateViewController(withIdentifier: "InstagramViewController") as! InstagramViewController
            //                self.pushViewController(InstagramVC)
            //
            //            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            }))
            
            // Add the actions
            self.picker.delegate = self
            self.picker.modalPresentationStyle = .overCurrentContext
            self.present(alert, animated: true)
            
            alert.view.tintColor = UIColor.black
        }
    }
    
    //MARK:- ********************** Don't Allowe Camera & Gallry Permission **********************
    
    //MARK:- Camera and gallery permission
    
    func requestCameraPermission() {
        
        DispatchQueue.main.async {
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                guard accessGranted == true else { return }
            })
        }
    }
    
    func alertCameraAccessNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Need Camera Access",
                message: "Camera access is required to capture photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func requestGalleryPermission() {
        
        DispatchQueue.main.async {
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                guard accessGranted == true else { return }
                
            })
        }
    }
    
    func alertGalleryAccessNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Need Gallery Access",
                message: "Gallery access is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Allow Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func alertGalleryAccessPermissionChangeNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Change Gallery Access",
                message: " Change Gallery access from Selected Photos to All Photos is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) -> Void in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Change Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
        if isLogin && FancyCoverID != 76
        {
            ((tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)
            self.tabBarController?.selectedIndex = 0
            
            isLogin = false
        }else
        {
            self.popViewController()
        }
    }
    
    func addRightButtons() {
        
        // Done Button
        let doneButton = UIButton(type: .custom)
        doneButton.setImage(UIImage(named: "ic_done"), for: .normal)
        doneButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        //        doneButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -25)
        doneButton.addTarget(self, action: #selector(self.addCartAction(_:)), for: .touchUpInside)
        let doneButtonItem = UIBarButtonItem(customView: doneButton)
        
        
        //        if userDefault.string(forKey: "RegionCode") == "IN"
        //        {
        // help Button
        let helpButton = UIButton(type: .custom)
        helpButton.setImage(UIImage(named: "ic_help"), for: .normal)
        //        helpButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        helpButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        helpButton.addTarget(self, action: #selector(self.helpAction(_:)), for: .touchUpInside)
        let helpButtonItem = UIBarButtonItem(customView: helpButton)
        
        if categoryId == 0
        {
            self.navigationItem.rightBarButtonItems = [doneButtonItem, helpButtonItem]
        }
        else
        {
            self.navigationItem.rightBarButtonItems = [doneButtonItem]
        }
        
        //        }else
        //        {
        //                self.navigationItem.rightBarButtonItems = [doneButtonItem]
        //        }
        
    }
    
    @objc func addCartAction(_ sender: UIButton) {
        if userDefault.value(forKey: "USER_ID") == nil || userDefault.integer(forKey: "USER_ID") == 0
        {
            self.presentAlertWithTitle(title: "Log In", message: "Need to log in first", options: "CANCEL","OK") { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.dismissViewController()
                }else
                {
                    isLoginOrNot = "edit_login"
                    
                    let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                    self.pushViewController(signinVC)
                    
                }
            }
        }else
        {
            _selectedStickerView?.showEditingHandlers = false
            
            if mainProductID == ID.Phonecase
            {
                if self.categoryId == 0 || FancyCoverID == 76
                {
                    if self.ViewforCapture.subviews.count > 2 || (!CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .clear)!) && !CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .white)!))
                    {
                        self.presentAlertWithTitle(title: "Add to cart?", message: "", options: "NO","YES") { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.dismissViewController()
                            }else
                            {
                                self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                                    
                                    (mainProductID == ID.Phonecase) ? self.Add_To_Cart_New() : self.Add_To_Cart_New1()
                                    
                                }
                            }
                        }
                    }else
                    {
                        self.presentAlertWithTitle(title: "Please select image", message: "", options: "OK") { (Int) in
                        }
                    }
                }else
                {
                    //                    if FancyCoverID == 76
                    //                    {
                    //                        // Fancy Phone case frame
                    //
                    //                        if self.ViewforCapture.subviews.count > 2 || (!CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .clear)!) && !CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .white)!))
                    //                        {
                    //                            self.presentAlertWithTitle(title: "Add to cart?", message: "", options: "NO","YES") { (ButtonIndex) in
                    //                                if ButtonIndex == 0
                    //                                {
                    //                                    self.dismissViewController()
                    //                                }else
                    //                                {
                    //                                    self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                    //                                    DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                    //                                        (mainProductID == ID.Phonecase) ? self.Add_To_Cart_New() : self.Add_To_Cart_New1()
                    //
                    //                                    }
                    //                                }
                    //                            }
                    //                        }else
                    //                        {
                    //                            self.presentAlertWithTitle(title: "Please select image", message: "", options: "OK") { (Int) in
                    //                            }
                    //                        }
                    //                    }else
                    //                    {
                    self.presentAlertWithTitle(title: "Add to cart?", message: "", options: "NO","YES") { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.dismissViewController()
                        }else
                        {
                            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                                (mainProductID == ID.Phonecase) ? self.Add_To_Cart_New() : self.Add_To_Cart_New1()
                                
                            }
                        }
                    }
                    //                    }
                }
            }else
            {
                // All Case without Set your photo index
                if self.ViewforCapture.subviews.count > 1 //|| (!CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .clear)!) && !CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .white)!))
                {
                    self.presentAlertWithTitle(title: "Add to cart?", message: "", options: "NO","YES") { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.dismissViewController()
                        }else
                        {
                            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                                (mainProductID == ID.Phonecase) ? self.Add_To_Cart_New() : self.Add_To_Cart_New1()
                            }
                        }
                    }
                }else
                {
                    self.presentAlertWithTitle(title: "Add image or stickers", message: "", options: "OK") { (Int) in
                    }
                }
            }
            
        }
    }
    
    func CompareImage (lhs: UIImage, rhs: UIImage) -> Bool
    {
        guard let data1 = lhs.pngData(),
            let data2 = rhs.pngData()
            else { return false }
        
        return data1 == data2
    }
    
    @objc func helpAction(_ sender: UIButton) {
        Help_Video()
    }
    
    
    //MARK:- ********************** IMAGE CAPTURE & IMAGE SIZE **********************
    
    func captureScreen(view:UIView) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0);
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func image(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //        let compressData = newImage?.jpegData(compressionQuality: 0.1)
        //        let compressedImage = UIImage(data: compressData!)!
        
        return newImage ?? UIImage()
    }
    
    func imageNew(with image: UIImage, scaledTo newSize: CGSize, completion: @escaping((UIImage) -> ())) -> () {
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            //        DispatchQueue.global(qos: .userInitiated).async {
            
            UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
            image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            //            DispatchQueue.main.async {
            //                let compressData = newImage?.jpegData(compressionQuality: 0.1)
            //                let compressedImage = UIImage(data: compressData!)!
            
            completion(newImage ?? UIImage())
            //            }
        }
    }
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bottomItemsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bottomTabbarCollCell", for: indexPath) as! bottomTabbarCollCell
        
        cell.ButtonImageView.image = bottomItemsArr[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            OpenImagePicker()
            
        }
        else if indexPath.row == 1 {
            
            let AddText = mainStoryBoard.instantiateViewController(withIdentifier: "AddTextViewController") as! AddTextViewController
            self.pushViewController(AddText)
        }
        else if indexPath.row == 2
        {
            guard !isLoaded else {
                return
            }
            
            isLoaded = true
            
            isSelectSticker = "EditingVC"
            let mainsticVC = mainStoryBoard.instantiateViewController(withIdentifier: "mainStickerViewController") as! mainStickerViewController
            pushViewController(mainsticVC)
            isLoaded = false
        }
        else if indexPath.row == 3
        {
            let TFPopup = MZFormSheetPresentationViewController.init(contentView: CromaVIew)
            TFPopup.presentationController?.contentViewSize = CGSize(width: self.view.Getwidth*(DeviceType.IS_IPAD ? 0.4 : 0.8), height: self.view.Getwidth*(DeviceType.IS_IPAD ? 0.4 : 0.8))
            TFPopup.presentationController?.shouldCenterVertically = true
            TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
            self.present(TFPopup, animated: true)
        }
        else if indexPath.row == 4
        {
            guard !isLoaded else {
                return
            }
            
            isLoaded = true
            
            let BGimagesVC = mainStoryBoard.instantiateViewController(withIdentifier: "BackgroundImagesViewController") as! BackgroundImagesViewController
            pushViewController(BGimagesVC)
            isLoaded = false
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.Getwidth / 5, height: DeviceType.IS_IPAD ? 100.0 : 50.0)
    }
    
    
    //MARK:- ********************** OTHER METHODS **********************
    
    func OpenCamera()
    {
        
        DispatchQueue.main.async {
            
            self.picker = UIImagePickerController()
            self.picker.delegate = self
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                //check permission
                if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                    //already authorized, open camera
                    DispatchQueue.main.async {
                        self.picker.sourceType = .camera
                        self.present(self.picker, animated: true)
                    }
                }
                    
                else if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
                    //request for camera permission
                    self.requestCameraPermission()
                    
                }
                else
                {
                    AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                        if granted {
                            //access allowed
                            
                            DispatchQueue.main.async {
                                self.picker.sourceType = .camera
                                self.present(self.picker, animated: true)
                            }
                            
                        } else {
                            //access denied or restricted
                            self.alertCameraAccessNeeded()
                        }
                    })
                }
                
            } else {
                //if camera is not available then show alert
                let alert = UIAlertController(title: "Alert", message: "No Camera Available.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true)
            }
        }
        
    }
    
    func OpenGallery()
    {
        DispatchQueue.main.async {
            
            //check user has allowed permission or not
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    
                    DispatchQueue.main.async {
                        self.picker = UIImagePickerController()
                        self.picker.delegate = self
                        self.picker.sourceType = .photoLibrary
                        self.present(self.picker, animated: true)
                    }
                    
                    break
                case .denied, .restricted:
                    self.alertGalleryAccessNeeded()
                    break
                case .notDetermined:
                    self.requestGalleryPermission()
                    break
                case .limited :
                    self.alertGalleryAccessPermissionChangeNeeded()
                default:
                    break
                }
                
            }
        }
    }
    
    //    func CallApi(isPng : Bool) {
    //
    //        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
    //
    //        let ApiUrl = "https://printphoto.in/Photo_case/api/demoImage"
    //
    ////        let imageData = isPng ? UIImage(view: ViewforCapture).pngData() : UIImage(view: ViewforCapture).jpegData(compressionQuality: 1.0)
    //
    //        let image123 = image(with: ViewforCapture.getImage(scale: 5.0), scaledTo: CGSize(width: 1188, height: 2064))
    //
    //        let image1 = UIImage(cgImage: image123.cgImage!, scale: 1.0, orientation: .upMirrored)
    //
    ////        let image123 = ViewforCapture.getImage(scale: 7.0)
    //        let imageData = image1.jpegData(compressionQuality: 1.0)
    //
    //        RenderAtTransparentPart(image: UIImage(named: "note5promask")!, viewformask: ViewforCapture)
    //
    //        manager.post(ApiUrl, parameters: nil, constructingBodyWith: { (formData) in
    //            var str = ProcessInfo.processInfo.globallyUniqueString
    //            str = isPng ? str.appending(".png") : str.appending(".jpg")
    //
    //
    //            formData.appendPart(withFileData: imageData!, name: "image", fileName: str, mimeType: "image/jpeg")
    //
    //
    //        }, progress: { (uploadProgress) in
    //
    //
    //        }, success: { (task, responseObject) in
    //
    //
    //            let predct = responseObject as! NSDictionary
    //
    //            print(predct)
    //
    //            self.activityIndicator.hide()
    //
    //            self.presentAlertWithTitle(title: "", message: "Press OK to watch image", options: "OK", completion: { (AlertAction) in
    //                if AlertAction == 0
    //                {
    //                    guard let url = URL(string: predct.value(forKeyPath: "data.image") as! String) else {
    //                        return
    //                    }
    //
    //                    if #available(iOS 10.0, *) {
    //                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    //                    } else {
    //                        UIApplication.shared.openURL(url)
    //                    }
    //                }
    //            })
    //
    //
    //
    //
    //
    //        }, failure: { (task, error) in
    //
    //            self.activityIndicator.hide()
    //            print(error)
    //
    //            self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (AlertAction) in
    //
    //            })
    //
    //
    //        })
    //
    //    }
    
    func RenderAtTransparentPart(image: UIImage, viewformask : UIView) {
        
        let cim = CIImage(image: image)
        let filter = CIFilter(name:"CIMaskToAlpha")!
        filter.setValue(cim, forKey: "inputImage")
        let out = filter.outputImage!
        let cgim = CIContext().createCGImage(out, from: out.extent)
        let lay = CALayer()
        lay.frame = viewformask.bounds
        lay.contents = cgim
        viewformask.layer.mask = lay
        
    }
    
    func AddStickertoView(Image : UIImage, isSticker : Bool) {
        
        let StickerImgView = UIImageView(image: Image)
        StickerImgView.isUserInteractionEnabled = true
        
        var ratioSize : CGFloat?
        
        if isSticker
        {
            ratioSize = 0.4
        }else
        {
            ratioSize = 1.2
        }
        
        let ratio = Image.size.width / Image.size.height
        
        if StickerImgView.frame.width > ViewforCapture.frame.height * ratioSize! {
            let newHeight = ViewforCapture.frame.width / ratio
            StickerImgView.frame.size = CGSize(width: ViewforCapture.frame.width * ratioSize!, height: newHeight * ratioSize!)
        }
        else{
            let newWidth = ViewforCapture.frame.height * ratio
            StickerImgView.frame.size = CGSize(width: newWidth * ratioSize!, height: ViewforCapture.frame.height * ratioSize!)
        }
        
        let StickerViewforAdd = StickerView.init(contentView: StickerImgView)
        StickerViewforAdd.delegate = self
        StickerViewforAdd.setImage(UIImage.init(named: "Close")!, forHandler: StickerViewHandler.close)
        StickerViewforAdd.enableRotate = false
        StickerViewforAdd.enableFlip = false
        StickerViewforAdd.setHandlerSize(25)
        StickerViewforAdd.outlineBorderColor = UIColor.darkGray
        StickerViewforAdd.showEditingHandlers = false
        
        if (StickerBackgroung != nil)
        {
            ViewforCapture.insertSubview(StickerViewforAdd, at: 1)
        }else
        {
            ViewforCapture.addSubview(StickerViewforAdd)
        }
        
        StickerViewforAdd.center = CGPoint(x: ViewforCapture.Getwidth * 0.5, y: ViewforCapture.Getheight * 0.5)
        
        if self.ViewforCapture.subviews.count > 2 && !self.isFromViewDidLoad
        {
            self.btnAddPhoto.isHidden = true
        }else
        {
            self.btnAddPhoto.isHidden = false
        }
        
        //        doubleTapStickerView = StickerViewforAdd
        //
        //        // Zoom Image when Double Tap on Gesture
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped(gesture:)))
        //        tap.numberOfTapsRequired = 2
        //        doubleTapStickerView?.addGestureRecognizer(tap)
    }
    
    func AddStickertoView(TextString : String , TextColor : UIColor ,FontStyle : String) {
        
        let StickerLbl = UILabel()
        StickerLbl.text = TextString
        StickerLbl.font = mainProductID == 129 ? UIFont.init(name: FontStyle, size: ViewforCapture.Getwidth * 0.60) : UIFont.init(name: FontStyle, size: ViewforCapture.Getwidth * 0.15)
        StickerLbl.textColor = TextColor
        StickerLbl.textAlignment = .center
        StickerLbl.sizeToFit()
        
        //        StickerLbl.frame = CGRect(x: StickerLbl.Getx + 25.0, y: StickerLbl.Gety + 25.0, width: StickerLbl.Getwidth + 100.0, height: StickerLbl.Getheight + 100.0)
        
        StickerLbl.frame = CGRect(x: 0.0, y: 0.0, width: StickerLbl.Getwidth + 50.0, height: StickerLbl.Getheight + 50.0)
        
        print(StickerLbl.text?.heightOfLabel(withConstrainedWidth: StickerLbl.Getwidth, font: StickerLbl.font) ?? "")
        
        let StickerViewforAdd = StickerView.init(contentView: StickerLbl)
        StickerViewforAdd.delegate = self
        StickerViewforAdd.setImage(UIImage.init(named: "Close")!, forHandler: StickerViewHandler.close)
        StickerViewforAdd.enableRotate = false
        StickerViewforAdd.enableFlip = false
        StickerViewforAdd.isLabel = true
        StickerViewforAdd.setHandlerSize(25)
        StickerViewforAdd.outlineBorderColor = UIColor.darkGray
        StickerViewforAdd.showEditingHandlers = false
        
        ViewforCapture.addSubview(StickerViewforAdd)
        
        StickerViewforAdd.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5) //(StickerViewforAdd.transform, 0.1, 0.1)
        //        StickerViewforAdd.setHandlerSize(DeviceType.IS_IPAD ? 25 : 18)
        
        StickerViewforAdd.center = CGPoint(x: ViewforCapture.Getwidth * 0.5, y: ViewforCapture.Getheight * 0.5)
    }
    
    //MARK:- ********************** IMAGEPICKERVIEW DELEGATE **********************
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if info[.imageURL] != nil && !((info[.imageURL] as? URL)?.absoluteString.uppercased().hasSuffix("GIF"))!{
            
            if let asset = info[.phAsset] as? PHAsset {
                PHImageManager.default().requestImageData(for: asset, options: nil) { (dataObj, uti, orientation, information) in
                    
                    //                    print("File Size \(nsdataObj.length/1000) KB")
                    
                    let heightInPoints = (info[.originalImage] as! UIImage).size.height
                    let heightInPixels = heightInPoints * (info[.originalImage] as! UIImage).scale
                    
                    let widthInPoints = (info[.originalImage] as! UIImage).size.width
                    let widthInPixels = widthInPoints * (info[.originalImage] as! UIImage).scale
                    
                    /*
                    if heightInPixels <= 720 || widthInPixels <= 720 //nsdataObj.length/1024 < 500
                    {
                        self.topMostViewController().view.makeToast("Please select high resolution image", duration: 2.0, position: .bottom)
                    }else
                    {
                    */
                        self.dismiss(animated: true) {
                            self.AddStickertoView(Image: info[.originalImage] as! UIImage , isSticker : false)
                            
                            // Enable or Disable Add Photo Button
                            
                            if self.ViewforCapture.subviews.count > 2 && !self.isFromViewDidLoad
                            {
                                self.btnAddPhoto.isHidden = true
                            }else
                            {
                                self.btnAddPhoto.isHidden = false
                            }
                        }
                    //}
                }
            }
        }
        else
        {
            self.AddStickertoView(Image: info[.originalImage] as! UIImage , isSticker : false)
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    //MARK:- ********************** CROMA COLOR PICKER DELEGATE **********************
    
    func colorPickerDidChooseColor(_ colorPicker: ChromaColorPicker, color: UIColor) {
        //Set color for the display view
        
        defaultImgView.image = UIImage(color: color)
        
        //        ViewforCapture.backgroundColor = color
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- ********************** STICKER DELEGATE **********************
    
    func stickerViewDidBeginMoving(_ stickerView: StickerView) {
        self.selectedStickerView = stickerView
    }
    
    func stickerViewDidChangeMoving(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidEndMoving(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidBeginRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidChangeRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidEndRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidClose(_ stickerView: StickerView) {
        
        if (self.ViewforCapture.subviews.count > 3)
        {
            self.btnAddPhoto.isHidden = true
        }else
        {
            self.btnAddPhoto.isHidden = false
        }
        
    }
    
    func stickerViewDidTap(_ stickerView: StickerView) {
        self.selectedStickerView = stickerView
    }
    
    
    //MARK:- ********************** API CALLING **********************
    
    func Add_To_Cart_New() {
        if Reachability.isConnectedToNetwork() {
            
            self.btnAddPhoto.isHidden = true
            
            let isMasked = false
            
            let scaleSize = CGSize(width: Int(((self.mainDictFromCoverCase["width"] as! NSString).doubleValue * 300).rounded()), height: Int(((self.mainDictFromCoverCase["height"] as! NSString).doubleValue * 300).rounded()))
            
            let croppedAlphaImg = self.ViewforCapture.getImage(scale: 5.0, isMasked: isMasked).cropAlpha()
            
            // Print Image
            self.imageNew(with: croppedAlphaImg, scaledTo: scaleSize) { (PrintImage) in
                
                let printMirrorImage = UIImage(cgImage: PrintImage.cgImage!, scale: 1.0, orientation: .upMirrored)
                
                printMirrorImage.rotateNew(radians: -.pi/2) { (rotatePrintImage) in
                    
                    let imgPrintRotate = (mainProductID == ID.Phonecase) ? rotatePrintImage : (rotatePrintImage?.rotate(radians: .pi/2)) //printMirrorImage
                    
                    // Covercase Image
                    let imgCover = self.captureSuperview.getImage(scale: 5.0 , isMasked: isMasked)
                    
                    self.imageNew(with: imgCover, scaledTo: scaleSize) { (CoverImage) in
                        
                        CoverImage.rotateNew(radians: -.pi/2) { (rotateCoverImage) in
                            
                            let imgCoverRotate = rotateCoverImage
                            
                            var modelID = ""
                            
                            //                            if mainProductID == ID.Phonecase
                            //                            {
                            modelID = "\(self.mainDictFromCoverCase["model_id"] ?? "")"
                            //                            }else
                            //                            {
                            //                                modelID = "\(self.mainDictFromCoverCase["id"] ?? "")"
                            //                            }
                            
                            let ParamDict = ["model_id": modelID,
                                             "user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                                "quantity": "1"] as NSDictionary
                            
                            AFNetworkingAPIClient.sharedInstance.postAPICallWithMultipleImages(url: "\(BaseURL)add_to_cart", parameters: ParamDict, imageArray: [imgPrintRotate ?? UIImage(),imgCoverRotate ?? UIImage()], imgKey: ["print_image","case_image"]) { (APIResponce, Responce, error1) in
                                
                                self.RemoveLoader()
                                
                                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                                {
                                    let responceDict = Responce! as NSDictionary
                                    
                                    if responceDict["ResponseCode"] as! String == "1"
                                    {
                                        //                                        let dataArray = responceDict.value(forKeyPath: "data.cart.cart_items") as? NSArray
                                        //
                                        //                                        let arr = dataArray?.filter({ (obj) -> Bool in
                                        //
                                        //                                            let objDict = obj as! NSDictionary
                                        //
                                        //                                            let model_id = objDict.value(forKey: "model_id") as? String ?? ""
                                        //
                                        //                                            if model_id == modelID
                                        //                                            {
                                        //                                                let model_name = objDict.value(forKey: "model_name") as? String ?? ""
                                        //                                                let price = objDict.value(forKey: "subtotal") as? Int ?? 0
                                        
                                        self.logAddToCartEvent(contentData: "\(self.mainDictFromCoverCase["modalName"] ?? "")", contentId: modelID, contentType: mainProductName ?? "", currency: "INR", price: Double("\(self.mainDictFromCoverCase?["price"] ?? "")") ?? 0.0)
                                        
                                        //                                                return true
                                        //                                            }
                                        //
                                        //                                            return false
                                        //
                                        //                                        })
                                        
                                        isCartBtnBack = false
                                        
                                        ((self.tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
                                        
                                        self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 1 : 2
                                        
                                    }
                                    else
                                    {
                                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                                            
                                        })
                                    }
                                }
                                else
                                {
                                    self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
                                        //                                        self.Add_To_Cart_New()
                                    })
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                //self.Add_To_Cart_New()
            })
        }
    }
    
    func Add_To_Cart_New1() {
        if Reachability.isConnectedToNetwork() {
            
            self.btnAddPhoto.isHidden = true
            
            let isMasked = true
            
            let scaleSize = CGSize(width: Int(((self.mainDictFromCoverCase["width"] as! NSString).doubleValue * 300).rounded()), height: Int(((self.mainDictFromCoverCase["height"] as! NSString).doubleValue * 300).rounded()))
            
            var croppedAlphaImg = UIImage()
            
            // Check MainID for Wrist Watch category // "117"
            if selectedMainProductId == 117
            {
                // Only for Watch category because not proper view watch image (tringle shap etc.)
                croppedAlphaImg = self.captureSuperview.getImage(scale: 5.0, isMasked: isMasked).cropAlpha()
            }else
            {
                croppedAlphaImg = self.ViewforCapture.getImage(scale: 5.0, isMasked: isMasked).cropAlpha()
            }
            
            // Print Image
            self.imageNew(with: croppedAlphaImg, scaledTo: scaleSize) { [weak self] (PrintImage) in
                
                let printMirrorImage = UIImage(cgImage: PrintImage.cgImage!, scale: 1.0, orientation: .upMirrored)
                
                printMirrorImage.rotateNew(radians: -.pi/2) { [weak self] (rotatePrintImage) in
                    
                    let imgPrintRotate = (rotatePrintImage?.rotate(radians: .pi/2))
                    
                    // Covercase Image
                    var imgCover: UIImage!
                    if mainProductID == ID.WallWoodenCanvasFrame
                    {
                        imgCover = self!.captureSuperview.getImage(scale: 5.0 , isMasked: isMasked).cropAlpha()
                    }
                    else
                    {
                        imgCover = self!.captureSuperview.getImage(scale: 5.0 , isMasked: isMasked)
                    }
                    
                    let imgCoverRotate = imgCover
                    
                    let modelID = "\(self!.mainDictFromCoverCase["model_id"] ?? "")"
                    
                    let ParamDict = ["model_id": modelID,
                                     "user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                        "quantity": "1"] as NSDictionary
                    
                    AFNetworkingAPIClient.sharedInstance.postAPICallWithMultipleImages(url: "\(BaseURL)add_to_cart", parameters: ParamDict, imageArray: [imgPrintRotate ?? UIImage(),imgCoverRotate ?? UIImage()], imgKey: ["print_image","case_image"]) { (APIResponce, Responce, error1) in
                        
                        if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                        {
                            self!.RemoveLoader()
                            
                            let responceDict = Responce! as NSDictionary
                            
                            if responceDict["ResponseCode"] as! String == "1"
                            {
                                //                                let dataArray = responceDict.value(forKeyPath: "data.cart.cart_items") as? NSArray
                                //
                                //                                let arr = dataArray?.filter({ (obj) -> Bool in
                                //
                                //                                    let objDict = obj as! NSDictionary
                                //
                                //                                    let model_id = objDict.value(forKey: "model_id") as? String ?? ""
                                //
                                //                                    if model_id == modelID
                                //                                    {
                                //                                        let model_name = objDict.value(forKey: "model_name") as? String ?? ""
                                //                                        let price = objDict.value(forKey: "subtotal") as? Int ?? 0
                                
                                self?.logAddToCartEvent(contentData: "\(self?.mainDictFromCoverCase["modalName"] ?? "")", contentId: modelID, contentType: mainProductName ?? "", currency: "INR", price: Double("\(self?.mainDictFromCoverCase?["price"] ?? "")") ?? 0.0)
                                
                                //                                        return true
                                //                                    }
                                //
                                //                                    return false
                                //
                                //                                })
                                
                                isCartBtnBack = false
                                
                                ((self!.tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
                                
                                self!.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 1 : 2
                                
                            }
                            else
                            {
                                self!.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                                    
                                })
                            }
                        }
                        else
                        {
                            self!.RemoveLoader()
                            
                            self!.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
                                //                                self!.Add_To_Cart_New1()
                            })
                        }
                    }
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                //                self.Add_To_Cart_New1()
            })
        }
    }
    
    
    
    //    func Add_To_Cart_New() {
    //        if Reachability.isConnectedToNetwork() {
    //
    //            self.btnAddPhoto.isHidden = true
    //
    //            let isMasked = mainProductID == 1 ? false : true
    //
    //            let scaleSize = CGSize(width: Int(((self.mainDictFromCoverCase["width"] as! NSString).doubleValue * 300).rounded()), height: Int(((self.mainDictFromCoverCase["height"] as! NSString).doubleValue * 300).rounded()))
    //
    //            let croppedAlphaImg = self.ViewforCapture.getImage(scale: 5.0, isMasked: isMasked).cropAlpha()
    //
    //            // Print Image
    //
    //            self.imageNew(with: croppedAlphaImg, scaledTo: scaleSize) { (PrintImage) in
    //
    //                let printMirrorImage = UIImage(cgImage: PrintImage.cgImage!, scale: 1.0, orientation: .upMirrored)
    //
    //                printMirrorImage.rotateNew(radians: -.pi/2) { (rotatePrintImage) in
    //
    //                    let imgPrintRotate = (mainProductID == 1) ? rotatePrintImage : (rotatePrintImage?.rotate(radians: .pi/2)) //printMirrorImage
    //
    //                    // Covercase Image
    //                    let imgCover = mainProductID == 27 ? self.captureSuperview.getImage(scale: 5.0 , isMasked: isMasked).cropAlpha() : self.captureSuperview.getImage(scale: 5.0 , isMasked: isMasked)
    //
    //                    self.imageNew(with: imgCover, scaledTo: scaleSize) { (CoverImage) in
    //
    //                        CoverImage.rotateNew(radians: -.pi/2) { (rotateCoverImage) in
    //
    //                            let imgCoverRotate = (mainProductID == 1) ? rotateCoverImage : mainProductID == 8 ? imgCover : CoverImage //(rotateCoverImage?.rotate(radians: .pi/2))
    //
    //                            var modelID = ""
    //
    //                            if mainProductID == 1
    //                            {
    //                                modelID = "\(self.mainDictFromCoverCase["model_id"] ?? "")"
    //                            }else
    //                            {
    //                                modelID = "\(self.mainDictFromCoverCase["id"] ?? "")"
    //                            }
    //
    //                            let ParamDict = ["model_id": modelID,
    //                                             "user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
    //                                            "quantity": "1"] as NSDictionary
    //
    //                            AFNetworkingAPIClient.sharedInstance.postAPICallWithMultipleImages(url: "\(BaseURL)add_to_cart", parameters: ParamDict, imageArray: [imgPrintRotate ?? UIImage(),imgCoverRotate ?? UIImage()], imgKey: ["print_image","case_image"]) { (APIResponce, Responce, error1) in
    //
    //                                self.RemoveLoader()
    //
    //                                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
    //                                {
    //
    //                                    let responceDict = Responce as! [String:Any]
    //
    //                                    if responceDict["ResponseCode"] as! String == "1"
    //                                    {
    //                                        isCartBtnBack = false
    //
    //                                        ((self.tabBarController!.viewControllers! as NSArray)[3] as! UINavigationController).popToRootViewController(animated: false)
    //
    //                                        self.tabBarController?.selectedIndex = 3
    //
    //                                    }
    //                                    else
    //                                    {
    //                                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
    //
    //                                        })
    //                                    }
    //                                }
    //                                else
    //                                {
    //                                    self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
    //
    //                                    })
    //                                }
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        else
    //        {
    //            self.RemoveLoader()
    //
    //            self.presentAlertWithTitle(title: "Internet is slow. Please check internet connection.", message: "", options: "OK", completion: { (ButtonIndex) in
    //
    //            })
    //        }
    //    }
    
    //    func Add_To_Cart() {
    //        if Reachability.isConnectedToNetwork() {
    //
    //            self.btnAddPhoto.isHidden = true
    //
    //            let isMasked = mainProductID == ID.Phonecase ? false : true
    //
    //            let scaleSize = CGSize(width: Int((self.mainDictFromCoverCase["width"] as! NSString).doubleValue * 300), height: Int((self.mainDictFromCoverCase["height"] as! NSString).doubleValue * 300))
    //
    //            let croppedAlphaImg = self.ViewforCapture.getImage(scale: 3.0, isMasked: isMasked).cropAlpha()
    //
    //            // Print Image
    //            let PrintImage = self.image(with: croppedAlphaImg, scaledTo: scaleSize)
    //            let printMirrorImage = UIImage(cgImage: PrintImage.cgImage!, scale: 1.0, orientation: .upMirrored)
    //            let rotatePrintImage =  printMirrorImage.rotate(radians: -.pi/2) // mainProductID == ID.Phonecase ? : printMirrorImage
    //            let imgPrintRotate = (mainProductID == ID.Phonecase) ? rotatePrintImage : (rotatePrintImage?.rotate(radians: .pi/2))
    //
    //            // Covercase Image
    //            let imgCover = self.captureSuperview.getImage(scale: 3.0 , isMasked: isMasked).cropAlpha()
    //            let CoverImage = self.image(with: imgCover, scaledTo: scaleSize)
    //            //            let coverCaseImage = self.captureScreen(view: captureSuperview)
    //            //            let coverImage = self.image(with: coverCaseImage, scaledTo: CoverImage)
    //            let rotateCoverImage = CoverImage.rotate(radians: -.pi/2)
    //            let imgCoverRotate = (mainProductID == ID.Phonecase) ? rotateCoverImage : CoverImage //(rotateCoverImage?.rotate(radians: .pi/2))
    //
    //            var modelID = ""
    //
    ////            if mainProductID == ID.Phonecase
    ////            {
    //                modelID = "\(self.mainDictFromCoverCase["model_id"] ?? "")"
    ////            }else
    ////            {
    ////                modelID = "\(self.mainDictFromCoverCase["id"] ?? "")"
    ////            }
    //
    //            let ParamDict = ["model_id": modelID,
    //                            "user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
    //                            "quantity": "1"] as NSDictionary
    //
    //            AFNetworkingAPIClient.sharedInstance.postAPICallWithMultipleImages(url: "\(BaseURL)add_to_cart", parameters: ParamDict, imageArray: [imgPrintRotate ?? UIImage(),imgCoverRotate ?? UIImage()], imgKey: ["print_image","case_image"]) { (APIResponce, Responce, error1) in
    //
    //                self.RemoveLoader()
    //
    //                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
    //                {
    //
    //                    let responceDict = Responce as! [String:Any]
    //
    //                    if responceDict["ResponseCode"] as! String == "1"
    //                    {
    //                        isCartBtnBack = false
    //
    //                        ((self.tabBarController!.viewControllers! as NSArray)[3] as! UINavigationController).popToRootViewController(animated: false)
    //
    //                        self.tabBarController?.selectedIndex = 3
    //
    //                    }
    //                    else
    //                    {
    //                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
    //
    //                        })
    //                    }
    //                }
    //                else
    //                {
    //                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "OK", completion: { (ButtonIndex) in
    //
    //                    })
    //
    //                }
    //            }
    //        }
    //        else
    //        {
    //            self.RemoveLoader()
    //
    //            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
    //
    //            })
    //        }
    //    }
    
    //MARK:- ********************** FACEBOOK ANALYTICS **********************
    
    /**
     * For more details, please take a look at:
     * developers.facebook.com/docs/swift/appevents
     */
    
    func logAddToCartEvent(contentData : String, contentId : String, contentType : String, currency : String, price : Double)
    {
        // Firebase Add To Cart Analytics
        
        Analytics.logEvent("add_to_cart", parameters: [
            AnalyticsParameterItemID: contentId,
            AnalyticsParameterItemName: contentData,
            AnalyticsParameterContentType: contentType,
            AnalyticsParameterPrice:price,
            AnalyticsParameterCurrency:currency
            ])
        
        // Facebook Add To Cart Analytics
        
        let params = [
            "item_id" : contentId, //"contentId"
            "item_name" : contentData, //"content"
            "content_type" : contentType, //contentType
            "price": price,
            "currency" : currency //currency
            ] as [String : Any]
        
        AppEvents.logEvent(.addedToCart, valueToSum: price, parameters: params)
    }
    
}

