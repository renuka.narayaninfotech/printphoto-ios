//
//  AddTextViewController.swift
//  Original_ImagesendDemo
//
//  Created by des on 22/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import ChromaColorPicker
import MZFormSheetPresentationController

class AddTextViewController: BaseViewController , ChromaColorPickerDelegate , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout , UICollectionViewDataSource,UITextFieldDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var Text_TF: KTextField!
    
    @IBOutlet var ChangeColorBtn: UIButton!
    
    @IBOutlet var FontStyleCollectionView: UICollectionView!
    
    @IBOutlet var CromaColorPicker: ChromaColorPicker!
    
    @IBOutlet var CromaVIew: UIView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var FontName = ["I Love You Monkey (Hearted)",
                    "Operating instructions",
                    "Vtks Focus",
                    "Aubrey",
                    "Spring",
                    "Janda Stylish Monogram",
                    "BouclettesDemo",
                    "Donree's Claws",
                    "SWEETCORRECTION ROTH",
                    "Curely",
                    "Rebecca",
                    "KR Floral Script",
                    "GANGLAND",
                    "Arellion",
                    "Raven Script DEMO",
                    "False Advertising",
                    "Sail",
                    "Batik Gangster",
                    "Fiddums Family",
                    "PWHappyNewYear",
                    "Love Letters",
                    "Kingthings Willow",
                    "Antherton Cloister",
                    "Feetish",
                    "KR Original Bean",
                    "Dark Garden",
                    "FoglihtenNo04",
                    "GlukMixer",
                    "unchanged thoughts$",
                    "OutOfAfrica",
                    "LoveNess",
                    "Nemo",
                    "Pyktor",
                    "Lovers Quarrel",
                    "Modeschrift"]
    
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetNavigationBarTitle(Startstring: "Add Font", EndString: "")
        
        Initialize()
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        addBackButton()
        addDoneBtn()
        CromaColorPicker.delegate = self
        Text_TF.delegate = self
        CromaColorPicker.backgroundColor = .white
        Text_TF.returnKeyType = .done
    }
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return FontName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddtextCollCell", for: indexPath) as! AddtextCollCell
        
        cell.HelloTextLbl.font = UIFont.init(name: FontName[indexPath.row], size: DeviceType.IS_IPAD ? 40 : 20)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        stickerFontString = FontName[indexPath.row]
        
        Text_TF.font = UIFont.init(name: FontName[indexPath.row], size: DeviceType.IS_IPAD ? 35 : 20)
        Text_TF.attributedPlaceholder = NSAttributedString(string: "Enter Text",
                                                           attributes: [NSAttributedString.Key.font : UIFont.init(name: FontName[indexPath.row], size: DeviceType.IS_IPAD ? 35 : 20)!])
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.Getwidth / 3, height: collectionView.Getwidth / 5 )
    }
    
    
    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addDoneBtn() {
        
        let donebutton = UIButton()
        donebutton.setImage(UIImage(named: "ic_done"), for: .normal)
        donebutton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        donebutton.addTarget(self, action:#selector(DonePressed(_:)), for: .touchUpInside)
        let rightbarButton = UIBarButtonItem(customView: donebutton)
        self.navigationItem.rightBarButtonItem = rightbarButton
        
    }
    
    //MARK:- ********************** BUTTON EVENTS **********************
    
    @IBAction func DonePressed(_ sender: UIBarButtonItem) {
        
        if Validation.isStringEmpty(Text_TF.text!) || Text_TF.text?.trim() == ""{
            presentAlertWithTitle(title: "Please enter text", message: "", options: "OK") { (Int) in
                self.Text_TF.becomeFirstResponder()
            }
        }else{
            self.view.endEditing(true)
            StickerTextString = Text_TF.text!
            self.popViewController()
        }
        
    }
    
    @IBAction func ChangeColor_Pressed(_ sender: UIButton) {
        
        let TFPopup = MZFormSheetPresentationViewController.init(contentView: CromaVIew)
        TFPopup.presentationController?.contentViewSize = CGSize(width: self.view.Getwidth*0.5, height: self.view.Getwidth*0.5)
        TFPopup.presentationController?.shouldCenterVertically = true
        TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
        self.present(TFPopup, animated: true)
        
    }
    
    //MARK:- ********************** CROMA COLOR PICKER DELEGATE **********************
    
    func colorPickerDidChooseColor(_ colorPicker: ChromaColorPicker, color: UIColor) {
        
        Text_TF.textColor = color
        StickerTextColor = color
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- **********************  TEXTFIELD DELEGATE METHOD **********************
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return Text_TF.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        textField.autocorrectionType = .no
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if textField.tag == 1
        {
            if newLength <= 20
            {
                return true
            } else {
                return false
            }
        }
        return true
    }
    
}
