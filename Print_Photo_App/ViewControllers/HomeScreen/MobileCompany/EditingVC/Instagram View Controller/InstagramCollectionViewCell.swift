//
//  InstagramCollectionViewCell.swift
//  Print_Photo_App
//
//  Created by Prakash on 04/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class InstagramCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageInstagram: UIImageView!
}
