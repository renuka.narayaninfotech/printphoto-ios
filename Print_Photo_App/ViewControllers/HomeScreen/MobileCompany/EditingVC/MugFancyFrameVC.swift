//
//  MugFancyFrameVC.swift
//  Print_Photo_App
//
//  Created by Mac Os on 14/08/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import Photos
import AssetsPickerViewController

class MugFancyFrameVC: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var mugFancyFrameCollectionView: UICollectionView!
    
    var arrMugFrame = [[String:Any]]()
    var mainDictFromCoverCase : [String:Any]!
    var internetSpeed : Double = 0
    var FancyFrameIndex : Int!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if mainProductID == ID.Mug
        {
            SetNavigationBarTitle(Startstring: "Mug Fancy Images", EndString: "")
        }else
        {
            SetNavigationBarTitle(Startstring: "Sipper Bottle Fancy Images", EndString: "")
        }
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        addBackButton()
        removeBackButton()
        
        CallApi()
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrMugFrame.count + 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MugFancyFrameCollectionViewCell", for: indexPath) as! MugFancyFrameCollectionViewCell
        
        if indexPath.row == 0
        {
            cell.imgFrame.image = mainProductID == ID.Mug ? UIImage(named: "ic_mug_addPhoto") : UIImage(named: "ic_sipper_fancyFrame")
        }else
        {

            let FrameDict = self.arrMugFrame[indexPath.row - 1] as NSDictionary
            
            let imgUrl = "\(FrameDict["image"] as? String ?? "")"
            
            if imgUrl != ""
            {
                cell.imgFrame.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: imgUrl)
            }
            else{
                cell.imgFrame.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: "tempString")
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            if ((self.mainDictFromCoverCase["n_mask_image"] as? String ?? "") != "") || ((self.mainDictFromCoverCase["n_modal_image"] as? String ?? "") != "")
            {
                isSelectZeroIndex = true
                
                if mainProductID == ID.Mug
                {
                    let MugEditingVC = mainStoryBoard.instantiateViewController(withIdentifier: "MugEditingViewController") as! MugEditingViewController
                    MugEditingVC.mainDictFromCoverCase = mainDictFromCoverCase
                    MugEditingVC.internetSpeed = internetSpeed
                    self.pushViewController(MugEditingVC)
                }else{
                    
                    let EditingVC = mainStoryBoard.instantiateViewController(withIdentifier: "EditingViewController") as! EditingViewController
                    EditingVC.mainDictFromCoverCase = mainDictFromCoverCase
                    EditingVC.internetSpeed = internetSpeed
                    self.pushViewController(EditingVC)
                }
                
            }
            
        }else
        {
            self.FancyFrameIndex = indexPath.row
            self.OpenGallery()
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let heightRatio : CGFloat = mainProductID == ID.Mug ? 0.2 : 0.4
//        
//        if arrMugFrame.count % 2 == 0 && indexPath.row >= (arrMugFrame.count - 2)
//        {
//            return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * heightRatio + 8)
//        }
//        else if indexPath.row == arrMugFrame.count - 1
//           {
//            return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * heightRatio + 8)
//        }
        
        return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * heightRatio)
        
    }
    
    func OpenGallery()
    {
        
        DispatchQueue.main.async {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            //check user has allowed permission or not
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    
                    DispatchQueue.main.async {
                        
                        isSelectZeroIndex = false
                        
                        let FrameDict = self.arrMugFrame[self.FancyFrameIndex - 1] as NSDictionary
                        
                        if FrameDict["image"] as? String ?? "" != ""
                        {
                            AssetsManager.shared.clear()
                            let FancyCoverEditingVc = mainStoryBoard.instantiateViewController(withIdentifier: "FancyFrameMugEditingVC") as! FancyFrameMugEditingVC
                            FancyCoverEditingVc.mugAndSipperBottleFancyDict = FrameDict as? [String : Any]
                            FancyCoverEditingVc.mainDictFromCoverCase = self.mainDictFromCoverCase
                            //            FancyCoverEditingVc.internetSpeed = speed
                            FancyCoverEditingVc.DefaultImgStr = "\(FrameDict["image"] as? String ?? "")"
                            self.pushViewController(FancyCoverEditingVc)
                        }
                    }
                    
                    break
                    
                case .denied, .restricted:
                    self.RemoveLoader()
                    self.alertGalleryAccessNeeded()
                    break
                case .notDetermined:
                    self.RemoveLoader()
                    self.requestGalleryPermission()
                    break
                    
                case .limited:
                    self.RemoveLoader()
                    self.alertGalleryAccessPermissionChangeNeeded()
                default:
                    break
                }
            }
        }
    }
    
    func requestGalleryPermission() {
        
        DispatchQueue.main.async {
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                guard accessGranted == true else { return }
                
            })
        }
    }
    
    func alertGalleryAccessNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Need Gallery Access",
                message: "Gallery access is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) -> Void in
            }))
            
            alert.addAction(UIAlertAction(title: "Allow Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func alertGalleryAccessPermissionChangeNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Change Gallery Access",
                message: " Change Gallery access from Selected Photos to All Photos is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) -> Void in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Change Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func CallApi() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let strAPI = mainProductID == ID.Mug ? "mug_fancy_images" : "fancy_bottle_shipper_images"
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)\(strAPI)", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["success"] as! Int == 1
                    {
                        self.arrMugFrame = responceDict["data"] as! [[String:Any]]
                        self.mugFancyFrameCollectionView.reloadData()
                    }else{
                        self.presentAlertWithTitle(title: "\(responceDict["message"]!)", message: "", options: "Retry", completion: { (ButtonIndex) in
                            self.CallApi()
                        })
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.CallApi()
                        }
                    })
                    
                }
            }
            
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.CallApi()
                }
            })
        }
        
    }
}
