//
//  FancyMobileCoverEditingVC.swift
//  Print_Photo_App
//
//  Created by Mac Os on 12/08/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import Photos
import BSImagePicker
import Alamofire
import AssetsPickerViewController

class FancyMobileCoverEditingVC: BaseViewController,StickerViewDelegate,UINavigationControllerDelegate,URLSessionDelegate, URLSessionDataDelegate,AssetsPickerViewControllerDelegate {
    
    
    
    //MARK:- ********************** OUTLAT DECLARTION **********************
    
    @IBOutlet weak var viewForCapture: UIView!
    @IBOutlet weak var defaultImageView: UIImageView!
    @IBOutlet weak var btnContinue: UIButton!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var internetSpeed : Double = 0
    var coverFancyDict : [String:Any]!
    var mainDictFromCoverCase : [String:Any]!
    var DefaultImgStr : String!
    var PickedImages = [UIImage]()
    var ParamDicts : [[String :Any]]!
    private var _selectedStickerView:StickerView?
    
    var selectedStickerView:StickerView? {
        get {
            return _selectedStickerView
        }
        set {
            
            // if other sticker choosed then resign the handler
            if _selectedStickerView != newValue {
                if let selectedStickerView = _selectedStickerView {
                    selectedStickerView.showEditingHandlers = false
                }
                _selectedStickerView = newValue
            }
            
            // assign handler to new sticker added
            if let selectedStickerView = _selectedStickerView {
                selectedStickerView.showEditingHandlers = true
                //                selectedStickerView.superview?.bringSubviewToFront(selectedStickerView)
            }
        }
    }
    
    // speed test
    typealias speedTestCompletionHandler = (_ megabytesPerSecond: Double? , _ error: Error?) -> Void
    
    var speedTestCompletionBlock : speedTestCompletionHandler?
    var startTime: CFAbsoluteTime!
    var stopTime: CFAbsoluteTime!
    var bytesReceived: Int!
    let picker = AssetsPickerViewController()
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Design Cover", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize()
    {
        addBackButton()
        
        //        if userDefault.string(forKey: "RegionCode") == "IN"
        //        {
        addHelpButton()
        //        }else
        //        {
        //            removeBackButton()
        //        }
        
        self.navigationController?.navigationBar.isHidden = true
        self.btnContinue.isHidden = true
        
        ParamDicts = FancyCoverDict[coverFancyDict["id"] as? Int ?? 0] //For take selected Dict
        
        //        OpenGallery()
        
        self.PickPhoto(numberOfItems: self.ParamDicts.count)  // for take multiple photo from gallary
        
    }
    
    //MARK:- **********************  BUTTON EVENT **********************
    
    @IBAction func press_btnContinue(_ sender: UIButton) {
        
        _selectedStickerView?.showEditingHandlers = false
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            // Check Internet Speed
            self.test(inSecond: 3.0) { [weak self] (speed) in
                print(speed)
                
                if speed < 0.03
                {
                    self!.RemoveLoader()
                    self!.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                    })
                }
                else
                {
                    if (self!.mainDictFromCoverCase["n_mask_image"] as? String ?? "") != ""
                    {
                        DispatchQueue.main.async {
                            let croppedAlphaImg = self!.viewForCapture.getImage(scale: 2.0, isMasked: true) // For make sticker to next screen
                            
                            let EditingVC = mainStoryBoard.instantiateViewController(withIdentifier: "EditingViewController") as! EditingViewController
                            EditingVC.fancyCaseImage = croppedAlphaImg
                            EditingVC.FancyCoverID = self!.coverFancyDict["category_id"] as? Int ?? 0
                            EditingVC.mainDictFromCoverCase = self!.mainDictFromCoverCase
                            EditingVC.internetSpeed = speed
                            self!.pushViewController(EditingVC)
                        }
                    }
                }
            }
        }
        else
        {
            self.RemoveLoader()
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        PickedImages.removeAll()
        OpenGallery()
    }
    
    func addHelpButton() {
        let helpButton = UIButton(type: .custom)
        helpButton.setImage(UIImage(named: "ic_help"), for: .normal)
        helpButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        helpButton.addTarget(self, action: #selector(self.helpAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: helpButton)
    }
    
    @objc func helpAction(_ sender: UIButton) {
        Help_Video()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- **********************  CUSTOME GALLERY PERMISSION**********************
    
    func OpenGallery()
    {
        DispatchQueue.main.async {
            
            self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            //check user has allowed permission or not
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    self.PickPhoto(numberOfItems: self.ParamDicts.count)  // for take multiple photo from gallary
                    self.RemoveLoader()
                    break
                case .denied, .restricted:
                    self.RemoveLoader()
                    self.alertGalleryAccessNeeded()
                    break
                case .notDetermined:
                    self.RemoveLoader()
                    self.requestGalleryPermission()
                    break
                    
                case .limited:
                    self.RemoveLoader()
                    self.alertGalleryAccessPermissionChangeNeeded()
                default:
                    break
                }
            }
        }
    }
    
    func requestGalleryPermission() {
        
        DispatchQueue.main.async {
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                guard accessGranted == true else { return }
                
            })
        }
    }
    
    func alertGalleryAccessNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Need Gallery Access",
                message: "Gallery access is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) -> Void in
                self.popViewController()
            }))
            
            alert.addAction(UIAlertAction(title: "Allow Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func alertGalleryAccessPermissionChangeNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Change Gallery Access",
                message: " Change Gallery access from Selected Photos to All Photos is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) -> Void in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Change Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- **********************  CUSTOME GALLERY **********************
    
    func assetsPickerDidCancel(controller: AssetsPickerViewController) {
        
        if isLogin
        {
            ((tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)
            self.tabBarController?.selectedIndex = 0
            
            isLogin = false
        }else
        {
            self.dismiss(animated: true, completion: nil)
            self.popViewController()
        }
    }
    
    func assetsPicker(controller: AssetsPickerViewController, selected assets: [PHAsset])  {

        if assets.count != self.ParamDicts.count {
            if(assets.count <= self.ParamDicts.count)
            {
                self.topMostViewController().view.makeToast("Select atleast \(self.ParamDicts.count) image.", duration: 2.0, position: .bottom)
            } else {
                self.topMostViewController().view.makeToast("Select only \(self.ParamDicts.count) image.", duration: 2.0, position: .bottom)
            }
        }
        
        DispatchQueue.main.async {
//            if assets.count != self.ParamDicts.count {
//                self.topMostViewController().view.makeToast("Select atleast \(self.ParamDicts.count) image.", duration: 2.0, position: .bottom)
//            }
//            else
//            {
                for imageAsset in assets
                {
                    let selectedImage = self.getUIImage(asset: imageAsset)

                        if selectedImage != nil
                        {
                            self.PickedImages.append(selectedImage!)
                            
                            self.setMainViewFrame() //For set viewforCapture as screens dimensions
                            
                            self.navigationController?.navigationBar.isHidden = false
                            self.btnContinue.isHidden = false
                            
                            self.dismiss(animated: true, completion: nil)
                        }
                }
//            }
        }
    }
    func assetsPicker(controller: AssetsPickerViewController, didDismissByCancelling byCancel: Bool) {
        print(byCancel)
    }
    
    func assetsPicker(controller: AssetsPickerViewController, shouldSelect asset: PHAsset, at indexPath: IndexPath) -> Bool {
        
       let selectedImage = self.getUIImage(asset: asset)
        
        if selectedImage != nil
        {
            let heightInPoints = selectedImage!.size.height
            let heightInPixels = heightInPoints * selectedImage!.scale
            
            let widthInPoints = selectedImage!.size.width
            let widthInPixels = widthInPoints * selectedImage!.scale
            
            // can limit selection count
            if controller.selectedAssets.count >= self.ParamDicts.count {
                
                self.topMostViewController().view.makeToast("Maximum \(self.ParamDicts.count) images can be selected", duration: 2.0, position: .bottom)
                return false
            }
            /*else
            {
                if heightInPixels <= 720 || widthInPixels <= 720 //nsdataObj.length/1024 < 500
                {
                    self.topMostViewController().view.makeToast("Please select high resolution image", duration: 2.0, position: .bottom)
                    return false
                }
            }*/
        }
        else{
            if controller.selectedAssets.count >= self.ParamDicts.count {
                self.topMostViewController().view.makeToast("Maximum \(self.ParamDicts.count) images can be selected", duration: 2.0, position: .bottom)
            }
            return false
        }
        
        return true
    }
    
    func PickPhoto(numberOfItems : Int)  {
        
        userDefault.set(numberOfItems, forKey: "assetsCount")
        
        picker.pickerDelegate = self
        
        DispatchQueue.main.async {
            self.picker.modalPresentationStyle = .fullScreen
            self.present(self.picker, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.RemoveLoader()
            })
        }
        
        //        let vc = BSImagePickerViewController()
        //        vc.maxNumberOfSelections = numberOfItems
        //
        //            bs_presentImagePickerController(vc, animated: true,select: { (asset: PHAsset) -> Void in
        //                print("Selected: \(asset)")
        //            }, deselect: { (asset: PHAsset) -> Void in
        //                print("Deselected: \(asset)")
        //            }, cancel: { (assets: [PHAsset]) -> Void in
        //                self.dismiss(animated: true, completion: nil)
        //                self.popViewController()
        //                print("Cancel: \(assets)")
        //            }, finish: { (assets: [PHAsset]) -> Void in
        //                print("Finish: \(assets)")
        //
        //                DispatchQueue.main.async {
        //
        //                    if assets.count != self.ParamDicts.count {
        //
        //                        self.topMostViewController().view.makeToast("Select atleast \(self.ParamDicts.count) image.", duration: 2.0, position: .bottom)
        //                    }
        //                    else
        //                    {
        //                        for imageAsset in assets
        //                        {
        //                            let selectedImage = self.getUIImage(asset: imageAsset)
        ////
        //                            if selectedImage != nil
        //                            {
        //                                self.PickedImages.append(selectedImage!)
        //                            }
        //                        }
        //
        //                        self.setMainViewFrame() //For set viewforCapture as screens dimensions
        //
        //                        self.navigationController?.navigationBar.isHidden = false
        //                        self.btnContinue.isHidden = false
        //
        //                        vc.dismiss(animated: true, completion: nil)
        //                    }
        //                }
        //            }, completion: nil, selectLimitReached: { (Int) in
        //                self.topMostViewController().view.makeToast("Maximum \(self.ParamDicts.count) images can be select", duration: 2.0, position: .bottom)
        //            })
        
    }
    
    func getUIImage(asset: PHAsset) -> UIImage? {
        
        let group = DispatchGroup()
        group.enter()
        
        var img: UIImage?
        let manager = PHImageManager.default()
        let options = PHImageRequestOptions()
        options.version = .original
        options.isSynchronous = true
        manager.requestImageData(for: asset, options: options) { data, _, _, _ in
            
            if let data = data {
                img = UIImage(data: data)
            }
            group.leave()
        }
        group.wait()

        return img
    }
    
    func RenderAtTransparentPart(image: UIImage, viewformask : UIView) {
        
        let cim = CIImage(image: image)
        let filter = CIFilter(name:"CIMaskToAlpha")!
        filter.setValue(cim, forKey: "inputImage")
        let out = filter.outputImage!
        let cgim = CIContext().createCGImage(out, from: out.extent)
        let lay = CALayer()
        lay.frame = viewformask.bounds
        lay.contents = cgim
        viewformask.layer.mask = lay
        
    }
    
    func addMaskedViews(ParametersDict : [[String:Any]]) {
        
        for views in viewForCapture.subviews
        {
            // for not remove imageview from view
            if  views.tag != 157  {
                views.removeFromSuperview()
            }
        }
        
        //adding masked images to view from dict
        for index in 0..<ParametersDict.count {
            
            let credentialDict = ParametersDict[index]
            
            let frameWidth = defaultImageView.image?.size.width
            let frameHeight = defaultImageView.image?.size.height
            
            let x1 = CGFloat((credentialDict["x"] as! Double)/Double(frameWidth!))
            let y1 = CGFloat((credentialDict["y"] as! Double)/Double(frameHeight!))
            let width1 = CGFloat((credentialDict["width"] as! Double)/Double(frameWidth!))
            let height1 = CGFloat((credentialDict["height"] as! Double)/Double(frameHeight!))
            
            //            let x1 = CGFloat(credentialDict["x"] as! Double)
            //            let y1 = CGFloat(credentialDict["y"] as! Double)
            //            let width1 = CGFloat(credentialDict["width"] as! Double)
            //            let height1 = CGFloat(credentialDict["height"] as! Double)
            //
            let maskImg = credentialDict["mask"] as! UIImage
            
            let mainviewWidth = viewForCapture.Getwidth
            let mainviewheight = viewForCapture.Getheight
            
            let rect = CGRect(x: (mainviewWidth * x1),
                              y: (mainviewheight * y1),
                              width: (mainviewWidth * width1),
                              height: (mainviewheight * height1))
            
            let view1 = UIView(frame: rect)
            view1.clipsToBounds = true
            RenderAtTransparentPart(image: maskImg.mask(with: .white), viewformask: view1)
            AddStickertoView(Image: PickedImages[index], ToView: view1)
            viewForCapture.insertSubview(view1, belowSubview: defaultImageView)
            
        }
    }
    
    func AddStickertoView(Image : UIImage, ToView : UIView) {
        
        var StickerImgView = UIImageView()
        
        if Image.size.width <= 10000 || Image.size.height <= 10000
        {
            StickerImgView = UIImageView(image: Image)
        }else
        {
            StickerImgView = UIImageView(image: Image.resizeWithWidth(width: 5400))
        }
        
        StickerImgView.isUserInteractionEnabled = true
        StickerImgView.contentMode = .scaleAspectFit
        
        let ratioSize : CGFloat = 1.0
        
        let ratio = Image.size.width / Image.size.height
        
        if StickerImgView.frame.width > viewForCapture.frame.height * ratioSize {
            let newHeight = viewForCapture.frame.width / ratio
            StickerImgView.frame.size = CGSize(width: ToView.Getwidth, height: ToView.Getheight)
        }
        else{
            let newWidth = viewForCapture.frame.height * ratio
            StickerImgView.frame.size = CGSize(width: ToView.Getwidth, height: ToView.Getheight)
        }
        
        let StickerViewforAdd = StickerView.init(contentView: StickerImgView)
        StickerViewforAdd.delegate = self
        //        StickerViewforAdd.setImage(UIImage.init(named: "Close")!, forHandler: StickerViewHandler.close)
        //        StickerViewforAdd.setImage(UIImage.init(named: "Close")!, forHandler: StickerViewHandler.rotate)
        StickerViewforAdd.enableRotate = false
        StickerViewforAdd.enableClose = false
        StickerViewforAdd.enableFlip = false
        StickerViewforAdd.setHandlerSize(25)
        StickerViewforAdd.outlineBorderColor = UIColor.clear
        StickerViewforAdd.showEditingHandlers = false
        
        ToView.addSubview(StickerViewforAdd)
        
        StickerViewforAdd.center = CGPoint(x: ToView.Getwidth * 0.5, y: ToView.Getheight * 0.5)
        
    }
    
    func setMainViewFrame() {
        
        if DefaultImgStr != nil{
            defaultImageView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: DefaultImgStr)
            
            if defaultImageView.image != nil
            {
                let ratio = defaultImageView.image!.size.height / defaultImageView.image!.size.width
                
                let ratioSize = DeviceType.IS_IPAD ? 0.6 : 0.7
                
                viewForCapture.frame.size = CGSize(width: UIScreen.main.bounds.size.width * CGFloat(ratioSize), height: UIScreen.main.bounds.size.width * CGFloat(ratioSize) * ratio)
                
                viewForCapture.center = self.view.center
                
                if self.PickedImages.count > 0 && self.ParamDicts!.count == self.PickedImages.count
                {
                    self.addMaskedViews(ParametersDict: self.ParamDicts)
                }
            }else
            {
                self.popViewController()
                self.dismiss(animated: true, completion: nil)
                
            }
        }
        
    }
    
    
    func stickerViewDidBeginMoving(_ stickerView: StickerView) {
        self.selectedStickerView = stickerView
        
    }
    
    func stickerViewDidChangeMoving(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidEndMoving(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidBeginRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidChangeRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidEndRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidClose(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidTap(_ stickerView: StickerView) {
        self.selectedStickerView = stickerView
    }
    
    //MARK:- ********************** TOUCHES LIFECYCLE **********************
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch: UITouch? = touches.first
        if touch?.view != selectedStickerView {
            selectedStickerView?.showEditingHandlers = false
        }
    }
    
    //MARK:- ********************** INTERNET SPEED TEST **********************
    
    func test(inSecond: Double, completion: @escaping((Double) -> ())) -> () {
        
        testDownloadSpeedWithTimout(timeout: inSecond) { (speed, error) in
            print("Download Speed:", speed ?? "KO")
            print("Speed Test Error:", error ?? "KO")
            completion(speed ?? 0.0)
        }
    }
    
    func testDownloadSpeedWithTimout(timeout: TimeInterval, withCompletionBlock: @escaping speedTestCompletionHandler) {
        
        guard let url = URL(string: "https://images.apple.com/v/imac-with-retina/a/images/overview/5k_image.jpg") else { return }
        
        startTime = CFAbsoluteTimeGetCurrent()
        stopTime = startTime
        bytesReceived = 0
        
        speedTestCompletionBlock = withCompletionBlock
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForResource = timeout
        let session = URLSession.init(configuration: configuration, delegate: self, delegateQueue: nil)
        session.dataTask(with: url).resume()
        
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        bytesReceived! += data.count
        stopTime = CFAbsoluteTimeGetCurrent()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        let elapsed = stopTime - startTime
        
        if let aTempError = error as NSError?, aTempError.domain != NSURLErrorDomain && aTempError.code != NSURLErrorTimedOut && elapsed == 0  {
            speedTestCompletionBlock?(nil, error)
            return
        }
        
        let speed = elapsed != 0 ? Double(bytesReceived) / elapsed / 1024.0 / 1024.0 : -1
        speedTestCompletionBlock?(speed, nil)
        
    }
}
