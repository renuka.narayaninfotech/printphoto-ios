//
//  maskDictionaryVC.swift
//  Print_Photo_App
//
//  Created by Mac Os on 12/08/19.
//  Copyright © 2019 des. All rights reserved.
//

import Foundation
import UIKit

// MARK:- Mobile Cover Fancy Frame Mask View Dictionary

// Calculatation :
// X = Distance between image X and Shape X
// Y = Distance between image Y and Shape Y
// Width = Image inside Shape Width
// Height = Image inside Shape Height


let FancyCoverDict = [
    
    1540 : [
        ["x": 195.0,
         "y": 506.0,
         "width": 701.0,
         "height": 970.0,
         "mask": UIImage(named: "mask_28")!]
    ],
    
    1543 : [
        ["x": 144.0,
         "y": 433.0,
         "width": 265.0,
         "height": 246.0,
         "mask": UIImage(named: "mask_9")!],
        ["x": 144.0,
         "y": 697.0,
         "width": 413.0,
         "height": 388.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1542 : [
        ["x": 134.0,
         "y": 442.0,
         "width": 435.0,
         "height": 311.0,
         "mask": UIImage(named: "mask_9")!],
        ["x": 134.0,
         "y": 769.0,
         "width": 435.0,
         "height": 311.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1541 : [
        ["x": 120.0,
         "y": 550.0,
         "width": 460.0,
         "height": 458.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1539 : [
        ["x": 126.0,
         "y": 636.0,
         "width": 447.0,
         "height": 340.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1538 : [
        ["x": 141.0,
         "y": 553.0,
         "width": 400.0,
         "height": 340.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1537 : [
        ["x": 132.0,
         "y": 500.0,
         "width": 429.0,
         "height": 408.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1536 : [
        ["x": 160.0,
         "y": 474.0,
         "width": 372.0,
         "height": 345.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1535 : [
        ["x": 143.0,
         "y": 480.0,
         "width": 305.0,
         "height": 442.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1534 : [
        ["x": 170.0,
         "y": 635.0,
         "width": 364.0,
         "height": 284.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1533 : [
        ["x": 205.0,
         "y": 327.0,
         "width": 414.0,
         "height": 383.0,
         "mask": UIImage(named: "mask_9")!],
        ["x": 114.0,
         "y": 723.0,
         "width": 414.0,
         "height": 383.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1532 : [
        ["x": 138.0,
         "y": 468.0,
         "width": 423.0,
         "height": 563.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1531 : [
        ["x": 286.0,
         "y": 395.0,
         "width": 280.0,
         "height": 430.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1530 : [
        ["x": 151.0,
         "y": 536.0,
         "width": 382.0,
         "height": 493.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1529 : [
        ["x": 169.0,
         "y": 370.0,
         "width": 372.0,
         "height": 450.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    1528 : [
        ["x": 170.0,
         "y": 419.0,
         "width": 347.0,
         "height": 493.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    1527 : [
        ["x": 103.0,
         "y": 252.0,
         "width": 395.0,
         "height": 287.0,
         "mask": UIImage(named: "mask_1")!],
        ["x": 205.0,
         "y": 565.0,
         "width": 395.0,
         "height": 287.0,
         "mask": UIImage(named: "mask_1")!],
        ["x": 109.0,
         "y": 872.0,
         "width": 395.0,
         "height": 287.0,
         "mask": UIImage(named: "mask_1")!]
    ],
    
    1526 : [
        ["x": 218.0,
         "y": 331.0,
         "width": 277.0,
         "height": 248.0,
         "mask": UIImage(named: "mask_6")!],
        ["x": 106.0,
         "y": 513.0,
         "width": 277.0,
         "height": 258.0,
         "mask": UIImage(named: "mask_7")!],
        ["x": 360.0,
         "y": 517.0,
         "width": 277.0,
         "height": 262.0,
         "mask": UIImage(named: "mask_5")!],
        ["x": 218.0,
         "y": 728.0,
         "width": 277.0,
         "height": 248.0,
         "mask": UIImage(named: "mask_4")!]
    ],
    
    1525 : [
        ["x": 159.0,
         "y": 286.0,
         "width": 227.0,
         "height": 227.0,
         "mask": UIImage(named: "mask_2")!],
        ["x": 335.0,
         "y": 570.0,
         "width": 216.0,
         "height": 209.0,
         "mask": UIImage(named: "mask_25")!],
        ["x": 157.0,
         "y": 839.0,
         "width": 200.0,
         "height": 200.0,
         "mask": UIImage(named: "mask_2")!]
    ],
    
    1524 : [
        ["x": 258.0,
         "y": 258.0,
         "width": 284.0,
         "height": 284.0,
         "mask": UIImage(named: "mask_2")!],
        ["x": 91.0,
         "y": 543.0,
         "width": 284.0,
         "height": 284.0,
         "mask": UIImage(named: "mask_2")!],
        ["x": 291.0,
         "y": 797.0,
         "width": 284.0,
         "height": 284.0,
         "mask": UIImage(named: "mask_2")!]
    ],
    
    1522 : [
        ["x": 128.0,
         "y": 341.0,
         "width": 443.0,
         "height": 288.0,
         "mask": UIImage(named: "mask_25")!],
        ["x": 128.0,
         "y": 647.0,
         "width": 213.0,
         "height": 213.0,
         "mask": UIImage(named: "mask_25")!],
        ["x": 358.0,
         "y": 647.0,
         "width": 213.0,
         "height": 213.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    1521 : [
        ["x": 318.0,
         "y": 250.0,
         "width": 260.0,
         "height": 235.0,
         "mask": UIImage(named: "mask_8")!],
        ["x": 111.0,
         "y": 517.0,
         "width": 280.0,
         "height": 235.0,
         "mask": UIImage(named: "mask_9")!],
        ["x": 285.0,
         "y": 776.0,
         "width": 270.0,
         "height": 235.0,
         "mask": UIImage(named: "mask_10")!]
    ],
    
    1520 : [
        ["x": 126.0,
         "y": 401.0,
         "width": 447.0,
         "height": 447.0,
         "mask": UIImage(named: "mask_16")!]
    ],
    
    1519 : [
        ["x": 136.0,
         "y": 449.0,
         "width": 423.0,
         "height": 423.0,
         "mask": UIImage(named: "mask_2")!]
    ]
    
    
    //    1265 : [
    //            ["x": 0.1857,
    //             "y": 0.2527,
    //             "width": 0.2929,
    //             "height": 0.1668,
    //             "mask": UIImage(named: "mask_26")!],
    //            ["x": 0.5114,
    //             "y": 0.2527,
    //             "width": 0.2929,
    //             "height": 0.1668,
    //             "mask": UIImage(named: "mask_27")!],
    //            ["x": 0.2743,
    //             "y": 0.4012,
    //             "width": 0.4457,
    //             "height": 0.2602,
    //             "mask": UIImage(named: "mask_28")!],
    //            ["x": 0.1857,
    //             "y": 0.6464,
    //             "width": 0.2929,
    //             "height": 0.1668,
    //             "mask": UIImage(named: "mask_29")!],
    //            ["x": 0.5114,
    //             "y": 0.6464,
    //             "width": 0.2929,
    //             "height": 0.1668,
    //             "mask": UIImage(named: "mask_30")!]
    //        ],
    //
    //    1262 : [
    //            ["x": 0.1957,
    //             "y": 0.2260,
    //             "width": 0.1571,
    //             "height": 0.1501,
    //             "mask": UIImage(named: "mask_9")!],
    //            ["x": 0.3671,
    //             "y": 0.2260,
    //             "width": 0.1529,
    //             "height": 0.0917,
    //             "mask": UIImage(named: "mask_9")!],
    //            ["x": 0.3629,
    //             "y": 0.3244,
    //             "width": 0.2614,
    //             "height": 0.1093,
    //             "mask": UIImage(named: "mask_9")!],
    //            ["x": 0.4729,
    //             "y": 0.4445,
    //             "width": 0.15,
    //             "height": 0.0926,
    //             "mask": UIImage(named: "mask_9")!],
    //            ["x": 0.6429,
    //             "y": 0.3862,
    //             "width": 0.1471,
    //             "height": 0.1493,
    //             "mask": UIImage(named: "mask_9")!],
    //            ["x": 0.1957,
    //             "y": 0.5480,
    //             "width": 0.1557,
    //             "height": 0.1501,
    //             "mask": UIImage(named: "mask_9")!],
    //            ["x": 0.3671,
    //             "y": 0.5480,
    //             "width": 0.1529,
    //             "height": 0.0917,
    //             "mask": UIImage(named: "mask_9")!],
    //            ["x": 0.3671,
    //             "y": 0.6497,
    //             "width": 0.2614,
    //             "height": 0.1093,
    //             "mask": UIImage(named: "mask_9")!],
    //            ["x": 0.4729,
    //             "y": 0.7665,
    //             "width": 0.1529,
    //             "height": 0.0917,
    //             "mask": UIImage(named: "mask_9")!],
    //            ["x": 0.6429,
    //             "y": 0.7089,
    //             "width": 0.1557,
    //             "height": 0.1501,
    //             "mask": UIImage(named: "mask_9")!]
    //        ],
    //
    //    1263 : [
    //            ["x": 0.2429,
    //             "y": 0.3095,
    //             "width": 0.2143,
    //             "height": 0.1184,
    //             "mask": UIImage(named: "mask_24")!],
    //            ["x": 0.4343,
    //             "y": 0.2877,
    //             "width": 0.1814,
    //             "height": 0.1460,
    //             "mask": UIImage(named: "mask_18")!],
    //            ["x": 0.5900,
    //             "y": 0.3555,
    //             "width": 0.1886,
    //             "height": 0.1168,
    //             "mask": UIImage(named: "mask_19")!],
    //            ["x": 0.1843,
    //             "y": 0.4279,
    //             "width": 0.2370,
    //             "height": 0.1226,
    //             "mask": UIImage(named: "mask_20")!],
    //            ["x": 0.3750,
    //             "y": 0.4170,
    //             "width": 0.2414,
    //             "height": 0.1309,
    //             "mask": UIImage(named: "mask_21")!],
    //            ["x": 0.5886,
    //             "y": 0.4704,
    //             "width": 0.2229,
    //             "height": 0.1226,
    //             "mask": UIImage(named: "mask_24")!],
    //            ["x": 0.2757,
    //             "y": 0.5421,
    //             "width": 0.2157,
    //             "height": 0.1334,
    //             "mask": UIImage(named: "mask_23")!],
    //            ["x": 0.4885,
    //             "y": 0.5755,
    //             "width": 0.2086,
    //             "height": 0.1209,
    //             "mask": UIImage(named: "mask_24")!]
    //        ],
    //
    //    1261 : [
    //            ["x": 0.3300,
    //             "y": 0.2735,
    //             "width": 0.3514,
    //             "height": 0.1993,
    //             "mask": UIImage(named: "mask_16")!],
    //            ["x": 0.1914,
    //             "y": 0.3986,
    //             "width": 0.2914,
    //             "height": 0.1701,
    //             "mask": UIImage(named: "mask_16")!],
    //            ["x": 0.5342,
    //             "y": 0.3953,
    //             "width": 0.2757,
    //             "height": 0.1609,
    //             "mask": UIImage(named: "mask_16")!],
    //            ["x": 0.3485,
    //             "y": 0.4870,
    //             "width": 0.3428,
    //             "height": 0.2001,
    //             "mask": UIImage(named: "mask_16")!],
    //            ["x": 0.2285,
    //             "y": 0.5921,
    //             "width": 0.2142,
    //             "height": 0.1251,
    //             "mask": UIImage(named: "mask_16")!]
    //        ],
    //
    //    1260 : [
    //        ["x": 0.0,
    //         "y": 0.0,
    //         "width": 0.2857,
    //         "height": 0.3753,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.2957,
    //         "y": 0.0,
    //         "width": 0.69,
    //         "height": 0.1751,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.0,
    //         "y": 0.3837,
    //         "width": 0.2829,
    //         "height": 0.2752,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.2957,
    //         "y": 0.1827,
    //         "width": 0.69,
    //         "height": 0.4737,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.0,
    //         "y": 0.6580,
    //         "width": 0.6357,
    //         "height": 0.3361,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.6529,
    //         "y": 0.6580,
    //         "width": 0.3357,
    //         "height": 0.1960,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.6529,
    //         "y": 0.8574,
    //         "width": 0.3357,
    //         "height": 0.1359,
    //         "mask": UIImage(named: "mask_25")!]
    //    ],
    //
    //    1264 : [
    //        ["x": 0.0,
    //         "y": 0.0,
    //         "width": 0.3229,
    //         "height": 0.2744,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.0,
    //         "y": 0.2811,
    //         "width": 0.3229,
    //         "height": 0.2744,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.0,
    //         "y": 0.5521,
    //         "width": 0.3229,
    //         "height": 0.2744,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.0,
    //         "y": 0.8274,
    //         "width": 0.3229,
    //         "height": 0.1668,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.3357,
    //         "y": 0.0,
    //         "width": 0.3229,
    //         "height": 0.1993,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.3357,
    //         "y": 0.2060,
    //         "width": 0.3229,
    //         "height": 0.1993,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.3357,
    //         "y": 0.4029,
    //         "width": 0.3229,
    //         "height": 0.1993,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.3357,
    //         "y": 0.5972,
    //         "width": 0.3229,
    //         "height": 0.1993,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.3357,
    //         "y": 0.7940,
    //         "width": 0.3229,
    //         "height": 0.1993,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.6586,
    //         "y": 0.0,
    //         "width": 0.3229,
    //         "height": 0.0909,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.6586,
    //         "y": 0.0992,
    //         "width": 0.3229,
    //         "height": 0.1877,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.6586,
    //         "y": 0.2894,
    //         "width": 0.3229,
    //         "height": 0.2744,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.6586,
    //         "y": 0.5605,
    //         "width": 0.3229,
    //         "height": 0.1785,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.6586,
    //         "y": 0.7389,
    //         "width": 0.3229,
    //         "height": 0.2560,
    //         "mask": UIImage(named: "mask_25")!]
    //    ],
    //
    //    1259 : [
    //        ["x": 0.1971,
    //         "y": 0.4696,
    //         "width": 0.3743,
    //         "height": 0.1801,
    //         "mask": UIImage(named: "mask_11")!],
    //        ["x": 0.5886,
    //         "y": 0.4696,
    //         "width": 0.2214,
    //         "height": 0.2018,
    //         "mask": UIImage(named: "mask_12")!],
    //        ["x": 0.1957,
    //         "y": 0.6180,
    //         "width": 0.2229,
    //         "height": 0.2085,
    //         "mask": UIImage(named: "mask_13")!],
    //        ["x": 0.43,
    //         "y": 0.6422,
    //         "width": 0.3786,
    //         "height": 0.1818,
    //         "mask": UIImage(named: "mask_14")!]
    //    ],
    //
    //    1256 : [
    //        ["x": 0.1986,
    //         "y": 0.4954,
    //         "width": 0.2757,
    //         "height": 0.1610,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.4957,
    //         "y": 0.4954,
    //         "width": 0.2757,
    //         "height": 0.1610,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.2414,
    //         "y": 0.6656,
    //         "width": 0.2757,
    //         "height": 0.1610,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.54,
    //         "y": 0.6656,
    //         "width": 0.2757,
    //         "height": 0.1610,
    //         "mask": UIImage(named: "mask_25")!]
    //    ],
    //
    //    1255 : [
    //        ["x": 0.2529,
    //         "y": 0.2277,
    //         "width": 0.2929,
    //         "height": 0.2502,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.4671,
    //         "y": 0.6222,
    //         "width": 0.2657,
    //         "height": 0.2277,
    //         "mask": UIImage(named: "mask_25")!]
    //    ],
    //
    //    1254 : [
    //        ["x": 0.19,
    //         "y": 0.3670,
    //         "width": 0.4114,
    //         "height": 0.2394,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.6171,
    //         "y": 0.3670,
    //         "width": 0.1971,
    //         "height": 0.1184,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.6171,
    //         "y": 0.4912,
    //         "width": 0.1971,
    //         "height": 0.1184,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.19,
    //         "y": 0.6163,
    //         "width": 0.1971,
    //         "height": 0.1184,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.4086,
    //         "y": 0.6163,
    //         "width": 0.1971,
    //         "height": 0.1184,
    //         "mask": UIImage(named: "mask_25")!],
    //        ["x": 0.6171,
    //         "y": 0.6163,
    //         "width": 0.1971,
    //         "height": 0.1184,
    //         "mask": UIImage(named: "mask_25")!]
    //    ]
    
]


// MARK:- Mug Fancy Frame  Mask View Dictionary

let FancyMugDict = [
    
    2 : [
        ["x": 549.0,
         "y": 86.0,
         "width": 984.0,
         "height": 912.0,
         "mask": UIImage(named: "mask_6")!]
    ],
    
    3 : [
        ["x": 150.0,
         "y": 30.0,
         "width": 1045.0,
         "height": 945.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    4 : [
        ["x": 150.0,
         "y": 26.0,
         "width": 1050.0,
         "height": 936.0,
         "mask": UIImage(named: "mask_25")!],
        ["x": 1506.0,
         "y": 35.0,
         "width": 1055.0,
         "height": 930.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    5 : [
        ["x": 210.0,
         "y": 80.0,
         "width": 933.0,
         "height": 833.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    6 : [
        ["x": 610.0,
         "y": 0.0,
         "width": 1331.0,
         "height": 1015.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    7 : [
        ["x": 0.0,
         "y": 0.0,
         "width": 1145.0,
         "height": 1018.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    8 : [
        ["x": 559.0,
         "y": 91.0,
         "width": 680.0,
         "height": 565.0,
         "mask": UIImage(named: "mask_6")!]
    ],
    
    9 : [
        ["x": 222.0,
         "y": 161.0,
         "width": 1350.0,
         "height": 694.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    10 : [
        ["x": 370.0,
         "y": 279.0,
         "width": 1051.0,
         "height": 655.0,
         "mask": UIImage(named: "mask_1")!]
    ],
    
    11 : [
        ["x": 960.0,
         "y": 183.0,
         "width": 738.0,
         "height": 738.0,
         "mask": UIImage(named: "mask_2")!]
    ],
    
    12 : [
        ["x": 465.0,
         "y": 177.0,
         "width": 578.0,
         "height": 706.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    13 : [
        ["x": 226.0,
         "y": 197.0,
         "width": 1235.0,
         "height": 625.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    14 : [
        ["x": 690.0,
         "y": 0.0,
         "width": 1350.0,
         "height": 1018.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    15 : [
        ["x": 1675.0,
         "y": 346.0,
         "width": 862.0,
         "height": 630.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    16 : [
        ["x": 400.0,
         "y": 376.0,
         "width": 1802.0,
         "height": 602.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    17 : [
        ["x": 280.0,
         "y": 105.0,
         "width": 925.0,
         "height": 860.0,
         "mask": UIImage(named: "mask_6")!]
    ],
    
    18 : [
        ["x": 714.0,
         "y": 57.0,
         "width": 1058.0,
         "height": 955.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    19 : [
        ["x": 161.0,
         "y": 150.0,
         "width": 760.0,
         "height": 732.0,
         "mask": UIImage(named: "mask_6")!],
        ["x": 1745.0,
         "y": 149.0,
         "width": 776.0,
         "height": 742.0,
         "mask": UIImage(named: "mask_6")!]
    ],
    
    20 : [
        ["x": 895.0,
         "y": 108.0,
         "width": 825.0,
         "height": 700.0,
         "mask": UIImage(named: "mask_10")!]
    ],
    
    21 : [
        ["x": 242.0,
         "y": 212.0,
         "width": 717.0,
         "height": 654.0,
         "mask": UIImage(named: "mask_10")!],
        ["x": 1347.0,
         "y": 63.0,
         "width": 1080.0,
         "height": 915.0,
         "mask": UIImage(named: "mask_10")!]
    ],
    
    22 : [
        ["x": 300.0,
         "y": 216.0,
         "width": 600.0,
         "height": 646.0,
         "mask": UIImage(named: "mask_9")!],
        ["x": 912.0,
         "y": 209.0,
         "width": 635.0,
         "height": 676.0,
         "mask": UIImage(named: "mask_10")!]
    ],
    
    23 : [
        ["x": 615.0,
         "y": 0.0,
         "width": 1370.0,
         "height": 690.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    24 : [
        ["x": 1415.0,
         "y": 65.0,
         "width": 1160.0,
         "height": 890.0,
         "mask": UIImage(named: "mask_6")!]
    ],
    
    25 : [
        ["x": 1490.0,
         "y": 0.0,
         "width": 1154.0,
         "height": 1017.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    26 : [
        ["x": 1392.0,
         "y": 0.0,
         "width": 1256.0,
         "height": 1014.0,
         "mask": UIImage(named: "mask_25")!]
    ]
    
    //    27 : [
    //        ["x": 0.0,
    //         "y": 0.0,
    //         "width": 0.4705,
    //         "height": 0.999,
    //         "mask": UIImage(named: "mask_25")!]
    //    ]
]


// MARK:- Sipper Bottle Fancy Frame Mask View Dictionary

let FancySipperBottelDict = [
    
    2 : [
        ["x": 68.0,
         "y": 199.0,
         "width": 385.0,
         "height": 350.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    3 : [
        ["x": 178.0,
         "y": 175.0,
         "width": 286.0,
         "height": 325.0,
         "mask": UIImage(named: "mask_31")!],
        ["x": 423.0,
         "y": 300.0,
         "width": 317.0,
         "height": 320.0,
         "mask": UIImage(named: "mask_24")!]
    ],
    
    4 : [
        ["x": 23.0,
         "y": 152.0,
         "width": 505.0,
         "height": 390.0,
         "mask": UIImage(named: "mask_6")!]
    ],
    
    5 : [
        ["x": 462.0,
         "y": 328.0,
         "width": 380.0,
         "height": 356.0,
         "mask": UIImage(named: "mask_6")!]
    ],
    
    6 : [
        ["x": 128.0,
         "y": 120.0,
         "width": 666.0,
         "height": 523.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    7 : [
        ["x": 125.0,
         "y": 121.0,
         "width": 677.0,
         "height": 522.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    8 : [
        ["x": 209.0,
         "y": 75.0,
         "width": 544.0,
         "height": 544.0,
         "mask": UIImage(named: "mask_2")!]
    ],
    
    
    9 : [
        ["x": 118.0,
         "y": 72.0,
         "width": 696.0,
         "height": 560.0,
         "mask": UIImage(named: "mask_6")!]
    ],
    
    10 : [
        ["x": 389.0,
         "y": 65.0,
         "width": 403.0,
         "height": 434.0,
         "mask": UIImage(named: "mask_9")!]
    ],
    
    11 : [
        ["x": 62.0,
         "y": 44.0,
         "width": 531.0,
         "height": 460.0,
         "mask": UIImage(named: "mask_6")!],
        ["x": 538.0,
         "y": 396.0,
         "width": 374.0,
         "height": 320.0,
         "mask": UIImage(named: "mask_6")!]
    ],
    
    12 : [
        ["x": 90.0,
         "y": 254.0,
         "width": 450.0,
         "height": 420.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    13 : [
        ["x": 148.0,
         "y": 57.0,
         "width": 320.0,
         "height": 320.0,
         "mask": UIImage(named: "mask_28")!],
        ["x": 526.0,
         "y": 54.0,
         "width": 309.0,
         "height": 382.0,
         "mask": UIImage(named: "mask_25")!],
        ["x": 230.0,
         "y": 433.0,
         "width": 484.0,
         "height": 276.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    14 : [
        ["x": 204.0,
         "y": 203.0,
         "width": 516.0,
         "height": 437.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    15 : [
        ["x": 445.0,
         "y": 103.0,
         "width": 465.0,
         "height": 563.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    16 : [
        ["x": 398.0,
         "y": 201.0,
         "width": 419.0,
         "height": 391.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    17 : [
        ["x": 94.0,
         "y": 310.0,
         "width": 410.0,
         "height": 373.0,
         "mask": UIImage(named: "mask_6")!],
        ["x": 474.0,
         "y": 175.0,
         "width": 381.0,
         "height": 350.0,
         "mask": UIImage(named: "mask_6")!]
    ],
    
    18 : [
        ["x": 159.0,
         "y": 65.0,
         "width": 600.0,
         "height": 552.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    19 : [
        ["x": 161.0,
         "y": 25.0,
         "width": 573.0,
         "height": 448.0,
         "mask": UIImage(named: "mask_25")!]
    ],
    
    20 : [
        ["x": 128.0,
         "y": 97.0,
         "width": 670.0,
         "height": 573.0,
         "mask": UIImage(named: "mask_25")!]
    ]
    
]
