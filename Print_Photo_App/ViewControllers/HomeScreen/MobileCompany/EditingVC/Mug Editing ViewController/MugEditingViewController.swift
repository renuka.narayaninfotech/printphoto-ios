//
//  MugEditingViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 04/04/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import AFNetworking
import Photos
import Toast_Swift
import ChromaColorPicker
import MZFormSheetPresentationController
import Alamofire
import FBSDKCoreKit
//import FBSDKMarketingKit
import FacebookCore
import Firebase

class MugEditingViewController: BaseViewController , StickerViewDelegate , UINavigationControllerDelegate , UIImagePickerControllerDelegate , ChromaColorPickerDelegate , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    //MARK:- ********************** OUTLAT DECLARTION **********************
    
    @IBOutlet var bottomMenuCollView: UICollectionView!
    @IBOutlet var ViewforCapture: UIView!
    @IBOutlet var defaultImgView: UIImageView!
    @IBOutlet var Cover_ImgView: UIImageView!
    @IBOutlet var Mask_ImgView: UIImageView!
    @IBOutlet var CromaColorPicker: ChromaColorPicker!
    @IBOutlet var CromaVIew: UIView!
    @IBOutlet weak var captureSuperview: UIView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
        
    //    var FancyMugID : Int?
    var fancyMugImage : UIImage!
    
    var isSetupAlreadyDone = false
    
    var categoryId : Int!
    var picker = UIImagePickerController()
    private var _selectedStickerView:StickerView?
    var mainDictFromCoverCase : [String:Any]!
    var DefaultImgStr : String!
    
    var btnAddPhoto = UIButton()
    
    var gestureRecognizer: UITapGestureRecognizer!
    
    var bottomItemsArr = [UIImage(named: "images"),
                          UIImage(named: "add_text"),
                          UIImage(named: "sticker"),
                          UIImage(named: "bg_color"),
                          UIImage(named: "bg_asset"),
                          UIImage(named: "Add_cart")]
    
    var defaultLabel = UILabel()
    var isFromViewDidLoad: Bool = true
    
    var internetSpeed : Double = 0
    var isLoaded = false
    
    var selectedStickerView:StickerView? {
        get {
            return _selectedStickerView
        }
        set {
            
            // if other sticker choosed then resign the handler
            if _selectedStickerView != newValue {
                if let selectedStickerView = _selectedStickerView {
                    selectedStickerView.showEditingHandlers = false
                }
                _selectedStickerView = newValue
            }
            
            // assign handler to new sticker added
            if let selectedStickerView = _selectedStickerView {
                selectedStickerView.showEditingHandlers = true
                //                selectedStickerView.superview?.bringSubviewToFront(selectedStickerView)
            }
        }
    }
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetNavigationBarTitle(Startstring: "\(mainDictFromCoverCase["modalName"] ?? "")", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
        
        self.tabBarController?.tabBar.isHidden = true
        
        if (StickerImage != nil) {
            
            AddStickertoView(Image: StickerImage , isSticker : true)
            StickerImage = nil
            
        }
        else if (StickerTextString != nil)
        {
            if StickerTextColor == nil
            {
                StickerTextColor = UIColor.black
            }
            if stickerFontString == nil {
                stickerFontString = CustomFontWeight.reguler
            }
            AddStickertoView(TextString: StickerTextString, TextColor: StickerTextColor, FontStyle: stickerFontString)
            StickerTextString = nil
            StickerTextColor = nil
            stickerFontString = nil
            
        }
        else if (StickerBackgroung != nil)
        {
            AddStickertoView(Image: StickerBackgroung , isSticker : false)
            StickerBackgroung = nil
        }else if instagramImage != nil
        {
            AddStickertoView(Image: instagramImage , isSticker : false)
            instagramImage = nil
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //        if Reachability.isConnectedToNetwork() {
        if isFromViewDidLoad
        {
            isFromViewDidLoad = false
            self.viewDidLayout()
        }
        
        //        }else
        //        {
        //            self.presentAlertWithTitle(title: "Network Connection", message: "Internet is not available", options: "OK", completion: { (ButtonIndex) in
        //            })
        //        }
        
    }
    
    func viewDidLayout() {
        
        Add_Footer_Label()
        
        //        let CoverImageURL = "\(modelImgpath)\(mainDictFromCoverCase["modal_image"] as! String)"
        
        let CoverImageURL = mainDictFromCoverCase["n_modal_image"] as? String ?? ""
        
        Cover_ImgView.sd_setShowActivityIndicatorView(false)
        Cover_ImgView.sd_setIndicatorStyle(.gray)
        Cover_ImgView.sd_setImage(with: URL(string: CoverImageURL), placeholderImage: nil, options: .refreshCached){ (img, error, catchType, url) in
            //            self.reloadViews()
            self.reloadView()
            
        }
        
        defaultImgView.image = UIImage(color: .clear)
        
    }
    
    //    func reloadViews()
    //    {
    //        self.ViewforCapture.isHidden = false
    //
    //        showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
    //
    //        var ratio : CGFloat = 0.5
    //        if let image = Cover_ImgView.image {
    //            ratio = image.size.width / image.size.height
    //        }
    //
    //        if Mask_ImgView.frame.width > Mask_ImgView.frame.height {
    //            let newHeight = Mask_ImgView.frame.width / ratio
    //
    //            if newHeight > (ScreenSize.SCREEN_HEIGHT - 100)
    //            {
    //                Mask_ImgView.frame.size = CGSize(width: (ScreenSize.SCREEN_HEIGHT - 100) * ratio, height: (ScreenSize.SCREEN_HEIGHT - 100))
    //            }
    //            else
    //            {
    //                Mask_ImgView.frame.size = CGSize(width: Mask_ImgView.frame.width - 50.0, height: newHeight + 20.0)
    //            }
    //
    //        }
    //        else{
    //            let newWidth = Mask_ImgView.frame.height * ratio
    //
    //            if newWidth > (ScreenSize.SCREEN_WIDTH - 20)
    //            {
    //                Mask_ImgView.frame.size = CGSize(width: (ScreenSize.SCREEN_WIDTH - 20), height: (ScreenSize.SCREEN_WIDTH - 20)/ratio)
    //            }
    //            else
    //            {
    //                Mask_ImgView.frame.size = CGSize(width: newWidth, height: Mask_ImgView.frame.height)
    //            }
    //        }
    //
    //        let x1 = Mask_ImgView.Getx - 25.0 //- 200
    //        let y1 = Mask_ImgView.Gety + 25.0 //- 50
    //        let width1 = Mask_ImgView.Getwidth + 50.0 //+ self.view.Getwidth * 0.3
    //        let height1 = Mask_ImgView.Getheight - 20.0 //+ self.view.Getheight * 0.1
    //
    //        let cgrect = CGRect(x: x1, y: y1, width: width1, height: height1)
    //
    //        Cover_ImgView.frame = cgrect
    //
    //        self.captureSuperview.frame = self.Mask_ImgView.frame
    //        self.Cover_ImgView.center = self.ViewforCapture.center
    //        self.captureSuperview.layoutSubviews()
    //
    //        let screenCenter = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
    //        captureSuperview.center = screenCenter
    //        captureSuperview.center.y = screenCenter.y * 0.95
    //
    //        //        let url = URL(string: "\(maskImgpath)\(mainDictFromCoverCase["mask_image"] as! String)")
    //
    //        let url = URL(string: mainDictFromCoverCase["n_mask_image"] as! String)
    //
    //        let data = try? Data(contentsOf: url!)
    //
    //        if let imageData = data {
    ////            self.afterReload(image: UIImage(data: imageData)!)
    //            RenderAtTransparentPart(image: UIImage(data: imageData)!, viewformask: ViewforCapture)
    //        }
    //        RemoveLoader()
    //
    //        self.defaultLabel.isHidden = false
    //        Add_Footer_Label()
    //
    //        self.btnAddPhoto = UIButton(frame: CGRect(x: 0, y: 0 , width: 100, height: 100))
    //        self.btnAddPhoto.setImage(UIImage(named: "add_photo"), for: .normal)
    //
    //        self.btnAddPhoto.center = CGPoint(x: self.captureSuperview.frame.size.width  / 2,
    //                                          y: self.captureSuperview.frame.size.height / 2)
    //
    //        self.btnAddPhoto.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
    //        self.ViewforCapture.addSubview(self.btnAddPhoto)
    //
    //        Mask_ImgView.center = self.captureSuperview.center
    //
    //    }
    
    
    func reloadView()
    {
        let url = URL(string: mainDictFromCoverCase["n_mask_image"] as? String ?? "")
        
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            self.afterReload(image: UIImage(data: imageData) ?? UIImage())
            RenderAtTransparentPart(image: UIImage(data: imageData)!, viewformask: ViewforCapture)
        }
    }
    
    func afterReload(image: UIImage)
    {
        RemoveLoader()
        
        self.ViewforCapture.isHidden = false
        
        //        var ratio : CGFloat = 0.5
        //        ratio = image.size.width / image.size.height
        
        var ratioCover : CGFloat = 0.5
        let imgCover = Cover_ImgView.image!
        ratioCover = imgCover.size.width / imgCover.size.height
        
        var frameCoverTemp = CGRect()
        var frameMaskTemp = CGRect()
        
        var diff = UIScreen.main.bounds.size.width - imgCover.size.width
        diff -= 80.0
        
        if (imgCover.size.width > (UIScreen.main.bounds.size.width - 80)) || (diff > 0.0)
        {
            frameCoverTemp = CGRect(x: 0.0, y: 0.0, width: (UIScreen.main.bounds.size.width - 80), height: (UIScreen.main.bounds.size.width - 80)/ratioCover)
            Cover_ImgView.frame = frameCoverTemp
            
            let ratioDecrease = (UIScreen.main.bounds.size.width - 80) / imgCover.size.width
            frameMaskTemp = CGRect(x: 0.0, y: 0.0, width: image.size.width * ratioDecrease, height: image.size.height * ratioDecrease)
            Mask_ImgView.frame = frameMaskTemp
        }
        else
        {
            frameCoverTemp = CGRect(x: 0.0, y: 0.0, width: imgCover.size.width, height: imgCover.size.height)
            Cover_ImgView.frame = frameCoverTemp
            
            frameMaskTemp = CGRect(x: 0.0, y: 0.0, width: image.size.width, height: image.size.height)
            Mask_ImgView.frame = frameMaskTemp
        }
        
        self.Mask_ImgView.layoutIfNeeded()
        self.Cover_ImgView.layoutIfNeeded()
        
        let frameMask = self.Cover_ImgView.frame
        self.captureSuperview.frame = frameMask
        
        self.ViewforCapture.frame = frameMaskTemp
        
        self.Mask_ImgView.frame = frameMaskTemp
        self.Cover_ImgView.frame = frameCoverTemp
        
        let screenCenter = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        captureSuperview.center = screenCenter
        captureSuperview.center.y = screenCenter.y * (DeviceType.IS_IPHONE_X ? 0.85 : 0.90)
        
        ViewforCapture.center = CGPoint(x: self.captureSuperview.frame.size.width  / 2,
                                        y: self.captureSuperview.frame.size.height / 2)
        
        if fancyMugImage != nil
        {
            AddStickertoView(Image: fancyMugImage , isSticker : false)
            fancyMugImage = nil
        }
        
        self.defaultLabel.isHidden = false
        Add_Footer_Label()
        
        //        if isSelectZeroIndex
        //        {
        self.btnAddPhoto = UIButton(frame: CGRect(x: 0, y: 0 , width: 100, height: 100))
        self.btnAddPhoto.setImage(UIImage(named: "add_photo"), for: .normal)
        
        self.btnAddPhoto.center = CGPoint(x: self.ViewforCapture.frame.size.width  / 2,
                                          y: self.ViewforCapture.frame.size.height / 2)
        
        self.btnAddPhoto.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        self.ViewforCapture.addSubview(self.btnAddPhoto)
        //        }
        
        Mask_ImgView.center = self.captureSuperview.center
        Cover_ImgView.center = self.ViewforCapture.center
        
        //        if isSelectZeroIndex
        //        {
        // Enable or Disable Add Photo Button
        if ViewforCapture.subviews.count > 2 && !isFromViewDidLoad
        {
            btnAddPhoto.isHidden = true
        }
        else
        {
            btnAddPhoto.isHidden = false
        }
        //        }
        //        else
        //        {
        //            if ViewforCapture.subviews.count == 1
        //            {
        //                btnAddPhoto.isHidden = true
        //            }
        //            else
        //            {
        //                btnAddPhoto.isHidden = false
        //            }
        //        }
        
        self.view.layoutIfNeeded()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.isSetupAlreadyDone = true
        }
        
    }
    
    func Add_Footer_Label() {
        
        // Create Bottom Default Label
        defaultLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(defaultLabel)
        defaultLabel.topAnchor.constraint(equalTo: self.captureSuperview.bottomAnchor, constant: 0).isActive = true
        defaultLabel.bottomAnchor.constraint(equalTo: self.bottomMenuCollView.topAnchor, constant: 0).isActive = true
        defaultLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        defaultLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        defaultLabel.textAlignment = .center
        defaultLabel.numberOfLines = 0
        
        if DeviceType.IS_IPAD
        {
            defaultLabel.font = UIFont(name: CustomFontWeight.medium, size: 22.0)
        }else
        {
            defaultLabel.font = UIFont(name: CustomFontWeight.medium, size: 15.0)
        }
        
        defaultLabel.text = "Please add high resolution image to get high clarity mug"
        
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        CromaColorPicker.delegate = self
        CromaColorPicker.backgroundColor = .white
        
        addBackButton()
        
        checkRechabilityStatus()
        
        showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
        
        self.ViewforCapture.isHidden = true
        self.defaultLabel.isHidden = true
        
        //        mainDict = userDefault.value(forKey: "MainDict") as! [String : Any]
        
    }
    
    //MARK:- ********************** CHECK RECHABILITY STATUS **********************
    
    func checkRechabilityStatus()
    {
        let reachabilityManager = NetworkReachabilityManager()

        reachabilityManager?.startListening(onUpdatePerforming: { _ in
            if let isNetworkReachable = reachabilityManager?.isReachable,
                isNetworkReachable == true {
                
                print(self.internetSpeed)
                
                //Internet Available
                print("Internet Available")
                if self.internetSpeed < 0.06
                {
                    self.RemoveLoader()
                    
                    self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                        self.popViewController()
                    })
                }else
                {
                    self.captureSuperview.isHidden = false
                }
                
            } else {
                //Internet Not Available"
                print("Internet Not Available")
                
                if !self.isSetupAlreadyDone
                {
                    self.RemoveLoader()
                    self.captureSuperview.isHidden = true
                    self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                        self.popViewController()
                    })
                }
                
            }
        })
    }
    
    //MARK:- ********************** TOUCHES LIFECYCLE **********************
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch: UITouch? = touches.first
        if touch?.view != selectedStickerView {
            selectedStickerView?.showEditingHandlers = false
        }
        
    }
    
    //MARK:- ********************** BUTTON EVENTS **********************
    
    @IBAction func ExportImage_Pressed(_ sender: UIButton) {
        
        selectedStickerView?.showEditingHandlers = false
        
        //        CallApi(isPng: false)
        
    }
    
    @IBAction func AddSticker_Pressed(_ sender: UIButton) {
        
        //        let SticerVC = mainStoryBoard.instantiateViewController(withIdentifier: "StickerCollectionView") as! StickerCollectionView
        //        self.navigationController?.pushViewController(SticerVC, animated: true)
        
        
    }
    
    @IBAction func AddTextSticker_Pressed(_ sender: UIButton) {
        
        //        let AddText = mainStoryBoard.instantiateViewController(withIdentifier: "AddTextViewController") as! AddTextViewController
        //        self.navigationController?.pushViewController(AddText, animated: true)
        
    }
    
    @objc func buttonClicked() {
        OpenImagePicker()
        
    }
    
    //    @objc func doubleTapped(gesture: UITapGestureRecognizer) {
    //
    //        doubleTapStickerView?.frame.size = CGSize(width: 100.0, height: 100.0)
    //
    //    }
    
    func OpenImagePicker() {
        
        DispatchQueue.main.async {
            
            let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { action in
                
                DispatchQueue.main.async {
                    self.OpenCamera()
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: { action in
                
                DispatchQueue.main.async {
                    self.OpenGallery()
                }
                
            }))
            
            //            alert.addAction(UIAlertAction(title: "Choose from Instagram", style: .default, handler: { action in
            //
            //                AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            //
            //                let InstagramVC = mainStoryBoard.instantiateViewController(withIdentifier: "InstagramViewController") as! InstagramViewController
            //                self.pushViewController(InstagramVC)
            //
            //            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            }))
            
            // Add the actions
            self.picker.delegate = self
            self.picker.modalPresentationStyle = .overCurrentContext
            self.present(alert, animated: true)
            
            alert.view.tintColor = UIColor.black
        }
    }
    
    //MARK:- ********************** Don't Allowe Camera & Gallry Permission **********************
    //MARK:- Camera and gallery permission
    
    func requestCameraPermission() {
        
        DispatchQueue.main.async {
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                guard accessGranted == true else { return }
            })
        }
    }
    
    func alertCameraAccessNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Need Camera Access",
                message: "Camera access is required to capture photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func requestGalleryPermission() {
        
        DispatchQueue.main.async {
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                guard accessGranted == true else { return }
                
            })
        }
    }
    func alertGalleryAccessNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Need Gallery Access",
                message: "Gallery access is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Allow Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func alertGalleryAccessPermissionChangeNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Change Gallery Access",
                message: " Change Gallery access from Selected Photos to All Photos is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) -> Void in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Change Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
        if isLogin && isSelectZeroIndex
        {
            AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            ((tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)
            self.tabBarController?.selectedIndex = 0
            
            isLogin = false
        }else
        {
            self.popViewController()
        }
        
    }
    
    func CompareImage (lhs: UIImage, rhs: UIImage) -> Bool
    {
        guard let data1 = lhs.pngData(),
            let data2 = rhs.pngData()
            else { return false }
        
        return data1 == data2
    }
    
    //MARK:- ********************** IMAGE CAPTURE & IMAGE SIZE **********************
    
    func captureScreen(view:UIView) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0);
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func image(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }
    
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bottomItemsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bottomTabbarCollCell", for: indexPath) as! bottomTabbarCollCell
        
        cell.ButtonImageView.image = bottomItemsArr[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            OpenImagePicker()
            
        }
        else if indexPath.row == 1 {
            
            AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            
            let AddText = mainStoryBoard.instantiateViewController(withIdentifier: "AddTextViewController") as! AddTextViewController
            pushViewController(AddText)
            
        }
        else if indexPath.row == 2
        {
            guard !isLoaded else {
                return
            }
            
            isLoaded = true
            
            isSelectSticker = "MugEditingVC"
            AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            let mainsticVC = mainStoryBoard.instantiateViewController(withIdentifier: "mainStickerViewController") as! mainStickerViewController
            pushViewController(mainsticVC)
            isLoaded = false
            
        }
        else if indexPath.row == 3
        {
            let TFPopup = MZFormSheetPresentationViewController.init(contentView: CromaVIew)
            TFPopup.presentationController?.contentViewSize = CGSize(width: self.view.Getwidth*0.4, height: self.view.Getwidth*0.4)
            TFPopup.presentationController?.shouldCenterVertically = true
            TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
            self.present(TFPopup, animated: true)
        }
        else if indexPath.row == 4
        {
            guard !isLoaded else {
                return
            }
            
            isLoaded = true
            
            AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            
            let BGimagesVC = mainStoryBoard.instantiateViewController(withIdentifier: "BackgroundImagesViewController") as! BackgroundImagesViewController
            pushViewController(BGimagesVC)
            isLoaded = false
            
        }
        else if indexPath.row == 5
        {
            if userDefault.value(forKey: "USER_ID") == nil || userDefault.integer(forKey: "USER_ID") == 0
            {
                self.presentAlertWithTitle(title: "Log In", message: "Need to log in first", options: "CANCEL","OK") { (ButtonIndex) in
                    if ButtonIndex == 0
                    {
                        self.dismissViewController()
                    }else
                    {
                        isLoginOrNot = "edit_login"
                        isFromMugModule = true
                        
                        //                        self.tabBarController?.selectedIndex = 4
                        
                        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
                        
                        let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.pushViewController(signinVC)
                    }
                }
            }else
            {
                _selectedStickerView?.showEditingHandlers = false
                
                if isSelectZeroIndex
                {
                    if self.ViewforCapture.subviews.count > 2 || (!CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .clear)!) && !CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .white)!))
                    {
                        self.presentAlertWithTitle(title: "Add to cart?", message: "", options: "NO","YES") { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.dismissViewController()
                            }else
                            {
                                self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                                    //                                    self.Add_To_Cart()
                                    
                                    self.Add_To_Cart_mug()
                                }
                            }
                        }
                    }else
                    {
                        self.presentAlertWithTitle(title: "Please Select Image", message: "", options: "OK") { (Int) in
                        }
                    }
                }else
                {
                    if self.ViewforCapture.subviews.count > 2 || (!CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .clear)!) && !CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .white)!))
                    {
                        self.presentAlertWithTitle(title: "Add to cart?", message: "", options: "NO","YES") { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.dismissViewController()
                            }else
                            {
                                self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                                    //                                    self.Add_To_Cart()
                                    self.Add_To_Cart_mug()
                                }
                            }
                        }
                    }else
                    {
                        self.presentAlertWithTitle(title: "Please Select Image", message: "", options: "OK") { (Int) in
                        }
                    }
                }
                
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.Getwidth / 6, height: DeviceType.IS_IPAD ? 100.0 : 50.0)
    }
    
    //MARK:- ********************** OTHER METHODS **********************
    
    func OpenCamera()
    {
        
        DispatchQueue.main.async {
            
            self.picker = UIImagePickerController()
            self.picker.delegate = self
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                //check permission
                if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                    //already authorized, open camera
                    
                    DispatchQueue.main.async {
                        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
                        
                        self.picker.sourceType = .camera
                        self.present(self.picker, animated: true)
                    }
                    
                }
                else if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
                    //request for camera permission
                    self.requestCameraPermission()
                }
                else
                {
                    AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                        if granted {
                            //access allowed
                            DispatchQueue.main.async {
                                
                                AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
                                
                                self.picker.sourceType = .camera
                                self.present(self.picker, animated: true)
                            }
                            
                        } else {
                            //access denied or restricted
                            self.alertCameraAccessNeeded()
                        }
                    })
                }
                
            } else {
                //if camera is not available then show alert
                let alert = UIAlertController(title: "Alert", message: "No Camera Available.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true)
                
            }
            
        }
        
    }
    
    func OpenGallery()
    {
        
        DispatchQueue.main.async {
            
            //check user has allowed permission or not
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    
                    DispatchQueue.main.async {
                        self.picker = UIImagePickerController()
                        self.picker.delegate = self
                        self.picker.sourceType = .photoLibrary
                        self.present(self.picker, animated: true)
                    }
                    
                    break
                case .denied, .restricted:
                    self.alertGalleryAccessNeeded()
                    break
                case .notDetermined:
                    self.requestGalleryPermission()
                    break
                case .limited:
                    self.alertGalleryAccessPermissionChangeNeeded()
                default:
                    break
                }
            }
        }
        
    }
    
    //    func CallApi(isPng : Bool) {
    //
    //        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
    //
    //        let ApiUrl = "https://printphoto.in/Photo_case/api/demoImage"
    //
    ////        let imageData = isPng ? UIImage(view: ViewforCapture).pngData() : UIImage(view: ViewforCapture).jpegData(compressionQuality: 1.0)
    //
    //        let image123 = image(with: ViewforCapture.getImage(scale: 5.0), scaledTo: CGSize(width: 1188, height: 2064))
    //
    //        let image1 = UIImage(cgImage: image123.cgImage!, scale: 1.0, orientation: .upMirrored)
    //
    ////        let image123 = ViewforCapture.getImage(scale: 7.0)
    //        let imageData = image1.jpegData(compressionQuality: 1.0)
    //
    //        RenderAtTransparentPart(image: UIImage(named: "note5promask")!, viewformask: ViewforCapture)
    //
    //        manager.post(ApiUrl, parameters: nil, constructingBodyWith: { (formData) in
    //            var str = ProcessInfo.processInfo.globallyUniqueString
    //            str = isPng ? str.appending(".png") : str.appending(".jpg")
    //
    //
    //            formData.appendPart(withFileData: imageData!, name: "image", fileName: str, mimeType: "image/jpeg")
    //
    //
    //        }, progress: { (uploadProgress) in
    //
    //
    //        }, success: { (task, responseObject) in
    //
    //
    //            let predct = responseObject as! NSDictionary
    //
    //            print(predct)
    //
    //            self.activityIndicator.hide()
    //
    //            self.presentAlertWithTitle(title: "", message: "Press OK to watch image", options: "OK", completion: { (AlertAction) in
    //                if AlertAction == 0
    //                {
    //                    guard let url = URL(string: predct.value(forKeyPath: "data.image") as! String) else {
    //                        return
    //                    }
    //
    //                    if #available(iOS 10.0, *) {
    //                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    //                    } else {
    //                        UIApplication.shared.openURL(url)
    //                    }
    //                }
    //            })
    //
    //
    //
    //
    //
    //        }, failure: { (task, error) in
    //
    //            self.activityIndicator.hide()
    //            print(error)
    //
    //            self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (AlertAction) in
    //
    //            })
    //
    //
    //        })
    //
    //    }
    
    func RenderAtTransparentPart(image: UIImage, viewformask : UIView) {
        
        let cim = CIImage(image: image)
        let filter = CIFilter(name:"CIMaskToAlpha")!
        filter.setValue(cim, forKey: "inputImage")
        let out = filter.outputImage!
        let cgim = CIContext().createCGImage(out, from: out.extent)
        let lay = CALayer()
        lay.frame = viewformask.bounds
        lay.contents = cgim
        viewformask.layer.mask = lay
        
    }
    
    func AddStickertoView(Image : UIImage , isSticker : Bool) {
        
        let StickerImgView = UIImageView(image: Image)
        StickerImgView.isUserInteractionEnabled = true
        
        var ratioSize : CGFloat?
        
        if isSticker
        {
            ratioSize = 0.2
        }else
        {
            ratioSize = 1.2
        }
        
        let ratio = Image.size.width / Image.size.height
        if StickerImgView.frame.width > ViewforCapture.frame.height * ratioSize! {
            let newHeight = ViewforCapture.frame.width / ratio
            StickerImgView.frame.size = CGSize(width: ViewforCapture.frame.width * ratioSize!, height: newHeight * ratioSize!)
        }
        else{
            let newWidth = ViewforCapture.frame.height * ratio
            StickerImgView.frame.size = CGSize(width: newWidth * ratioSize!, height: ViewforCapture.frame.height * ratioSize!)
        }
        
        let StickerViewforAdd = StickerView.init(contentView: StickerImgView)
        StickerViewforAdd.delegate = self
        StickerViewforAdd.setImage(UIImage.init(named: "Close")!, forHandler: StickerViewHandler.close)
        StickerViewforAdd.enableRotate = false
        StickerViewforAdd.enableFlip = false
        StickerViewforAdd.setHandlerSize(25)
        StickerViewforAdd.outlineBorderColor = UIColor.darkGray
        StickerViewforAdd.showEditingHandlers = false
        
        if (StickerBackgroung != nil)
        {
            ViewforCapture.insertSubview(StickerViewforAdd, at: 1)
        }else
        {
            ViewforCapture.addSubview(StickerViewforAdd)
        }
        
        StickerViewforAdd.center = CGPoint(x: ViewforCapture.Getwidth * 0.5, y: ViewforCapture.Getheight * 0.5)
        
        //        if isSelectZeroIndex
        //        {
        if self.ViewforCapture.subviews.count > 2 && !self.isFromViewDidLoad
        {
            self.btnAddPhoto.isHidden = true
        }else
        {
            self.btnAddPhoto.isHidden = false
        }
        //        }
        
        
        //        doubleTapStickerView = StickerViewforAdd
        //
        //        // Zoom Image when Double Tap on Gesture
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped(gesture:)))
        //        tap.numberOfTapsRequired = 2
        //        doubleTapStickerView?.addGestureRecognizer(tap)
    }
    
    func AddStickertoView(TextString : String , TextColor : UIColor ,FontStyle : String) {
        
        let StickerLbl = UILabel()
        StickerLbl.text = TextString
        StickerLbl.font = UIFont.init(name: FontStyle, size: ViewforCapture.Getwidth * 0.15)
        StickerLbl.textColor = TextColor
        StickerLbl.textAlignment = .center
        StickerLbl.sizeToFit()
        
        //        StickerLbl.frame = CGRect(x: StickerLbl.Getx + 25.0, y: StickerLbl.Gety + 25.0, width: StickerLbl.Getwidth + 100.0, height: StickerLbl.Getheight + 100.0)
        
        StickerLbl.frame = CGRect(x: 0.0, y: 0.0, width: StickerLbl.Getwidth + 50.0, height: StickerLbl.Getheight + 50.0)
        
        print(StickerLbl.text?.heightOfLabel(withConstrainedWidth: StickerLbl.Getwidth, font: StickerLbl.font) ?? "")
        
        let StickerViewforAdd = StickerView.init(contentView: StickerLbl)
        StickerViewforAdd.delegate = self
        StickerViewforAdd.setImage(UIImage.init(named: "Close")!, forHandler: StickerViewHandler.close)
        StickerViewforAdd.enableRotate = false
        StickerViewforAdd.enableFlip = false
        StickerViewforAdd.isLabel = true
        StickerViewforAdd.setHandlerSize(25)
        StickerViewforAdd.outlineBorderColor = UIColor.darkGray
        StickerViewforAdd.showEditingHandlers = false
        
        ViewforCapture.addSubview(StickerViewforAdd)
        
        StickerViewforAdd.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5) //(StickerViewforAdd.transform, 0.1, 0.1)
        //        StickerViewforAdd.setHandlerSize(DeviceType.IS_IPAD ? 25 : 18)
        
        StickerViewforAdd.center = CGPoint(x: ViewforCapture.Getwidth * 0.5, y: ViewforCapture.Getheight * 0.5)
        
        if self.ViewforCapture.subviews.count > 2 && !self.isFromViewDidLoad
        {
            self.btnAddPhoto.isHidden = true
        }else
        {
            self.btnAddPhoto.isHidden = false
        }
        
    }
    
    //MARK:- ********************** IMAGEPICKERVIEW DELEGATE **********************
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
        
        if info[.imageURL] != nil && !((info[.imageURL] as? URL)?.absoluteString.uppercased().hasSuffix("GIF"))!{
            
            if let asset = info[.phAsset] as? PHAsset {
                PHImageManager.default().requestImageData(for: asset, options: nil) { (dataObj, uti, orientation, information) in
                    
                    //                        let nsdataObj = dataObj! as NSData
                    
                    //                        print("File Size \(nsdataObj.length/1000) KB")
                    
                    let heightInPoints = (info[.originalImage] as! UIImage).size.height
                    let heightInPixels = heightInPoints * (info[.originalImage] as! UIImage).scale
                    
                    let widthInPoints = (info[.originalImage] as! UIImage).size.width
                    let widthInPixels = widthInPoints * (info[.originalImage] as! UIImage).scale
                    
                    /*
                    if heightInPixels <= 720 || widthInPixels <= 720 //nsdataObj.length/1024 < 500
                    {
                        self.topMostViewController().view.makeToast("Please select high resolution image", duration: 2.0, position: .bottom)
                    }else
                    {*/
                        self.dismiss(animated: true) {
                            self.AddStickertoView(Image: info[.originalImage] as! UIImage , isSticker : false)
                            
                            if isSelectZeroIndex
                            {
                                // Enable or Disable Add Photo Button
                                if self.ViewforCapture.subviews.count > 2 && !self.isFromViewDidLoad
                                {
                                    self.btnAddPhoto.isHidden = true
                                }else
                                {
                                    self.btnAddPhoto.isHidden = false
                                }
                            }
                        }
                    //}
                    
                }
            }
        }
        else
        {
            self.AddStickertoView(Image: info[.originalImage] as! UIImage , isSticker : false)
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
        self.dismissViewController()
        
    }
    
    //MARK:- ********************** CROMA COLOR PICKER DELEGATE **********************
    
    func colorPickerDidChooseColor(_ colorPicker: ChromaColorPicker, color: UIColor) {
        //Set color for the display view
        
        defaultImgView.image = UIImage(color: color)
        
        //        ViewforCapture.backgroundColor = color
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- ********************** STICKER DELEGATE **********************
    
    func stickerViewDidBeginMoving(_ stickerView: StickerView) {
        self.selectedStickerView = stickerView
    }
    
    func stickerViewDidChangeMoving(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidEndMoving(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidBeginRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidChangeRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidEndRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidClose(_ stickerView: StickerView) {
        
        //        if isSelectZeroIndex
        //        {
        if self.ViewforCapture.subviews.count > 3
        {
            self.btnAddPhoto.isHidden = true
        }else
        {
            self.btnAddPhoto.isHidden = false
        }
        //        }
        
    }
    
    func stickerViewDidTap(_ stickerView: StickerView) {
        self.selectedStickerView = stickerView
    }
    
    
    //MARK:- ********************** API CALLING **********************
    
    func Add_To_Cart_mug() {
        
        if Reachability.isConnectedToNetwork() {
            
            btnAddPhoto.isHidden = true
            
            let scaleSize = CGSize(width: Int(((self.mainDictFromCoverCase["height"] as! NSString).doubleValue * 300).rounded()), height: Int(((self.mainDictFromCoverCase["width"] as! NSString).doubleValue * 300).rounded()))
            
            // Print Image
            self.imageNew(with: ViewforCapture.getImage(scale: 5.0, isMasked: false), scaledTo: scaleSize) { (PrintImage) in
                
                let printMirrorImage = UIImage(cgImage: PrintImage.cgImage!, scale: 1.0, orientation: .upMirrored)
                
                printMirrorImage.rotateNew(radians: -.pi/2) { (rotatePrintImage) in
                    
                    // Covercase Image
                    let imgCover = self.captureSuperview.getImage(scale: 5.0, isMasked: true)
                    
                    self.imageNew(with: imgCover, scaledTo: scaleSize) { (CoverImage) in
                        
                        CoverImage.rotateNew(radians: -.pi/2) { (rotateCoverImage) in
                            
                            let modelID = "\(self.mainDictFromCoverCase["model_id"] ?? "")"
                            
                            let ParamDict = ["model_id": modelID,
                                             "user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                                "quantity": "1"] as NSDictionary
                            
                            AFNetworkingAPIClient.sharedInstance.postAPICallWithMultipleImages(url: "\(BaseURL)add_to_cart", parameters: ParamDict, imageArray: [rotatePrintImage!,rotateCoverImage!], imgKey: ["print_image","case_image"]) { (APIResponce, Responce, error1) in
                                
                                self.RemoveLoader()
                                
                                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                                {
                                    
                                    let responceDict = Responce! as NSDictionary
                                    
                                    if responceDict["ResponseCode"] as! String == "1"
                                    {
                                        // Maintain flag when go to login screen
                                        
                                        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
                                        
                                        self.logAddToCartEvent(contentData: "\(self.mainDictFromCoverCase["modalName"] ?? "")", contentId: modelID, contentType: mainProductName ?? "", currency: "INR", price: Double("\(self.mainDictFromCoverCase?["price"] ?? "")") ?? 0.0)
                                        
                                        isCartBtnBack = false
                                        
                                        ((self.tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
                                        self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 1 : 2
                                        
                                    }
                                    else
                                    {
                                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                                            
                                        })
                                    }
                                }
                                else
                                {
                                    self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
                                    })
                                    
                                }
                            }
                        }
                    }
                }
            }
            
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    func imageNew(with image: UIImage, scaledTo newSize: CGSize, completion: @escaping((UIImage) -> ())) -> () {
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            //        DispatchQueue.global(qos: .userInitiated).async {
            
            UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
            image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            //            DispatchQueue.main.async {
            //                let compressData = newImage?.jpegData(compressionQuality: 0.1)
            //                let compressedImage = UIImage(data: compressData!)!
            
            completion(newImage ?? UIImage())
            //            }
        }
    }
    
    func Add_To_Cart() {
        
        if Reachability.isConnectedToNetwork() {
            
            btnAddPhoto.isHidden = true
            
            // Print Image
            let PrintImage = image(with: ViewforCapture.getImage(scale: 5.0, isMasked: false), scaledTo: CGSize(width: Int((self.mainDictFromCoverCase["height"] as! NSString).doubleValue * 300), height: Int((self.mainDictFromCoverCase["width"] as! NSString).doubleValue * 300)))
            let printMirrorImage = UIImage(cgImage: PrintImage.cgImage!, scale: 1.0, orientation: .upMirrored)
            let rotatePrintImage = printMirrorImage.rotate(radians: -.pi/2)
            
            // Covercase Image
            let CoverImage = image(with: captureSuperview.getImage(scale: 5.0, isMasked: true), scaledTo: CGSize(width: Int((self.mainDictFromCoverCase["height"] as! NSString).doubleValue * 300), height: Int((self.mainDictFromCoverCase["width"] as! NSString).doubleValue * 300)))
            let rotateCoverImage = CoverImage.rotate(radians: -.pi/2)
            
            let modelID = "\(mainDictFromCoverCase["model_id"] ?? "")"
            
            let ParamDict = ["model_id": modelID,
                             "user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                "quantity": "1"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICallWithMultipleImages(url: "\(BaseURL)add_to_cart", parameters: ParamDict, imageArray: [rotatePrintImage!,rotateCoverImage!], imgKey: ["print_image","case_image"]) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce! as NSDictionary
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        // Maintain flag when go to login screen
                        
                        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
                        
                        //                        let dataArray = responceDict.value(forKeyPath: "data.cart.cart_items") as? NSArray
                        //
                        //                        let arr = dataArray?.filter({ (obj) -> Bool in
                        //
                        //                            let objDict = obj as! NSDictionary
                        //
                        //                            let model_id = objDict.value(forKey: "model_id") as? String ?? ""
                        //
                        //                            if model_id == modelID
                        //                            {
                        //                                let model_name = objDict.value(forKey: "model_name") as? String ?? ""
                        //                                let price = objDict.value(forKey: "subtotal") as? Int ?? 0
                        
                        self.logAddToCartEvent(contentData: "\(self.mainDictFromCoverCase["modalName"] ?? "")", contentId: modelID, contentType: mainProductName ?? "", currency: "INR", price: Double("\(self.mainDictFromCoverCase?["price"] ?? "")") ?? 0.0)
                        
                        //                                return true
                        //                            }
                        //
                        //                            return false
                        //
                        //                        })
                        
                        isCartBtnBack = false
                        
                        ((self.tabBarController!.viewControllers! as NSArray)[3] as! UINavigationController).popToRootViewController(animated: false)
                        self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 1 : 2
                        
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
    //MARK:- ********************** FACEBOOK ANALYTICS **********************
    
    /**
     * For more details, please take a look at:
     * developers.facebook.com/docs/swift/appevents
     */
    
    func logAddToCartEvent(contentData : String, contentId : String, contentType : String, currency : String, price : Double)
    {
        
        // Firebase Add To Cart Analytics
        
        Analytics.logEvent("add_to_cart", parameters: [
            AnalyticsParameterItemID: contentId,
            AnalyticsParameterItemName: contentData,
            AnalyticsParameterContentType: contentType,
            AnalyticsParameterPrice:price,
            AnalyticsParameterCurrency:currency
            ])
        
        
        // Facebook Add To Cart Analytics
        
//        let params = [
//            "content" : contentData,
//            "contentId" : contentId,
//            "contentType" : contentType,
//            "currency" : currency
//            ] as [String : Any]
        let params = [
            "item_id" : contentId, //"contentId"
            "item_name" : contentData, //"content"
            "content_type" : contentType, //contentType
            "price": price,
            "currency" : currency //currency
        ] as [String : Any]
        
        AppEvents.logEvent(.addedToCart, valueToSum: price, parameters: params)
    }
    
}
