//
//  BackgroundImagesViewController.swift
//  Print_Photo_App
//
//  Created by des on 31/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class BackgroundImagesViewController: BaseViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var backgrounImaegCollView: UICollectionView!
    
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var imgWidth : CGFloat?
    var imgHeight : CGFloat?
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Background Images", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
            self.backgrounImaegCollView.isHidden = false
    }
    
    //MARK:- ********************** Initialize **********************
    
    func Initialize() {
        
//        if BackgroundImgArr.count == 0
//        {
            self.backgrounImaegCollView.isHidden = true
            CallApi()
//        }
        
        addBackButton()
        removeBackButton()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func CallApi() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)background_images?category_id=\(mainProductID ?? 0)", parameters: nil) { (APIResponce, Responce, error1) in
                            
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["success"] as! Int == 1
                    {
                        BackgroundImgArr = responceDict["data"] as! [[String:Any]]
                        self.backgrounImaegCollView.reloadData()
                    }else{
                        self.presentAlertWithTitle(title: "\(responceDict["message"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.CallApi()
                        }
                    })
                    
                }
            }
            
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.popViewController()
                }
            })
        }
    }
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return BackgroundImgArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "backgrounImgCollCell", for: indexPath) as! backgrounImgCollCell
        
        cell.BackgrndImgView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: (BackgroundImgArr[indexPath.row])["n_image"] as! String)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (collectionView.cellForItem(at: indexPath) as! backgrounImgCollCell).BackgrndImgView.image != nil {
            
            StickerBackgroung = (collectionView.cellForItem(at: indexPath) as! backgrounImgCollCell).BackgrndImgView.image!
            
            popViewController()
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        var imgSize = CGSize()

        if BackgroundImgArr.count > 0
        {
            let imageDict = BackgroundImgArr[indexPath.row] as NSDictionary
            
            let categoryWidth = (imageDict.value(forKeyPath: "category") as! NSDictionary).value(forKey: "ios_width") as? CGFloat
            let categoryHeight = (imageDict.value(forKeyPath: "category") as! NSDictionary).value(forKey: "ios_height") as? CGFloat
            
            if categoryHeight != nil && categoryWidth != nil
            {
                if categoryHeight != 0 || categoryWidth != 0
                {
                    imgSize = CGSize(width: self.backgrounImaegCollView.frame.width / categoryWidth!, height: self.backgrounImaegCollView.frame.width / categoryHeight!)

                }else
                {
                    imgSize = CGSize(width: self.backgrounImaegCollView.frame.width / 3.0, height: self.backgrounImaegCollView.frame.width / 1.8)
                }
            }else
            {
                imgSize = CGSize(width: self.backgrounImaegCollView.frame.width / 3.0, height: self.backgrounImaegCollView.frame.width / 1.8)
            }
        }else
        {
            imgSize = CGSize(width: self.backgrounImaegCollView.frame.width / 3.0, height: self.backgrounImaegCollView.frame.width / 1.8)
        }
        
        return imgSize //CGSize(width: collectionView.Getwidth * 0.33, height: collectionView.Getwidth * 0.5)
  }
    
}
