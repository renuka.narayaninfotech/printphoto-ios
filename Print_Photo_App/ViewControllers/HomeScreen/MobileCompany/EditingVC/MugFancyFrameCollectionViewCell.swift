//
//  MugFancyFrameCollectionViewCell.swift
//  Print_Photo_App
//
//  Created by Mac Os on 14/08/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class MugFancyFrameCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgFrame: UIImageView!
    
}
