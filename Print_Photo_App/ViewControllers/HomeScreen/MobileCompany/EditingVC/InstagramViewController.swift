//
//  InstagramViewController.swift
//  Print_Photo_App
//
//  Created by Prakash on 03/06/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import WebKit
import Foundation
import CoreFoundation
import Alamofire

struct INSTAGRAM_IDS {
    
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    
    static let INSTAGRAM_CLIENT_ID  = clientID
    
    static let INSTAGRAM_CLIENTSERCRET = clientSecret
    
    static let INSTAGRAM_REDIRECT_URI = REDIRECTURI
    
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    
    static let INSTAGRAM_SCOPE = "likes+comments+relationships"
    
}

class InstagramViewController: BaseViewController, WKNavigationDelegate, WKUIDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var instagramImageWebView: WKWebView!
    @IBOutlet weak var instagramCollectionView: UICollectionView!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var instagramLoginView: UIView!
    @IBOutlet weak var btnLoginView: UIView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************

    var url : String?
    var imageArray = NSMutableArray()
    var isendLoad = false
    var maxId = ""
    
    // Set flag when click on Instagram Login Button And Back button together
    var isClickLoginButton = false
    
    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()

        SetNavigationBarTitle(Startstring: "Instagram", EndString: "")
       
        Initialize()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
  
    //MARK:- ********************** INITIALIZE **********************

    func Initialize() {
 
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)

        addBackButton()
        removeBackButton()
//        CallApi()
        checkRechabilityStatus()
        
        instagramImageWebView.uiDelegate = self
        instagramImageWebView.navigationDelegate = self
        btnLoginView.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        
//        self.instagramImageWebView.isHidden = true
//        self.instagramCollectionView.isHidden = true
//        self.btnLogout.isHidden = true
//        self.instagramLoginView.isHidden = false

        if userDefault.value(forKey: "AuthToken") == nil
        {
            self.instagramImageWebView.isHidden = true
            self.instagramCollectionView.isHidden = true
            self.btnLogout.isHidden = true
            self.instagramLoginView.isHidden = false

        }
        else
        {
            self.instagramImageWebView.isHidden = false
            self.instagramCollectionView.isHidden = false
            self.btnLogout.isHidden = false
            self.instagramLoginView.isHidden = true

            unSignedRequest()
            
            let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI ])
            let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
            instagramImageWebView.load(urlRequest)
            
            self.instagramImageWebView.isHidden = false
            self.instagramCollectionView.isHidden = false
            self.btnLogout.isHidden = false
            self.instagramLoginView.isHidden = true
        }
        
    }
    
    //MARK:- ********************** CHECK RECHABILITY STATUS **********************
    
    func checkRechabilityStatus()
    {
        let reachabilityManager = NetworkReachabilityManager()
        
        reachabilityManager?.startListening(onUpdatePerforming: { _ in
            if let isNetworkReachable = reachabilityManager?.isReachable,
                isNetworkReachable == true {
                //Internet Available
                print("Internet Available")
               
                
            } else {

                self.RemoveLoader()

                //Internet Not Available"
                print("Internet Not Available")
                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                    self.popViewController()
                })
            }
        })
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    @IBAction func press_btn_login(_ sender: UIButton) {
        
        if !isClickLoginButton
        {
            if Reachability.isConnectedToNetwork() {
                
                isClickLoginButton = true

                showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                
                unSignedRequest()
                
                let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI ])
                let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
                instagramImageWebView.load(urlRequest)
                
                self.instagramImageWebView.isHidden = false
                self.instagramCollectionView.isHidden = false
                self.btnLogout.isHidden = false
                self.instagramLoginView.isHidden = true
                
            }
            else
            {
                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                    
                    self.instagramImageWebView.isHidden = true
                    self.instagramCollectionView.isHidden = true
                    self.btnLogout.isHidden = true
                    self.instagramLoginView.isHidden = false
                })
            }
        }
       
    }
    
    @IBAction func press_btn_logout(_ sender: UIButton) {
        
        userDefault.removeObject(forKey: "AuthToken")
        
        self.instagramImageWebView.isHidden = true
        self.instagramCollectionView.isHidden = true
        self.btnLogout.isHidden = true
        self.instagramLoginView.isHidden = false
        
        let dataTypes = Set([WKWebsiteDataTypeCookies,
                             WKWebsiteDataTypeLocalStorage, WKWebsiteDataTypeSessionStorage,
                             WKWebsiteDataTypeWebSQLDatabases, WKWebsiteDataTypeIndexedDBDatabases])
        WKWebsiteDataStore.default().removeData(ofTypes: dataTypes, modifiedSince: NSDate.distantPast, completionHandler: {})
        
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
        if !isClickLoginButton
        {
            isClickLoginButton = true
            self.popViewController()
        }
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** UNSIGNREQUEST **********************
    
    func unSignedRequest () {

        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
        instagramImageWebView.load(urlRequest)
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        }
        
        return true
    }
    
    func handleAuth(authToken: String)  {
        print("Instagram authentication token ==", authToken)
        
//        let url = URL(string: "https://api.instagram.com/v1/users/\(authToken.split(separator: ".")[0])/media/recent/?access_token=\(authToken)")

        userDefault.set(authToken, forKey: "AuthToken")
        
        url = "https://api.instagram.com/v1/users/\(authToken.split(separator: ".")[0])/media/recent/?access_token=\(authToken)"
        
        DispatchQueue.main.async {
            self.Call_Instagram_API(responceUrl: "\(self.url ?? "")&max_id=\(self.maxId)")
        }
        
//        instagramImageWebView.load(URLRequest(url: url!))
        
    }
    
    //MARK:- ********************** WKWEBVIEW METHOD **********************

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        
        let  requestURLString = (navigationResponse.response.url?.absoluteString)
        print(requestURLString ?? "")
        
        decisionHandler(.allow)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.RemoveLoader()
        }

        isClickLoginButton = false

    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        //        print(navigationAction.request.url)
        
        let  requestURLString = (navigationAction.request.url?.absoluteString)
        
        if requestURLString?.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) ?? false {
            if requestURLString!.contains("#access_token=")
            {
                let range: Range<String.Index> = (requestURLString?.range(of: "#access_token=")!)!
                handleAuth(authToken: requestURLString!.substring(from: range.upperBound))
            }
            
        }
        decisionHandler(.allow)
        
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        
        print(error)
        
    }
    
    //MARK:- ********************** COLLECTION VIEW DELEGATE METHOD **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InstagramCollectionViewCell", for: indexPath) as! InstagramCollectionViewCell
        
        let imgUrl = "\((((imageArray.object(at: indexPath.row) as? NSDictionary)?.value(forKeyPath: "images") as? NSDictionary)?.value(forKeyPath: "standard_resolution") as? NSDictionary)?.value(forKey: "url") as? String ?? "")"
        
        cell.imageInstagram.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: imgUrl)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (collectionView.cellForItem(at: indexPath) as! InstagramCollectionViewCell).imageInstagram.image != nil {

            instagramImage = (collectionView.cellForItem(at: indexPath) as! InstagramCollectionViewCell).imageInstagram.image!

            popViewController()

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if DeviceType.IS_IPAD
        {
            if imageArray.count % 2 == 0 && indexPath.row >= (imageArray.count - 4)
            {
                return CGSize(width: collectionView.Getwidth * 0.25, height: collectionView.Getwidth * 0.25 + 10)
            }
            else if indexPath.row == imageArray.count - 1
            {
                return CGSize(width: collectionView.Getwidth * 0.25, height: collectionView.Getwidth * 0.25 + 10)
            }
            
            return CGSize(width: collectionView.Getwidth * 0.25, height: collectionView.Getwidth * 0.25)
        }else
        {
            if imageArray.count % 2 == 0 && indexPath.row >= (imageArray.count - 2)
            {
                return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.5 + 10)
            }
            else if indexPath.row == imageArray.count - 1
            {
                return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.5 + 10)
            }

            return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.5)

        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        if indexPath.row >= self.imageArray.count - 1 && self.maxId != ""
        {
            self.Call_Instagram_API(responceUrl: "\(self.url ?? "")&max_id=\(self.maxId)")
        }
    }
    
    //MARK:- ********************** API CALLING **********************

    func Call_Instagram_API(responceUrl:String) {

        if Reachability.isConnectedToNetwork() {

            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)

            AFNetworkingAPIClient.sharedInstance.getAPICall(url: responceUrl, parameters: nil) { (APIResponce, Responce, error1) in

                self.RemoveLoader()

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {

                        let responceDict = Responce as! [String:Any]

                        self.imageArray.addObjects(from: (responceDict["data"] as! [Any]))
                    
                        self.maxId = (responceDict["pagination"] as! NSDictionary).value(forKeyPath: "next_max_id") as? String ?? ""
                    
                        self.instagramImageWebView.isHidden = true
                        self.instagramCollectionView.isHidden = false
                        self.btnLogout.isHidden = false
                        self.instagramLoginView.isHidden = true
                    
                        self.instagramCollectionView.reloadData()

                }
                else if (APIResponce.response as? HTTPURLResponse)?.statusCode != 200
                {
                    self.instagramImageWebView.isHidden = true
                    self.instagramCollectionView.isHidden = true
                    self.btnLogout.isHidden = true
                    self.instagramLoginView.isHidden = false
                }else
                {
                    self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
                        
                    })

                }
            }

        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
               
            })
        }

    }
}
