//
//  MultiplePhotoFrameVC.swift
//  Print_Photo_App
//
//  Created by Mac Os on 05/08/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import SVGKit
import FBSDKCoreKit
//import FBSDKMarketingKit
import FacebookCore
import Firebase
import TPPDF
import Alamofire
import PocketSVG

//MARK:- Shape cell
class CellShape: UICollectionViewCell
{
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var lblImgNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

let WIDTH = UIScreen.main.bounds.width
let HEIGHT = UIScreen.main.bounds.height

class MultiplePhotoFrameVC: BaseViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,EditDelegate
{
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var widthViewMain: NSLayoutConstraint!
    @IBOutlet weak var heightViewMain: NSLayoutConstraint!
    @IBOutlet weak var colShape: UICollectionView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    let dictParts = NSMutableDictionary()
    var svgImgView: SVGImageView?
    var scaleDecreased : CGFloat = 0.0
    var widthSVG : CGFloat = 0.0
    var heightSVG : CGFloat = 0.0
    var selectedIndex : Int = 0
    var arrShapeLayer = [CAShapeLayer]()
    var selectedCase : Int = 0
    
    var arrSelectedImgs = NSMutableArray()
    
    var mainDictFromCoverCase : [String:Any]!
    var internetSpeed : Double = 0
    var imgNumber = 1
    
    var fileURL = NSURL()
    var fileData = NSData()
    var fileName = String()
    var arrImage = [UIImage]()
    var svgImageView : SVGImageView?
    var diffHorizontal : CGFloat = 0.0
    var diffVertical : CGFloat = 0.0
    let reachabilityManager = NetworkReachabilityManager()

    //MARK:- ********************** VIEW LIFECYLE **********************
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetNavigationBarTitle(Startstring: "\(mainDictFromCoverCase["modalName"] ?? "")", EndString: "")
        
        Initialize()
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        reachabilityManager?.stopListening()
    }
    
    func Initialize() {
        
        addBackButton()
        addRightButtons()
        
        checkRechabilityStatus()
        
        self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.changeCase()
        })
        
        //        //MARK: add tap gesture on main view
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        //        viewMain.isUserInteractionEnabled = true
        //        viewMain.addGestureRecognizer(tap)
    }
    
    func checkRechabilityStatus()
    {
        reachabilityManager?.startListening(onUpdatePerforming: { _ in
            if let isNetworkReachable = self.reachabilityManager?.isReachable,
                isNetworkReachable == true {
                //Internet Available
                print("Internet Available")
                
//                self.dismissViewController()
                
            } else {
                //Internet Not Available"
                print("Internet Not Available")
                
                self.RemoveLoader()
                self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                    self.popViewController()
                })
            }
        })
    }
    
    //MARK:- ********************** BUTTON ACTION  **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        
        if isLogin
        {
            ((tabBarController!.viewControllers! as NSArray)[0] as! UINavigationController).popToRootViewController(animated: false)
            self.tabBarController?.selectedIndex = 0
            
            isLogin = false
        }else
        {
            self.popViewController()
        }
    }
    
    func addRightButtons() {
        
        // Done Button
        let doneButton = UIButton(type: .custom)
        doneButton.setImage(UIImage(named: "ic_done"), for: .normal)
        doneButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        doneButton.addTarget(self, action: #selector(self.addCartAction(_:)), for: .touchUpInside)
        let doneButtonItem = UIBarButtonItem(customView: doneButton)
        
        self.navigationItem.rightBarButtonItems = [doneButtonItem]
        
    }
    
    @objc func addCartAction(_ sender: UIButton) {
        if userDefault.value(forKey: "USER_ID") == nil || userDefault.integer(forKey: "USER_ID") == 0
        {
            self.presentAlertWithTitle(title: "Log In", message: "Need to log in first", options: "CANCEL","OK") { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.dismissViewController()
                }else
                {
                    isLoginOrNot = "multiple_photo_frame_login"
                    let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                    self.pushViewController(signinVC)
                    
                }
            }
        }else
        {
            
            // Add to cart frame when add all images in frame
            
            if !arrSelectedImgs.contains(UIImage(named: "ic_placeholder")!)
            {
                self.presentAlertWithTitle(title: "Add to cart?", message: "", options: "NO","YES") { (ButtonIndex) in
                    if ButtonIndex == 0
                    {
                        self.dismissViewController()
                    }else
                    {
                        self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                            self.generateExamplePDF()
                        }
                    }
                }
                
            }else
            {
                self.presentAlertWithTitle(title: "Alert", message: "Please add all image.", options: "OK") { (ButtonIndex) in
                    
                }
            }
        }
    }
    
    
    //    MARK:- ********************** GESTURE ACTION **********************
    //    @objc func tapFunction(sender:UITapGestureRecognizer) {
    //        let point = sender.location(in:self.viewMain)
    //        print("\(point) tap working")
    //
    //        let pointConverted = CGPoint.init(x: point.x / self.scaleDecreased, y: point.y / self.scaleDecreased)
    //
    //        if self.svgImgView!.image != nil
    //        {
    //            //MARK:- Old logic
    //
    //            //            if let layer = self.svgImgView!.image!.caLayerTree!.hitTest(pointConverted) as? CAShapeLayer { // If you hit a layer and if its a Shapelayer
    //            //
    //            //                for i in 0..<self.arrShapeLayer.count
    //            //                {
    //            //                    if layer == self.arrShapeLayer[i]
    //            //                    {
    //            //                        self.selectedIndex = i
    //            //
    //            //                        let story = UIStoryboard.init(name: "Main", bundle: nil)
    //            //                        let vc = story.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
    //            //                        vc.shapeLayer = arrShapeLayer[i]
    //            //                        vc.selectedIndex = i
    //            //                        vc.delegate = self
    //            //                        self.navigationController?.pushViewController(vc, animated: true)
    //            //
    //            //                        break
    //            //                    }
    //            //                }
    //            //            }
    //
    //            //MARK:- New logic
    //
    //            var indexTemp: Int!
    //
    //            if let arrLayers = svgImgView!.image!.caLayerTree!.sublayers // get all sub layers
    //            {
    //                for i in 0..<arrLayers.count
    //                {
    //                    if let _ = indexTemp { break }
    //                    if arrLayers[i].frame.contains(pointConverted) { // Optional, if you are inside its content path
    //                        print("Hit Layer")
    //
    //                        if let layer = arrLayers[i] as? CAShapeLayer { // If its shape layer
    //
    //                            for j in 0..<self.arrShapeLayer.count
    //                            {
    //                                if layer == self.arrShapeLayer[j] // If its in our selected shapes
    //                                {
    //                                    indexTemp = j // get index of object
    //                                    break
    //                                }
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //
    //            if let idx = indexTemp // If there is any index
    //            {
    //                let arrShapeImage = mainDictFromCoverCase["frame_details"] as! NSArray
    //
    //
    //                var shapeImage = self.arrSelectedImgs.object(at: idx) as! UIImage
    //
    //                if shapeImage == UIImage(named: "ic_placeholder")!
    //                {
    //                    let url = URL(string:(arrShapeImage.object(at: idx) as! NSDictionary).value(forKey: "mask_image") as? String ?? "")
    //
    //                    if let data = try? Data(contentsOf: url!)
    //                    {
    //                        let image: UIImage = UIImage(data: data)!
    //                        shapeImage = image
    //                    }
    //                }
    //
    //                self.selectedIndex = idx
    //
    //                // Redirect to editor
    //                let EditingPhotoFrameVC = mainStoryBoard.instantiateViewController(withIdentifier: "EditingMultiplePhotoFrameVC") as! EditingMultiplePhotoFrameVC
    //                EditingPhotoFrameVC.shapeLayer = shapeImage
    //                EditingPhotoFrameVC.selectedIndex = self.selectedIndex
    //                EditingPhotoFrameVC.delegate = self
    //                self.pushViewController(EditingPhotoFrameVC)
    //
    //
    //            }
    //        }
    //    }
    
    //MARK:- ********************** CHANGE MAIN SVG FRAME **********************
    
    func changeCase()
    {
        
        self.dictParts.removeAllObjects() // remove old parts
        self.arrShapeLayer.removeAll() // remove old shapes
        
        //MARK: remove main SVG imageview
        if let view = self.viewMain.viewWithTag(1000) as? SVGKFastImageView // If there is SVG imageview in main view
        {
            view.removeFromSuperview() // remove it for new imageview
        }
        
        //MARK: remove all shape images
        for i in 1...10 // loop for 10 shapes
        {
            if let view = self.viewMain.viewWithTag(i) as? UIImageView // If any images added on main view
            {
                view.removeFromSuperview() // remove it from main view
            }
        }
        
        //MARK: Install new SVG image When Internet is ON
        
        guard let imgUrl = URL(string: self.mainDictFromCoverCase["n_modal_image"] as? String ?? "") else {return} // Open SVG image When Internet is ON other wise return
        guard (try? Data(contentsOf: imgUrl )) != nil else { return } // Open SVG image When Internet is ON other wise return
        
        let svgImage = SVGKImage.init(contentsOf: imgUrl) // get new SVG image by name
        
        svgImageView = SVGImageView.init(contentsOf: imgUrl)
        
        //MARK: Initialize SVG ImageView
        
        var widthTemp : CGFloat = svgImage!.size.width
        var heightTemp : CGFloat = svgImage!.size.height
        
        self.scaleDecreased = UIScreen.main.bounds.width / widthTemp
        
        if widthTemp > WIDTH // If image width is greater than screen width
        {
            widthTemp = WIDTH // set image width as screen width
            heightTemp *= self.scaleDecreased // set heigth appropriate
            
            let heightArea = UIScreen.main.bounds.height - UIApplication.shared.keyWindow!.safeAreaInsets.bottom - UIApplication.shared.keyWindow!.safeAreaInsets.top - 130.0
            
            if heightTemp > heightArea // If image height is greater after decrease width
            {
                self.scaleDecreased = heightArea / svgImage!.size.height
                
                heightTemp = heightArea
                widthTemp = svgImage!.size.width * self.scaleDecreased
                
            }
        }
        
        self.widthSVG = widthTemp
        self.heightSVG = heightTemp
        
        self.widthViewMain.constant = widthTemp // make main view width same as image width
        self.heightViewMain.constant = heightTemp // height also
        
        svgImageView?.contentMode = .scaleAspectFit
        svgImageView!.frame = CGRect.init(x: 0.0, y: 0.0, width: widthTemp, height: heightTemp)
        svgImageView!.tag = 1000
        //        self.viewMain.backgroundColor = UIColor.green.withAlphaComponent(0.2)
        self.viewMain.addSubview(svgImageView!)
        self.viewMain.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            
            var arrLeft = [CGFloat]()
            var arrTop = [CGFloat]()
            var arrRight = [CGFloat]()
            var arrBottom = [CGFloat]()
            
            for lay in self.svgImageView!.layer.sublayers!
            {
                arrLeft.append(lay.frame.origin.x)
                arrTop.append(lay.frame.origin.y)
                arrRight.append(lay.frame.origin.x + lay.frame.width)
                arrBottom.append(lay.frame.origin.y + lay.frame.height)
            }
            
            let frameMain = CGRect.init(x: arrLeft.min()!, y: arrTop.min()!, width: arrRight.max()! - arrLeft.min()!, height: arrBottom.max()! - arrTop.min()!)
            
            let leftOffset = frameMain.origin.x
            let rightOffset = self.svgImageView!.frame.width - leftOffset - frameMain.width
            let topOffset = frameMain.origin.y
            let bottomOffset = self.svgImageView!.frame.height - topOffset - frameMain.height
            
            if leftOffset != rightOffset
            {
                let avgHorizontal = (leftOffset + rightOffset) / 2.0
                self.diffHorizontal = avgHorizontal - leftOffset
                self.svgImageView!.frame.origin.x += self.diffHorizontal
            }
            
            if topOffset != bottomOffset
            {
                let avgVertical = (topOffset + bottomOffset) / 2.0
                self.diffVertical = avgVertical - topOffset
                self.svgImageView!.frame.origin.y += self.diffVertical
            }
            
            self.viewMain.isHidden = false
            
            let dicInt = NSMutableDictionary()
            
            //get index for identify shape's position
            if self.svgImageView != nil
            {
                for i in 0..<self.svgImageView!.paths.count
                {
                    let path = self.svgImageView!.paths[i]
                    
                    if path.svgRepresentation.contains("id=")
                    {
                        let arrStrs = path.svgRepresentation.components(separatedBy: "id=\"")
                        let arrStrs2 = arrStrs.last!.components(separatedBy: "\"")
                        let strID = arrStrs2.first!
                        
                        if Int(strID) != nil
                        {
                            dicInt.setValue(strID, forKey: "\(i)")
                        }
                    }
                }
            }
            
            //Get frame for those layer which has identifier
            for i in 0..<self.svgImageView!.layer.sublayers!.count
            {
                let lay = self.svgImageView!.layer.sublayers![i]
                
                if let id = dicInt.value(forKey: "\(i)") as? String
                {
                    
                    let view = UIView(frame: lay.frame)
                    //                    view.backgroundColor = UIColor.red.withAlphaComponent(0.5)
                    view.frame.origin.x += self.diffHorizontal
                    view.frame.origin.y += self.diffVertical
                    
                    self.viewMain.addSubview(view)
                    
                    self.dictParts.setValue(lay.frame, forKey: id) // add frame of shape
                    
                }
            }
            
            //MARK: Clean array of selected images
            self.arrSelectedImgs.removeAllObjects()
            for _ in 0..<self.dictParts.count
            {
                self.arrSelectedImgs.add(UIImage(named: "ic_placeholder")!) // add any image to check it's by default
            }
            
            // Fill Shap Image Array in CollectionView
            let arrShapeImage = self.mainDictFromCoverCase["frame_details"] as! NSArray
            
            for i in 0..<arrShapeImage.count
            {
                let url = URL(string:(arrShapeImage.object(at: i) as! NSDictionary).value(forKey: "mask_image") as? String ?? "")
                
                if let data = try? Data(contentsOf: url!)
                {
                    let image: UIImage = UIImage(data: data)!
                    
                    self.arrImage.append(image)
                }
            }
            // reload bottom collection
            self.colShape.reloadData()
            
            self.RemoveLoader()
            
            self.viewMain.setNeedsLayout()
            self.viewMain.layoutIfNeeded()
        })
        
    }
    
    //MARK:- **********************  Collection view delegate and Data-source methods **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellShape", for: indexPath) as! CellShape
        
        cell.borderView.layer.borderColor = UIColor.lightGray.cgColor
        cell.borderView.layer.borderWidth = 1.5
        
        let arrShapeImage = mainDictFromCoverCase["frame_details"] as! NSArray
        
        let shapeImageNumber = imgNumber + indexPath.row
        
        cell.lblImgNumber.text = arrShapeImage.count > 9 ? "\(shapeImageNumber)" : "0\(shapeImageNumber)"
        
        //            let url = URL(string:(arrShapeImage.object(at: indexPath.row) as! NSDictionary).value(forKey: "mask_image") as? String ?? "")
        //
        //                if let data = try? Data(contentsOf: url!)
        //                {
        let imgTemp = self.arrSelectedImgs.object(at: indexPath.item) as! UIImage
        
        if imgTemp == UIImage(named: "ic_placeholder")!
        {
            //                        let image: UIImage = UIImage(data: data)!
            cell.img.image = arrImage[indexPath.row]
        }
        else
        {
            cell.img.image = imgTemp
        }
        //                }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.item
        
        //        let arrShapeImage = mainDictFromCoverCase["frame_details"] as! NSArray
        
        var shapeImage = self.arrSelectedImgs.object(at: indexPath.item) as! UIImage
        
        // When add any type of image on imageview in EditingMultiplePhotoFrame (maskImageView isn't hidden)
        isBlankMask = false
        
        if shapeImage == UIImage(named: "ic_placeholder")!
        {
            //            let url = URL(string:(arrShapeImage.object(at: indexPath.item) as! NSDictionary).value(forKey: "mask_image") as? String ?? "")
            //
            //            if let data = try? Data(contentsOf: url!)
            //            {
            //                let image: UIImage = UIImage(data: data)!
            
            shapeImage = arrImage[indexPath.row]
            
            // Get First time mask is white (maskImageView is hidden)
            isBlankMask = true
            //            }
        }
        
        // Redirect to editor
        let EditingPhotoFrameVC = mainStoryBoard.instantiateViewController(withIdentifier: "EditingMultiplePhotoFrameVC") as! EditingMultiplePhotoFrameVC
        EditingPhotoFrameVC.shapeLayer = shapeImage
        EditingPhotoFrameVC.selectedIndex = self.selectedIndex
        EditingPhotoFrameVC.delegate = self
        self.pushViewController(EditingPhotoFrameVC)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        if collectionView == colShape
        //        {
        //            if let frame = dictParts["\(indexPath.item)"] as? CGRect
        //            {
        //                let width = frame.width
        //                let height = frame.height
        //                let newWidth = (width / height) * 60.0
        //                return CGSize.init(width: newWidth + 10.0, height: 77.0)
        //            }
        //            return CGSize.init(width: 120.0, height: 65.0)
        //        }
        //        else
        //        {
        //            return CGSize.init(width: 100.0, height: 50.0)
        //        }
        
        return CGSize(width: DeviceType.IS_IPAD ? 120.0 : 80.0, height: DeviceType.IS_IPAD ? 142.5 : 95.0)
        
    }
    
    //MARK:- **********************  Editor delegate method **********************
    
    func addImage(img: UIImage, data: Data, atIndex: Int)
    {
        //        let imgg = UIImage.init(data: data)
        self.arrSelectedImgs.replaceObject(at: atIndex, with: img) // replace image in array for bottom collection view
        
        if let view = self.viewMain.viewWithTag(atIndex + 1) as? UIImageView // If there is any image? (added previously)
        {
            view.removeFromSuperview() // remove it
        }
        
        //MARK: Initialize new image view
        let imgView = UIImageView.init(image: img)
        
        var xTemp : CGFloat = 0.0
        var yTemp : CGFloat = 0.0
        var widthTemp : CGFloat = img.size.width
        var heightTemp : CGFloat = img.size.height
        
        let value_x_y : CGFloat = 2.0 //DeviceType.IS_IPAD ? 2.0 : 1.0
        let value_width_height : CGFloat = 4.0 //DeviceType.IS_IPAD ? 4.0 : 2.0
        
        // resize for new frame of main SVG image
        if let frame = dictParts["\(self.selectedIndex)"] as? CGRect
        {
            xTemp = frame.origin.x + self.diffHorizontal - value_x_y //* self.scaleDecreased
            yTemp = frame.origin.y + self.diffVertical - value_x_y //* self.scaleDecreased
            widthTemp = frame.size.width + value_width_height //* self.scaleDecreased
            heightTemp = frame.size.height + value_width_height //* self.scaleDecreased
        }
        
        imgView.frame = CGRect.init(x: xTemp, y: yTemp, width: widthTemp, height: heightTemp)
        imgView.contentMode = .scaleToFill
        imgView.tag = atIndex + 1 // set tag to remove easily
        imgView.clipsToBounds = true
        self.viewMain.addSubview(imgView)
        
        self.colShape.reloadData() // reload bottom collection
        
        self.RemoveLoader()
        
    }
    
    func image(from layer: CALayer?) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(layer?.frame.size ?? CGSize.zero, _: false, _: 0)
        
        if let context = UIGraphicsGetCurrentContext() {
            layer?.render(in: context)
        }
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage
    }
    
    
    // Check for touches
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let point = touches.first?.location(in: self.viewMain) else { // Where you pressed
            return
        }
        print(point)
        
        //MARK: Oldest logic
        
        //        if self.svgImgView!.image != nil
        //        {
        //            if let layer = self.svgImgView!.image!.caLayerTree!.hitTest(point) as? CAShapeLayer { // If you hit a layer and if its a Shapelayer
        //
        //                for i in 0..<self.arrShapeLayer.count
        //                {
        //                    if layer == self.arrShapeLayer[i]
        //                    {
        //                        self.selectedIndex = i
        //
        //                        let story = UIStoryboard.init(name: "Main", bundle: nil)
        //                        let vc = story.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
        //                        vc.shapeLayer = arrShapeLayer[i]
        //                        vc.selectedIndex = i
        //                        vc.delegate = self
        //                        self.navigationController?.pushViewController(vc, animated: true)
        //
        //                        break
        //                    }
        //                }
        //                if layer.path!.contains(point) { // Optional, if you are inside its content path
        //                    print("Hit shapeLayer") // Do something
        //                }
        //            }
        //        }
    }
    
    //MARK:- ********************** GENERATE PDF **********************
    
    func generateExamplePDF() {
        
        var BtnY : CGFloat = 40.0
        var Btnx : CGFloat = 40.0
        var tempwidt : CGFloat = 0.0
        var tempHeightX : CGFloat = 40.0
        var tempHeightY : CGFloat = 0.0
        var tempmaxHeight : CGFloat = 0.0
        var Pagecounter : Int = 0
        
        let viewforPDF = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 2480.0, height: 3508.0))
        let pdfName = NSDate().timeIntervalSince1970
        
        var TempPDFArr = [String]()
        
        for i in 0..<self.arrSelectedImgs.count
        {
            let scaleSize = CGSize(width: Int(((((mainDictFromCoverCase["frame_details"] as! NSArray).object(at: i) as! NSDictionary).value(forKey: "width") as! NSString).doubleValue * 300).rounded()), height: Int(((((mainDictFromCoverCase["frame_details"] as! NSArray).object(at: i) as! NSDictionary).value(forKey: "height") as! NSString).doubleValue * 300).rounded()))
            
            let printMirrorImage = UIImage(cgImage: (self.arrSelectedImgs[i] as! UIImage).cgImage!, scale: 1.0, orientation: .upMirrored)
            
            var img = image(with: printMirrorImage, scaledTo: scaleSize)
            
            if ((img.size.width) > (viewforPDF.Getwidth - Btnx)) {
                
                if tempHeightX > (img.size.height + BtnY)
                {
                    Btnx = tempwidt + 40.0
                    BtnY += tempHeightY
                }
                else
                {
                    Btnx = 40.0
                    BtnY += tempmaxHeight
                    tempHeightY = 0.0
                    tempmaxHeight = 0.0
                }
                
            }
            
            if (viewforPDF.Getwidth - Btnx) <= img.size.width
            {
                img = img.rotate(radians: .pi/2)!
                tempwidt = img.size.width
                tempHeightX = img.size.height
            }
            
            let imggview = UIImageView(image: img)
            imggview.frame = CGRect(x: Btnx, y: BtnY, width: img.size.width, height: img.size.height)
            viewforPDF.addSubview(imggview)
            
            if imggview.Getbottom > viewforPDF.Getheight
            {
                imggview.removeFromSuperview()
                
                Pagecounter += 1
                TempPDFArr.append(viewforPDF.exportAsPdfFromView(name: "\(pdfName) \(Pagecounter)"))
                Pagecounter += 1
                
                for i in viewforPDF.subviews
                {
                    i.removeFromSuperview()
                }
                
                BtnY = 40.0
                Btnx = 40.0
                tempwidt = 0.0
                tempHeightX = 40.0
                tempHeightY = 0.0
                
                imggview.frame = CGRect(x: Btnx, y: BtnY, width: img.size.width, height: img.size.height)
                viewforPDF.addSubview(imggview)
                
            }
            
            Btnx += imggview.Getwidth + 40.0
            tempHeightY = imggview.Getheight + 40.0
            
            if (tempmaxHeight < imggview.Getheight) && (imggview.Gety == BtnY) {
                
                tempmaxHeight = imggview.Getheight + 40.0
                
            }
            
        }
        
        TempPDFArr.append(viewforPDF.exportAsPdfFromView(name: "\(pdfName) \(Pagecounter)"))
        print("PDF : \(TempPDFArr)")
        
        let name = TempPDFArr[0].components(separatedBy: " ")
        
        mergePdfFiles(sourcePdfFiles: TempPDFArr, destPdfFile: "\(name[0]).pdf")
        
        // Remove pdf File from document directory
        for pathSrt in TempPDFArr {
            
            let fileManager = FileManager.default
            do {
                try fileManager.removeItem(atPath: pathSrt)
            } catch {
                print("Could not clear temp folder: \(error)")
            }
            
        }
        
        let coverImage = captureScreen(view: viewMain)
        Add_To_Cart_New(imgPrint: URL(string: "\(name[0]).pdf")!,imageCover: coverImage)
        
    }
    
    func mergePdfFiles(sourcePdfFiles:[String], destPdfFile:String) {
        
        guard UIGraphicsBeginPDFContextToFile(destPdfFile, CGRect.zero, nil) else {
            return
        }
        guard let destContext = UIGraphicsGetCurrentContext() else {
            return
        }
        
        for index in 0 ..< sourcePdfFiles.count {
            let pdfFile = sourcePdfFiles[index]
            let pdfUrl = NSURL(fileURLWithPath: pdfFile)
            guard let pdfRef = CGPDFDocument(pdfUrl) else {
                continue
            }
            
            for i in 1 ... pdfRef.numberOfPages {
                if let page = pdfRef.page(at: i) {
                    var mediaBox = page.getBoxRect(.mediaBox)
                    destContext.beginPage(mediaBox: &mediaBox)
                    destContext.drawPDFPage(page)
                    destContext.endPage()
                }
            }
        }
        
        destContext.closePDF()
        UIGraphicsEndPDFContext()
        
    }
    
    
    //    func generateExamplePDF() {
    //
    //        var layout = PDFPageFormat.a4.layout
    //        layout.size = CGSize.init(width: 2480.0, height: 3508.0) // width: 2480.0, height: 3508.0
    //        let document = PDFDocument(layout: layout)
    //
    ////        // Set document meta data
    ////        document.info.title = "TPPDF Example"
    ////        document.info.subject = "Building a PDF easily"
    //
    //        // Set spacing of header and footer
    //        document.layout.space.header = 10
    //        document.layout.space.footer = 10
    //
    //        var images = PDFImage(image: UIImage())
    //        var arrTempImage = [PDFImage]()
    //        var imageSize : CGFloat = 0
    //
    //        for i in 0..<self.arrSelectedImgs.count
    //        {
    //            let scaleSize = CGSize(width: Int(((((mainDictFromCoverCase["frame_details"] as! NSArray).object(at: i) as! NSDictionary).value(forKey: "width") as! NSString).doubleValue * 300).rounded()), height: Int(((((mainDictFromCoverCase["frame_details"] as! NSArray).object(at: i) as! NSDictionary).value(forKey: "height") as! NSString).doubleValue * 300).rounded()))
    //
    //            let printMirrorImage = UIImage(cgImage: (self.arrSelectedImgs[i] as! UIImage).cgImage!, scale: 1.0, orientation: .upMirrored)
    //
    //            let img = image(with: printMirrorImage, scaledTo: scaleSize)
    //
    ////            let size = layout.size.width - img.size.width
    //
    //            imageSize = img.size.width
    //
    //            if layout.size.width <= imageSize
    //            {
    //                let rotateImage = img.rotate(radians: .pi/2)
    //
    //                images = PDFImage.init(image: rotateImage!)
    //            }else
    //            {
    //                images = PDFImage.init(image: img)
    //            }
    //
    //            images.quality = 1.0
    //
    //            arrTempImage.append(images)
    //
    ////            document.addImage(image: images)
    //        }
    //
    //        document.addImagesInRow(images: arrTempImage, spacing: 5.0)
    //
    //
    //        do {
    //
    //            let timestamp = NSDate().timeIntervalSince1970
    //
    //            let url = try PDFGenerator.generateURL(document: document, filename: "\(timestamp).pdf", progress: {
    //                (progressValue: CGFloat) in
    //                print("progress: ", progressValue)
    //            }, debug: false)
    //
    //            let documento = NSData.init(contentsOf: url)
    //            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [documento!], applicationActivities: nil)
    //            activityViewController.popoverPresentationController?.sourceView=self.view
    //            present(activityViewController, animated: true, completion: nil)
    //
    //            fileData = try! NSData(contentsOf: url) //NSData(contentsOfFile: url.path)
    //            fileName = url.pathComponents.last!
    //            fileURL = url as NSURL
    //            print("PDF URL : \(fileURL)")
    //
    ////            let coverImage = captureScreen(view: viewMain) //getImage(scale: 1.0 , isMasked: isMasked)
    ////
    ////            Add_To_Cart_New(imgPrint: fileURL as URL,imageCover: coverImage)
    //
    //        } catch {
    //            print("Error while generating PDF: " + error.localizedDescription)
    //        }
    //    }
    
    func captureScreen(view:UIView) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0);
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func image(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //        let compressData = newImage?.jpegData(compressionQuality: 0.1)
        //        let compressedImage = UIImage(data: compressData!)!
        
        return newImage ?? UIImage()
    }
    
    func imageNew(with image: UIImage, scaledTo newSize: CGSize, completion: @escaping((UIImage) -> ())) -> () {
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            //        DispatchQueue.global(qos: .userInitiated).async {
            
            UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
            image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            //            DispatchQueue.main.async {
            //                let compressData = newImage?.jpegData(compressionQuality: 0.1)
            //                let compressedImage = UIImage(data: compressData!)!
            
            completion(newImage ?? UIImage())
            //            }
        }
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func Add_To_Cart_New(imgPrint : URL, imageCover: UIImage)
    {
        if Reachability.isConnectedToNetwork() {
            
            let modelID = "\(self.mainDictFromCoverCase["model_id"] ?? "")"
            
            let ParamDict = ["model_id": modelID,
                             "user_id": "\(userDefault.value(forKey: "USER_ID") ?? "")",
                "quantity": "1"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICallImageWithPDf(url: "\(BaseURL)add_to_cart", parameters: ParamDict, imageArray: [imgPrint,imageCover], imgKey: ["print_image","case_image"]) { (APIResponce, Responce, error1) in
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    self.RemoveLoader()
                    
                    let responceDict = Responce! as NSDictionary
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        
                        self.logAddToCartEvent(contentData: "\(self.mainDictFromCoverCase["modalName"] ?? "")", contentId: modelID, contentType: mainProductName ?? "", currency: "INR", price: Double("\(self.mainDictFromCoverCase?["price"] ?? "")") ?? 0.0)
                        
                        isCartBtnBack = false
                        
                        ((self.tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
                        
                        self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 1 : 2
                        
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                }
                else
                {
                    self.RemoveLoader()
                    
                    self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "OK", completion: { (ButtonIndex) in
                        //                        self.Add_To_Cart_New(imgPrint : imgPrint, imageCover: imageCover)
                    })
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                //                self.Add_To_Cart_New(imgPrint : imgPrint, imageCover: imageCover)
            })
        }
    }
    
    //MARK:- ********************** FACEBOOK ANALYTICS **********************
    
    /**
     * For more details, please take a look at:
     * developers.facebook.com/docs/swift/appevents
     */
    
    func logAddToCartEvent(contentData : String, contentId : String, contentType : String, currency : String, price : Double)
    {
        // Firebase Add To Cart Analytics
        
        Analytics.logEvent("add_to_cart", parameters: [
            AnalyticsParameterItemID: contentId,
            AnalyticsParameterItemName: contentData,
            AnalyticsParameterContentType: contentType,
            AnalyticsParameterPrice:price,
            AnalyticsParameterCurrency:currency
            ])
        
        // Facebook Add To Cart Analytics
        
//        let params = [
//            "content" : contentData,
//            "contentId" : contentId,
//            "contentType" : contentType,
//            "currency" : currency
//            ] as [String : Any]
        let params = [
            "item_id" : contentId, //"contentId"
            "item_name" : contentData, //"content"
            "content_type" : contentType, //contentType
            "price": price,
            "currency" : currency //currency
        ] as [String : Any]
        
        AppEvents.logEvent(.addedToCart, valueToSum: price, parameters: params)
    }
}
