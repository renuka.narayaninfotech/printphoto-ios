//
//  MobileCompanyCollCell.swift
//  Print_Photo_App
//
//  Created by des on 23/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class MobileCompanyCollCell: UICollectionViewCell {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet var ShadowView: UIView!
    @IBOutlet var CompanyNameLbl: UILabel!
    @IBOutlet var ShadowViewBottomConst: NSLayoutConstraint!
}
