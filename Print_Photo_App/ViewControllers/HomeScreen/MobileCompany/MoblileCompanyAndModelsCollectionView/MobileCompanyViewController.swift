//
//  MobileCompanyViewController.swift
//  Print_Photo_App
//
//  Created by des on 23/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
//import Crashlytics

class MobileCompanyViewController: BaseViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout , UISearchBarDelegate{
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var MobileCompanyCollView: UICollectionView!
    @IBOutlet var SearchBar: UISearchBar!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var refreshControl = UIRefreshControl()
    var FilteredArr = [[String:Any]]()
    var BarandMainArr = [[String:Any]]()
    var isFromBrand = false
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: mainProductName ?? "", EndString: "")
        
        Initialize()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.view.endEditing(true)
        
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        addBackButton()
        addCartButtonWithBadgeIcon()
        
        SearchBar.placeholder = "Search for phone brand..."
        
        self.nodataLbl.text = "Model not available"
        
        //        if !isFromBrand
        //        {
        if BarandMainArr.count == 0
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                
                //                self.CallApi()
                
                self.FilteredArr = self.BarandMainArr.sorted {(($0["name"] as? String)!) < ($1["name"] as! String)}
                
                if (self.FilteredArr.count <= 0)
                {
                    self.MobileCompanyCollView.isHidden = true
                    self.nodataLbl.isHidden = false
                }
                else
                {
                    self.MobileCompanyCollView.isHidden = false
                    self.nodataLbl.isHidden = true
                }
            })
        }else
        {
            FilteredArr = BarandMainArr.sorted {(($0["name"] as? String)!) < ($1["name"] as! String)}
            self.MobileCompanyCollView.reloadData()
        }
        
        //Make 'iphone' brand first
        let elementForIphone = FilteredArr.filter({$0["id"] as! Int == 32}).first
        
        if elementForIphone != nil
        {
            //remove "iPhone" dictionary from array
            FilteredArr =  FilteredArr.filter { dic in
                if dic["id"] as? Int == 32 {
                    return false
                }
                return true
            }
            
            //Insert "iPhone" dictionary at first index of array
            FilteredArr.insert(elementForIphone!, at: 0)
        }
                 
        // ADD Custome "Looking for other brand" In FilteredArr at last index
        
        if BarandMainArr.count == 0 || !isFromBrand
        {
            let tempDict = ["name":"Looking for other brand"]
            self.BarandMainArr.append(tempDict)
            self.FilteredArr.append(tempDict)
        }
        
        //        }
        
        
        SearchBar.returnKeyType = .done
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addCartButtonWithBadgeIcon() {
        
        var badgelabel = UILabel()
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            // badge label
            badgelabel = UILabel(frame: CGRect(x: 20, y: -5, width: 17, height: 17))
            badgelabel.layer.borderColor = UIColor.clear.cgColor
            badgelabel.layer.borderWidth = 2
            badgelabel.layer.cornerRadius = badgelabel.bounds.size.height / 2
            badgelabel.textAlignment = .center
            badgelabel.layer.masksToBounds = true
            badgelabel.font = UIFont(name: CustomFontWeight.medium, size: 10)
            badgelabel.textColor = .white
            badgelabel.backgroundColor = hexStringToUIColor(hex: "0ABAD4")
            
            if (userDefault.integer(forKey: "BadgeNumber")) != 0
            {
                badgelabel.isHidden = false
                badgelabel.text = "\(userDefault.integer(forKey: "BadgeNumber"))"
            }else
            {
                badgelabel.isHidden = true
            }
        }
        
        // Button
        let cartButton = UIButton(type: .custom)
        cartButton.setImage(UIImage(named: "ic_cart"), for: .normal)
        cartButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cartButton.addTarget(self, action: #selector(self.cartAction(_:)), for: .touchUpInside)
        cartButton.addSubview(badgelabel)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cartButton)
        
    }
    
    @objc func cartAction(_ sender: UIButton) {
        
        isCartBtnBack = false
        ((tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
        
        self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 1 : 2
        
    }
    
    //MARK:- ********************** API CALLING **********************
    
    //    func CallApi() {
    //
    //        if Reachability.isConnectedToNetwork() {
    //
    //            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
    //
    //            let ParamDict = ["category_id":1] as NSDictionary
    //
    //            AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)get_companies", parameters: ParamDict) { (APIResponce, Responce, error1) in
    //
    //
    //                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
    //                {
    //
    //                        let responceDict = Responce as! [String:Any]
    //
    //                        if responceDict["ResponseCode"] as! String == "1"
    //                        {
    //                            BarandMainArr = responceDict["data"] as! [[String:Any]]
    //
    //                            self.FilteredArr = BarandMainArr
    //
    ////                            userDefault.set(self.FilteredArr, forKey: "MainArray")
    //
    //                            if (self.FilteredArr.count <= 0)
    //                            {
    //                                self.MobileCompanyCollView.isHidden = true
    //                                self.nodataLbl.isHidden = false
    //                            }
    //                            else
    //                            {
    //                                self.MobileCompanyCollView.isHidden = false
    //                                self.nodataLbl.isHidden = true
    //                            }
    //
    //                            self.MobileCompanyCollView.reloadData()
    //                        }else{
    //
    //                            self.presentAlertWithTitle(title: "\(responceDict["ResponseMessage"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
    //
    //                            })
    //                        }
    //                }
    //                else
    //                {
    //                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
    //                        if ButtonIndex == 0
    //                        {
    //                            self.CallApi()
    //                        }
    //                    })
    //
    //                }
    //                self.RemoveLoader()
    //
    //            }
    //        }
    //        else
    //        {
    //            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
    //                if ButtonIndex == 0
    //                {
    //                    self.CallApi()
    //                }
    //            })
    //        }
    //    }
    
    func Get_Product_API(productId:Int, modelName : String) {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let strURL : String?
            
            if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
            {
                strURL = "\(BaseURL)get_products?category_id=\(productId)&country_code=&user_id=\(userDefault.value(forKey: "USER_ID") ?? "")"
            }else
            {
                strURL = "\(BaseURL)get_products?category_id=\(productId)&country_code=\(userDefault.string(forKey: "RegionCode") ?? "")&user_id="
            }
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: strURL ?? "" , parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        
                        let dataArray = responceDict["data"] as? [[String:Any]]
                        
                        //                        if productId == 0
                        //                        { // for last index navigate to "Request brand VC"
                        //
                        //                            if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
                        //                            {
                        //                                let RequestBrandVC = mainStoryBoard.instantiateViewController(withIdentifier: "RequestBrandViewController") as! RequestBrandViewController
                        //                                self.pushViewController(RequestBrandVC)
                        //                            }else
                        //                            {
                        //                                isLoginOrNot = "mobile_company_login"
                        //                                let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        //                                self.pushViewController(signinVC)
                        //                            }
                        //
                        //                        }
                        //                        else
                        //                        {
                        let MobileModelVC = mainStoryBoard.instantiateViewController(withIdentifier: "MobileModelsViewController") as! MobileModelsViewController
                        MobileModelVC.mainArr = dataArray ?? [[String:Any]]()
                        MobileModelVC.modelName = modelName
                        MobileModelVC.currencySymbol = responceDict["currency_symbol"] as? String ?? ""
                        self.pushViewController(MobileModelVC)
                        //                        }
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: "Something went wrong!!", message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Get_Product_API(productId: productId, modelName: modelName)
                        }
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                
                if ButtonIndex == 0
                {
                    self.Get_Product_API(productId:productId, modelName:modelName)
                }
            })
        }
        
    }
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return FilteredArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MobileCompanyCollCell", for: indexPath) as! MobileCompanyCollCell
        
        if FilteredArr.count % 2 == 0 && indexPath.row >= (FilteredArr.count - 2)
        {
            cell.ShadowViewBottomConst.constant = 10
        }
        else if indexPath.row == FilteredArr.count - 1
        {
            cell.ShadowViewBottomConst.constant = 10
        }
        else
        {
            cell.ShadowViewBottomConst.constant = 2
        }
        
        cell.ShadowView.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        
        cell.CompanyNameLbl.font = UIFont.init(name: CustomFontWeight.reguler, size: cell.Getwidth * 0.12)
        //        cell.CompanyNameLbl.font = UIFont.systemFont(ofSize: cell.CompanyNameLbl.Getwidth * 0.1)
        cell.CompanyNameLbl.text = ((FilteredArr[indexPath.row])["name"] as? String ?? "")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        
        let childArray = (FilteredArr[indexPath.row])["all_childs"] as? NSArray ?? NSArray()
        
        if childArray.count > 0
        {
            let CompanyVC = mainStoryBoard.instantiateViewController(withIdentifier: "MobileCompanyViewController") as! MobileCompanyViewController
            CompanyVC.isFromBrand = true
            CompanyVC.BarandMainArr = childArray as! [[String : Any]]
            pushViewController(CompanyVC)
        }else
        {
            
            let cell = collectionView.cellForItem(at: indexPath) as! MobileCompanyCollCell
            
            if cell.CompanyNameLbl.text == "Looking for other brand"
            {
                let RequestBrandVC = mainStoryBoard.instantiateViewController(withIdentifier: "RequestBrandViewController") as! RequestBrandViewController
                self.pushViewController(RequestBrandVC)
            }else
            {
                let categoryId = (FilteredArr[indexPath.row])["id"]
                let modelName = (FilteredArr[indexPath.row])["name"] as? String ?? ""
                
                Get_Product_API(productId:categoryId as! Int, modelName : modelName)
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if FilteredArr.count % 2 == 0 && indexPath.row >= (FilteredArr.count - 2)
        {
            return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.3 + 8)
        }
        else if indexPath.row == FilteredArr.count - 1
        {
            return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.3 + 8)
        }
        
        return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.3)
        
    }
    
    
    // MARK: - ********************** TOUCHES LIFECYCLE **********************
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //        let touch: UITouch? = touches.first
        //
        //        self.view.endEditing(true)
        SearchBar.endEditing(true)
        
    }
    
    //MARK:- ********************** SEARCH METHODS **********************
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        SearchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //            FilteredArr = searchText.isEmpty ? mainArr : mainArr.filter { (item: Any) -> Bool in
        //                return ((item as! [String:Any])["company_name"] as! String).range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        //                } as! NSMutableArray
        
        
        let arrBrandMain = (BarandMainArr.sorted {(($0["name"] as? String)!) < ($1["name"] as! String)})
        
        FilteredArr = searchText.isEmpty ? arrBrandMain : BarandMainArr.filter { team in
            
            return (team["name"] as! String).lowercased().contains(searchText.lowercased())
        }
        
        //Make 'iphone' brand first
        let arrTempForIhone = FilteredArr.filter({$0["id"] as? Int == 32})
        if arrTempForIhone.count > 0{
            let elementForIphone = arrTempForIhone.first
            if elementForIphone != nil
            {
                //remove "iPhone" dictionary from array
                FilteredArr =  FilteredArr.filter { dic in
                    if dic["id"] as? Int == 32 {
                        return false
                    }
                    return true
                }
                
                //Insert "iPhone" dictionary at first index of array
                FilteredArr.insert(elementForIphone!, at: 0)
            }
        }
        
        // Remove "Looking for other brand" Dictionary from array
        
        var isThere: Bool = false
        
        FilteredArr = FilteredArr.filter({ (obj) -> Bool in
            
            if (obj["name"] as? String ?? "") == "Looking for other brand"
            {
                isThere = true
                return false
            }
            
            return true
        })
        
        let tempDict = ["name":"Looking for other brand"]
        
        if isThere
        {
            self.BarandMainArr.append(tempDict)
            self.FilteredArr.append(tempDict)
        }
        
        if (FilteredArr.count <= 0)
        {
            self.MobileCompanyCollView.isHidden = true
            self.nodataLbl.isHidden = false
        }
        else
        {
            self.MobileCompanyCollView.isHidden = false
            self.nodataLbl.isHidden = true
        }
        
        self.MobileCompanyCollView.reloadData()
        
    }
    
}
