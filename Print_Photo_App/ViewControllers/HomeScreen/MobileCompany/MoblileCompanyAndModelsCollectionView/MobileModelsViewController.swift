//
//  MobileModelsViewController.swift
//  Print_Photo_App
//
//  Created by des on 24/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class MobileModelsViewController: BaseViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout , UISearchBarDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var MobileModelsCollView: UICollectionView!
    @IBOutlet var SearchBar: UISearchBar!
    @IBOutlet weak var bottomTblModel: NSLayoutConstraint!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var mainArr = [[String:Any]]()
    var FilteredArr = [[String:Any]]()
    var refreshControl = UIRefreshControl()
    var modelName : String?
    var currencySymbol : String?
    var getKeyBoardHeight : CGFloat!

    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Select Model", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
        if (self.FilteredArr.count <= 0)
        {
            self.MobileModelsCollView.isHidden = true
            self.nodataLbl.isHidden = false
        }
        else
        {
            self.MobileModelsCollView.isHidden = false
            self.nodataLbl.isHidden = true
            
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.view.endEditing(true)
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        SearchBar.placeholder = "Search for \(modelName ?? "") model..."
        
        self.FilteredArr = self.mainArr.sorted {(($0["modalName"] as? String)!) < ($1["modalName"] as! String)}
        
        //        if userDefault.string(forKey: "RegionCode") == "IN"
        //        {
        addHelpButton()
        //        }else
        //        {
        //            removeBackButton()
        //        }
        
        addBackButton()
        
        //set observer to get height of keyboard

        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow),name: UIResponder.keyboardWillShowNotification,object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboardNotification(notif:)), name: UIResponder.keyboardWillHideNotification , object: nil)
    }
    
     //MARK:- **********************  KEY-BOARD EVENT **********************

    @objc func keyboardWillShow(_ notification: NSNotification) {

        if let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("keyboard height = \(keyboardRect.height)")

            DispatchQueue.main.async {
                //set bottom of segment view
                if #available(iOS 11.0, *) {
                    let window = UIApplication.shared.keyWindow
                    let bottomPadding = window?.safeAreaInsets.bottom
                    if  self.bottomTblModel != nil{
                    self.bottomTblModel.constant = -(keyboardRect.height - bottomPadding!)
                    }
                    self.view.layoutIfNeeded()
                }
            }
        }
    }

    @objc func hideKeyboardNotification(notif: NSNotification) -> Void {
        guard let userInfo = notif.userInfo else {return}

        if (userInfo["UIKeyboardFrameEndUserInfoKey"] as? CGRect) != nil {

            //set bottom of segment view
            if self.bottomTblModel != nil{
            bottomTblModel.constant = 0.0
            }
            self.view.layoutIfNeeded()
        }
    }
        
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addHelpButton() {
        let helpButton = UIButton(type: .custom)
        helpButton.setImage(UIImage(named: "ic_help"), for: .normal)
        helpButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        helpButton.addTarget(self, action: #selector(self.helpAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: helpButton)
    }
    
    @objc func helpAction(_ sender: UIButton) {
        Help_Video()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return FilteredArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MobileCompanyCollCell", for: indexPath) as! MobileCompanyCollCell
        
        if FilteredArr.count % 2 == 0 && indexPath.row >= (FilteredArr.count - 2)
        {
            cell.ShadowViewBottomConst.constant = 10
        }
        else if indexPath.row == FilteredArr.count - 1
        {
            cell.ShadowViewBottomConst.constant = 10
        }
        else
        {
            cell.ShadowViewBottomConst.constant = 2
        }
        
        cell.ShadowView.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        
        cell.CompanyNameLbl.font = UIFont.init(name: CustomFontWeight.reguler, size: cell.Getwidth * 0.12)
        //        cell.CompanyNameLbl.font = UIFont.systemFont(ofSize: cell.CompanyNameLbl.Getwidth * 0.1)
        cell.CompanyNameLbl.text = ((FilteredArr[indexPath.row])["modalName"] as! String)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let childArray = FilteredArr[indexPath.row] as [String:Any]
        
        //        let categoryId = childArray["id"] as? Int ?? 0
        
        //        if categoryId == 0
        //        { // for last index navigate to "Request brand VC"
        //
        //            if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        //            {
        //                let RequestBrandVC = mainStoryBoard.instantiateViewController(withIdentifier: "RequestBrandViewController") as! RequestBrandViewController
        //                self.pushViewController(RequestBrandVC)
        //            }else
        //            {
        //                isLoginOrNot = "mobile_model_login"
        //                let signinVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        //                self.pushViewController(signinVC)
        //            }
        //
        //        }
        //        else
        //        {
        let coverCaseVC = mainStoryBoard.instantiateViewController(withIdentifier: "CoverCaseViewController") as! CoverCaseViewController
        coverCaseVC.mainDict = childArray
        coverCaseVC.currencySymbol = currencySymbol
        self.pushViewController(coverCaseVC)
        
        //            //            // Push to Add to Cart API in Edit View Controller
        //            //            let mainDict = FilteredArr[indexPath.row]
        //            //            userDefault.set(mainDict, forKey: "MainDict")
        //            //            userDefault.synchronize()
        //            //
        //        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if FilteredArr.count % 2 == 0 && indexPath.row >= (FilteredArr.count - 2)
        {
            return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.3 + 8)
        }
        else if indexPath.row == FilteredArr.count - 1
        {
            return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.3 + 8)
        }
        
        return CGSize(width: collectionView.Getwidth * 0.5, height: collectionView.Getwidth * 0.3)
        
    }
    
    //MARK:- ********************** SEARCH METHODS **********************
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        SearchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let arrMain = mainArr.sorted {(($0["modalName"] as? String)!) < ($1["modalName"] as! String)}
        
        FilteredArr = searchText.isEmpty ? arrMain : arrMain.filter { team in
            return (team["modalName"] as! String).lowercased().contains(searchText.lowercased())
        }
        
        if (self.FilteredArr.count <= 0)
        {
            self.MobileModelsCollView.isHidden = true
            self.nodataLbl.isHidden = false
        }
        else
        {
            self.MobileModelsCollView.isHidden = false
            self.nodataLbl.isHidden = true
        }
        
        self.MobileModelsCollView.reloadData()
        
    }
    
}
