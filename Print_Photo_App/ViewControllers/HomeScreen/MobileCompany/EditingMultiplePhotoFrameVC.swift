//
//  EditingMultiplePhotoFrameVC.swift
//  Print_Photo_App
//
//  Created by Mac Os on 06/08/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import AFNetworking
import Photos
import Toast_Swift
import ChromaColorPicker
import MZFormSheetPresentationController
import Alamofire
import FBSDKCoreKit
//import FBSDKMarketingKit
import FacebookCore
import Firebase
import SDWebImage

protocol EditDelegate: class {
    func addImage(img: UIImage, data: Data, atIndex: Int)
}

class EditingMultiplePhotoFrameVC: BaseViewController , StickerViewDelegate , UINavigationControllerDelegate , UIImagePickerControllerDelegate , ChromaColorPickerDelegate , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate  {
    
    //MARK:- ********************** OUTLAT DECLARTION **********************
    
    @IBOutlet weak var bottomMenuCollView: UICollectionView!
    @IBOutlet weak var CromaColorPicker: ChromaColorPicker!
    @IBOutlet var CromVIew: UIView!
    @IBOutlet weak var viewForCapture: UIView!
    @IBOutlet weak var maskImageView: UIImageView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var isSetupDone = false
    var picker = UIImagePickerController()
    private var _selectedStickerView:StickerView?
    var gestureRecognizer: UITapGestureRecognizer!
    var bottomItemsArr: [UIImage?]!
    var defaultLabel = UILabel()
    var isFromViewDidLoad: Bool = true
    var internetSpeed : Double = 0
    
    var shapeLayer = UIImage()
    var overlayView = UIView()
    var selectedIndex: Int = 0
    var scaleDecreased : CGFloat = 0.0
    weak var delegate: EditDelegate?
    var isLoaded = false
    
    //    var shapeImage = UIImageView()
    
    var selectedStickerView:StickerView? {
        get {
            return _selectedStickerView
        }
        set {
            
            // if other sticker choosed then resign the handler
            if _selectedStickerView != newValue {
                if let selectedStickerView = _selectedStickerView {
                    selectedStickerView.showEditingHandlers = false
                }
                _selectedStickerView = newValue
            }
            
            // assign handler to new sticker added
            if let selectedStickerView = _selectedStickerView {
                selectedStickerView.showEditingHandlers = true
            }
        }
    }
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetNavigationBarTitle(Startstring: "Frame", EndString: "")
        
        Initialize()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Managed maskImageView is hide or Not when get from multiplePhotoFrame
        if isBlankMask
        {
            // First time get from multiPhotoFrameVC
            maskImageView.isHidden = true
        }else
        {
            // After First time get from multiPhotoFrameVC
            maskImageView.isHidden = false
        }
        
        bottomItemsArr = [UIImage(named: "images"),
                          UIImage(named: "add_text"),
                          UIImage(named: "sticker"),
                          UIImage(named: "bg_color"),
                          UIImage(named: "bg_asset")]
        
        self.tabBarController?.tabBar.isHidden = true
        
        if (StickerImage != nil) {
            
            AddStickertoView(Image: StickerImage , isSticker : true)
            StickerImage = nil
        }
        else if (StickerTextString != nil)
        {
            
            if StickerTextColor == nil
            {
                StickerTextColor = UIColor.black
            }
            if stickerFontString == nil {
                stickerFontString = CustomFontWeight.reguler
            }
            AddStickertoView(TextString: StickerTextString, TextColor: StickerTextColor, FontStyle: stickerFontString)
            StickerTextString = nil
            StickerTextColor = nil
            stickerFontString = nil
            
        }
        else if (StickerBackgroung != nil)
        {
            AddStickertoView(Image: StickerBackgroung , isSticker : false)
            StickerBackgroung = nil
            
        }else if instagramImage != nil
        {
            AddStickertoView(Image: instagramImage , isSticker : false)
            instagramImage = nil
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        Add_Footer_Label()
        
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        CromaColorPicker.delegate = self
        CromaColorPicker.backgroundColor = .white
        
        addBackButton()
        addRightButtons()
        
        //        checkRechabilityStatus()
        
        showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
        
        self.defaultLabel.isHidden = true
        
        self.viewDidLayout()
        
    }
    
    //MARK:- Create placeholder view
    func viewDidLayout() {
        
        maskImageView.image = shapeLayer
        
        //        AddStickertoView(Image: shapeLayer, isSticker: false)
        
        //MARK: create frame fit to screen
        let boundry = WIDTH - (DeviceType.IS_IPAD ? 350.0 : 100.0)
        scaleDecreased = boundry / self.shapeLayer.size.width
        
        //MARK: create blank view as object
        viewForCapture.frame = CGRect.init(x: 0.0, y: 0.0, width: self.shapeLayer.size.width*scaleDecreased, height: self.shapeLayer.size.height*scaleDecreased)
        
        viewForCapture.backgroundColor = UIColor.white
        
        viewForCapture.center = self.view.center
        
        self.defaultLabel.isHidden = false
        
        RenderAtTransparentPart(image: shapeLayer.mask(with: .white), viewformask: viewForCapture)
        
        self.RemoveLoader()
        
    }
    
    func RenderAtTransparentPart(image: UIImage, viewformask : UIView) {
        
        let cim = CIImage(image: image)
        let filter = CIFilter(name:"CIMaskToAlpha")!
        filter.setValue(cim, forKey: "inputImage")
        let out = filter.outputImage!
        let cgim = CIContext().createCGImage(out, from: out.extent)
        let lay = CALayer()
        lay.frame = viewformask.bounds
        lay.contents = cgim
        viewformask.layer.mask = lay
        
    }
    
    func Add_Footer_Label() {
        
        // Create Bottom Default Label
        defaultLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(defaultLabel)
        defaultLabel.topAnchor.constraint(equalTo: self.viewForCapture.bottomAnchor, constant: 0).isActive = true
        defaultLabel.bottomAnchor.constraint(equalTo: self.bottomMenuCollView.topAnchor, constant: 0).isActive = true
        defaultLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        defaultLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        defaultLabel.textAlignment = .center
        defaultLabel.numberOfLines = 0
        
        if DeviceType.IS_IPAD
        {
            defaultLabel.font = UIFont(name: CustomFontWeight.medium, size: 22.0)
        }else
        {
            defaultLabel.font = UIFont(name: CustomFontWeight.medium, size: 15.0)
        }
        
        defaultLabel.text = "Please add high resolution image to get high clarity product"
    }
    
    //MARK:- ********************** CHECK RECHABILITY STATUS **********************
    
    //    func checkRechabilityStatus()
    //    {
    //        let reachabilityManager = NetworkReachabilityManager()
    //
    //        reachabilityManager?.startListening()
    //        reachabilityManager?.listener = { _ in
    //            if let isNetworkReachable = reachabilityManager?.isReachable,
    //                isNetworkReachable == true {
    //                //Internet Available
    //                print("Internet Available")
    //
    //                if self.internetSpeed < 0.06
    //                {
    //                    self.RemoveLoader()
    //
    //                    self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
    //                        self.popViewController()
    //                    })
    //                }else
    //                {
    //                    self.captureSuperview.isHidden = false
    //                }
    //
    //            } else {
    //                //Internet Not Available"
    //                print("Internet Not Available")
    //
    //                if !self.isSetupDone
    //                {
    //                    self.RemoveLoader()
    //                    self.captureSuperview.isHidden = true
    //                    self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
    //                        self.popViewController()
    //                    })
    //                }
    //
    //            }
    //        }
    //    }
    
    //MARK:- ********************** TOUCHES LIFECYCLE **********************
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch: UITouch? = touches.first
        if touch?.view != selectedStickerView {
            selectedStickerView?.showEditingHandlers = false
        }
    }
    
    func OpenImagePicker() {
        
        DispatchQueue.main.async {
            
            let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { action in
                
                DispatchQueue.main.async {
                    self.OpenCamera()
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: { action in
                
                DispatchQueue.main.async {
                    self.OpenGallery()
                }
                
            }))
            
            //            alert.addAction(UIAlertAction(title: "Choose from Instagram", style: .default, handler: { action in
            //
            //                let InstagramVC = mainStoryBoard.instantiateViewController(withIdentifier: "InstagramViewController") as! InstagramViewController
            //                self.pushViewController(InstagramVC)
            //
            //            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            }))
            
            // Add the actions
            self.picker.delegate = self
            self.picker.modalPresentationStyle = .overCurrentContext
            self.present(alert, animated: true)
            
            alert.view.tintColor = UIColor.black
        }
    }
    
    //MARK:- ********************** Don't Allowe Camera & Gallry Permission **********************
    
    //MARK:- Camera and gallery permission
    
    func requestCameraPermission() {
        
        DispatchQueue.main.async {
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                guard accessGranted == true else { return }
            })
        }
    }
    
    func alertCameraAccessNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Need Camera Access",
                message: "Camera access is required to capture photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func requestGalleryPermission() {
        
        DispatchQueue.main.async {
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                guard accessGranted == true else { return }
                
            })
        }
    }
    func alertGalleryAccessNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Need Gallery Access",
                message: "Gallery access is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Allow Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func alertGalleryAccessPermissionChangeNeeded() {
        
        DispatchQueue.main.async {
            
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            
            let alert = UIAlertController(
                title: "Change Gallery Access",
                message: " Change Gallery access from Selected Photos to All Photos is required to get photo for set theme.",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) -> Void in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Change Gallery Access", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- ********************** NAVIGATION BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addRightButtons() {
        
        // Done Button
        let doneButton = UIButton(type: .custom)
        doneButton.setImage(UIImage(named: "ic_done"), for: .normal)
        doneButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        //        doneButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -25)
        doneButton.addTarget(self, action: #selector(self.addCartAction(_:)), for: .touchUpInside)
        let doneButtonItem = UIBarButtonItem(customView: doneButton)
        
        self.navigationItem.rightBarButtonItems = [doneButtonItem]
    }
    
    @objc func addCartAction(_ sender: UIButton) {
        
        _selectedStickerView?.showEditingHandlers = false
        
        if self.viewForCapture.subviews.count >= 2 || maskImageView.isHidden == false || viewForCapture.backgroundColor != UIColor.white //|| (!CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .clear)!) && !CompareImage(lhs: defaultImgView.image!, rhs: UIImage(color: .white)!))
        {
            self.presentAlertWithTitle(title: "Want to save?", message: "", options: "NO","YES") { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.dismissViewController()
                }else
                {
                    self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                    
                    self.scaleDecreased = 5.0
                    
                    UIGraphicsBeginImageContextWithOptions(self.viewForCapture.bounds.size, false, self.scaleDecreased);
                    self.viewForCapture.layer.render(in: UIGraphicsGetCurrentContext()!)
                    let image = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    let data = image!.pngData()
                    self.delegate?.addImage(img: image!, data: data!, atIndex: self.selectedIndex)
                    
                    self.popViewController()
                    
                }
            }
            
        }else
        {
            self.presentAlertWithTitle(title: "Add image or stickers", message: "", options: "OK") { (Int) in
            }
        }
    }
    
    func CompareImage (lhs: UIImage, rhs: UIImage) -> Bool
    {
        guard let data1 = lhs.pngData(),
            let data2 = rhs.pngData()
            else { return false }
        
        return data1 == data2
    }
    
    //MARK:- ********************** COLLECTIONVIEW METHODS **********************
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bottomItemsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bottomTabbarCollCell", for: indexPath) as! bottomTabbarCollCell
        
        cell.ButtonImageView.image = bottomItemsArr[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            OpenImagePicker()
        }
        else if indexPath.row == 1 {
            
            let AddText = mainStoryBoard.instantiateViewController(withIdentifier: "AddTextViewController") as! AddTextViewController
            pushViewController(AddText)
        }
        else if indexPath.row == 2
        {
            guard !isLoaded else {
                return
            }
            
            isLoaded = true
            
            isSelectSticker = "EditingMultiplePhotoFrame"
            
            let mainsticVC = mainStoryBoard.instantiateViewController(withIdentifier: "mainStickerViewController") as! mainStickerViewController
            pushViewController(mainsticVC)
            isLoaded = false
        }
        else if indexPath.row == 3
        {
            let TFPopup = MZFormSheetPresentationViewController.init(contentView: CromVIew)
            TFPopup.presentationController?.contentViewSize = CGSize(width: self.view.Getwidth*(DeviceType.IS_IPAD ? 0.4 : 0.8), height: self.view.Getwidth*(DeviceType.IS_IPAD ? 0.4 : 0.8))
            TFPopup.presentationController?.shouldCenterVertically = true
            TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
            self.present(TFPopup, animated: true)
        }
        else if indexPath.row == 4
        {
            guard !isLoaded else {
                return
            }
            
            isLoaded = true
            
            let BGimagesVC = mainStoryBoard.instantiateViewController(withIdentifier: "BackgroundImagesViewController") as! BackgroundImagesViewController
            pushViewController(BGimagesVC)
            isLoaded = false
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.Getwidth / 5, height: DeviceType.IS_IPAD ? 100.0 : 50.0)
    }
    
    //MARK:- ********************** OTHER METHODS **********************
    
    func OpenCamera()
    {
        
        DispatchQueue.main.async {
            
            self.picker = UIImagePickerController()
            self.picker.delegate = self
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                //check permission
                if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                    //already authorized, open camera
                    
                    DispatchQueue.main.async {
                        self.picker.sourceType = .camera
                        self.present(self.picker, animated: true)
                    }
                }
                    
                else if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
                    //request for camera permission
                    self.requestCameraPermission()
                    
                }
                else
                {
                    AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                        if granted {
                            
                            DispatchQueue.main.async {
                                //access allowed
                                self.picker.sourceType = .camera
                                self.present(self.picker, animated: true)
                            }
                            
                        } else {
                            //access denied or restricted
                            self.alertCameraAccessNeeded()
                        }
                    })
                }
                
            } else {
                //if camera is not available then show alert
                let alert = UIAlertController(title: "Alert", message: "No Camera Available.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true)
                
            }
            
        }
        
    }
    
    func OpenGallery()
    {
        DispatchQueue.main.async {
            
            //check user has allowed permission or not
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    
                    DispatchQueue.main.async {
                        self.picker = UIImagePickerController()
                        self.picker.delegate = self
                        self.picker.sourceType = .photoLibrary
                        self.present(self.picker, animated: true)
                    }
                    
                    break
                case .denied, .restricted:
                    self.alertGalleryAccessNeeded()
                    break
                case .notDetermined:
                    self.requestGalleryPermission()
                    break
                case .limited:
                    self.alertGalleryAccessPermissionChangeNeeded()
                default:
                    break
                }
            }
        }
        
    }
    
    func AddStickertoView(Image : UIImage, isSticker : Bool) {
        
        //        let StickerImgView = UIImageView(image: Image)
        //        StickerImgView.isUserInteractionEnabled = true
        //
        //        var ratioSize : CGFloat?
        //
        //        if isSticker
        //        {
        //            ratioSize = 1.0
        //        }else
        //        {
        //            ratioSize = 1.2
        //        }
        //
        //        let ratio = Image.size.width / Image.size.height
        //
        //        if StickerImgView.frame.width > overlayView.frame.height * ratioSize! {
        //            let newHeight = overlayView.frame.width / ratio
        //            StickerImgView.frame.size = CGSize(width: overlayView.frame.width * ratioSize!, height: newHeight * ratioSize!)
        //        }
        //        else{
        //            let newWidth = overlayView.frame.height * ratio
        //            StickerImgView.frame.size = CGSize(width: newWidth * ratioSize!, height: overlayView.frame.height * ratioSize!)
        //        }
        //
        //        let StickerViewforAdd = StickerView.init(contentView: StickerImgView)
        //        StickerViewforAdd.delegate = self
        //        StickerViewforAdd.setImage(UIImage.init(named: "Close")!, forHandler: StickerViewHandler.close)
        //        StickerViewforAdd.enableRotate = false
        //        StickerViewforAdd.enableFlip = false
        //        StickerViewforAdd.outlineBorderColor = UIColor.darkGray
        //        StickerViewforAdd.showEditingHandlers = false
        //
        //        overlayView.addSubview(StickerViewforAdd)
        //
        //        let scaleTrans = StickerViewforAdd.transform.scaledBy(x: 1.0/scaleDecreased, y: 1.0/scaleDecreased)
        //        StickerViewforAdd.transform = scaleTrans
        //
        //        StickerViewforAdd.center = CGPoint(x: overlayView.frame.width * 0.5, y: overlayView.frame.height * 0.5)
        //
        //        self.selectedStickerView = StickerViewforAdd
        
        
        let StickerImgView = UIImageView(image: Image)
        StickerImgView.isUserInteractionEnabled = true
        
        var ratioSize : CGFloat?
        
        if isSticker
        {
            ratioSize = 0.4
        }else
        {
            ratioSize = 1.2
        }
        
        let ratio = Image.size.width / Image.size.height
        
        if StickerImgView.frame.width > viewForCapture.frame.height * ratioSize! {
            let newHeight = viewForCapture.frame.width / ratio
            StickerImgView.frame.size = CGSize(width: viewForCapture.frame.width * ratioSize!, height: newHeight * ratioSize!)
        }
        else{
            let newWidth = viewForCapture.frame.height * ratio
            StickerImgView.frame.size = CGSize(width: newWidth * ratioSize!, height: viewForCapture.frame.height * ratioSize!)
        }
        
        let StickerViewforAdd = StickerView.init(contentView: StickerImgView)
        StickerViewforAdd.delegate = self
        StickerViewforAdd.setImage(UIImage.init(named: "Close")!, forHandler: StickerViewHandler.close)
        StickerViewforAdd.enableRotate = false
        StickerViewforAdd.enableFlip = false
        StickerViewforAdd.setHandlerSize(25)
        StickerViewforAdd.outlineBorderColor = UIColor.darkGray
        StickerViewforAdd.showEditingHandlers = false
        
        if (StickerBackgroung != nil)
        {
            viewForCapture.insertSubview(StickerViewforAdd, at: 1)
        }else
        {
            viewForCapture.addSubview(StickerViewforAdd)
        }
        
        StickerViewforAdd.center = CGPoint(x: viewForCapture.Getwidth * 0.5, y: viewForCapture.Getheight * 0.5)
        
        
        //        doubleTapStickerView = StickerViewforAdd
        //
        //        // Zoom Image when Double Tap on Gesture
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped(gesture:)))
        //        tap.numberOfTapsRequired = 2
        //        doubleTapStickerView?.addGestureRecognizer(tap)
    }
    
    func AddStickertoView(TextString : String , TextColor : UIColor ,FontStyle : String) {
        
        let StickerLbl = UILabel()
        StickerLbl.text = TextString
        StickerLbl.font = UIFont.init(name: FontStyle, size: viewForCapture.Getwidth * 0.15)
        StickerLbl.textColor = TextColor
        StickerLbl.textAlignment = .center
        StickerLbl.sizeToFit()
        
        //        StickerLbl.frame = CGRect(x: StickerLbl.Getx + 25.0, y: StickerLbl.Gety + 25.0, width: StickerLbl.Getwidth + 100.0, height: StickerLbl.Getheight + 100.0)
        
        StickerLbl.frame = CGRect(x: 0.0, y: 0.0, width: StickerLbl.Getwidth + 50.0, height: StickerLbl.Getheight + 50.0)
        
        print(StickerLbl.text?.heightOfLabel(withConstrainedWidth: StickerLbl.Getwidth, font: StickerLbl.font) ?? "")
        
        let StickerViewforAdd = StickerView.init(contentView: StickerLbl)
        StickerViewforAdd.delegate = self
        StickerViewforAdd.setImage(UIImage.init(named: "Close")!, forHandler: StickerViewHandler.close)
        StickerViewforAdd.enableRotate = false
        StickerViewforAdd.enableFlip = false
        StickerViewforAdd.isLabel = true
        StickerViewforAdd.setHandlerSize(25)
        StickerViewforAdd.outlineBorderColor = UIColor.darkGray
        StickerViewforAdd.showEditingHandlers = false
        
        viewForCapture.addSubview(StickerViewforAdd)
        
        StickerViewforAdd.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5) //(StickerViewforAdd.transform, 0.1, 0.1)
        //        StickerViewforAdd.setHandlerSize(DeviceType.IS_IPAD ? 25 : 18)
        
        StickerViewforAdd.center = CGPoint(x: viewForCapture.Getwidth * 0.5, y: viewForCapture.Getheight * 0.5)
    }
    
    //MARK:- ********************** IMAGEPICKERVIEW DELEGATE **********************
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if info[.imageURL] != nil && !((info[.imageURL] as? URL)?.absoluteString.uppercased().hasSuffix("GIF"))!{
            
            if let asset = info[.phAsset] as? PHAsset {
                PHImageManager.default().requestImageData(for: asset, options: nil) { (dataObj, uti, orientation, information) in
                    
                    //                    let nsdataObj = dataObj! as NSData
                    
                    //                    print("File Size \(nsdataObj.length/1000) KB")
                    
                    let heightInPoints = (info[.originalImage] as! UIImage).size.height
                    let heightInPixels = heightInPoints * (info[.originalImage] as! UIImage).scale
                    
                    let widthInPoints = (info[.originalImage] as! UIImage).size.width
                    let widthInPixels = widthInPoints * (info[.originalImage] as! UIImage).scale
                    
                    /*
                    if heightInPixels <= 720 || widthInPixels <= 720 //nsdataObj.length/1024 < 500
                    {
                        self.topMostViewController().view.makeToast("Please select high resolution image", duration: 2.0, position: .bottom)
                    }else
                    {*/
                        self.dismiss(animated: true) {
                            self.AddStickertoView(Image: info[.originalImage] as! UIImage , isSticker : false)
                            //                            self.maskImageView.isHidden = true
                        }
                    //}
                }
            }
        }
        else
        {
            self.AddStickertoView(Image: info[.originalImage] as! UIImage , isSticker : false)
            //            self.maskImageView.isHidden = true
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    //MARK:- ********************** CROMA COLOR PICKER DELEGATE **********************
    
    func colorPickerDidChooseColor(_ colorPicker: ChromaColorPicker, color: UIColor) {
        //Set color for the display view
        
        viewForCapture.backgroundColor = color
        maskImageView.image = nil
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- ********************** STICKER DELEGATE **********************
    
    func stickerViewDidBeginMoving(_ stickerView: StickerView) {
        self.selectedStickerView = stickerView
    }
    
    func stickerViewDidChangeMoving(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidEndMoving(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidBeginRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidChangeRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidEndRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidClose(_ stickerView: StickerView) {
        //        if self.viewForCapture.subviews.count > 1 //|| self.viewForCapture.backgroundColor != hexStringToUIColor(hex: "FFFFFF")
        //        {
        //            maskImageView.isHidden = true
        //        }else
        //        {
        //            maskImageView.isHidden = false
        //        }
    }
    
    func stickerViewDidTap(_ stickerView: StickerView) {
        self.selectedStickerView = stickerView
    }
    
}
