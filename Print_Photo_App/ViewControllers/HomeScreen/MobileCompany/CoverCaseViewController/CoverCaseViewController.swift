//
//  CoverCaseViewController.swift
//  Print_Photo_App
//
//  Created by des on 25/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class CoverCaseViewController: BaseViewController {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var PriceValueLbl: UILabel!
    @IBOutlet var ModelValueLbl: UILabel!
    @IBOutlet var SizeValueLbl: UILabel!
    @IBOutlet var DescriptionLbl: UILabel!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var btnSelectCase: UIButton!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var mainDict = [String:Any]()
    var currencySymbol : String?
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Cover Style", EndString: "")
        
        Initialize()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize()
    {
        
        addBackButton()
        
        //        if userDefault.string(forKey: "RegionCode") == "IN"
        //        {
        addHelpButton()
        //        }else
        //        {
        //            removeBackButton()
        //        }
        
        imgHeader.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        btnSelectCase.addShadow(color: UIColor.darkGray, opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 2, scale: true)
        
        PriceValueLbl.text = "\(currencySymbol ?? "")\((mainDict["price"]) as! String)"
        
        ModelValueLbl.text = ((mainDict["modalName"]) as! String)
        
        let CaseWidth = ((mainDict["width"]) as! String)
        let Caseheight = ((mainDict["height"]) as! String)
        
        userDefault.set(true, forKey: "ViewVideo")
        
        SizeValueLbl.text = "\(CaseWidth) X \(Caseheight) CM"
        
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addHelpButton() {
        let helpButton = UIButton(type: .custom)
        helpButton.setImage(UIImage(named: "ic_help"), for: .normal)
        helpButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        helpButton.addTarget(self, action: #selector(self.helpAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: helpButton)
    }
    
    @objc func helpAction(_ sender: UIButton) {
        Help_Video()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** BUTTON EVENTS **********************
    
    @IBAction func SelectCase_Pressed(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork() {
            
            //            if userDefault.string(forKey: "RegionCode") == "IN"
            //            {
            if (userDefault.value(forKey: "VideoLoad") == nil)
            {
                self.presentAlertWithTitle(title: "Tutorial", message: "If you want to learn how to edit the cover. Please Press Yes", options: "No","Yes") { (ButtonIndex) in
                    if ButtonIndex == 0
                    {
                        self.dismissViewController()
                        
                        let defaulImgVC = mainStoryBoard.instantiateViewController(withIdentifier: "DefaultImagesViewController") as! DefaultImagesViewController
                        defaulImgVC.mainDictfromCovercase = self.mainDict
                        self.pushViewController(defaulImgVC)
                        
                    }else
                    {
                        // One time Load help Video
                        userDefault.set(true, forKey: "VideoLoad")
                        
                        self.Help_Video()
                    }
                }
            }else
            {
                let defaulImgVC = mainStoryBoard.instantiateViewController(withIdentifier: "DefaultImagesViewController") as! DefaultImagesViewController
                defaulImgVC.mainDictfromCovercase = self.mainDict
                self.pushViewController(defaulImgVC)
            }
            //            }else
            //            {
            //
            //                userDefault.removeObject(forKey: "VideoLoad")
            //
            //                let defaulImgVC = mainStoryBoard.instantiateViewController(withIdentifier: "DefaultImagesViewController") as! DefaultImagesViewController
            //                defaulImgVC.mainDictfromCovercase = self.mainDict
            //                self.pushViewController(defaulImgVC)
            //            }
            
        }
        else
        {
            self.presentAlertWithTitle(title: "Download Failed", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
    }
    
}
