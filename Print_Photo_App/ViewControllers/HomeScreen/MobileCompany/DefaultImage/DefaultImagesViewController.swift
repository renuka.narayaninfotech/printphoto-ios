//
//  DefaultImagesViewController.swift
//  Print_Photo_App
//
//  Created by des on 25/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class DefaultImagesViewController: BaseViewController , UITableViewDataSource , UITableViewDelegate, UISearchBarDelegate, URLSessionDelegate, URLSessionDataDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var defImgTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var mainDictfromCovercase = [String:Any]()
    var CategoryFilteredArr = [[String:Any]]()
    
    // speed test
    typealias speedTestCompletionHandler = (_ megabytesPerSecond: Double? , _ error: Error?) -> Void
    
    var speedTestCompletionBlock : speedTestCompletionHandler?
    
    var startTime: CFAbsoluteTime!
    var stopTime: CFAbsoluteTime!
    var bytesReceived: Int!
    var imgNew = UIImage()
    
    //MARK:- ********************** VIEW LIFECYCLE **********************
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Default Images", EndString: "")
        
        Initialize()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        addBackButton()
        
        //        if userDefault.string(forKey: "RegionCode") == "IN"
        //        {
        addHelpButton()
        //        }else
        //        {
        //            removeBackButton()
        //        }
        
        self.view.isExclusiveTouch = true
        
        searchBar.returnKeyType = .done
        
        if CategoryMainArr.count == 0
        {
            CallApi()
        }else
        {
            self.CategoryFilteredArr = CategoryMainArr
            self.defImgTableView.reloadData()
        }
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    func addHelpButton() {
        let helpButton = UIButton(type: .custom)
        helpButton.setImage(UIImage(named: "ic_help"), for: .normal)
        helpButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        helpButton.addTarget(self, action: #selector(self.helpAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: helpButton)
    }
    
    @objc func helpAction(_ sender: UIButton) {
        Help_Video()
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func CallApi() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)case_categories?platform=1", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["success"] as! Int == 1
                    {
                        CategoryMainArr = responceDict["data"] as! [[String:Any]]
                        self.CategoryFilteredArr = CategoryMainArr
                        
                        if (self.CategoryFilteredArr.count <= 0)
                        {
                            self.defImgTableView.isHidden = true
                            self.nodataLbl.text = "Category not available"
                            self.nodataLbl.isHidden = false
                        }
                        else
                        {
                            self.defImgTableView.isHidden = false
                            self.nodataLbl.isHidden = true
                        }
                        
                        self.defImgTableView.reloadData()
                    }else{
                        self.presentAlertWithTitle(title: "\(responceDict["message"]!)", message: "", options: "OK", completion: { (ButtonIndex) in
                            
                        })
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.CallApi()
                        }
                    })
                }
            }
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.CallApi()
                }
            })
        }
        
    }
    
    //MARK:- ********************** TABLEVIEW METHODS **********************
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CategoryFilteredArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultImgTableCell") as! DefaultImgTableCell
        
        cell.ImgNameLbl.text = ((CategoryFilteredArr[indexPath.row])["name"] as! String)
        
        // Display New category GIF image using flag from API
        
        let imgFlag = (CategoryFilteredArr[indexPath.row] as NSDictionary).value(forKey: "flag") as? Int ?? 0
        
        if imgFlag == 1
        {
            // Load GIF without Stuck
            let urlpath : String = Bundle.main.path(forResource: "ic_new", ofType: "gif")!
            let url = URL(fileURLWithPath: urlpath)
            let data = try? Data(contentsOf: url)
            imgNew = UIImage.sd_animatedGIF(with: data)
            
            cell.imgNew.image = imgNew
            
            cell.imgNew.isHidden = false
            cell.widthImgNew.constant = 60.0
            cell.lblRightPadding.constant = 15.0
        }else
        {
            cell.imgNew.isHidden = true
            cell.widthImgNew.constant = 0.0
            cell.lblRightPadding.constant = 0.0
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Hide Keyboard
        self.searchBar.endEditing(true)
        
        if Reachability.isConnectedToNetwork() {
            
            if ((self.CategoryFilteredArr[indexPath.row])["id"] as? Int) == 0 {
                
                self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
                
                // Check Internet Speed
                self.test(inSecond: 3.0) { [weak self] (speed) in
                    print(speed)
                    
                    self!.RemoveLoader()
                    
                    if speed < 0.03
                    {
                        self!.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
                        })
                    }else
                    {
                        DispatchQueue.main.async {
                            if self!.CategoryFilteredArr.count > 0
                            {
                                let catID = (self!.CategoryFilteredArr[indexPath.row])["id"] as? Int
                                
                                if catID != nil && (self!.mainDictfromCovercase["n_mask_image"] as? String ?? "") != ""
                                {
                                    let EditVc = mainStoryBoard.instantiateViewController(withIdentifier: "EditingViewController") as! EditingViewController
                                    EditVc.categoryId = (catID)
                                    EditVc.internetSpeed = speed
                                    EditVc.mainDictFromCoverCase = self!.mainDictfromCovercase
                                    self!.pushViewController(EditVc)
                                }
                            }
                        }
                    }
                }
            }else
            {
                if self.CategoryFilteredArr.count > 0
                {
                    let catID = (self.CategoryFilteredArr[indexPath.row])["id"] as? Int
                    
                    if catID != nil
                    {
                        DispatchQueue.main.async {
                            let SelectCaseVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectCaseViewController") as! SelectCaseViewController
                            SelectCaseVC.categoryId = ((self.CategoryFilteredArr[indexPath.row])["id"] as? Int)
                            SelectCaseVC.mainDictFromCoverCase = self.mainDictfromCovercase
                            SelectCaseVC.strTitle = ((self.CategoryFilteredArr[indexPath.row])["name"] as? String)
                            self.pushViewController(SelectCaseVC)
                        }
                    }
                }
                
            }
            
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "OK", completion: { (ButtonIndex) in
            })
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    // MARK: - ********************** TOUCHES LIFECYCLE **********************
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.endEditing(true)
    }
    
    //MARK:- ********************** SEARCH METHODS **********************
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    //MARK:- ********************** SEARCHBAR METHODS **********************
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        CategoryFilteredArr = searchText.isEmpty ? CategoryMainArr : CategoryMainArr.filter { team in
            return (team["name"] as! String).lowercased().contains(searchText.lowercased())
        }
        
        if (CategoryFilteredArr.count <= 0)
        {
            self.defImgTableView.isHidden = true
            self.nodataLbl.text = "Category not available"
            self.nodataLbl.isHidden = false
        }
        else
        {
            self.defImgTableView.isHidden = false
            self.nodataLbl.isHidden = true
        }
        
        defImgTableView.reloadData()
        
    }
    
    //MARK:- ********************** INTERNET SPEED TEST **********************
    
    func test(inSecond: Double, completion: @escaping((Double) -> ())) -> () {
        
        testDownloadSpeedWithTimout(timeout: inSecond) { (speed, error) in
            print("Download Speed:", speed ?? "KO")
            print("Speed Test Error:", error ?? "KO")
            completion(speed ?? 0.0)
        }
    }
    
    func testDownloadSpeedWithTimout(timeout: TimeInterval, withCompletionBlock: @escaping speedTestCompletionHandler) {
        
        guard let url = URL(string: "https://images.apple.com/v/imac-with-retina/a/images/overview/5k_image.jpg") else { return }
        
        startTime = CFAbsoluteTimeGetCurrent()
        stopTime = startTime
        bytesReceived = 0
        
        speedTestCompletionBlock = withCompletionBlock
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForResource = timeout
        let session = URLSession.init(configuration: configuration, delegate: self, delegateQueue: nil)
        session.dataTask(with: url).resume()
        
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        bytesReceived! += data.count
        stopTime = CFAbsoluteTimeGetCurrent()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        let elapsed = stopTime - startTime
        
        if let aTempError = error as NSError?, aTempError.domain != NSURLErrorDomain && aTempError.code != NSURLErrorTimedOut && elapsed == 0  {
            speedTestCompletionBlock?(nil, error)
            return
        }
        
        let speed = elapsed != 0 ? Double(bytesReceived) / elapsed / 1024.0 / 1024.0 : -1
        speedTestCompletionBlock?(speed, nil)
        
    }
    
}
