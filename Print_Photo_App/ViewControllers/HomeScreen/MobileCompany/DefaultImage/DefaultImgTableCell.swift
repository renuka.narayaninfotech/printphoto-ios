//
//  DefaultImgTableCell.swift
//  Print_Photo_App
//
//  Created by des on 25/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class DefaultImgTableCell: UITableViewCell {

    @IBOutlet var ImgNameLbl: UILabel!
    @IBOutlet weak var imgNew: UIImageView!
    @IBOutlet weak var widthImgNew: NSLayoutConstraint!
    @IBOutlet weak var lblRightPadding: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
