//
//  ViewController.swift
//  Print_Photo_App
//
//  Created by des on 21/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import SDWebImage

class HomeViewController: BaseViewController , UITableViewDataSource , UITableViewDelegate {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet var HomeTableview: UITableView!
    var isFromHome = false
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    var MainArr = NSMutableArray()
    var refreshControl = UIRefreshControl()
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Initialize()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        
        DispatchQueue.main.async {
//            self.HomeTableview.reloadData()
            self.HomeTableview.scroll(to: .top, animated: true)
            if !self.isFromHome{
                self.CallApi()
            }
        }
        
        if !isFromHome
        {
            self.navigationController?.navigationBar.isHidden = true
        }
        else
        {
            self.navigationController?.navigationBar.isHidden = false
        }
        
        // Tableview is Enable Scroll to top
        //        if isScroll
        //        {
        //            self.HomeTableview.scroll(to: .top, animated: true)
        ////            isScroll = false
        //        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        if !isFromHome
        {
            DispatchQueue.main.async {
                self.showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
//                self.CallApi()
            }
            
        }else
        {
            //            print("Response Not Recieve")
            //            appDelegate.notiManually(str: "Response Not Recieve")
            
            SetNavigationBarTitle(Startstring: mainProductName ?? "", EndString: "")
            
            addCartButtonWithBadgeIcon()
            addBackButton()
        }
        
    }
    
    func addCartButtonWithBadgeIcon() {
        
        var badgelabel = UILabel()
        
        if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
        {
            // badge label
            badgelabel = UILabel(frame: CGRect(x: 20, y: -5, width: 17, height: 17))
            badgelabel.layer.borderColor = UIColor.clear.cgColor
            badgelabel.layer.borderWidth = 2
            badgelabel.layer.cornerRadius = badgelabel.bounds.size.height / 2
            badgelabel.textAlignment = .center
            badgelabel.layer.masksToBounds = true
            badgelabel.font = UIFont(name: CustomFontWeight.medium, size: 10)
            badgelabel.textColor = .white
            badgelabel.backgroundColor = hexStringToUIColor(hex: "0ABAD4")
            
            if (userDefault.integer(forKey: "BadgeNumber")) != 0
            {
                badgelabel.isHidden = false
                badgelabel.text = "\(userDefault.integer(forKey: "BadgeNumber"))"
            }else
            {
                badgelabel.isHidden = true
            }
        }
        
        // Button
        let cartButton = UIButton(type: .custom)
        cartButton.setImage(UIImage(named: "ic_cart"), for: .normal)
        cartButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cartButton.addTarget(self, action: #selector(self.cartAction(_:)), for: .touchUpInside)
        cartButton.addSubview(badgelabel)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cartButton)
        
    }
    
    @objc func cartAction(_ sender: UIButton) {
        
        isCartBtnBack = false
        ((tabBarController!.viewControllers! as NSArray)[2] as! UINavigationController).popToRootViewController(animated: false)
        
        self.tabBarController?.selectedIndex = (userDefault.string(forKey: "RegionCode") == "SA") ? 1 : 2
        
    }
    
    func addBackButton() {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -24, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.popViewController()
    }
    
    //MARK:- ********************** TABLEVIEW METHODS **********************
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MainArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.Getwidth * 0.525
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        
        if !((MainArr.object(at: indexPath.row) as! [String:Any])["app_image"] is NSNull) && !((MainArr.object(at: indexPath.row) as! [String:Any])["internation_app_image"] is NSNull)
        {
            DispatchQueue.global(qos: .background).async {
                
                cell.ImgView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: (userDefault.string(forKey: "RegionCode") == "IN") ? ((self.MainArr.object(at: indexPath.row) as! [String:Any])["app_image"] as! String) : ((self.MainArr.object(at: indexPath.row) as! [String:Any])["internation_app_image"] as! String))
            }
            
        }else{
            
            // Temp string is a setlement
            cell.ImgView.setImageWithActivityIndicator(indicatorStyle: .gray, imageURL: "tempString")
            
        }
        
        cell.ImgViewBottomConstraint.constant = 0.0
        if indexPath.row == MainArr.count - 1 {
            if DeviceType.IS_IPAD
            {
                cell.ImgViewBottomConstraint.constant = 7.0
            }
            else
            {
                cell.ImgViewBottomConstraint.constant = 5.0
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //do not allow user to tap
        tableView.isUserInteractionEnabled = false
        
        isLogin = false
        
        tableView.isExclusiveTouch = true
        tableView.isMultipleTouchEnabled = false
        
        if let cellTemp = tableView.cellForRow(at: indexPath) as? HomeTableViewCell
        {
            if cellTemp.ImgView.image == nil
            {
                return
            }
        }
        
        self.navigationController?.navigationBar.isHidden = false
        
        //        // Tableview is disable scroll to top
        //        isScroll = false
        
        // Store selected product's ID to recognize which product is selected for editing.
        // For ex. If mainProductID = 1 ; Mobile Cover Case
        //         If mainProductID = 2 ; Mug
        //         If mainProductID = 5 ; Keychain   , etc.
        
        let childArray = (MainArr.object(at: indexPath.row) as! [String:Any])["all_childs"] as? NSArray ?? NSArray()
        
        if !isFromHome
        {
            mainProductID = (MainArr.object(at: indexPath.row) as! [String:Any])["id"] as? Int
        }
        
        mainProductName = (MainArr.object(at: indexPath.row) as! [String:Any])["name"] as? String
        
        if childArray.count > 0
        {
            //allow user to tap
            DispatchQueue.main.async {
                tableView.isUserInteractionEnabled = true
            }
            
            if mainProductID == ID.Phonecase {
                let CompanyVC = mainStoryBoard.instantiateViewController(withIdentifier: "MobileCompanyViewController") as! MobileCompanyViewController
                CompanyVC.BarandMainArr = childArray as! [[String : Any]]
                self.pushViewController(CompanyVC)
            }else
            {
                let homeVC = mainStoryBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                homeVC.MainArr = NSMutableArray(array: childArray)
                homeVC.isFromHome = true
                pushViewController(homeVC)
            }
            
        }else
        {
            //allow user to tap
            DispatchQueue.main.async {
                tableView.isUserInteractionEnabled = true
            }
            
            self.navigationController?.navigationBar.isHidden = true
            
            Get_Products_API(productId: (MainArr.object(at: indexPath.row) as! [String:Any])["id"] as? Int ?? 0)
        }
        
    }
    
    //MARK:- ********************** API CALLING **********************
    
    func CallApi() {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)get_categories\(userDefault.string(forKey: "RegionCode") == "SA" ? "?country_code=SA&ln=\(Locale.preferredLanguages[0] == "ar-US" ? "ar" : "eg")" : "")", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    
                    let responceDict = Responce as? [String:Any]
                    
                    if responceDict?["ResponseCode"] as? String == "1"
                    {
                        
                        self.MainArr.removeAllObjects()
                        
                        let arr = responceDict?["all_childs"] as? [[String:Any]]
                        
                        // Fill array and used to Bulk Order screen in Product list (When get all_childs array then take sub category name and Id, Take only main name in Phone Case
                        
                        if (arr?.count ?? 0) > 0
                        {
                            //                            print("Response Recieve")
                            //                            appDelegate.notiManually(str: "Response Recieve")
                            //
                            for i in 0..<arr!.count
                            {
                                let childDict = arr?[i] as NSDictionary? ?? NSDictionary()
                                
                                let mainProductId = childDict.value(forKeyPath: "id") as? Int ?? 0
                                let childArray = childDict.value(forKey: "all_childs") as? NSArray
                                
                                if mainProductId == ID.Phonecase || childArray?.count == 0
                                {
                                    categoryArray.add(childDict.value(forKey: "name") as? String ?? "")
                                    categoryIdArray.add(childDict.value(forKey: "id") as? Int ?? 0)
                                    
                                }else
                                {
                                    categoryArray.addObjects(from: childArray?.value(forKeyPath: "name") as? [Any] ?? [Any]())
                                    categoryIdArray.addObjects(from: childArray?.value(forKeyPath: "id") as? [Any] ?? [Any]())
                                }
                            }
                            
                            self.MainArr.addObjects(from: arr ?? [[String:Any]]())
                            
                            self.HomeTableview.reloadData()
//                            self.HomeTableview.scroll(to: .top, animated: true)
                            
                            for i in 0..<self.MainArr.count
                            {
                                if let id = (self.MainArr.object(at: i) as! [String:Any])["id"] as? Int, id != ID.Phonecase
                                {
                                    if let arrChild = (self.MainArr.object(at: i) as! [String:Any])["all_childs"] as? NSArray, arrChild.count > 0
                                    {
                                        for j in 0..<arrChild.count
                                        {
                                            if let strImg = userDefault.string(forKey: "RegionCode") == "IN" ? (arrChild.object(at: j) as! [String:Any])["app_image"] as? String : (arrChild.object(at: j) as! [String:Any])["internation_app_image"] as? String, strImg.contains(".gif")
                                            {
                                                SDImageCache().diskImageExists(withKey: strImg) { (bool) in
                                                    if !bool
                                                    {
                                                        let url = URL.init(string: strImg)
                                                        let data = try? Data(contentsOf: url!)
                                                        SDImageCache().storeImageData(toDisk: data, forKey: strImg)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                    }else{
                        
                        //                         print("Error Recieve")
                        //                        appDelegate.notiManually(str: "Error Recieve")
                        
                        self.isCheck_Error(message: "\(responceDict?["ResponseMessage"] ?? "")")
                        
                    }
                    
                }
                else
                {
                    
                    //                     print("Error1 Recieve")
                    //                    appDelegate.notiManually(str: "Error1 Recieve")
                    
                    if error1?.localizedDescription.contains("could not be found") ?? false
                    {
                        self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.CallApi()
                            }
                        })
                    }else
                    {
                        self.presentAlertWithTitle(title: "Server Error", message: "Server under maintenance!!! Try after sometime", options: "Retry", completion: { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.CallApi()
                            }
                        })
                    }
                }
            }
            
        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.CallApi()
                }
            })
        }
    }
    
    func isCheck_Error(message:String) {
        
        if message.contains("connect timed out") || message.contains("timeout")
        {
            self.presentAlertWithTitle(title: "Time Out", message: "Connect timed out. Please try again later.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.CallApi()
                }
            })
            
        }else if message.contains("Handshake failed") || message.contains("Failed to connect to printphoto")
        {
            self.presentAlertWithTitle(title: "Server Error", message: "Server under maintenance!!! Try after sometime", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.CallApi()
                }
            })
        }else
        {
            self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.CallApi()
                }
            })
        }
        
    }
    
    func Get_Products_API(productId:Int) {
        
        if Reachability.isConnectedToNetwork() {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)
            
            let strURL : String?
            
            if userDefault.value(forKey: "USER_ID") != nil || userDefault.integer(forKey: "USER_ID") != 0
            {
                strURL = "\(BaseURL)get_products?category_id=\(productId)&country_code=&user_id=\(userDefault.value(forKey: "USER_ID") ?? "")"
            }else
            {
                strURL = "\(BaseURL)get_products?category_id=\(productId)&country_code=\(userDefault.string(forKey: "RegionCode") ?? "")&user_id="
            }
            
            AFNetworkingAPIClient.sharedInstance.getAPICall(url: strURL ?? "", parameters: nil) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        if let dataArray = responceDict["data"] as? NSArray
                        {
                            let MugStickerTableVC = mainStoryBoard.instantiateViewController(withIdentifier: "MugStickerTableViewController") as! MugStickerTableViewController
                            MugStickerTableVC.MainArr = dataArray as! [[String: Any]]
                            selectedMainProductId = productId
                            MugStickerTableVC.currencySymbol = responceDict["currency_symbol"] as? String ?? ""
                            MugStickerTableVC.strNavTitle = mainProductName ?? ""
                            self.pushViewController(MugStickerTableVC)
                        }
                        
                    }
                }
                else
                {
                    self.presentAlertWithTitle(title: "Something went wrong!!", message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Get_Products_API(productId: productId)
                        }
                    })
                    
                }
            }
        }
        else
        {
            self.RemoveLoader()
            
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                
                if ButtonIndex == 0
                {
                    self.Get_Products_API(productId:productId)
                }
            })
        }
        
    }
    
}

