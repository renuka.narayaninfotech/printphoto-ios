//
//  HomeTableViewCell.swift
//  Print_Photo_App
//
//  Created by des on 23/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet var ImgView: UIImageView!
    @IBOutlet var ImgViewBottomConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
