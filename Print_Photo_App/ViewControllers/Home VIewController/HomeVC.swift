//
//  HomeVC.swift
//  colorpixelphoto
//
//  Created by Vasundhara Vision on 2/22/17.
//  Copyright © 2017 Vasundhara Vision. All rights reserved.
//

import UIKit
//import Crashlytics
import AVKit
import CoreTelephony
//import Crashlytics

// Add Product Id in API
class ID {
    static var Customisable: Int = 0
    static var Mall: Int = 0
    static var Phonecase: Int = 0
    static var Mug: Int = 0
    static var Keychain: Int = 0
    static var Popsocket: Int = 0
    static var Tshirt: Int = 0
    static var WallWoodenCanvasFrame: Int = 0
    static var MultiImagePhotoFrames: Int = 0

}

class HomeVC: BaseViewController
{
    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var img: UIImageView!

    //MARK:- ********************** VARIABLE DECLARATION **********************

    var window: UIWindow?

    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()

//        Crashlytics.sharedInstance().crash()

        self.navigationController?.navigationBar.isHidden = true

        DispatchQueue.main.async {
            self.Get_Country_State_City()
        }

        if DeviceType.IS_IPHONE_X {
            img.image = UIImage(named: "ic_x_LoadingSplash")
        }
    }

    //    override var prefersStatusBarHidden: Bool {
    //        return true
    //    }

    //    override func viewWillDisappear(_ animated: Bool)
    //    {
    //        UIApplication.shared.setStatusBarHidden(false, with: UIStatusBarAnimation.none)
    //
    //    }

    func PasstoNextView()
    {

        if userDefault.string(forKey: "RegionCode") == "" || userDefault.string(forKey: "RegionCode") == nil
        {
            let SelectRegionVC = mainStoryBoard.instantiateViewController(withIdentifier: "SelectRegionVC") as! SelectRegionVC
            self.pushViewController(SelectRegionVC)
        } else
        {
            let InstVC = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
            UIApplication.shared.delegate!.window!!.rootViewController = InstVC
            UIApplication.shared.delegate!.window!!.makeKeyAndVisible()
        }

    }

    //MARK:- ********************** API CALLING **********************

    func Get_Configuration_API() {

        if Reachability.isConnectedToNetwork() {

            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)

            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)get_configuration?country_code=\(userDefault.string(forKey: "RegionCode") ?? "")", parameters: nil) { (APIResponce, Responce, error1) in

                self.RemoveLoader()

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String: Any]

                    if responceDict["ResponseCode"] as! String == "1"
                    {
                        let dataDict = responceDict["data"] as! [String: Any]

                        // Set bulk sms status
                        isBulk_SMS = "\(dataDict["bulk_sms"] as! String)"

                        ticketsURL = "\(dataDict["tickets_url"] as! String)"

                        // Send shipping charges to Place Order screen
                        let shippingCharges = "\(dataDict["shipping_charge"] ?? "")"
                        userDefault.set(shippingCharges, forKey: "ShippingCharges")

                        //  Help Video URL for India and Other Country
                        indiaVideoURl = URL(string: "\(dataDict["tutorial_link"] as? String ?? "")")
                        otherCountryVideoURl = URL(string: "\(dataDict["tutorial_link_international"] as? String ?? "")")

                        // Set Help Video URL
                        videoURL = (userDefault.string(forKey: "RegionCode") == "IN") ? indiaVideoURl : otherCountryVideoURl

                        // Create Contactus dictionary
                        let contactusDict = ["email": "\(dataDict["contact_email"] as? String ?? "")",
                                             "mobile": "\(dataDict["contact_mobile"] as? String ?? "")",
                                             "address": "\(dataDict["contact_address"] as? String ?? "")",
                                             "time": "\(dataDict["contact_support_timing"] as? String ?? "")"] as [String: Any]

                        userDefault.set(contactusDict, forKey: "contactus")

                        if(userDefault.string(forKey: "RegionCode") == "IN") {
                            userDefault.set(dataDict["wa_india"], forKey: "contactusWhatsapp")
                        } else if(userDefault.string(forKey: "RegionCode") == "SA") {
                            userDefault.set(dataDict["wa_saudi"], forKey: "contactusWhatsapp")
                        } else {
                            userDefault.set(dataDict["wa_internatinal"], forKey: "contactusWhatsapp")
                        }

                        userDefault.synchronize()

                        // Fill currencies Code Array

                        currenciesArray = (dataDict["currencies"] as! NSArray).mutableCopy() as? NSMutableArray ?? NSMutableArray()
                        cancel_reson_Array = (dataDict["cancel_reasons"] as! NSArray).mutableCopy() as? NSMutableArray ?? NSMutableArray()

                        // Set Product Id usign ID Class and implement check Product Id

                        let productTypeDict = dataDict["ProductType"] as! [String: Any]

                        ID.Customisable = productTypeDict["Customisable"] as? Int ?? 0
                        ID.Mall = productTypeDict["Mall"] as? Int ?? 0
                        ID.Phonecase = productTypeDict["Phonecase"] as? Int ?? 0
                        ID.Mug = productTypeDict["Mug"] as? Int ?? 0
                        ID.Keychain = productTypeDict["Keychain"] as? Int ?? 0
                        ID.Popsocket = productTypeDict["Popsocket"] as? Int ?? 0
                        ID.Tshirt = productTypeDict["Tshirt"] as? Int ?? 0
                        ID.WallWoodenCanvasFrame = productTypeDict["WallWoodenCanvasFrame"] as? Int ?? 0
                        ID.MultiImagePhotoFrames = productTypeDict["MultiImagePhotoFrames"] as? Int ?? 0

                        let isUpdate = "\(dataDict["ios_forcefully_update"] as? String ?? "")"
                        let iosVersion = "\(dataDict["ios_version"] as? String ?? "")"

                        // App Update Forcefully when status is "1"
                        if isUpdate == "1"
                        {
                            // Check live version and App version
                            if (Float(appVersion)?.isLess(than: Float(iosVersion) ?? 0.0))!
                            {
                                self.showAlertForForcefullyVersion()
                            } else
                            {
                                self.PasstoNextView()
                            }
                        } else
                        {
                            self.PasstoNextView()
                        }

                    } else {
                        self.isCheck_Error(message: "\(responceDict["ResponseMessage"] ?? "")")
                    }
                }
                else
                {
                    if error1!.localizedDescription.contains("could not be found")
                    {
                        self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.Get_Configuration_API()
                            }
                        })
                    } else if error1!.localizedDescription.contains("data couldn’t be read")
                    {
                        self.presentAlertWithTitle(title: error1!.localizedDescription, message: "", options: "Retry", completion: { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.Get_Configuration_API()
                            }
                        })
                    } else
                    {
                        self.presentAlertWithTitle(title: "Server Error", message: "Server under maintenance!!! Try after sometime", options: "Retry", completion: { (ButtonIndex) in
                            if ButtonIndex == 0
                            {
                                self.Get_Configuration_API()
                            }
                        })
                    }
                }
            }

        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Get_Configuration_API()
                }
            })
        }
    }

    func isCheck_Error(message: String) {

        if message.contains("connect timed out") || message.contains("timeout")
        {
            self.presentAlertWithTitle(title: "Time Out", message: "Connect timed out. Please try again later.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Get_Configuration_API()
                }
            })

        } else if message.contains("Handshake failed") || message.contains("Failed to connect to printphoto")
        {
            self.presentAlertWithTitle(title: "Server Error", message: "Server under maintenance!!! Try after sometime", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Get_Configuration_API()
                }
            })
        } else
        {
            self.presentAlertWithTitle(title: "Something went wrong", message: "", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Get_Configuration_API()
                }
            })
        }

    }

    func showAlertForForcefullyVersion()
    {
        self.presentAlertWithTitle(title: "Update Available", message: "A new version of Print photo is available. Please update to latest version now!!", options: "Update", completion: { (ButtonIndex) in
            if ButtonIndex == 0
            {
                self.showAlertForForcefullyVersion()
                UIApplication.shared.open(NSURL(string: "https://itunes.apple.com/us/app/print-photo-phone-case-maker/id1458325325?ls=1&mt=8")! as URL)
            }
        })
    }

    //********************** GET COUNTRY,STATE,CITY API CALLING **********************

    func Get_Country_State_City() {

        if Reachability.isConnectedToNetwork() {

            showLoaderwithMessage(message: "Please wait...", type: .ballTrianglePath, color: .white)

            AFNetworkingAPIClient.sharedInstance.getAPICall(url: "\(BaseURL)country_data", parameters: nil) { (APIResponce, Responce, error1) in

                self.RemoveLoader()

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {

                    let responceDict = Responce as! [String: Any]

                    countryArray = (responceDict["data"] as! NSArray).mutableCopy() as? NSMutableArray ?? NSMutableArray()

                    for i in 0..<countryArray.count
                    {
                        let phoneCodeDict = countryArray.object(at: i) as! NSDictionary

                        // Concatenate string for used select phone code
                        phoneCodeArray.add("+\(phoneCodeDict.value(forKey: "phonecode") as! Int)" + " " + "(\(phoneCodeDict.value(forKey: "sortname") as! String))" + " " + "\(phoneCodeDict.value(forKey: "name") as! String)")
                    }

                    self.Get_Configuration_API()

                }
                else
                {
                    self.presentAlertWithTitle(title: (error1?.localizedDescription ?? ""), message: "", options: "Retry", completion: { (ButtonIndex) in
                        if ButtonIndex == 0
                        {
                            self.Get_Country_State_City()
                        }
                    })

                }
            }

        }
        else
        {
            self.presentAlertWithTitle(title: "Internet Connection", message: "Internet is slow. Please check internet connection.", options: "Retry", completion: { (ButtonIndex) in
                if ButtonIndex == 0
                {
                    self.Get_Country_State_City()
                }
            })
        }
    }
}


