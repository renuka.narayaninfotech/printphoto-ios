//
//  SelectRegionVC.swift
//  Print_Photo_App
//
//  Created by Mac Os on 27/08/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit

class SelectRegionVC: BaseViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- ********************** OUTLATE DECLARATION **********************
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblRegion: UITableView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var arrRegion = NSArray()
    var filterArrRegion = NSArray()
    var countryName = ""
    var regionCode = ""
    var countryID : String?
    var currencyID : String?
    
    //MARK:- ********************** VIEW LIFECYLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetNavigationBarTitle(Startstring: "Select Country", EndString: "")
        
        Initialize()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK:- ********************** INITIALIZE **********************
    
    func Initialize() {
        
        addRightButtons()
        removeBackButton()
        
        //        searchBar.becomeFirstResponder()
        
        arrRegion = countryArray //.value(forKey: "name") as! NSArray
        filterArrRegion = arrRegion
        
    }
    
    //MARK:- ********************** BUTTON EVENT **********************
    
    func addRightButtons() {
        
        // Done Button
        let doneButton = UIButton(type: .custom)
        doneButton.setImage(UIImage(named: "ic_done"), for: .normal)
        doneButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        doneButton.addTarget(self, action: #selector(self.SelectButtonAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: doneButton)
        
    }
    
    @objc func SelectButtonAction(_ sender: UIButton) {
        
        if !(filterArrRegion.value(forKey: "name") as! NSArray).contains(countryName) || filterArrRegion.count <= 0
        {
            self.topMostViewController().view.makeToast("Please select atleast one region!!", duration: 2.0, position: .center)
            
        }else
        {
            userDefault.set(regionCode, forKey: "RegionCode")
            userDefault.set(countryID, forKey: "countryId")
            
            if userDefault.value(forKey: "EditAccountDict") != nil
            {
                let userDict = ((userDefault.value(forKey: "EditAccountDict") as! NSDictionary).mutableCopy()) as? NSMutableDictionary ?? NSMutableDictionary()
                
                let arrayOfKeys = userDict.allKeys as NSArray
                
                if !arrayOfKeys.contains("countryCode") && !arrayOfKeys.contains("currencyID")
                {
                    
                    userDict.setValue(regionCode, forKey: "countryCode")
                    userDict.setValue(currencyID, forKey: "currencyID")
                    
                    userDefault.set(userDict, forKey: "EditAccountDict")
                    userDefault.synchronize()
                }
            }
            
            // Set Help Video URL
            videoURL = (userDefault.string(forKey: "RegionCode") == "IN") ? indiaVideoURl : otherCountryVideoURl
            
            let InstVC = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
            UIApplication.shared.delegate!.window!!.rootViewController = InstVC
            UIApplication.shared.delegate!.window!!.makeKeyAndVisible()
        }
        
    }
    
    func removeBackButton() {
        //add nil view to remove back button
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40)))
    }
    
    //MARK:- ********************** TABLEVIEW METHODS **********************
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterArrRegion.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultImgTableCell") as! DefaultImgTableCell
        
        let name = (filterArrRegion[indexPath.row] as? NSDictionary)?.value(forKey: "name") as? String ?? ""
        cell.ImgNameLbl.text = name //filterArrRegion[indexPath.row] as? String ?? ""
        
        if name == countryName
        {
            
            let dict = filterArrRegion[indexPath.row] as! NSDictionary
            regionCode = dict.value(forKey: "sortname") as? String ?? ""
            
            // Store country Id and used to DeliveryAddressVC, Registarion
            
            countryID = "\(dict.value(forKey: "id") as? Int ?? 0)"
            
            currencyID = "\(dict.value(forKey: "currency_id") as? Int ?? 0)"
            
            cell.imgNew.isHidden = false
            cell.widthImgNew.constant = 25.0
            cell.lblRightPadding.constant = 15.0
            
        }else
        {
            
            cell.imgNew.isHidden = true
            cell.widthImgNew.constant = 0.0
            cell.lblRightPadding.constant = 0.0
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let name = (filterArrRegion[indexPath.row] as? NSDictionary)?.value(forKey: "name") as? String ?? ""
        countryName = name
        
        tblRegion.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // MARK: - ********************** TOUCHES LIFECYCLE **********************
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    //MARK:- ********************** SEARCHBAR METHODS **********************
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filterArrRegion = searchText.isEmpty ? arrRegion : arrRegion.filter { team in
            return ((team as! NSDictionary)["name"] as! String).lowercased().contains(searchText.lowercased())
            } as NSArray
        
        if (filterArrRegion.count == 0)
        {
            self.tblRegion.isHidden = true
            self.nodataLbl.text = "Region not available"
            self.nodataLbl.isHidden = false
        }
        else
        {
            self.tblRegion.isHidden = false
            self.nodataLbl.isHidden = true
        }
        
        tblRegion.reloadData()
        
    }
    
}
