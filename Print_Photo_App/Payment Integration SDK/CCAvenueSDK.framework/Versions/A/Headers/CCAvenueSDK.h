//
//  CCAvenueSDK.h
//  CCAvenueSDK
//
//  Created by Mayur Shinde on 01/02/18.
//  Copyright © 2018 Avenues. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <CCAvenueSDK/CCAvenueSDK.h>
#import <CCAvenueSDK/InitialViewController.h>

@interface CCAvenueSDK : NSObject

@end
