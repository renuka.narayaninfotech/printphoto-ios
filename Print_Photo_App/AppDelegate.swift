//
//  AppDelegate.swift
//  Print_Photo_App
//
//  Created by des on 21/01/19.
//  Copyright © 2019 des. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseCore
import FirebaseMessaging
import UserNotifications
import OneSignal
//import Fabric
//import Crashlytics
import FBSDKCoreKit
import FirebaseCrashlytics
//import FacebookCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {
    
    var window: UIWindow?
    let APPDELEGATE = UIApplication.shared.delegate as? AppDelegate
    var backgroundUpdateTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)
    
    var orientationLock = UIInterfaceOrientationMask.portrait
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // if use textview in xcode11 and < ios 13 --app crash
        UITextViewWorkaround.unique.executeWorkaround()
        
        IQKeyboardManager.shared.enable = true
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        //         Fabric Crashlatics Configure
//        Fabric.sharedSDK().debug = true
        
        // Facebook
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
                
        //get permission for notification
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        Messaging.messaging().delegate = self
        
        // One Signal Notification
        
      //  OneSignal.initWithLaunchOptions(launchOptions, appId: "af859287-40ce-41e4-ba95-ea87efab1c28")
        OneSignal.setLogLevel(ONE_S_LOG_LEVEL.LL_ERROR, visualLevel: ONE_S_LOG_LEVEL.LL_FATAL)
        
        //        notiManually()
        
//        var img: UIImageView!
//        print(img.image)
        
        AppLinkUtility.fetchDeferredAppLink { (url, error) in
            if let error = error {
                print("Received error while fetching deferred app link %@", error)
            }
            if let url = url {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
//        FBSDKCoreKit.ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
    //MARK:- ********************** SCREEN ORIENTATION **********************
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    //MARK:- ********************** FACEBOOK METHOD **********************
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if let sch = url.scheme, sch == "com.vasu.printphoto"
        {
            if url.description.contains("paypal_continue")
            {
                let str = url.description.components(separatedBy: "/").last
                // next process
                
                NotificationCenter.default.post(name: Notification.Name("ContinuePaypalPayment"), object: str)
                
            }
            else
            {
                // process cancelled
                NotificationCenter.default.post(name: Notification.Name("CancelPaypalPayment"), object: nil)
                
            }
            return true
        }
        else if(url.path.starts(with: "/home/"))
        {
            let tabBarController = self.window?.rootViewController as? UITabBarController
            
            switch url.path.last {
            case "0":
                tabBarController?.selectedIndex = 0
                break
            case "1":
                tabBarController?.selectedIndex = 2//3
                break
            default:
                break
            }
            return false
        }
        else
        {
            let handled = ApplicationDelegate.shared.application(app, open: url, options: options)
            
            return handled
        }
    }
    
    //MARK:- ********************** APPDELEGATE METHOD **********************
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        // App running in Background Mode for Running Timer
        self.backgroundUpdateTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            self.endBackgroundUpdateTask()
        })
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        //        // App running in Background Mode for Running Timer
        //        func applicationWillEnterForeground(application: UIApplication) {
        //            self.endBackgroundUpdateTask()
        //        }
        
        self.endBackgroundUpdateTask()
        
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        AppEvents.activateApp()
        //
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func endBackgroundUpdateTask() {
        UIApplication.shared.endBackgroundTask(self.backgroundUpdateTask)
        self.backgroundUpdateTask = UIBackgroundTaskIdentifier.invalid
    }
    
    //MARK:- ********************** FIREBASE METHOD **********************
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        
        userDefault.set(fcmToken, forKey: "FIREBASE_TOKEN")
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("Data recieve in BG = \(userInfo)")
        completionHandler(.newData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound,.badge])
    }
    
    //when user tap on notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.notification.request.identifier == "LocalNotification" {
            isFromLocalKilled = true
        }else
        {
            if UIApplication.shared.applicationState == .active || UIApplication.shared.applicationState == .background
            {
                //                 Open Order View Controller click on notification when App running in Foreground
                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
            }
            else
            {
                isFromPushKilled = true
            }
        }
    }
    
    //    func notiManually(str: String)
    //    {
    //        let notification = UILocalNotification()
    //        notification.fireDate = Date()
    //        notification.alertBody = str
    //        notification.timeZone = NSTimeZone.default //NSTimeZone(name: "UTC")! as TimeZone
    //        notification.soundName = UILocalNotificationDefaultSoundName
    //        notification.applicationIconBadgeNumber = 1
    //
    //        UIApplication.shared.scheduleLocalNotification(notification)
    //    }
    
    //MARK:- Set Badge Icon
    func SetBadgeIcon() {
        
        let tabBarController = self.window?.rootViewController as? UITabBarController
        
        if Reachability.isConnectedToNetwork() {
            
            if userDefault.value(forKey: "USER_ID") != nil
            {
                let ParamDict = ["user_id": userDefault.value(forKey: "USER_ID")!] as NSDictionary
                
                AFNetworkingAPIClient.sharedInstance.postAPICall(url: "\(BaseURL)get_cart", parameters: ParamDict) { (APIResponce, Responce, error1) in
                    
                    if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                    {
                        
                        let responceDict = Responce as! [String:Any]
                        
                        if responceDict["ResponseCode"] as! String == "1"
                        {
                            let cartDict = responceDict["data"] as? [String:Any]
                            
                            cartInfoMainArray = cartDict?["cart_items"] as! [[String : Any]]
                            
                            if cartInfoMainArray.count == 0
                            {
                                userDefault.removeObject(forKey: "BadgeNumber")
                                
                                if let tabItems = tabBarController?.tabBar.items {
                                    // In this case we want to modify the badge number of the third tab:
                                    let tabItem = tabItems[(userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3]
                                    tabItem.badgeValue = nil
                                }
                            }
                            else
                            {
                                var BadgeNumber = 0
                                if let tabItems = tabBarController?.tabBar.items {
                                    // In this case we want to modify the badge number of the third tab:
                                    BadgeNumber = cartInfoMainArray.count
                                    let tabItem = tabItems[(userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3]
                                    tabItem.badgeValue = "\(BadgeNumber)"
                                    tabItem.badgeColor = hexStringToUIColor(hex: "0ABAD4")
                                    userDefault.set(BadgeNumber, forKey: "BadgeNumber")
                                }
                            }
                        }else
                        {
                            userDefault.removeObject(forKey: "BadgeNumber")
                            
                            if let tabItems = tabBarController?.tabBar.items {
                                // In this case we want to modify the badge number of the third tab:
                                let tabItem = tabItems[(userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3]
                                tabItem.badgeValue = nil
                            }
                            
                        }
                    }
                    
                }
            }else
            {
                userDefault.removeObject(forKey: "BadgeNumber")
                
                if let tabItems = tabBarController?.tabBar.items {
                    // In this case we want to modify the badge number of the third tab:
                    let tabItem = tabItems[(userDefault.string(forKey: "RegionCode") == "SA") ? 2 : 3]
                    tabItem.badgeValue = nil
                }
            }
        }
        
    }
    
}

